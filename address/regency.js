vm.regencies = [
  {
    regency_name: "KABUPATEN SIMEULUE",
    regency_code: 1101,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH SINGKIL",
    regency_code: 1102,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH SELATAN",
    regency_code: 1103,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH TENGGARA",
    regency_code: 1104,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH TIMUR",
    regency_code: 1105,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH TENGAH",
    regency_code: 1106,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH BARAT",
    regency_code: 1107,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH BESAR",
    regency_code: 1108,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN PIDIE",
    regency_code: 1109,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN BIREUEN",
    regency_code: 1110,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH UTARA",
    regency_code: 1111,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH BARAT DAYA",
    regency_code: 1112,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN GAYO LUES",
    regency_code: 1113,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH TAMIANG",
    regency_code: 1114,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN NAGAN RAYA",
    regency_code: 1115,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN ACEH JAYA",
    regency_code: 1116,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN BENER MERIAH",
    regency_code: 1117,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN PIDIE JAYA",
    regency_code: 1118,
    province_code: 11
  },
  {
    regency_name: "KOTA BANDA ACEH",
    regency_code: 1171,
    province_code: 11
  },
  {
    regency_name: "KOTA SABANG",
    regency_code: 1172,
    province_code: 11
  },
  {
    regency_name: "KOTA LANGSA",
    regency_code: 1173,
    province_code: 11
  },
  {
    regency_name: "KOTA LHOKSEUMAWE",
    regency_code: 1174,
    province_code: 11
  },
  {
    regency_name: "KOTA SUBULUSSALAM",
    regency_code: 1175,
    province_code: 11
  },
  {
    regency_name: "KABUPATEN NIAS",
    regency_code: 1201,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN MANDAILING NATAL",
    regency_code: 1202,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN TAPANULI SELATAN",
    regency_code: 1203,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN TAPANULI TENGAH",
    regency_code: 1204,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN TAPANULI UTARA",
    regency_code: 1205,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN TOBA SAMOSIR",
    regency_code: 1206,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN LABUHAN BATU",
    regency_code: 1207,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN ASAHAN",
    regency_code: 1208,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN SIMALUNGUN",
    regency_code: 1209,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN DAIRI",
    regency_code: 1210,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN KARO",
    regency_code: 1211,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN DELI SERDANG",
    regency_code: 1212,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN LANGKAT",
    regency_code: 1213,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN NIAS SELATAN",
    regency_code: 1214,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN HUMBANG HASUNDUTAN",
    regency_code: 1215,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN PAKPAK BHARAT",
    regency_code: 1216,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN SAMOSIR",
    regency_code: 1217,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN SERDANG BEDAGAI",
    regency_code: 1218,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN BATU BARA",
    regency_code: 1219,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN PADANG LAWAS UTARA",
    regency_code: 1220,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN PADANG LAWAS",
    regency_code: 1221,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN LABUHAN BATU SELATAN",
    regency_code: 1222,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN LABUHAN BATU UTARA",
    regency_code: 1223,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN NIAS UTARA",
    regency_code: 1224,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN NIAS BARAT",
    regency_code: 1225,
    province_code: 12
  },
  {
    regency_name: "KOTA SIBOLGA",
    regency_code: 1271,
    province_code: 12
  },
  {
    regency_name: "KOTA TANJUNG BALAI",
    regency_code: 1272,
    province_code: 12
  },
  {
    regency_name: "KOTA PEMATANG SIANTAR",
    regency_code: 1273,
    province_code: 12
  },
  {
    regency_name: "KOTA TEBING TINGGI",
    regency_code: 1274,
    province_code: 12
  },
  {
    regency_name: "KOTA MEDAN",
    regency_code: 1275,
    province_code: 12
  },
  {
    regency_name: "KOTA BINJAI",
    regency_code: 1276,
    province_code: 12
  },
  {
    regency_name: "KOTA PADANGSIDIMPUAN",
    regency_code: 1277,
    province_code: 12
  },
  {
    regency_name: "KOTA GUNUNGSITOLI",
    regency_code: 1278,
    province_code: 12
  },
  {
    regency_name: "KABUPATEN KEPULAUAN MENTAWAI",
    regency_code: 1301,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN PESISIR SELATAN",
    regency_code: 1302,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN SOLOK",
    regency_code: 1303,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN SIJUNJUNG",
    regency_code: 1304,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN TANAH DATAR",
    regency_code: 1305,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN PADANG PARIAMAN",
    regency_code: 1306,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN AGAM",
    regency_code: 1307,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN LIMA PULUH KOTA",
    regency_code: 1308,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN PASAMAN",
    regency_code: 1309,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN SOLOK SELATAN",
    regency_code: 1310,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN DHARMASRAYA",
    regency_code: 1311,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN PASAMAN BARAT",
    regency_code: 1312,
    province_code: 13
  },
  {
    regency_name: "KOTA PADANG",
    regency_code: 1371,
    province_code: 13
  },
  {
    regency_name: "KOTA SOLOK",
    regency_code: 1372,
    province_code: 13
  },
  {
    regency_name: "KOTA SAWAH LUNTO",
    regency_code: 1373,
    province_code: 13
  },
  {
    regency_name: "KOTA PADANG PANJANG",
    regency_code: 1374,
    province_code: 13
  },
  {
    regency_name: "KOTA BUKITTINGGI",
    regency_code: 1375,
    province_code: 13
  },
  {
    regency_name: "KOTA PAYAKUMBUH",
    regency_code: 1376,
    province_code: 13
  },
  {
    regency_name: "KOTA PARIAMAN",
    regency_code: 1377,
    province_code: 13
  },
  {
    regency_name: "KABUPATEN KUANTAN SINGINGI",
    regency_code: 1401,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN INDRAGIRI HULU",
    regency_code: 1402,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN INDRAGIRI HILIR",
    regency_code: 1403,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN PELALAWAN",
    regency_code: 1404,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN S I A K",
    regency_code: 1405,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN KAMPAR",
    regency_code: 1406,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN ROKAN HULU",
    regency_code: 1407,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN BENGKALIS",
    regency_code: 1408,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN ROKAN HILIR",
    regency_code: 1409,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN KEPULAUAN MERANTI",
    regency_code: 1410,
    province_code: 14
  },
  {
    regency_name: "KOTA PEKANBARU",
    regency_code: 1471,
    province_code: 14
  },
  {
    regency_name: "KOTA D U M A I",
    regency_code: 1473,
    province_code: 14
  },
  {
    regency_name: "KABUPATEN KERINCI",
    regency_code: 1501,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN MERANGIN",
    regency_code: 1502,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN SAROLANGUN",
    regency_code: 1503,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN BATANG HARI",
    regency_code: 1504,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN MUARO JAMBI",
    regency_code: 1505,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN TANJUNG JABUNG TIMUR",
    regency_code: 1506,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN TANJUNG JABUNG BARAT",
    regency_code: 1507,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN TEBO",
    regency_code: 1508,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN BUNGO",
    regency_code: 1509,
    province_code: 15
  },
  {
    regency_name: "KOTA JAMBI",
    regency_code: 1571,
    province_code: 15
  },
  {
    regency_name: "KOTA SUNGAI PENUH",
    regency_code: 1572,
    province_code: 15
  },
  {
    regency_name: "KABUPATEN OGAN KOMERING ULU",
    regency_code: 1601,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN OGAN KOMERING ILIR",
    regency_code: 1602,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN MUARA ENIM",
    regency_code: 1603,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN LAHAT",
    regency_code: 1604,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN MUSI RAWAS",
    regency_code: 1605,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN MUSI BANYUASIN",
    regency_code: 1606,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN BANYU ASIN",
    regency_code: 1607,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN OGAN KOMERING ULU SELATAN",
    regency_code: 1608,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN OGAN KOMERING ULU TIMUR",
    regency_code: 1609,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN OGAN ILIR",
    regency_code: 1610,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN EMPAT LAWANG",
    regency_code: 1611,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN PENUKAL ABAB LEMATANG ILIR",
    regency_code: 1612,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN MUSI RAWAS UTARA",
    regency_code: 1613,
    province_code: 16
  },
  {
    regency_name: "KOTA PALEMBANG",
    regency_code: 1671,
    province_code: 16
  },
  {
    regency_name: "KOTA PRABUMULIH",
    regency_code: 1672,
    province_code: 16
  },
  {
    regency_name: "KOTA PAGAR ALAM",
    regency_code: 1673,
    province_code: 16
  },
  {
    regency_name: "KOTA LUBUKLINGGAU",
    regency_code: 1674,
    province_code: 16
  },
  {
    regency_name: "KABUPATEN BENGKULU SELATAN",
    regency_code: 1701,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN REJANG LEBONG",
    regency_code: 1702,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN BENGKULU UTARA",
    regency_code: 1703,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN KAUR",
    regency_code: 1704,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN SELUMA",
    regency_code: 1705,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN MUKOMUKO",
    regency_code: 1706,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN LEBONG",
    regency_code: 1707,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN KEPAHIANG",
    regency_code: 1708,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN BENGKULU TENGAH",
    regency_code: 1709,
    province_code: 17
  },
  {
    regency_name: "KOTA BENGKULU",
    regency_code: 1771,
    province_code: 17
  },
  {
    regency_name: "KABUPATEN LAMPUNG BARAT",
    regency_code: 1801,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN TANGGAMUS",
    regency_code: 1802,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN LAMPUNG SELATAN",
    regency_code: 1803,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN LAMPUNG TIMUR",
    regency_code: 1804,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN LAMPUNG TENGAH",
    regency_code: 1805,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN LAMPUNG UTARA",
    regency_code: 1806,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN WAY KANAN",
    regency_code: 1807,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN TULANGBAWANG",
    regency_code: 1808,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN PESAWARAN",
    regency_code: 1809,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN PRINGSEWU",
    regency_code: 1810,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN MESUJI",
    regency_code: 1811,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN TULANG BAWANG BARAT",
    regency_code: 1812,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN PESISIR BARAT",
    regency_code: 1813,
    province_code: 18
  },
  {
    regency_name: "KOTA BANDAR LAMPUNG",
    regency_code: 1871,
    province_code: 18
  },
  {
    regency_name: "KOTA METRO",
    regency_code: 1872,
    province_code: 18
  },
  {
    regency_name: "KABUPATEN BANGKA",
    regency_code: 1901,
    province_code: 19
  },
  {
    regency_name: "KABUPATEN BELITUNG",
    regency_code: 1902,
    province_code: 19
  },
  {
    regency_name: "KABUPATEN BANGKA BARAT",
    regency_code: 1903,
    province_code: 19
  },
  {
    regency_name: "KABUPATEN BANGKA TENGAH",
    regency_code: 1904,
    province_code: 19
  },
  {
    regency_name: "KABUPATEN BANGKA SELATAN",
    regency_code: 1905,
    province_code: 19
  },
  {
    regency_name: "KABUPATEN BELITUNG TIMUR",
    regency_code: 1906,
    province_code: 19
  },
  {
    regency_name: "KOTA PANGKAL PINANG",
    regency_code: 1971,
    province_code: 19
  },
  {
    regency_name: "KABUPATEN KARIMUN",
    regency_code: 2101,
    province_code: 21
  },
  {
    regency_name: "KABUPATEN BINTAN",
    regency_code: 2102,
    province_code: 21
  },
  {
    regency_name: "KABUPATEN NATUNA",
    regency_code: 2103,
    province_code: 21
  },
  {
    regency_name: "KABUPATEN LINGGA",
    regency_code: 2104,
    province_code: 21
  },
  {
    regency_name: "KABUPATEN KEPULAUAN ANAMBAS",
    regency_code: 2105,
    province_code: 21
  },
  {
    regency_name: "KOTA B A T A M",
    regency_code: 2171,
    province_code: 21
  },
  {
    regency_name: "KOTA TANJUNG PINANG",
    regency_code: 2172,
    province_code: 21
  },
  {
    regency_name: "KABUPATEN KEPULAUAN SERIBU",
    regency_code: 3101,
    province_code: 31
  },
  {
    regency_name: "KOTA JAKARTA SELATAN",
    regency_code: 3171,
    province_code: 31
  },
  {
    regency_name: "KOTA JAKARTA TIMUR",
    regency_code: 3172,
    province_code: 31
  },
  {
    regency_name: "KOTA JAKARTA PUSAT",
    regency_code: 3173,
    province_code: 31
  },
  {
    regency_name: "KOTA JAKARTA BARAT",
    regency_code: 3174,
    province_code: 31
  },
  {
    regency_name: "KOTA JAKARTA UTARA",
    regency_code: 3175,
    province_code: 31
  },
  {
    regency_name: "KABUPATEN BOGOR",
    regency_code: 3201,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN SUKABUMI",
    regency_code: 3202,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN CIANJUR",
    regency_code: 3203,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN BANDUNG",
    regency_code: 3204,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN GARUT",
    regency_code: 3205,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN TASIKMALAYA",
    regency_code: 3206,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN CIAMIS",
    regency_code: 3207,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN KUNINGAN",
    regency_code: 3208,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN CIREBON",
    regency_code: 3209,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN MAJALENGKA",
    regency_code: 3210,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN SUMEDANG",
    regency_code: 3211,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN INDRAMAYU",
    regency_code: 3212,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN SUBANG",
    regency_code: 3213,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN PURWAKARTA",
    regency_code: 3214,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN KARAWANG",
    regency_code: 3215,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN BEKASI",
    regency_code: 3216,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN BANDUNG BARAT",
    regency_code: 3217,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN PANGANDARAN",
    regency_code: 3218,
    province_code: 32
  },
  {
    regency_name: "KOTA BOGOR",
    regency_code: 3271,
    province_code: 32
  },
  {
    regency_name: "KOTA SUKABUMI",
    regency_code: 3272,
    province_code: 32
  },
  {
    regency_name: "KOTA BANDUNG",
    regency_code: 3273,
    province_code: 32
  },
  {
    regency_name: "KOTA CIREBON",
    regency_code: 3274,
    province_code: 32
  },
  {
    regency_name: "KOTA BEKASI",
    regency_code: 3275,
    province_code: 32
  },
  {
    regency_name: "KOTA DEPOK",
    regency_code: 3276,
    province_code: 32
  },
  {
    regency_name: "KOTA CIMAHI",
    regency_code: 3277,
    province_code: 32
  },
  {
    regency_name: "KOTA TASIKMALAYA",
    regency_code: 3278,
    province_code: 32
  },
  {
    regency_name: "KOTA BANJAR",
    regency_code: 3279,
    province_code: 32
  },
  {
    regency_name: "KABUPATEN CILACAP",
    regency_code: 3301,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN BANYUMAS",
    regency_code: 3302,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN PURBALINGGA",
    regency_code: 3303,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN BANJARNEGARA",
    regency_code: 3304,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN KEBUMEN",
    regency_code: 3305,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN PURWOREJO",
    regency_code: 3306,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN WONOSOBO",
    regency_code: 3307,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN MAGELANG",
    regency_code: 3308,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN BOYOLALI",
    regency_code: 3309,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN KLATEN",
    regency_code: 3310,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN SUKOHARJO",
    regency_code: 3311,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN WONOGIRI",
    regency_code: 3312,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN KARANGANYAR",
    regency_code: 3313,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN SRAGEN",
    regency_code: 3314,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN GROBOGAN",
    regency_code: 3315,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN BLORA",
    regency_code: 3316,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN REMBANG",
    regency_code: 3317,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN PATI",
    regency_code: 3318,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN KUDUS",
    regency_code: 3319,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN JEPARA",
    regency_code: 3320,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN DEMAK",
    regency_code: 3321,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN SEMARANG",
    regency_code: 3322,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN TEMANGGUNG",
    regency_code: 3323,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN KENDAL",
    regency_code: 3324,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN BATANG",
    regency_code: 3325,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN PEKALONGAN",
    regency_code: 3326,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN PEMALANG",
    regency_code: 3327,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN TEGAL",
    regency_code: 3328,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN BREBES",
    regency_code: 3329,
    province_code: 33
  },
  {
    regency_name: "KOTA MAGELANG",
    regency_code: 3371,
    province_code: 33
  },
  {
    regency_name: "KOTA SURAKARTA",
    regency_code: 3372,
    province_code: 33
  },
  {
    regency_name: "KOTA SALATIGA",
    regency_code: 3373,
    province_code: 33
  },
  {
    regency_name: "KOTA SEMARANG",
    regency_code: 3374,
    province_code: 33
  },
  {
    regency_name: "KOTA PEKALONGAN",
    regency_code: 3375,
    province_code: 33
  },
  {
    regency_name: "KOTA TEGAL",
    regency_code: 3376,
    province_code: 33
  },
  {
    regency_name: "KABUPATEN KULON PROGO",
    regency_code: 3401,
    province_code: 34
  },
  {
    regency_name: "KABUPATEN BANTUL",
    regency_code: 3402,
    province_code: 34
  },
  {
    regency_name: "KABUPATEN GUNUNG KIDUL",
    regency_code: 3403,
    province_code: 34
  },
  {
    regency_name: "KABUPATEN SLEMAN",
    regency_code: 3404,
    province_code: 34
  },
  {
    regency_name: "KOTA YOGYAKARTA",
    regency_code: 3471,
    province_code: 34
  },
  {
    regency_name: "KABUPATEN PACITAN",
    regency_code: 3501,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN PONOROGO",
    regency_code: 3502,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN TRENGGALEK",
    regency_code: 3503,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN TULUNGAGUNG",
    regency_code: 3504,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN BLITAR",
    regency_code: 3505,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN KEDIRI",
    regency_code: 3506,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN MALANG",
    regency_code: 3507,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN LUMAJANG",
    regency_code: 3508,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN JEMBER",
    regency_code: 3509,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN BANYUWANGI",
    regency_code: 3510,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN BONDOWOSO",
    regency_code: 3511,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN SITUBONDO",
    regency_code: 3512,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN PROBOLINGGO",
    regency_code: 3513,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN PASURUAN",
    regency_code: 3514,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN SIDOARJO",
    regency_code: 3515,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN MOJOKERTO",
    regency_code: 3516,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN JOMBANG",
    regency_code: 3517,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN NGANJUK",
    regency_code: 3518,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN MADIUN",
    regency_code: 3519,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN MAGETAN",
    regency_code: 3520,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN NGAWI",
    regency_code: 3521,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN BOJONEGORO",
    regency_code: 3522,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN TUBAN",
    regency_code: 3523,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN LAMONGAN",
    regency_code: 3524,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN GRESIK",
    regency_code: 3525,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN BANGKALAN",
    regency_code: 3526,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN SAMPANG",
    regency_code: 3527,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN PAMEKASAN",
    regency_code: 3528,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN SUMENEP",
    regency_code: 3529,
    province_code: 35
  },
  {
    regency_name: "KOTA KEDIRI",
    regency_code: 3571,
    province_code: 35
  },
  {
    regency_name: "KOTA BLITAR",
    regency_code: 3572,
    province_code: 35
  },
  {
    regency_name: "KOTA MALANG",
    regency_code: 3573,
    province_code: 35
  },
  {
    regency_name: "KOTA PROBOLINGGO",
    regency_code: 3574,
    province_code: 35
  },
  {
    regency_name: "KOTA PASURUAN",
    regency_code: 3575,
    province_code: 35
  },
  {
    regency_name: "KOTA MOJOKERTO",
    regency_code: 3576,
    province_code: 35
  },
  {
    regency_name: "KOTA MADIUN",
    regency_code: 3577,
    province_code: 35
  },
  {
    regency_name: "KOTA SURABAYA",
    regency_code: 3578,
    province_code: 35
  },
  {
    regency_name: "KOTA BATU",
    regency_code: 3579,
    province_code: 35
  },
  {
    regency_name: "KABUPATEN PANDEGLANG",
    regency_code: 3601,
    province_code: 36
  },
  {
    regency_name: "KABUPATEN LEBAK",
    regency_code: 3602,
    province_code: 36
  },
  {
    regency_name: "KABUPATEN TANGERANG",
    regency_code: 3603,
    province_code: 36
  },
  {
    regency_name: "KABUPATEN SERANG",
    regency_code: 3604,
    province_code: 36
  },
  {
    regency_name: "KOTA TANGERANG",
    regency_code: 3671,
    province_code: 36
  },
  {
    regency_name: "KOTA CILEGON",
    regency_code: 3672,
    province_code: 36
  },
  {
    regency_name: "KOTA SERANG",
    regency_code: 3673,
    province_code: 36
  },
  {
    regency_name: "KOTA TANGERANG SELATAN",
    regency_code: 3674,
    province_code: 36
  },
  {
    regency_name: "KABUPATEN JEMBRANA",
    regency_code: 5101,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN TABANAN",
    regency_code: 5102,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN BADUNG",
    regency_code: 5103,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN GIANYAR",
    regency_code: 5104,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN KLUNGKUNG",
    regency_code: 5105,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN BANGLI",
    regency_code: 5106,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN KARANG ASEM",
    regency_code: 5107,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN BULELENG",
    regency_code: 5108,
    province_code: 51
  },
  {
    regency_name: "KOTA DENPASAR",
    regency_code: 5171,
    province_code: 51
  },
  {
    regency_name: "KABUPATEN LOMBOK BARAT",
    regency_code: 5201,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN LOMBOK TENGAH",
    regency_code: 5202,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN LOMBOK TIMUR",
    regency_code: 5203,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN SUMBAWA",
    regency_code: 5204,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN DOMPU",
    regency_code: 5205,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN BIMA",
    regency_code: 5206,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN SUMBAWA BARAT",
    regency_code: 5207,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN LOMBOK UTARA",
    regency_code: 5208,
    province_code: 52
  },
  {
    regency_name: "KOTA MATARAM",
    regency_code: 5271,
    province_code: 52
  },
  {
    regency_name: "KOTA BIMA",
    regency_code: 5272,
    province_code: 52
  },
  {
    regency_name: "KABUPATEN SUMBA BARAT",
    regency_code: 5301,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN SUMBA TIMUR",
    regency_code: 5302,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN KUPANG",
    regency_code: 5303,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN TIMOR TENGAH SELATAN",
    regency_code: 5304,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN TIMOR TENGAH UTARA",
    regency_code: 5305,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN BELU",
    regency_code: 5306,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN ALOR",
    regency_code: 5307,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN LEMBATA",
    regency_code: 5308,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN FLORES TIMUR",
    regency_code: 5309,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN SIKKA",
    regency_code: 5310,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN ENDE",
    regency_code: 5311,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN NGADA",
    regency_code: 5312,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN MANGGARAI",
    regency_code: 5313,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN ROTE NDAO",
    regency_code: 5314,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN MANGGARAI BARAT",
    regency_code: 5315,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN SUMBA TENGAH",
    regency_code: 5316,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN SUMBA BARAT DAYA",
    regency_code: 5317,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN NAGEKEO",
    regency_code: 5318,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN MANGGARAI TIMUR",
    regency_code: 5319,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN SABU RAIJUA",
    regency_code: 5320,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN MALAKA",
    regency_code: 5321,
    province_code: 53
  },
  {
    regency_name: "KOTA KUPANG",
    regency_code: 5371,
    province_code: 53
  },
  {
    regency_name: "KABUPATEN SAMBAS",
    regency_code: 6101,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN BENGKAYANG",
    regency_code: 6102,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN LANDAK",
    regency_code: 6103,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN MEMPAWAH",
    regency_code: 6104,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN SANGGAU",
    regency_code: 6105,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN KETAPANG",
    regency_code: 6106,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN SINTANG",
    regency_code: 6107,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN KAPUAS HULU",
    regency_code: 6108,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN SEKADAU",
    regency_code: 6109,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN MELAWI",
    regency_code: 6110,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN KAYONG UTARA",
    regency_code: 6111,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN KUBU RAYA",
    regency_code: 6112,
    province_code: 61
  },
  {
    regency_name: "KOTA PONTIANAK",
    regency_code: 6171,
    province_code: 61
  },
  {
    regency_name: "KOTA SINGKAWANG",
    regency_code: 6172,
    province_code: 61
  },
  {
    regency_name: "KABUPATEN KOTAWARINGIN BARAT",
    regency_code: 6201,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN KOTAWARINGIN TIMUR",
    regency_code: 6202,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN KAPUAS",
    regency_code: 6203,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN BARITO SELATAN",
    regency_code: 6204,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN BARITO UTARA",
    regency_code: 6205,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN SUKAMARA",
    regency_code: 6206,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN LAMANDAU",
    regency_code: 6207,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN SERUYAN",
    regency_code: 6208,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN KATINGAN",
    regency_code: 6209,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN PULANG PISAU",
    regency_code: 6210,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN GUNUNG MAS",
    regency_code: 6211,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN BARITO TIMUR",
    regency_code: 6212,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN MURUNG RAYA",
    regency_code: 6213,
    province_code: 62
  },
  {
    regency_name: "KOTA PALANGKA RAYA",
    regency_code: 6271,
    province_code: 62
  },
  {
    regency_name: "KABUPATEN TANAH LAUT",
    regency_code: 6301,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN KOTA BARU",
    regency_code: 6302,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN BANJAR",
    regency_code: 6303,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN BARITO KUALA",
    regency_code: 6304,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN TAPIN",
    regency_code: 6305,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN HULU SUNGAI SELATAN",
    regency_code: 6306,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN HULU SUNGAI TENGAH",
    regency_code: 6307,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN HULU SUNGAI UTARA",
    regency_code: 6308,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN TABALONG",
    regency_code: 6309,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN TANAH BUMBU",
    regency_code: 6310,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN BALANGAN",
    regency_code: 6311,
    province_code: 63
  },
  {
    regency_name: "KOTA BANJARMASIN",
    regency_code: 6371,
    province_code: 63
  },
  {
    regency_name: "KOTA BANJAR BARU",
    regency_code: 6372,
    province_code: 63
  },
  {
    regency_name: "KABUPATEN PASER",
    regency_code: 6401,
    province_code: 64
  },
  {
    regency_name: "KABUPATEN KUTAI BARAT",
    regency_code: 6402,
    province_code: 64
  },
  {
    regency_name: "KABUPATEN KUTAI KARTANEGARA",
    regency_code: 6403,
    province_code: 64
  },
  {
    regency_name: "KABUPATEN KUTAI TIMUR",
    regency_code: 6404,
    province_code: 64
  },
  {
    regency_name: "KABUPATEN BERAU",
    regency_code: 6405,
    province_code: 64
  },
  {
    regency_name: "KABUPATEN PENAJAM PASER UTARA",
    regency_code: 6409,
    province_code: 64
  },
  {
    regency_name: "KABUPATEN MAHAKAM HULU",
    regency_code: 6411,
    province_code: 64
  },
  {
    regency_name: "KOTA BALIKPAPAN",
    regency_code: 6471,
    province_code: 64
  },
  {
    regency_name: "KOTA SAMARINDA",
    regency_code: 6472,
    province_code: 64
  },
  {
    regency_name: "KOTA BONTANG",
    regency_code: 6474,
    province_code: 64
  },
  {
    regency_name: "KABUPATEN MALINAU",
    regency_code: 6501,
    province_code: 65
  },
  {
    regency_name: "KABUPATEN BULUNGAN",
    regency_code: 6502,
    province_code: 65
  },
  {
    regency_name: "KABUPATEN TANA TIDUNG",
    regency_code: 6503,
    province_code: 65
  },
  {
    regency_name: "KABUPATEN NUNUKAN",
    regency_code: 6504,
    province_code: 65
  },
  {
    regency_name: "KOTA TARAKAN",
    regency_code: 6571,
    province_code: 65
  },
  {
    regency_name: "KABUPATEN BOLAANG MONGONDOW",
    regency_code: 7101,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN MINAHASA",
    regency_code: 7102,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN KEPULAUAN SANGIHE",
    regency_code: 7103,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN KEPULAUAN TALAUD",
    regency_code: 7104,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN MINAHASA SELATAN",
    regency_code: 7105,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN MINAHASA UTARA",
    regency_code: 7106,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN BOLAANG MONGONDOW UTARA",
    regency_code: 7107,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN SIAU TAGULANDANG BIARO",
    regency_code: 7108,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN MINAHASA TENGGARA",
    regency_code: 7109,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN BOLAANG MONGONDOW SELATAN",
    regency_code: 7110,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN BOLAANG MONGONDOW TIMUR",
    regency_code: 7111,
    province_code: 71
  },
  {
    regency_name: "KOTA MANADO",
    regency_code: 7171,
    province_code: 71
  },
  {
    regency_name: "KOTA BITUNG",
    regency_code: 7172,
    province_code: 71
  },
  {
    regency_name: "KOTA TOMOHON",
    regency_code: 7173,
    province_code: 71
  },
  {
    regency_name: "KOTA KOTAMOBAGU",
    regency_code: 7174,
    province_code: 71
  },
  {
    regency_name: "KABUPATEN BANGGAI KEPULAUAN",
    regency_code: 7201,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN BANGGAI",
    regency_code: 7202,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN MOROWALI",
    regency_code: 7203,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN POSO",
    regency_code: 7204,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN DONGGALA",
    regency_code: 7205,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN TOLI-TOLI",
    regency_code: 7206,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN BUOL",
    regency_code: 7207,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN PARIGI MOUTONG",
    regency_code: 7208,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN TOJO UNA-UNA",
    regency_code: 7209,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN SIGI",
    regency_code: 7210,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN BANGGAI LAUT",
    regency_code: 7211,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN MOROWALI UTARA",
    regency_code: 7212,
    province_code: 72
  },
  {
    regency_name: "KOTA PALU",
    regency_code: 7271,
    province_code: 72
  },
  {
    regency_name: "KABUPATEN KEPULAUAN SELAYAR",
    regency_code: 7301,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN BULUKUMBA",
    regency_code: 7302,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN BANTAENG",
    regency_code: 7303,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN JENEPONTO",
    regency_code: 7304,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN TAKALAR",
    regency_code: 7305,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN GOWA",
    regency_code: 7306,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN SINJAI",
    regency_code: 7307,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN MAROS",
    regency_code: 7308,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN PANGKAJENE DAN KEPULAUAN",
    regency_code: 7309,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN BARRU",
    regency_code: 7310,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN BONE",
    regency_code: 7311,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN SOPPENG",
    regency_code: 7312,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN WAJO",
    regency_code: 7313,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN SIDENRENG RAPPANG",
    regency_code: 7314,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN PINRANG",
    regency_code: 7315,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN ENREKANG",
    regency_code: 7316,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN LUWU",
    regency_code: 7317,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN TANA TORAJA",
    regency_code: 7318,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN LUWU UTARA",
    regency_code: 7322,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN LUWU TIMUR",
    regency_code: 7325,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN TORAJA UTARA",
    regency_code: 7326,
    province_code: 73
  },
  {
    regency_name: "KOTA MAKASSAR",
    regency_code: 7371,
    province_code: 73
  },
  {
    regency_name: "KOTA PAREPARE",
    regency_code: 7372,
    province_code: 73
  },
  {
    regency_name: "KOTA PALOPO",
    regency_code: 7373,
    province_code: 73
  },
  {
    regency_name: "KABUPATEN BUTON",
    regency_code: 7401,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN MUNA",
    regency_code: 7402,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN KONAWE",
    regency_code: 7403,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN KOLAKA",
    regency_code: 7404,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN KONAWE SELATAN",
    regency_code: 7405,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN BOMBANA",
    regency_code: 7406,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN WAKATOBI",
    regency_code: 7407,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN KOLAKA UTARA",
    regency_code: 7408,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN BUTON UTARA",
    regency_code: 7409,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN KONAWE UTARA",
    regency_code: 7410,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN KOLAKA TIMUR",
    regency_code: 7411,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN KONAWE KEPULAUAN",
    regency_code: 7412,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN MUNA BARAT",
    regency_code: 7413,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN BUTON TENGAH",
    regency_code: 7414,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN BUTON SELATAN",
    regency_code: 7415,
    province_code: 74
  },
  {
    regency_name: "KOTA KENDARI",
    regency_code: 7471,
    province_code: 74
  },
  {
    regency_name: "KOTA BAUBAU",
    regency_code: 7472,
    province_code: 74
  },
  {
    regency_name: "KABUPATEN BOALEMO",
    regency_code: 7501,
    province_code: 75
  },
  {
    regency_name: "KABUPATEN GORONTALO",
    regency_code: 7502,
    province_code: 75
  },
  {
    regency_name: "KABUPATEN POHUWATO",
    regency_code: 7503,
    province_code: 75
  },
  {
    regency_name: "KABUPATEN BONE BOLANGO",
    regency_code: 7504,
    province_code: 75
  },
  {
    regency_name: "KABUPATEN GORONTALO UTARA",
    regency_code: 7505,
    province_code: 75
  },
  {
    regency_name: "KOTA GORONTALO",
    regency_code: 7571,
    province_code: 75
  },
  {
    regency_name: "KABUPATEN MAJENE",
    regency_code: 7601,
    province_code: 76
  },
  {
    regency_name: "KABUPATEN POLEWALI MANDAR",
    regency_code: 7602,
    province_code: 76
  },
  {
    regency_name: "KABUPATEN MAMASA",
    regency_code: 7603,
    province_code: 76
  },
  {
    regency_name: "KABUPATEN MAMUJU",
    regency_code: 7604,
    province_code: 76
  },
  {
    regency_name: "KABUPATEN MAMUJU UTARA",
    regency_code: 7605,
    province_code: 76
  },
  {
    regency_name: "KABUPATEN MAMUJU TENGAH",
    regency_code: 7606,
    province_code: 76
  },
  {
    regency_name: "KABUPATEN MALUKU TENGGARA BARAT",
    regency_code: 8101,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN MALUKU TENGGARA",
    regency_code: 8102,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN MALUKU TENGAH",
    regency_code: 8103,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN BURU",
    regency_code: 8104,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN KEPULAUAN ARU",
    regency_code: 8105,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN SERAM BAGIAN BARAT",
    regency_code: 8106,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN SERAM BAGIAN TIMUR",
    regency_code: 8107,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN MALUKU BARAT DAYA",
    regency_code: 8108,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN BURU SELATAN",
    regency_code: 8109,
    province_code: 81
  },
  {
    regency_name: "KOTA AMBON",
    regency_code: 8171,
    province_code: 81
  },
  {
    regency_name: "KOTA TUAL",
    regency_code: 8172,
    province_code: 81
  },
  {
    regency_name: "KABUPATEN HALMAHERA BARAT",
    regency_code: 8201,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN HALMAHERA TENGAH",
    regency_code: 8202,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN KEPULAUAN SULA",
    regency_code: 8203,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN HALMAHERA SELATAN",
    regency_code: 8204,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN HALMAHERA UTARA",
    regency_code: 8205,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN HALMAHERA TIMUR",
    regency_code: 8206,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN PULAU MOROTAI",
    regency_code: 8207,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN PULAU TALIABU",
    regency_code: 8208,
    province_code: 82
  },
  {
    regency_name: "KOTA TERNATE",
    regency_code: 8271,
    province_code: 82
  },
  {
    regency_name: "KOTA TIDORE KEPULAUAN",
    regency_code: 8272,
    province_code: 82
  },
  {
    regency_name: "KABUPATEN FAKFAK",
    regency_code: 9101,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN KAIMANA",
    regency_code: 9102,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN TELUK WONDAMA",
    regency_code: 9103,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN TELUK BINTUNI",
    regency_code: 9104,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN MANOKWARI",
    regency_code: 9105,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN SORONG SELATAN",
    regency_code: 9106,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN SORONG",
    regency_code: 9107,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN RAJA AMPAT",
    regency_code: 9108,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN TAMBRAUW",
    regency_code: 9109,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN MAYBRAT",
    regency_code: 9110,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN MANOKWARI SELATAN",
    regency_code: 9111,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN PEGUNUNGAN ARFAK",
    regency_code: 9112,
    province_code: 91
  },
  {
    regency_name: "KOTA SORONG",
    regency_code: 9171,
    province_code: 91
  },
  {
    regency_name: "KABUPATEN MERAUKE",
    regency_code: 9401,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN JAYAWIJAYA",
    regency_code: 9402,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN JAYAPURA",
    regency_code: 9403,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN NABIRE",
    regency_code: 9404,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN KEPULAUAN YAPEN",
    regency_code: 9408,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN BIAK NUMFOR",
    regency_code: 9409,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN PANIAI",
    regency_code: 9410,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN PUNCAK JAYA",
    regency_code: 9411,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN MIMIKA",
    regency_code: 9412,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN BOVEN DIGOEL",
    regency_code: 9413,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN MAPPI",
    regency_code: 9414,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN ASMAT",
    regency_code: 9415,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN YAHUKIMO",
    regency_code: 9416,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN PEGUNUNGAN BINTANG",
    regency_code: 9417,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN TOLIKARA",
    regency_code: 9418,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN SARMI",
    regency_code: 9419,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN KEEROM",
    regency_code: 9420,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN WAROPEN",
    regency_code: 9426,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN SUPIORI",
    regency_code: 9427,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN MAMBERAMO RAYA",
    regency_code: 9428,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN NDUGA",
    regency_code: 9429,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN LANNY JAYA",
    regency_code: 9430,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN MAMBERAMO TENGAH",
    regency_code: 9431,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN YALIMO",
    regency_code: 9432,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN PUNCAK",
    regency_code: 9433,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN DOGIYAI",
    regency_code: 9434,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN INTAN JAYA",
    regency_code: 9435,
    province_code: 94
  },
  {
    regency_name: "KABUPATEN DEIYAI",
    regency_code: 9436,
    province_code: 94
  },
  {
    regency_name: "KOTA JAYAPURA",
    regency_code: 9471,
    province_code: 94
  }
];
