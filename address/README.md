# Instructions

Before running the script, ensure you have these installed on your system:

* Python 3
* Pandas v0.22.0

Clone the repository. To run the script, open a terminal and run this command:

`python scripy.py > output.js`

The file `output.js` will contain these dictionary components: `vm.province`, `vm.regency`, `vm.district` and `vm.village`.

Each of these components will have attributes assigned to them that describes the relationships between the various components. 

# Possible Improvements

Right now, I'm forcing the output to be piped into a file called `output.js` via terminal.

We can consider moving the script to write to a file itself, thereby, removing the need for the pipe.