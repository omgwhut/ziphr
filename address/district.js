vm.district = [
  {
    district_name: "TEUPAH SELATAN",
    district_code: 1101010,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "SIMEULUE TIMUR",
    district_code: 1101020,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "TEUPAH BARAT",
    district_code: 1101021,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "TEUPAH TENGAH",
    district_code: 1101022,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "SIMEULUE TENGAH",
    district_code: 1101030,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "TELUK DALAM",
    district_code: 1101031,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "SIMEULUE CUT",
    district_code: 1101032,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "SALANG",
    district_code: 1101040,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "SIMEULUE BARAT",
    district_code: 1101050,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "ALAFAN",
    district_code: 1101051,
    regency_code: 1101,
    province_code: 11
  },
  {
    district_name: "PULAU BANYAK",
    district_code: 1102010,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "PULAU BANYAK BARAT",
    district_code: 1102011,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "SINGKIL",
    district_code: 1102020,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "SINGKIL UTARA",
    district_code: 1102021,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "KUALA BARU",
    district_code: 1102022,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "SIMPANG KANAN",
    district_code: 1102030,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "GUNUNG MERIAH",
    district_code: 1102031,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "DANAU PARIS",
    district_code: 1102032,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "SURO",
    district_code: 1102033,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "SINGKOHOR",
    district_code: 1102042,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "KOTA BAHARU",
    district_code: 1102043,
    regency_code: 1102,
    province_code: 11
  },
  {
    district_name: "TRUMON",
    district_code: 1103010,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "TRUMON TIMUR",
    district_code: 1103011,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "TRUMON TENGAH",
    district_code: 1103012,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "BAKONGAN",
    district_code: 1103020,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "BAKONGAN TIMUR",
    district_code: 1103021,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "KOTA BAHAGIA",
    district_code: 1103022,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "KLUET SELATAN",
    district_code: 1103030,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "KLUET TIMUR",
    district_code: 1103031,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "KLUET UTARA",
    district_code: 1103040,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "PASIE RAJA",
    district_code: 1103041,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "KLUET TENGAH",
    district_code: 1103042,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "TAPAK TUAN",
    district_code: 1103050,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "SAMA DUA",
    district_code: 1103060,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "SAWANG",
    district_code: 1103070,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "MEUKEK",
    district_code: 1103080,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "LABUHAN HAJI",
    district_code: 1103090,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "LABUHAN HAJI TIMUR",
    district_code: 1103091,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "LABUHAN HAJI BARAT",
    district_code: 1103092,
    regency_code: 1103,
    province_code: 11
  },
  {
    district_name: "LAWE ALAS",
    district_code: 1104010,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "BABUL RAHMAH",
    district_code: 1104011,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "TANOH ALAS",
    district_code: 1104012,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "LAWE SIGALA-GALA",
    district_code: 1104020,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "BABUL MAKMUR",
    district_code: 1104021,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "SEMADAM",
    district_code: 1104022,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "LEUSER",
    district_code: 1104023,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "BAMBEL",
    district_code: 1104030,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "BUKIT TUSAM",
    district_code: 1104031,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "LAWE SUMUR",
    district_code: 1104032,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "BABUSSALAM",
    district_code: 1104040,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "LAWE BULAN",
    district_code: 1104041,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "BADAR",
    district_code: 1104050,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "DARUL HASANAH",
    district_code: 1104051,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "KETAMBE",
    district_code: 1104052,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "DELENG POKHKISEN",
    district_code: 1104053,
    regency_code: 1104,
    province_code: 11
  },
  {
    district_name: "SERBA JADI",
    district_code: 1105080,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "SIMPANG JERNIH",
    district_code: 1105081,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "PEUNARON",
    district_code: 1105082,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "BIREM BAYEUN",
    district_code: 1105090,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "RANTAU SELAMAT",
    district_code: 1105100,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "SUNGAI RAYA",
    district_code: 1105101,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "PEUREULAK",
    district_code: 1105110,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "PEUREULAK TIMUR",
    district_code: 1105111,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "PEUREULAK BARAT",
    district_code: 1105112,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "RANTO PEUREULAK",
    district_code: 1105120,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "IDI RAYEUK",
    district_code: 1105130,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "PEUDAWA",
    district_code: 1105131,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "BANDA ALAM",
    district_code: 1105132,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "IDI TUNONG",
    district_code: 1105133,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "DARUL IHSAN",
    district_code: 1105134,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "IDI TIMUR",
    district_code: 1105135,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "DARUL AMAN",
    district_code: 1105140,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "NURUSSALAM",
    district_code: 1105150,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "DARUL FALAH",
    district_code: 1105151,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "JULOK",
    district_code: 1105160,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "INDRA MAKMUR",
    district_code: 1105161,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "PANTE BIDARI",
    district_code: 1105170,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "SIMPANG ULIM",
    district_code: 1105180,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "MADAT",
    district_code: 1105181,
    regency_code: 1105,
    province_code: 11
  },
  {
    district_name: "LINGE",
    district_code: 1106010,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "ATU LINTANG",
    district_code: 1106011,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "JAGONG JEGET",
    district_code: 1106012,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "BINTANG",
    district_code: 1106020,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "LUT TAWAR",
    district_code: 1106031,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "KEBAYAKAN",
    district_code: 1106032,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "PEGASING",
    district_code: 1106040,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "BIES",
    district_code: 1106041,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "BEBESEN",
    district_code: 1106050,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "KUTE PANANG",
    district_code: 1106051,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "SILIH NARA",
    district_code: 1106060,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "KETOL",
    district_code: 1106061,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "CELALA",
    district_code: 1106062,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "RUSIP ANTARA",
    district_code: 1106063,
    regency_code: 1106,
    province_code: 11
  },
  {
    district_name: "JOHAN PAHLAWAN",
    district_code: 1107050,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "SAMATIGA",
    district_code: 1107060,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "BUBON",
    district_code: 1107061,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "ARONGAN LAMBALEK",
    district_code: 1107062,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "WOYLA",
    district_code: 1107070,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "WOYLA BARAT",
    district_code: 1107071,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "WOYLA TIMUR",
    district_code: 1107072,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "KAWAY XVI",
    district_code: 1107080,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "MEUREUBO",
    district_code: 1107081,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "PANTAI CEUREMEN",
    district_code: 1107082,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "PANTON REU",
    district_code: 1107083,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "SUNGAI MAS",
    district_code: 1107090,
    regency_code: 1107,
    province_code: 11
  },
  {
    district_name: "LHOONG",
    district_code: 1108010,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "LHOKNGA",
    district_code: 1108020,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "LEUPUNG",
    district_code: 1108021,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "INDRAPURI",
    district_code: 1108030,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "KUTA COT GLIE",
    district_code: 1108031,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "SEULIMEUM",
    district_code: 1108040,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "KOTA JANTHO",
    district_code: 1108041,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "LEMBAH SEULAWAH",
    district_code: 1108042,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "MESJID RAYA",
    district_code: 1108050,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "DARUSSALAM",
    district_code: 1108060,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "BAITUSSALAM",
    district_code: 1108061,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "KUTA BARO",
    district_code: 1108070,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "MONTASIK",
    district_code: 1108080,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "BLANG BINTANG",
    district_code: 1108081,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "INGIN JAYA",
    district_code: 1108090,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "KRUENG BARONA JAYA",
    district_code: 1108091,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "SUKA MAKMUR",
    district_code: 1108100,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "KUTA MALAKA",
    district_code: 1108101,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "SIMPANG TIGA",
    district_code: 1108102,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "DARUL IMARAH",
    district_code: 1108110,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "DARUL KAMAL",
    district_code: 1108111,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "PEUKAN BADA",
    district_code: 1108120,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "PULO ACEH",
    district_code: 1108130,
    regency_code: 1108,
    province_code: 11
  },
  {
    district_name: "GEUMPANG",
    district_code: 1109010,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "MANE",
    district_code: 1109011,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "GLUMPANG TIGA",
    district_code: 1109070,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "GLUMPANG BARO",
    district_code: 1109071,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "MUTIARA",
    district_code: 1109080,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "MUTIARA TIMUR",
    district_code: 1109081,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "TIRO/TRUSEB",
    district_code: 1109090,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "TANGSE",
    district_code: 1109100,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "KEUMALA",
    district_code: 1109111,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "TITEUE",
    district_code: 1109112,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "SAKTI",
    district_code: 1109120,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "MILA",
    district_code: 1109130,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "PADANG TIJI",
    district_code: 1109140,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "DELIMA",
    district_code: 1109150,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "GRONG GRONG",
    district_code: 1109151,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "INDRAJAYA",
    district_code: 1109160,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "PEUKAN BARO",
    district_code: 1109170,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "KEMBANG TANJONG",
    district_code: 1109180,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "SIMPANG TIGA",
    district_code: 1109190,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "KOTA SIGLI",
    district_code: 1109200,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "PIDIE",
    district_code: 1109210,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "BATEE",
    district_code: 1109220,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "MUARA TIGA",
    district_code: 1109230,
    regency_code: 1109,
    province_code: 11
  },
  {
    district_name: "SAMALANGA",
    district_code: 1110010,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "SIMPANG MAMPLAM",
    district_code: 1110011,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "PANDRAH",
    district_code: 1110020,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "JEUNIEB",
    district_code: 1110030,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "PEULIMBANG",
    district_code: 1110031,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "PEUDADA",
    district_code: 1110040,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "JULI",
    district_code: 1110050,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "JEUMPA",
    district_code: 1110060,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "KOTA JUANG",
    district_code: 1110061,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "KUALA",
    district_code: 1110062,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "JANGKA",
    district_code: 1110070,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "PEUSANGAN",
    district_code: 1110080,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "PEUSANGAN SELATAN",
    district_code: 1110081,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "PEUSANGAN SIBLAH KRUENG",
    district_code: 1110082,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "MAKMUR",
    district_code: 1110090,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "GANDA PURA",
    district_code: 1110100,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "KUTA BLANG",
    district_code: 1110101,
    regency_code: 1110,
    province_code: 11
  },
  {
    district_name: "SAWANG",
    district_code: 1111010,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "NISAM",
    district_code: 1111020,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "NISAM ANTARA",
    district_code: 1111021,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "BANDA BARO",
    district_code: 1111022,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "KUTA MAKMUR",
    district_code: 1111030,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "SIMPANG KERAMAT",
    district_code: 1111031,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "SYAMTALIRA BAYU",
    district_code: 1111040,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "GEUREUDONG PASE",
    district_code: 1111041,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "MEURAH MULIA",
    district_code: 1111050,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "MATANGKULI",
    district_code: 1111060,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "PAYA BAKONG",
    district_code: 1111061,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "PIRAK TIMU",
    district_code: 1111062,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "COT GIREK",
    district_code: 1111070,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "TANAH JAMBO AYE",
    district_code: 1111080,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "LANGKAHAN",
    district_code: 1111081,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "SEUNUDDON",
    district_code: 1111090,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "BAKTIYA",
    district_code: 1111100,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "BAKTIYA BARAT",
    district_code: 1111101,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "LHOKSUKON",
    district_code: 1111110,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "TANAH LUAS",
    district_code: 1111120,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "NIBONG",
    district_code: 1111121,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "SAMUDERA",
    district_code: 1111130,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "SYAMTALIRA ARON",
    district_code: 1111140,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "TANAH PASIR",
    district_code: 1111150,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "LAPANG",
    district_code: 1111151,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "MUARA BATU",
    district_code: 1111160,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "DEWANTARA",
    district_code: 1111170,
    regency_code: 1111,
    province_code: 11
  },
  {
    district_name: "MANGGENG",
    district_code: 1112010,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "LEMBAH SABIL",
    district_code: 1112011,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "TANGAN-TANGAN",
    district_code: 1112020,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "SETIA",
    district_code: 1112021,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "BLANG PIDIE",
    district_code: 1112030,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "JEUMPA",
    district_code: 1112031,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "SUSOH",
    district_code: 1112040,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "KUALA BATEE",
    district_code: 1112050,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "BABAH ROT",
    district_code: 1112060,
    regency_code: 1112,
    province_code: 11
  },
  {
    district_name: "KUTA PANJANG",
    district_code: 1113010,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "BLANG JERANGO",
    district_code: 1113011,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "BLANGKEJEREN",
    district_code: 1113020,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "PUTRI BETUNG",
    district_code: 1113021,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "DABUN GELANG",
    district_code: 1113022,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "BLANG PEGAYON",
    district_code: 1113023,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "PINING",
    district_code: 1113030,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "RIKIT GAIB",
    district_code: 1113040,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "PANTAN CUACA",
    district_code: 1113041,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "TERANGUN",
    district_code: 1113050,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "TRIPE JAYA",
    district_code: 1113051,
    regency_code: 1113,
    province_code: 11
  },
  {
    district_name: "TAMIANG HULU",
    district_code: 1114010,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "BANDAR PUSAKA",
    district_code: 1114011,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "KEJURUAN MUDA",
    district_code: 1114020,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "TENGGULUN",
    district_code: 1114021,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "RANTAU",
    district_code: 1114030,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "KOTA KUALA SIMPANG",
    district_code: 1114040,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "SERUWAY",
    district_code: 1114050,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "BENDAHARA",
    district_code: 1114060,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "BANDA MULIA",
    district_code: 1114061,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "KARANG BARU",
    district_code: 1114070,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "SEKERAK",
    district_code: 1114071,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "MANYAK PAYED",
    district_code: 1114080,
    regency_code: 1114,
    province_code: 11
  },
  {
    district_name: "DARUL MAKMUR",
    district_code: 1115010,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "TRIPA MAKMUR",
    district_code: 1115011,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "KUALA",
    district_code: 1115020,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "KUALA PESISIR",
    district_code: 1115021,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "TADU RAYA",
    district_code: 1115022,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "BEUTONG",
    district_code: 1115030,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "BEUTONG ATEUH BANGGALANG",
    district_code: 1115031,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "SEUNAGAN",
    district_code: 1115040,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "SUKA MAKMUE",
    district_code: 1115041,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "SEUNAGAN TIMUR",
    district_code: 1115050,
    regency_code: 1115,
    province_code: 11
  },
  {
    district_name: "TEUNOM",
    district_code: 1116010,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "PASIE RAYA",
    district_code: 1116011,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "PANGA",
    district_code: 1116020,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "KRUENG SABEE",
    district_code: 1116030,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "SETIA BAKTI",
    district_code: 1116040,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "SAMPOINIET",
    district_code: 1116050,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "DARUL HIKMAH",
    district_code: 1116051,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "JAYA",
    district_code: 1116060,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "INDRA JAYA",
    district_code: 1116061,
    regency_code: 1116,
    province_code: 11
  },
  {
    district_name: "TIMANG GAJAH",
    district_code: 1117010,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "GAJAH PUTIH",
    district_code: 1117011,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "PINTU RIME GAYO",
    district_code: 1117020,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "BUKIT",
    district_code: 1117030,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "WIH PESAM",
    district_code: 1117040,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "BANDAR",
    district_code: 1117050,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "BENER KELIPAH",
    district_code: 1117051,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "SYIAH UTAMA",
    district_code: 1117060,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "MESIDAH",
    district_code: 1117061,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "PERMATA",
    district_code: 1117070,
    regency_code: 1117,
    province_code: 11
  },
  {
    district_name: "MEUREUDU",
    district_code: 1118010,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "MEURAH DUA",
    district_code: 1118020,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "BANDAR DUA",
    district_code: 1118030,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "JANGKA BUYA",
    district_code: 1118040,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "ULIM",
    district_code: 1118050,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "TRIENGGADENG",
    district_code: 1118060,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "PANTERAJA",
    district_code: 1118070,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "BANDAR BARU",
    district_code: 1118080,
    regency_code: 1118,
    province_code: 11
  },
  {
    district_name: "MEURAXA",
    district_code: 1171010,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "JAYA BARU",
    district_code: 1171011,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "BANDA RAYA",
    district_code: 1171012,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "BAITURRAHMAN",
    district_code: 1171020,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "LUENG BATA",
    district_code: 1171021,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "KUTA ALAM",
    district_code: 1171030,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "KUTA RAJA",
    district_code: 1171031,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "SYIAH KUALA",
    district_code: 1171040,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "ULEE KARENG",
    district_code: 1171041,
    regency_code: 1171,
    province_code: 11
  },
  {
    district_name: "SUKAJAYA",
    district_code: 1172010,
    regency_code: 1172,
    province_code: 11
  },
  {
    district_name: "SUKAKARYA",
    district_code: 1172020,
    regency_code: 1172,
    province_code: 11
  },
  {
    district_name: "LANGSA TIMUR",
    district_code: 1173010,
    regency_code: 1173,
    province_code: 11
  },
  {
    district_name: "LANGSA LAMA",
    district_code: 1173011,
    regency_code: 1173,
    province_code: 11
  },
  {
    district_name: "LANGSA BARAT",
    district_code: 1173020,
    regency_code: 1173,
    province_code: 11
  },
  {
    district_name: "LANGSA BARO",
    district_code: 1173021,
    regency_code: 1173,
    province_code: 11
  },
  {
    district_name: "LANGSA KOTA",
    district_code: 1173030,
    regency_code: 1173,
    province_code: 11
  },
  {
    district_name: "BLANG MANGAT",
    district_code: 1174010,
    regency_code: 1174,
    province_code: 11
  },
  {
    district_name: "MUARA DUA",
    district_code: 1174020,
    regency_code: 1174,
    province_code: 11
  },
  {
    district_name: "MUARA SATU",
    district_code: 1174021,
    regency_code: 1174,
    province_code: 11
  },
  {
    district_name: "BANDA SAKTI",
    district_code: 1174030,
    regency_code: 1174,
    province_code: 11
  },
  {
    district_name: "SIMPANG KIRI",
    district_code: 1175010,
    regency_code: 1175,
    province_code: 11
  },
  {
    district_name: "PENANGGALAN",
    district_code: 1175020,
    regency_code: 1175,
    province_code: 11
  },
  {
    district_name: "RUNDENG",
    district_code: 1175030,
    regency_code: 1175,
    province_code: 11
  },
  {
    district_name: "SULTAN DAULAT",
    district_code: 1175040,
    regency_code: 1175,
    province_code: 11
  },
  {
    district_name: "LONGKIB",
    district_code: 1175050,
    regency_code: 1175,
    province_code: 11
  },
  {
    district_name: "IDANO GAWO",
    district_code: 1201060,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "BAWOLATO",
    district_code: 1201061,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "ULUGAWO",
    district_code: 1201062,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "GIDO",
    district_code: 1201070,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "SOGAEADU",
    district_code: 1201071,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "MA U",
    district_code: 1201081,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "SOMOLO - MOLO",
    district_code: 1201082,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "HILIDUHO",
    district_code: 1201130,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "HILI SERANGKAI",
    district_code: 1201131,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "BOTOMUZOI",
    district_code: 1201132,
    regency_code: 1201,
    province_code: 12
  },
  {
    district_name: "BATAHAN",
    district_code: 1202010,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "SINUNUKAN",
    district_code: 1202011,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "BATANG NATAL",
    district_code: 1202020,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "LINGGA BAYU",
    district_code: 1202021,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "RANTO BAEK",
    district_code: 1202022,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "KOTANOPAN",
    district_code: 1202030,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "ULU PUNGKUT",
    district_code: 1202031,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "TAMBANGAN",
    district_code: 1202032,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "LEMBAH SORIK MARAPI",
    district_code: 1202033,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "PUNCAK SORIK MARAPI",
    district_code: 1202034,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "MUARA SIPONGI",
    district_code: 1202040,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "PAKANTAN",
    district_code: 1202041,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "PANYABUNGAN",
    district_code: 1202050,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "PANYABUNGAN SELATAN",
    district_code: 1202051,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "PANYABUNGAN BARAT",
    district_code: 1202052,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "PANYABUNGAN UTARA",
    district_code: 1202053,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "PANYABUNGAN TIMUR",
    district_code: 1202054,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "HUTA BARGOT",
    district_code: 1202055,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "NATAL",
    district_code: 1202060,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "MUARA BATANG GADIS",
    district_code: 1202070,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "SIABU",
    district_code: 1202080,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "BUKIT MALINTANG",
    district_code: 1202081,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "NAGA JUANG",
    district_code: 1202082,
    regency_code: 1202,
    province_code: 12
  },
  {
    district_name: "BATANG ANGKOLA",
    district_code: 1203010,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "SAYUR MATINGGI",
    district_code: 1203011,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "TANO TOMBANGAN ANGKOLA",
    district_code: 1203012,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "ANGKOLA TIMUR",
    district_code: 1203070,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "ANGKOLA SELATAN",
    district_code: 1203080,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "ANGKOLA  BARAT",
    district_code: 1203090,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "ANGKOLA SANGKUNUR",
    district_code: 1203091,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "BATANG TORU",
    district_code: 1203100,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "MARANCAR",
    district_code: 1203101,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "MUARA BATANG TORU",
    district_code: 1203102,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "SIPIROK",
    district_code: 1203110,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "ARSE",
    district_code: 1203120,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "SAIPAR DOLOK HOLE",
    district_code: 1203160,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "AEK BILAH",
    district_code: 1203161,
    regency_code: 1203,
    province_code: 12
  },
  {
    district_name: "PINANG SORI",
    district_code: 1204010,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "BADIRI",
    district_code: 1204011,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SIBABANGUN",
    district_code: 1204020,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "LUMUT",
    district_code: 1204021,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SUKABANGUN",
    district_code: 1204022,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "PANDAN",
    district_code: 1204030,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "TUKKA",
    district_code: 1204031,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SARUDIK",
    district_code: 1204032,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "TAPIAN NAULI",
    district_code: 1204040,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SITAHUIS",
    district_code: 1204041,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "KOLANG",
    district_code: 1204050,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SORKAM",
    district_code: 1204060,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SORKAM BARAT",
    district_code: 1204061,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "PASARIBU TOBING",
    district_code: 1204062,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "BARUS",
    district_code: 1204070,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SOSOR GADONG",
    district_code: 1204071,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "ANDAM DEWI",
    district_code: 1204072,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "BARUS UTARA",
    district_code: 1204073,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "MANDUAMAS",
    district_code: 1204080,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "SIRANDORUNG",
    district_code: 1204081,
    regency_code: 1204,
    province_code: 12
  },
  {
    district_name: "PARMONANGAN",
    district_code: 1205030,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "ADIANKOTING",
    district_code: 1205040,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "SIPOHOLON",
    district_code: 1205050,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "TARUTUNG",
    district_code: 1205060,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "SIATAS BARITA",
    district_code: 1205061,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "PAHAE JULU",
    district_code: 1205070,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "PAHAE JAE",
    district_code: 1205080,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "PURBATUA",
    district_code: 1205081,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "SIMANGUMBAN",
    district_code: 1205082,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "PANGARIBUAN",
    district_code: 1205090,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "GAROGA",
    district_code: 1205100,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "SIPAHUTAR",
    district_code: 1205110,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "SIBORONGBORONG",
    district_code: 1205120,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "PAGARAN",
    district_code: 1205130,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "MUARA",
    district_code: 1205180,
    regency_code: 1205,
    province_code: 12
  },
  {
    district_name: "BALIGE",
    district_code: 1206030,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "TAMPAHAN",
    district_code: 1206031,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "LAGUBOTI",
    district_code: 1206040,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "HABINSARAN",
    district_code: 1206050,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "BORBOR",
    district_code: 1206051,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "NASSAU",
    district_code: 1206052,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "SILAEN",
    district_code: 1206060,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "SIGUMPAR",
    district_code: 1206061,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "PORSEA",
    district_code: 1206070,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "PINTU POHAN MERANTI",
    district_code: 1206071,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "SIANTAR NARUMONDA",
    district_code: 1206072,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "PARMAKSIAN",
    district_code: 1206073,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "LUMBAN JULU",
    district_code: 1206080,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "ULUAN",
    district_code: 1206081,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "AJIBATA",
    district_code: 1206082,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "BONATUA LUNASI",
    district_code: 1206083,
    regency_code: 1206,
    province_code: 12
  },
  {
    district_name: "BILAH HULU",
    district_code: 1207050,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "PANGKATAN",
    district_code: 1207070,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "BILAH BARAT",
    district_code: 1207080,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "BILAH HILIR",
    district_code: 1207130,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "PANAI HULU",
    district_code: 1207140,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "PANAI TENGAH",
    district_code: 1207150,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "PANAI HILIR",
    district_code: 1207160,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "RANTAU SELATAN",
    district_code: 1207210,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "RANTAU UTARA",
    district_code: 1207220,
    regency_code: 1207,
    province_code: 12
  },
  {
    district_name: "BANDAR PASIR MANDOGE",
    district_code: 1208010,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "BANDAR PULAU",
    district_code: 1208020,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "AEK SONGSONGAN",
    district_code: 1208021,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "RAHUNING",
    district_code: 1208022,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "PULAU RAKYAT",
    district_code: 1208030,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "AEK KUASAN",
    district_code: 1208031,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "AEK LEDONG",
    district_code: 1208032,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SEI KEPAYANG",
    district_code: 1208040,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SEI KEPAYANG BARAT",
    district_code: 1208041,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SEI KEPAYANG TIMUR",
    district_code: 1208042,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "TANJUNG BALAI",
    district_code: 1208050,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SIMPANG EMPAT",
    district_code: 1208060,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "TELUK DALAM",
    district_code: 1208061,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "AIR BATU",
    district_code: 1208070,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SEI DADAP",
    district_code: 1208071,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "BUNTU PANE",
    district_code: 1208080,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "TINGGI RAJA",
    district_code: 1208081,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SETIA JANJI",
    district_code: 1208082,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "MERANTI",
    district_code: 1208090,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "PULO BANDRING",
    district_code: 1208091,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "RAWANG PANCA ARGA",
    district_code: 1208092,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "AIR JOMAN",
    district_code: 1208100,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SILAU LAUT",
    district_code: 1208101,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "KISARAN BARAT",
    district_code: 1208160,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "KISARAN TIMUR",
    district_code: 1208170,
    regency_code: 1208,
    province_code: 12
  },
  {
    district_name: "SILIMAKUTA",
    district_code: 1209010,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "PEMATANG SILIMAHUTA",
    district_code: 1209011,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "PURBA",
    district_code: 1209020,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "HARANGGAOL HORISON",
    district_code: 1209021,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "SIDAMANIK",
    district_code: 1209040,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "PEMATANG SIDAMANIK",
    district_code: 1209041,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "GIRSANG SIPANGAN BOLON",
    district_code: 1209050,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "TANAH JAWA",
    district_code: 1209060,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "HATONDUHAN",
    district_code: 1209061,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "DOLOK PANRIBUAN",
    district_code: 1209070,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "JORLANG HATARAN",
    district_code: 1209080,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "PANEI",
    district_code: 1209090,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "PANOMBEAN PANEI",
    district_code: 1209091,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "RAYA",
    district_code: 1209100,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "DOLOG MASAGAL",
    district_code: 1209101,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "DOLOK SILAU",
    district_code: 1209110,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "SILAU KAHEAN",
    district_code: 1209120,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "RAYA KAHEAN",
    district_code: 1209130,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "TAPIAN DOLOK",
    district_code: 1209140,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "DOLOK BATU NANGGAR",
    district_code: 1209150,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "SIANTAR",
    district_code: 1209160,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "GUNUNG MALELA",
    district_code: 1209161,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "GUNUNG MALIGAS",
    district_code: 1209162,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "HUTABAYU RAJA",
    district_code: 1209170,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "JAWA MARAJA BAH JAMBI",
    district_code: 1209171,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "PEMATANG BANDAR",
    district_code: 1209180,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "BANDAR HULUAN",
    district_code: 1209181,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "BANDAR",
    district_code: 1209190,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "BANDAR MASILAM",
    district_code: 1209191,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "BOSAR MALIGAS",
    district_code: 1209200,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "UJUNG PADANG",
    district_code: 1209210,
    regency_code: 1209,
    province_code: 12
  },
  {
    district_name: "SIDIKALANG",
    district_code: 1210030,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "BERAMPU",
    district_code: 1210031,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "SITINJO",
    district_code: 1210032,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "PARBULUAN",
    district_code: 1210040,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "SUMBUL",
    district_code: 1210050,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "SILAHISABUNGAN",
    district_code: 1210051,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "SILIMA PUNGGA-PUNGGA",
    district_code: 1210060,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "LAE PARIRA",
    district_code: 1210061,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "SIEMPAT NEMPU",
    district_code: 1210070,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "SIEMPAT NEMPU HULU",
    district_code: 1210080,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "SIEMPAT NEMPU HILIR",
    district_code: 1210090,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "TIGA LINGGA",
    district_code: 1210100,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "GUNUNG SITEMBER",
    district_code: 1210101,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "PEGAGAN HILIR",
    district_code: 1210110,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "TANAH PINEM",
    district_code: 1210120,
    regency_code: 1210,
    province_code: 12
  },
  {
    district_name: "MARDINGDING",
    district_code: 1211010,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "LAUBALENG",
    district_code: 1211020,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "TIGA BINANGA",
    district_code: 1211030,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "JUHAR",
    district_code: 1211040,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "MUNTE",
    district_code: 1211050,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "KUTA BULUH",
    district_code: 1211060,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "PAYUNG",
    district_code: 1211070,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "TIGANDERKET",
    district_code: 1211071,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "SIMPANG EMPAT",
    district_code: 1211080,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "NAMAN TERAN",
    district_code: 1211081,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "MERDEKA",
    district_code: 1211082,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "KABANJAHE",
    district_code: 1211090,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "BERASTAGI",
    district_code: 1211100,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "TIGAPANAH",
    district_code: 1211110,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "DOLAT RAYAT",
    district_code: 1211111,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "MEREK",
    district_code: 1211120,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "BARUSJAHE",
    district_code: 1211130,
    regency_code: 1211,
    province_code: 12
  },
  {
    district_name: "GUNUNG MERIAH",
    district_code: 1212010,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "SINEMBAH TANJUNG MUDA HULU",
    district_code: 1212020,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "SIBOLANGIT",
    district_code: 1212030,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "KUTALIMBARU",
    district_code: 1212040,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "PANCUR BATU",
    district_code: 1212050,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "NAMO RAMBE",
    district_code: 1212060,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "BIRU-BIRU",
    district_code: 1212070,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "SINEMBAH TANJUNG MUDA HILIR",
    district_code: 1212080,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "BANGUN PURBA",
    district_code: 1212090,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "GALANG",
    district_code: 1212190,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "TANJUNG MORAWA",
    district_code: 1212200,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "PATUMBAK",
    district_code: 1212210,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "DELI TUA",
    district_code: 1212220,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "SUNGGAL",
    district_code: 1212230,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "HAMPARAN PERAK",
    district_code: 1212240,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "LABUHAN DELI",
    district_code: 1212250,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "PERCUT SEI TUAN",
    district_code: 1212260,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "BATANG KUIS",
    district_code: 1212270,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "PANTAI LABU",
    district_code: 1212280,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "BERINGIN",
    district_code: 1212290,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "LUBUK PAKAM",
    district_code: 1212300,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "PAGAR MERBAU",
    district_code: 1212310,
    regency_code: 1212,
    province_code: 12
  },
  {
    district_name: "BOHOROK",
    district_code: 1213010,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "SIRAPIT",
    district_code: 1213011,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "SALAPIAN",
    district_code: 1213020,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "KUTAMBARU",
    district_code: 1213021,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "SEI BINGAI",
    district_code: 1213030,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "KUALA",
    district_code: 1213040,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "SELESAI",
    district_code: 1213050,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "BINJAI",
    district_code: 1213060,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "STABAT",
    district_code: 1213070,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "WAMPU",
    district_code: 1213080,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "BATANG SERANGAN",
    district_code: 1213090,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "SAWIT SEBERANG",
    district_code: 1213100,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "PADANG TUALANG",
    district_code: 1213110,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "HINAI",
    district_code: 1213120,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "SECANGGANG",
    district_code: 1213130,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "TANJUNG PURA",
    district_code: 1213140,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "GEBANG",
    district_code: 1213150,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "BABALAN",
    district_code: 1213160,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "SEI LEPAN",
    district_code: 1213170,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "BRANDAN BARAT",
    district_code: 1213180,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "BESITANG",
    district_code: 1213190,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "PANGKALAN SUSU",
    district_code: 1213200,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "PEMATANG JAYA",
    district_code: 1213201,
    regency_code: 1213,
    province_code: 12
  },
  {
    district_name: "HIBALA",
    district_code: 1214010,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "TANAH MASA",
    district_code: 1214011,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "PULAU-PULAU BATU",
    district_code: 1214020,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "PULAU-PULAU BATU TIMUR",
    district_code: 1214021,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "SIMUK",
    district_code: 1214022,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "PULAU-PULAU BATU BARAT",
    district_code: 1214023,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "PULAU-PULAU BATU UTARA",
    district_code: 1214024,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "TELUK DALAM",
    district_code: 1214030,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "FANAYAMA",
    district_code: 1214031,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "TOMA",
    district_code: 1214032,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "MANIAMOLO",
    district_code: 1214033,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "MAZINO",
    district_code: 1214034,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "LUAHAGUNDRE MANIAMOLO",
    district_code: 1214035,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "ONOLALU",
    district_code: 1214036,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "AMANDRAYA",
    district_code: 1214040,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "ARAMO",
    district_code: 1214041,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "ULUSUSUA",
    district_code: 1214042,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "LAHUSA",
    district_code: 1214050,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "SIDUAORI",
    district_code: 1214051,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "SOMAMBAWA",
    district_code: 1214052,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "GOMO",
    district_code: 1214060,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "SUSUA",
    district_code: 1214061,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "MAZO",
    district_code: 1214062,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "UMBUNASI",
    district_code: 1214063,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "IDANOTAE",
    district_code: 1214064,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "ULUIDANOTAE",
    district_code: 1214065,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "BORONADU",
    district_code: 1214066,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "LOLOMATUA",
    district_code: 1214070,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "ULUNOYO",
    district_code: 1214071,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "HURUNA",
    district_code: 1214072,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "LOLOWA'U",
    district_code: 1214080,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "HILIMEGAI",
    district_code: 1214081,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "OOU",
    district_code: 1214082,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "ONOHAZUMBA",
    district_code: 1214083,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "HILISALAWAAHE",
    district_code: 1214084,
    regency_code: 1214,
    province_code: 12
  },
  {
    district_name: "PAKKAT",
    district_code: 1215010,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "ONAN GANJANG",
    district_code: 1215020,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "SIJAMA POLANG",
    district_code: 1215030,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "DOLOK SANGGUL",
    district_code: 1215040,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "LINTONG NIHUTA",
    district_code: 1215050,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "PARANGINAN",
    district_code: 1215060,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "BAKTI RAJA",
    district_code: 1215070,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "POLLUNG",
    district_code: 1215080,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "PARLILITAN",
    district_code: 1215090,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "TARA BINTANG",
    district_code: 1215100,
    regency_code: 1215,
    province_code: 12
  },
  {
    district_name: "SALAK",
    district_code: 1216010,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "SITELLU TALI URANG JEHE",
    district_code: 1216011,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "PAGINDAR",
    district_code: 1216012,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "SITELLU TALI URANG JULU",
    district_code: 1216013,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "PERGETTENG-GETTENG SENGKUT",
    district_code: 1216014,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "KERAJAAN",
    district_code: 1216020,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "TINADA",
    district_code: 1216021,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "SIEMPAT RUBE",
    district_code: 1216022,
    regency_code: 1216,
    province_code: 12
  },
  {
    district_name: "SIANJUR MULA MULA",
    district_code: 1217010,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "HARIAN",
    district_code: 1217020,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "SITIO-TIO",
    district_code: 1217030,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "ONAN RUNGGU",
    district_code: 1217040,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "NAINGGOLAN",
    district_code: 1217050,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "PALIPI",
    district_code: 1217060,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "RONGGUR NIHUTA",
    district_code: 1217070,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "PANGURURAN",
    district_code: 1217080,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "SIMANINDO",
    district_code: 1217090,
    regency_code: 1217,
    province_code: 12
  },
  {
    district_name: "KOTARIH",
    district_code: 1218010,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "SILINDA",
    district_code: 1218011,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "BINTANG BAYU",
    district_code: 1218012,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "DOLOK MASIHUL",
    district_code: 1218020,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "SERBAJADI",
    district_code: 1218021,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "SIPISPIS",
    district_code: 1218030,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "DOLOK MERAWAN",
    district_code: 1218040,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "TEBINGTINGGI",
    district_code: 1218050,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "TEBING SYAHBANDAR",
    district_code: 1218051,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "BANDAR KHALIPAH",
    district_code: 1218060,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "TANJUNG BERINGIN",
    district_code: 1218070,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "SEI RAMPAH",
    district_code: 1218080,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "SEI BAMBAN",
    district_code: 1218081,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "TELUK MENGKUDU",
    district_code: 1218090,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "PERBAUNGAN",
    district_code: 1218100,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "PEGAJAHAN",
    district_code: 1218101,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "PANTAI CERMIN",
    district_code: 1218110,
    regency_code: 1218,
    province_code: 12
  },
  {
    district_name: "SEI BALAI",
    district_code: 1219010,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "TANJUNG TIRAM",
    district_code: 1219020,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "NIBUNG HANGUS",
    district_code: 1219021,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "TALAWI",
    district_code: 1219030,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "DATUK TANAH DATAR",
    district_code: 1219031,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "LIMAPULUH",
    district_code: 1219040,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "LIMA PULUH PESISIR",
    district_code: 1219041,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "DATUK LIMA PULUH",
    district_code: 1219042,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "AIR PUTIH",
    district_code: 1219050,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "SEI SUKA",
    district_code: 1219060,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "LAUT TADOR",
    district_code: 1219061,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "MEDANG DERAS",
    district_code: 1219070,
    regency_code: 1219,
    province_code: 12
  },
  {
    district_name: "BATANG ONANG",
    district_code: 1220010,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "PADANG BOLAK JULU",
    district_code: 1220020,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "PORTIBI",
    district_code: 1220030,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "PADANG BOLAK",
    district_code: 1220040,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "PADANG BOLAK TENGGARA",
    district_code: 1220041,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "SIMANGAMBAT",
    district_code: 1220050,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "UJUNG BATU",
    district_code: 1220051,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "HALONGONAN",
    district_code: 1220060,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "HALONGONAN TIMUR",
    district_code: 1220061,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "DOLOK",
    district_code: 1220070,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "DOLOK SIGOMPULON",
    district_code: 1220080,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "HULU SIHAPAS",
    district_code: 1220090,
    regency_code: 1220,
    province_code: 12
  },
  {
    district_name: "SOSOPAN",
    district_code: 1221010,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "ULU BARUMUN",
    district_code: 1221020,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "BARUMUN",
    district_code: 1221030,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "BARUMUN SELATAN",
    district_code: 1221031,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "LUBUK BARUMUN",
    district_code: 1221040,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "SOSA",
    district_code: 1221050,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "BATANG LUBU SUTAM",
    district_code: 1221060,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "HUTA RAJA TINGGI",
    district_code: 1221070,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "HURISTAK",
    district_code: 1221080,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "BARUMUN TENGAH",
    district_code: 1221090,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "AEK NABARA BARUMUN",
    district_code: 1221091,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "SIHAPAS BARUMUN",
    district_code: 1221092,
    regency_code: 1221,
    province_code: 12
  },
  {
    district_name: "SUNGAI KANAN",
    district_code: 1222010,
    regency_code: 1222,
    province_code: 12
  },
  {
    district_name: "TORGAMBA",
    district_code: 1222020,
    regency_code: 1222,
    province_code: 12
  },
  {
    district_name: "KOTA PINANG",
    district_code: 1222030,
    regency_code: 1222,
    province_code: 12
  },
  {
    district_name: "SILANGKITANG",
    district_code: 1222040,
    regency_code: 1222,
    province_code: 12
  },
  {
    district_name: "KAMPUNG RAKYAT",
    district_code: 1222050,
    regency_code: 1222,
    province_code: 12
  },
  {
    district_name: "NA IX-X",
    district_code: 1223010,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "MARBAU",
    district_code: 1223020,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "AEK KUO",
    district_code: 1223030,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "AEK NATAS",
    district_code: 1223040,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "KUALUH SELATAN",
    district_code: 1223050,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "KUALUH HILIR",
    district_code: 1223060,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "KUALUH HULU",
    district_code: 1223070,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "KUALUH LEIDONG",
    district_code: 1223080,
    regency_code: 1223,
    province_code: 12
  },
  {
    district_name: "TUGALA OYO",
    district_code: 1224010,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "ALASA",
    district_code: 1224020,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "ALASA TALU MUZOI",
    district_code: 1224030,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "NAMOHALU ESIWA",
    district_code: 1224040,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "SITOLU ORI",
    district_code: 1224050,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "TUHEMBERUA",
    district_code: 1224060,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "SAWO",
    district_code: 1224070,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "LOTU",
    district_code: 1224080,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "LAHEWA TIMUR",
    district_code: 1224090,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "AFULU",
    district_code: 1224100,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "LAHEWA",
    district_code: 1224110,
    regency_code: 1224,
    province_code: 12
  },
  {
    district_name: "SIROMBU",
    district_code: 1225010,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "LAHOMI",
    district_code: 1225020,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "ULU MORO O",
    district_code: 1225030,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "LOLOFITU MOI",
    district_code: 1225040,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "MANDREHE UTARA",
    district_code: 1225050,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "MANDREHE",
    district_code: 1225060,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "MANDREHE BARAT",
    district_code: 1225070,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "MORO O",
    district_code: 1225080,
    regency_code: 1225,
    province_code: 12
  },
  {
    district_name: "SIBOLGA UTARA",
    district_code: 1271010,
    regency_code: 1271,
    province_code: 12
  },
  {
    district_name: "SIBOLGA KOTA",
    district_code: 1271020,
    regency_code: 1271,
    province_code: 12
  },
  {
    district_name: "SIBOLGA SELATAN",
    district_code: 1271030,
    regency_code: 1271,
    province_code: 12
  },
  {
    district_name: "SIBOLGA SAMBAS",
    district_code: 1271031,
    regency_code: 1271,
    province_code: 12
  },
  {
    district_name: "DATUK BANDAR",
    district_code: 1272010,
    regency_code: 1272,
    province_code: 12
  },
  {
    district_name: "DATUK BANDAR TIMUR",
    district_code: 1272011,
    regency_code: 1272,
    province_code: 12
  },
  {
    district_name: "TANJUNG BALAI SELATAN",
    district_code: 1272020,
    regency_code: 1272,
    province_code: 12
  },
  {
    district_name: "TANJUNG BALAI UTARA",
    district_code: 1272030,
    regency_code: 1272,
    province_code: 12
  },
  {
    district_name: "SEI TUALANG RASO",
    district_code: 1272040,
    regency_code: 1272,
    province_code: 12
  },
  {
    district_name: "TELUK NIBUNG",
    district_code: 1272050,
    regency_code: 1272,
    province_code: 12
  },
  {
    district_name: "SIANTAR MARIHAT",
    district_code: 1273010,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "SIANTAR MARIMBUN",
    district_code: 1273011,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "SIANTAR SELATAN",
    district_code: 1273020,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "SIANTAR BARAT",
    district_code: 1273030,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "SIANTAR UTARA",
    district_code: 1273040,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "SIANTAR TIMUR",
    district_code: 1273050,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "SIANTAR MARTOBA",
    district_code: 1273060,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "SIANTAR SITALASARI",
    district_code: 1273061,
    regency_code: 1273,
    province_code: 12
  },
  {
    district_name: "PADANG HULU",
    district_code: 1274010,
    regency_code: 1274,
    province_code: 12
  },
  {
    district_name: "TEBING TINGGI KOTA",
    district_code: 1274011,
    regency_code: 1274,
    province_code: 12
  },
  {
    district_name: "RAMBUTAN",
    district_code: 1274020,
    regency_code: 1274,
    province_code: 12
  },
  {
    district_name: "BAJENIS",
    district_code: 1274021,
    regency_code: 1274,
    province_code: 12
  },
  {
    district_name: "PADANG HILIR",
    district_code: 1274030,
    regency_code: 1274,
    province_code: 12
  },
  {
    district_name: "MEDAN TUNTUNGAN",
    district_code: 1275010,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN JOHOR",
    district_code: 1275020,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN AMPLAS",
    district_code: 1275030,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN DENAI",
    district_code: 1275040,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN AREA",
    district_code: 1275050,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN KOTA",
    district_code: 1275060,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN MAIMUN",
    district_code: 1275070,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN POLONIA",
    district_code: 1275080,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN BARU",
    district_code: 1275090,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN SELAYANG",
    district_code: 1275100,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN SUNGGAL",
    district_code: 1275110,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN HELVETIA",
    district_code: 1275120,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN PETISAH",
    district_code: 1275130,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN BARAT",
    district_code: 1275140,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN TIMUR",
    district_code: 1275150,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN PERJUANGAN",
    district_code: 1275160,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN TEMBUNG",
    district_code: 1275170,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN DELI",
    district_code: 1275180,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN LABUHAN",
    district_code: 1275190,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN MARELAN",
    district_code: 1275200,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "MEDAN BELAWAN",
    district_code: 1275210,
    regency_code: 1275,
    province_code: 12
  },
  {
    district_name: "BINJAI SELATAN",
    district_code: 1276010,
    regency_code: 1276,
    province_code: 12
  },
  {
    district_name: "BINJAI KOTA",
    district_code: 1276020,
    regency_code: 1276,
    province_code: 12
  },
  {
    district_name: "BINJAI TIMUR",
    district_code: 1276030,
    regency_code: 1276,
    province_code: 12
  },
  {
    district_name: "BINJAI UTARA",
    district_code: 1276040,
    regency_code: 1276,
    province_code: 12
  },
  {
    district_name: "BINJAI BARAT",
    district_code: 1276050,
    regency_code: 1276,
    province_code: 12
  },
  {
    district_name: "PADANGSIDIMPUAN TENGGARA",
    district_code: 1277010,
    regency_code: 1277,
    province_code: 12
  },
  {
    district_name: "PADANGSIDIMPUAN SELATAN",
    district_code: 1277020,
    regency_code: 1277,
    province_code: 12
  },
  {
    district_name: "PADANGSIDIMPUAN BATUNADUA",
    district_code: 1277030,
    regency_code: 1277,
    province_code: 12
  },
  {
    district_name: "PADANGSIDIMPUAN UTARA",
    district_code: 1277040,
    regency_code: 1277,
    province_code: 12
  },
  {
    district_name: "PADANGSIDIMPUAN HUTAIMBARU",
    district_code: 1277050,
    regency_code: 1277,
    province_code: 12
  },
  {
    district_name: "PADANGSIDIMPUAN ANGKOLA JULU",
    district_code: 1277051,
    regency_code: 1277,
    province_code: 12
  },
  {
    district_name: "GUNUNGSITOLI IDANOI",
    district_code: 1278010,
    regency_code: 1278,
    province_code: 12
  },
  {
    district_name: "GUNUNGSITOLI SELATAN",
    district_code: 1278020,
    regency_code: 1278,
    province_code: 12
  },
  {
    district_name: "GUNUNGSITOLI BARAT",
    district_code: 1278030,
    regency_code: 1278,
    province_code: 12
  },
  {
    district_name: "GUNUNG SITOLI",
    district_code: 1278040,
    regency_code: 1278,
    province_code: 12
  },
  {
    district_name: "GUNUNGSITOLI ALO OA",
    district_code: 1278050,
    regency_code: 1278,
    province_code: 12
  },
  {
    district_name: "GUNUNGSITOLI UTARA",
    district_code: 1278060,
    regency_code: 1278,
    province_code: 12
  },
  {
    district_name: "PAGAI SELATAN",
    district_code: 1301011,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SIKAKAP",
    district_code: 1301012,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "PAGAI UTARA",
    district_code: 1301013,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SIPORA SELATAN",
    district_code: 1301021,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SIPORA UTARA",
    district_code: 1301022,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SIBERUT SELATAN",
    district_code: 1301030,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SEBERUT BARAT DAYA",
    district_code: 1301031,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SIBERUT TENGAH",
    district_code: 1301032,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SIBERUT UTARA",
    district_code: 1301040,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SIBERUT BARAT",
    district_code: 1301041,
    regency_code: 1301,
    province_code: 13
  },
  {
    district_name: "SILAUT",
    district_code: 1302011,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "LUNANG",
    district_code: 1302012,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "BASA AMPEK BALAI TAPAN",
    district_code: 1302020,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "RANAH AMPEK HULU TAPAN",
    district_code: 1302021,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "PANCUNG SOAL",
    district_code: 1302030,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "AIRPURA",
    district_code: 1302031,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "LINGGO SARI BAGANTI",
    district_code: 1302040,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "RANAH PESISIR",
    district_code: 1302050,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "LENGAYANG",
    district_code: 1302060,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "SUTERA",
    district_code: 1302070,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "BATANG KAPAS",
    district_code: 1302080,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "IV JURAI",
    district_code: 1302090,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "BAYANG",
    district_code: 1302100,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "IV  NAGARI BAYANG UTARA",
    district_code: 1302101,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "KOTO XI TARUSAN",
    district_code: 1302110,
    regency_code: 1302,
    province_code: 13
  },
  {
    district_name: "PANTAI CERMIN",
    district_code: 1303040,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "LEMBAH GUMANTI",
    district_code: 1303050,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "HILIRAN GUMANTI",
    district_code: 1303051,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "PAYUNG SEKAKI",
    district_code: 1303060,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "TIGO LURAH",
    district_code: 1303061,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "LEMBANG JAYA",
    district_code: 1303070,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "DANAU KEMBAR",
    district_code: 1303071,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "GUNUNG TALANG",
    district_code: 1303080,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "BUKIT SUNDI",
    district_code: 1303090,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "IX KOTO SUNGAI LASI",
    district_code: 1303100,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "KUBUNG",
    district_code: 1303110,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "X KOTO DIATAS",
    district_code: 1303120,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "X KOTO SINGKARAK",
    district_code: 1303130,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "JUNJUNG SIRIH",
    district_code: 1303140,
    regency_code: 1303,
    province_code: 13
  },
  {
    district_name: "KAMANG BARU",
    district_code: 1304050,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "TANJUNG GADANG",
    district_code: 1304060,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "SIJUNJUNG",
    district_code: 1304070,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "LUBUK TAROK",
    district_code: 1304071,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "IV NAGARI",
    district_code: 1304080,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "KUPITAN",
    district_code: 1304090,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "KOTO TUJUH",
    district_code: 1304100,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "SUMPUR KUDUS",
    district_code: 1304110,
    regency_code: 1304,
    province_code: 13
  },
  {
    district_name: "SEPULUH KOTO",
    district_code: 1305010,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "BATIPUH",
    district_code: 1305020,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "BATIPUH SELATAN",
    district_code: 1305021,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "PARIANGAN",
    district_code: 1305030,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "RAMBATAN",
    district_code: 1305040,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "LIMA KAUM",
    district_code: 1305050,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "TANJUNG EMAS",
    district_code: 1305060,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "PADANG GANTING",
    district_code: 1305070,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "LINTAU BUO",
    district_code: 1305080,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "LINTAU BUO UTARA",
    district_code: 1305081,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "SUNGAYANG",
    district_code: 1305090,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "SUNGAI TARAB",
    district_code: 1305100,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "SALIMPAUNG",
    district_code: 1305110,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "TANJUNG BARU",
    district_code: 1305111,
    regency_code: 1305,
    province_code: 13
  },
  {
    district_name: "BATANG ANAI",
    district_code: 1306010,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "LUBUK ALUNG",
    district_code: 1306020,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "SINTUK TOBOH GADANG",
    district_code: 1306021,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "ULAKAN TAPAKIS",
    district_code: 1306030,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "NAN SABARIS",
    district_code: 1306040,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "2 X 11 ENAM LINGKUNG",
    district_code: 1306050,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "ENAM LINGKUNG",
    district_code: 1306051,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "2 X 11 KAYU TANAM",
    district_code: 1306052,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "VII KOTO SUNGAI SARIAK",
    district_code: 1306060,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "PATAMUAN",
    district_code: 1306061,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "PADANG SAGO",
    district_code: 1306062,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "V KOTO KP DALAM",
    district_code: 1306070,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "V KOTO TIMUR",
    district_code: 1306071,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "SUNGAI LIMAU",
    district_code: 1306080,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "BATANG GASAN",
    district_code: 1306081,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "SUNGAI GERINGGING",
    district_code: 1306090,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "IV KOTO AUR MALINTANG",
    district_code: 1306100,
    regency_code: 1306,
    province_code: 13
  },
  {
    district_name: "TANJUNG MUTIARA",
    district_code: 1307010,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "LUBUK BASUNG",
    district_code: 1307020,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "AMPEK NAGARI",
    district_code: 1307021,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "TANJUNG RAYA",
    district_code: 1307030,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "MATUR",
    district_code: 1307040,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "IV KOTO",
    district_code: 1307050,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "MALALAK",
    district_code: 1307051,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "BANUHAMPU",
    district_code: 1307061,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "SUNGAI PUA",
    district_code: 1307062,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "AMPEK ANGKEK",
    district_code: 1307070,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "CANDUANG",
    district_code: 1307071,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "BASO",
    district_code: 1307080,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "TILATANG KAMANG",
    district_code: 1307090,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "KAMANG MAGEK",
    district_code: 1307091,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "PALEMBAYAN",
    district_code: 1307100,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "PALUPUH",
    district_code: 1307110,
    regency_code: 1307,
    province_code: 13
  },
  {
    district_name: "PAYAKUMBUH",
    district_code: 1308010,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "AKABILURU",
    district_code: 1308011,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "LUAK",
    district_code: 1308020,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "LAREH SAGO HALABAN",
    district_code: 1308021,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "SITUJUAH LIMO NAGARI",
    district_code: 1308022,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "HARAU",
    district_code: 1308030,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "GUGUAK",
    district_code: 1308040,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "MUNGKA",
    district_code: 1308041,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "SULIKI",
    district_code: 1308050,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "BUKIK BARISAN",
    district_code: 1308051,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "GUNUANG OMEH",
    district_code: 1308060,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "KAPUR IX",
    district_code: 1308070,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "PANGKALAN KOTO BARU",
    district_code: 1308080,
    regency_code: 1308,
    province_code: 13
  },
  {
    district_name: "BONJOL",
    district_code: 1309070,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "TIGO NAGARI",
    district_code: 1309071,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "SIMPANG ALAHAN MATI",
    district_code: 1309072,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "LUBUK SIKAPING",
    district_code: 1309080,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "DUA KOTO",
    district_code: 1309100,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "PANTI",
    district_code: 1309110,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "PADANG GELUGUR",
    district_code: 1309111,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "RAO",
    district_code: 1309121,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "MAPAT TUNGGUL",
    district_code: 1309122,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "MAPAT TUNGGUL SELATAN",
    district_code: 1309123,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "RAO SELATAN",
    district_code: 1309124,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "RAO UTARA",
    district_code: 1309125,
    regency_code: 1309,
    province_code: 13
  },
  {
    district_name: "SANGIR",
    district_code: 1310010,
    regency_code: 1310,
    province_code: 13
  },
  {
    district_name: "SANGIR JUJUAN",
    district_code: 1310020,
    regency_code: 1310,
    province_code: 13
  },
  {
    district_name: "SANGIR BALAI JANGGO",
    district_code: 1310021,
    regency_code: 1310,
    province_code: 13
  },
  {
    district_name: "SANGIR BATANG HARI",
    district_code: 1310030,
    regency_code: 1310,
    province_code: 13
  },
  {
    district_name: "SUNGAI PAGU",
    district_code: 1310040,
    regency_code: 1310,
    province_code: 13
  },
  {
    district_name: "PAUAH DUO",
    district_code: 1310041,
    regency_code: 1310,
    province_code: 13
  },
  {
    district_name: "KOTO PARIK GADANG DIATEH",
    district_code: 1310050,
    regency_code: 1310,
    province_code: 13
  },
  {
    district_name: "SUNGAI RUMBAI",
    district_code: 1311010,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "KOTO BESAR",
    district_code: 1311011,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "ASAM JUJUHAN",
    district_code: 1311012,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "KOTO BARU",
    district_code: 1311020,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "KOTO SALAK",
    district_code: 1311021,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "TIUMANG",
    district_code: 1311022,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "PADANG LAWEH",
    district_code: 1311023,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "SITIUNG",
    district_code: 1311030,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "TIMPEH",
    district_code: 1311031,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "PULAU PUNJUNG",
    district_code: 1311040,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "IX KOTO",
    district_code: 1311041,
    regency_code: 1311,
    province_code: 13
  },
  {
    district_name: "SUNGAI BEREMAS",
    district_code: 1312010,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "RANAH BATAHAN",
    district_code: 1312020,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "KOTO BALINGKA",
    district_code: 1312030,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "SUNGAI AUR",
    district_code: 1312040,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "LEMBAH MALINTANG",
    district_code: 1312050,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "GUNUNG TULEH",
    district_code: 1312060,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "TALAMAU",
    district_code: 1312070,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "PASAMAN",
    district_code: 1312080,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "LUHAK NAN DUO",
    district_code: 1312090,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "SASAK RANAH PASISIE",
    district_code: 1312100,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "KINALI",
    district_code: 1312110,
    regency_code: 1312,
    province_code: 13
  },
  {
    district_name: "BUNGUS TELUK KABUNG",
    district_code: 1371010,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "LUBUK KILANGAN",
    district_code: 1371020,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "LUBUK BEGALUNG",
    district_code: 1371030,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "PADANG SELATAN",
    district_code: 1371040,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "PADANG TIMUR",
    district_code: 1371050,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "PADANG BARAT",
    district_code: 1371060,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "PADANG UTARA",
    district_code: 1371070,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "NANGGALO",
    district_code: 1371080,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "KURANJI",
    district_code: 1371090,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "PAUH",
    district_code: 1371100,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "KOTO TANGAH",
    district_code: 1371110,
    regency_code: 1371,
    province_code: 13
  },
  {
    district_name: "LUBUK SIKARAH",
    district_code: 1372010,
    regency_code: 1372,
    province_code: 13
  },
  {
    district_name: "TANJUNG HARAPAN",
    district_code: 1372020,
    regency_code: 1372,
    province_code: 13
  },
  {
    district_name: "SILUNGKANG",
    district_code: 1373010,
    regency_code: 1373,
    province_code: 13
  },
  {
    district_name: "LEMBAH SEGAR",
    district_code: 1373020,
    regency_code: 1373,
    province_code: 13
  },
  {
    district_name: "BARANGIN",
    district_code: 1373030,
    regency_code: 1373,
    province_code: 13
  },
  {
    district_name: "TALAWI",
    district_code: 1373040,
    regency_code: 1373,
    province_code: 13
  },
  {
    district_name: "PADANG PANJANG BARAT",
    district_code: 1374010,
    regency_code: 1374,
    province_code: 13
  },
  {
    district_name: "PADANG PANJANG TIMUR",
    district_code: 1374020,
    regency_code: 1374,
    province_code: 13
  },
  {
    district_name: "GUGUK PANJANG",
    district_code: 1375010,
    regency_code: 1375,
    province_code: 13
  },
  {
    district_name: "MANDIANGIN KOTO SELAYAN",
    district_code: 1375020,
    regency_code: 1375,
    province_code: 13
  },
  {
    district_name: "AUR BIRUGO TIGO BALEH",
    district_code: 1375030,
    regency_code: 1375,
    province_code: 13
  },
  {
    district_name: "PAYAKUMBUH BARAT",
    district_code: 1376010,
    regency_code: 1376,
    province_code: 13
  },
  {
    district_name: "PAYAKUMBUH SELATAN",
    district_code: 1376011,
    regency_code: 1376,
    province_code: 13
  },
  {
    district_name: "PAYAKUMBUH TIMUR",
    district_code: 1376020,
    regency_code: 1376,
    province_code: 13
  },
  {
    district_name: "PAYAKUMBUH UTARA",
    district_code: 1376030,
    regency_code: 1376,
    province_code: 13
  },
  {
    district_name: "LAMPOSI TIGO NAGORI",
    district_code: 1376031,
    regency_code: 1376,
    province_code: 13
  },
  {
    district_name: "PARIAMAN SELATAN",
    district_code: 1377010,
    regency_code: 1377,
    province_code: 13
  },
  {
    district_name: "PARIAMAN TENGAH",
    district_code: 1377020,
    regency_code: 1377,
    province_code: 13
  },
  {
    district_name: "PARIAMAN TIMUR",
    district_code: 1377021,
    regency_code: 1377,
    province_code: 13
  },
  {
    district_name: "PARIAMAN UTARA",
    district_code: 1377030,
    regency_code: 1377,
    province_code: 13
  },
  {
    district_name: "KUANTAN MUDIK",
    district_code: 1401010,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "HULU KUANTAN",
    district_code: 1401011,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "GUNUNG TOAR",
    district_code: 1401012,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "PUCUK RANTAU",
    district_code: 1401013,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "SINGINGI",
    district_code: 1401020,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "SINGINGI HILIR",
    district_code: 1401021,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "KUANTAN TENGAH",
    district_code: 1401030,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "SENTAJO RAYA",
    district_code: 1401031,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "BENAI",
    district_code: 1401040,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "KUANTAN HILIR",
    district_code: 1401050,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "PANGEAN",
    district_code: 1401051,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "LOGAS TANAH DARAT",
    district_code: 1401052,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "KUANTAN HILIR SEBERANG",
    district_code: 1401053,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "CERENTI",
    district_code: 1401060,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "INUMAN",
    district_code: 1401061,
    regency_code: 1401,
    province_code: 14
  },
  {
    district_name: "PERANAP",
    district_code: 1402010,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "BATANG PERANAP",
    district_code: 1402011,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "SEBERIDA",
    district_code: 1402020,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "BATANG CENAKU",
    district_code: 1402021,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "BATANG GANSAL",
    district_code: 1402022,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "KELAYANG",
    district_code: 1402030,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "RAKIT KULIM",
    district_code: 1402031,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "PASIR PENYU",
    district_code: 1402040,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "LIRIK",
    district_code: 1402041,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "SUNGAI LALA",
    district_code: 1402042,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "LUBUK BATU JAYA",
    district_code: 1402043,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "RENGAT BARAT",
    district_code: 1402050,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "RENGAT",
    district_code: 1402060,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "KUALA CENAKU",
    district_code: 1402061,
    regency_code: 1402,
    province_code: 14
  },
  {
    district_name: "KERITANG",
    district_code: 1403010,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "KEMUNING",
    district_code: 1403011,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "RETEH",
    district_code: 1403020,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "SUNGAI BATANG",
    district_code: 1403021,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "ENOK",
    district_code: 1403030,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "TANAH MERAH",
    district_code: 1403040,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "KUALA INDRAGIRI",
    district_code: 1403050,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "CONCONG",
    district_code: 1403051,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "TEMBILAHAN",
    district_code: 1403060,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "TEMBILAHAN HULU",
    district_code: 1403061,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "TEMPULING",
    district_code: 1403070,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "KEMPAS",
    district_code: 1403071,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "BATANG TUAKA",
    district_code: 1403080,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "GAUNG ANAK SERKA",
    district_code: 1403090,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "GAUNG",
    district_code: 1403100,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "MANDAH",
    district_code: 1403110,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "KATEMAN",
    district_code: 1403120,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "PELANGIRAN",
    district_code: 1403121,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "TELUK BELENGKONG",
    district_code: 1403122,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "PULAU BURUNG",
    district_code: 1403123,
    regency_code: 1403,
    province_code: 14
  },
  {
    district_name: "LANGGAM",
    district_code: 1404010,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "PANGKALAN KERINCI",
    district_code: 1404011,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "BANDAR SEIKIJANG",
    district_code: 1404012,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "PANGKALAN KURAS",
    district_code: 1404020,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "UKUI",
    district_code: 1404021,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "PANGKALAN LESUNG",
    district_code: 1404022,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "BUNUT",
    district_code: 1404030,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "PELALAWAN",
    district_code: 1404031,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "BANDAR PETALANGAN",
    district_code: 1404032,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "KUALA KAMPAR",
    district_code: 1404040,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "KERUMUTAN",
    district_code: 1404041,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "TELUK MERANTI",
    district_code: 1404042,
    regency_code: 1404,
    province_code: 14
  },
  {
    district_name: "MINAS",
    district_code: 1405010,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "SUNGAI MANDAU",
    district_code: 1405011,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "KANDIS",
    district_code: 1405012,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "SIAK",
    district_code: 1405020,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "KERINCI KANAN",
    district_code: 1405021,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "TUALANG",
    district_code: 1405022,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "DAYUN",
    district_code: 1405023,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "LUBUK DALAM",
    district_code: 1405024,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "KOTO GASIB",
    district_code: 1405025,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "MEMPURA",
    district_code: 1405026,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "SUNGAI APIT",
    district_code: 1405030,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "BUNGA RAYA",
    district_code: 1405031,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "SABAK AUH",
    district_code: 1405032,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "PUSAKO",
    district_code: 1405033,
    regency_code: 1405,
    province_code: 14
  },
  {
    district_name: "KAMPAR KIRI",
    district_code: 1406010,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KAMPAR KIRI HULU",
    district_code: 1406011,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KAMPAR KIRI HILIR",
    district_code: 1406012,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "GUNUNG SAHILAN",
    district_code: 1406013,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KAMPAR KIRI TENGAH",
    district_code: 1406014,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "XIII KOTO KAMPAR",
    district_code: 1406020,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KOTO KAMPAR HULU",
    district_code: 1406021,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KUOK",
    district_code: 1406030,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "SALO",
    district_code: 1406031,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "TAPUNG",
    district_code: 1406040,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "TAPUNG HULU",
    district_code: 1406041,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "TAPUNG HILIR",
    district_code: 1406042,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "BANGKINANG KOTA",
    district_code: 1406050,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "BANGKINANG",
    district_code: 1406051,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KAMPAR",
    district_code: 1406060,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KAMPA",
    district_code: 1406061,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "RUMBIO JAYA",
    district_code: 1406062,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "KAMPAR UTARA",
    district_code: 1406063,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "TAMBANG",
    district_code: 1406070,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "SIAK HULU",
    district_code: 1406080,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "PERHENTIAN RAJA",
    district_code: 1406081,
    regency_code: 1406,
    province_code: 14
  },
  {
    district_name: "ROKAN IV KOTO",
    district_code: 1407010,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "PENDALIAN IV KOTO",
    district_code: 1407011,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "TANDUN",
    district_code: 1407020,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "KABUN",
    district_code: 1407021,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "UJUNG BATU",
    district_code: 1407022,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "RAMBAH SAMO",
    district_code: 1407030,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "RAMBAH",
    district_code: 1407040,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "RAMBAH HILIR",
    district_code: 1407041,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "BANGUN PURBA",
    district_code: 1407042,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "TAMBUSAI",
    district_code: 1407050,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "TAMBUSAI UTARA",
    district_code: 1407051,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "KEPENUHAN",
    district_code: 1407060,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "KEPENUHAN HULU",
    district_code: 1407061,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "KUNTO DARUSSALAM",
    district_code: 1407070,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "PAGARAN TAPAH DARUSSALAM",
    district_code: 1407071,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "BONAI DARUSSALAM",
    district_code: 1407072,
    regency_code: 1407,
    province_code: 14
  },
  {
    district_name: "MANDAU",
    district_code: 1408010,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "PINGGIR",
    district_code: 1408011,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "BATHIN SOLAPAN",
    district_code: 1408012,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "TALANG MUANDAU",
    district_code: 1408013,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "BUKIT BATU",
    district_code: 1408020,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "SIAK KECIL",
    district_code: 1408021,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "BANDAR LAKSAMANA",
    district_code: 1408022,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "RUPAT",
    district_code: 1408030,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "RUPAT UTARA",
    district_code: 1408031,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "BENGKALIS",
    district_code: 1408040,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "BANTAN",
    district_code: 1408050,
    regency_code: 1408,
    province_code: 14
  },
  {
    district_name: "TANAH PUTIH",
    district_code: 1409010,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "PUJUD",
    district_code: 1409011,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "TANAH PUTIH TANJUNG MELAWAN",
    district_code: 1409012,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "RANTAU KOPAR",
    district_code: 1409013,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "TANJUNG MEDAN",
    district_code: 1409014,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "BAGAN SINEMBAH",
    district_code: 1409020,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "SIMPANG KANAN",
    district_code: 1409021,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "BAGAN SINEMBAH RAYA",
    district_code: 1409022,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "BALAI JAYA",
    district_code: 1409023,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "KUBU",
    district_code: 1409030,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "PASIR LIMAU KAPAS",
    district_code: 1409031,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "KUBU BABUSSALAM",
    district_code: 1409032,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "BANGKO",
    district_code: 1409040,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "SINABOI",
    district_code: 1409041,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "BATU HAMPAR",
    district_code: 1409042,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "PEKAITAN",
    district_code: 1409043,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "RIMBA MELINTANG",
    district_code: 1409050,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "BANGKO PUSAKO",
    district_code: 1409051,
    regency_code: 1409,
    province_code: 14
  },
  {
    district_name: "TEBING TINGGI BARAT",
    district_code: 1410010,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "TEBING TINGGI",
    district_code: 1410020,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "TEBING TINGGI TIMUR",
    district_code: 1410021,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "RANGSANG",
    district_code: 1410030,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "RANGSANG PESISIR",
    district_code: 1410031,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "RANGSANG BARAT",
    district_code: 1410040,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "MERBAU",
    district_code: 1410050,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "PULAU MERBAU",
    district_code: 1410051,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "TASIK PUTRI PUYU",
    district_code: 1410052,
    regency_code: 1410,
    province_code: 14
  },
  {
    district_name: "TAMPAN",
    district_code: 1471010,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "PAYUNG SEKAKI",
    district_code: 1471011,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "BUKIT RAYA",
    district_code: 1471020,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "MARPOYAN DAMAI",
    district_code: 1471021,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "TENAYAN RAYA",
    district_code: 1471022,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "LIMAPULUH",
    district_code: 1471030,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "SAIL",
    district_code: 1471040,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "PEKANBARU KOTA",
    district_code: 1471050,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "SUKAJADI",
    district_code: 1471060,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "SENAPELAN",
    district_code: 1471070,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "RUMBAI",
    district_code: 1471080,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "RUMBAI PESISIR",
    district_code: 1471081,
    regency_code: 1471,
    province_code: 14
  },
  {
    district_name: "BUKIT KAPUR",
    district_code: 1473010,
    regency_code: 1473,
    province_code: 14
  },
  {
    district_name: "MEDANG KAMPAI",
    district_code: 1473011,
    regency_code: 1473,
    province_code: 14
  },
  {
    district_name: "SUNGAI SEMBILAN",
    district_code: 1473012,
    regency_code: 1473,
    province_code: 14
  },
  {
    district_name: "DUMAI BARAT",
    district_code: 1473020,
    regency_code: 1473,
    province_code: 14
  },
  {
    district_name: "DUMAI SELATAN",
    district_code: 1473021,
    regency_code: 1473,
    province_code: 14
  },
  {
    district_name: "DUMAI TIMUR",
    district_code: 1473030,
    regency_code: 1473,
    province_code: 14
  },
  {
    district_name: "DUMAI KOTA",
    district_code: 1473031,
    regency_code: 1473,
    province_code: 14
  },
  {
    district_name: "GUNUNG RAYA",
    district_code: 1501010,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "BUKIT KERMAN",
    district_code: 1501011,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "BATANG MERANGIN",
    district_code: 1501020,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "KELILING DANAU",
    district_code: 1501030,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "DANAU KERINCI",
    district_code: 1501040,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "SITINJAU LAUT",
    district_code: 1501050,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "AIR HANGAT",
    district_code: 1501070,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "AIR HANGAT TIMUR",
    district_code: 1501071,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "DEPATI VII",
    district_code: 1501072,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "AIR HANGAT BARAT",
    district_code: 1501073,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "GUNUNG KERINCI",
    district_code: 1501080,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "SIULAK",
    district_code: 1501081,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "SIULAK MUKAI",
    district_code: 1501082,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "KAYU ARO",
    district_code: 1501090,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "GUNUNG TUJUH",
    district_code: 1501091,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "KAYU ARO BARAT",
    district_code: 1501092,
    regency_code: 1501,
    province_code: 15
  },
  {
    district_name: "JANGKAT",
    district_code: 1502010,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "SUNGAI TENANG",
    district_code: 1502011,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "MUARA SIAU",
    district_code: 1502020,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "LEMBAH MASURAI",
    district_code: 1502021,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TIANG PUMPUNG",
    district_code: 1502022,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "PAMENANG",
    district_code: 1502030,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "PAMENANG BARAT",
    district_code: 1502031,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "RENAH PAMENANG",
    district_code: 1502032,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "PAMENANG SELATAN",
    district_code: 1502033,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "BANGKO",
    district_code: 1502040,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "BANGKO BARAT",
    district_code: 1502041,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "NALO TANTAN",
    district_code: 1502042,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "BATANG MASUMAI",
    district_code: 1502043,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "SUNGAI MANAU",
    district_code: 1502050,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "RENAH PEMBARAP",
    district_code: 1502051,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "PANGKALAN JAMBU",
    district_code: 1502052,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TABIR",
    district_code: 1502060,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TABIR ULU",
    district_code: 1502061,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TABIR SELATAN",
    district_code: 1502062,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TABIR ILIR",
    district_code: 1502063,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TABIR TIMUR",
    district_code: 1502064,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TABIR LINTAS",
    district_code: 1502065,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "MARGO TABIR",
    district_code: 1502066,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "TABIR BARAT",
    district_code: 1502067,
    regency_code: 1502,
    province_code: 15
  },
  {
    district_name: "BATANG ASAI",
    district_code: 1503010,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "LIMUN",
    district_code: 1503020,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "CERMIN NAN GEDANG",
    district_code: 1503021,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "PELAWAN",
    district_code: 1503030,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "SINGKUT",
    district_code: 1503031,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "SAROLANGUN",
    district_code: 1503040,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "BATHIN VIII",
    district_code: 1503041,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "PAUH",
    district_code: 1503050,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "AIR HITAM",
    district_code: 1503051,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "MANDIANGIN",
    district_code: 1503060,
    regency_code: 1503,
    province_code: 15
  },
  {
    district_name: "MERSAM",
    district_code: 1504010,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "MARO SEBO ULU",
    district_code: 1504011,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "BATIN XXIV",
    district_code: 1504020,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "MUARA TEMBESI",
    district_code: 1504030,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "MUARA BULIAN",
    district_code: 1504040,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "BAJUBANG",
    district_code: 1504041,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "MARO SEBO ILIR",
    district_code: 1504042,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "PEMAYUNG",
    district_code: 1504050,
    regency_code: 1504,
    province_code: 15
  },
  {
    district_name: "MESTONG",
    district_code: 1505010,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "SUNGAI BAHAR",
    district_code: 1505011,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "BAHAR SELATAN",
    district_code: 1505012,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "BAHAR UTARA",
    district_code: 1505013,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "KUMPEH ULU",
    district_code: 1505020,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "SUNGAI GELAM",
    district_code: 1505021,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "KUMPEH",
    district_code: 1505030,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "MARO SEBO",
    district_code: 1505040,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "TAMAN RAJO",
    district_code: 1505041,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "JAMBI LUAR KOTA",
    district_code: 1505050,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "SEKERNAN",
    district_code: 1505060,
    regency_code: 1505,
    province_code: 15
  },
  {
    district_name: "MENDAHARA",
    district_code: 1506010,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "MENDAHARA ULU",
    district_code: 1506011,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "GERAGAI",
    district_code: 1506012,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "DENDANG",
    district_code: 1506020,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "MUARA SABAK BARAT",
    district_code: 1506031,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "MUARA SABAK TIMUR",
    district_code: 1506032,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "KUALA JAMBI",
    district_code: 1506033,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "RANTAU RASAU",
    district_code: 1506040,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "BERBAK",
    district_code: 1506041,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "NIPAH PANJANG",
    district_code: 1506050,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "SADU",
    district_code: 1506060,
    regency_code: 1506,
    province_code: 15
  },
  {
    district_name: "TUNGKAL ULU",
    district_code: 1507010,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "MERLUNG",
    district_code: 1507011,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "BATANG ASAM",
    district_code: 1507012,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "TEBING TINGGI",
    district_code: 1507013,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "RENAH MENDALUH",
    district_code: 1507014,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "MUARA PAPALIK",
    district_code: 1507015,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "PENGABUAN",
    district_code: 1507020,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "SENYERANG",
    district_code: 1507021,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "TUNGKAL ILIR",
    district_code: 1507030,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "BRAM ITAM",
    district_code: 1507031,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "SEBERANG KOTA",
    district_code: 1507032,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "BETARA",
    district_code: 1507040,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "KUALA BETARA",
    district_code: 1507041,
    regency_code: 1507,
    province_code: 15
  },
  {
    district_name: "TEBO ILIR",
    district_code: 1508010,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "MUARA TABIR",
    district_code: 1508011,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "TEBO TENGAH",
    district_code: 1508020,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "SUMAY",
    district_code: 1508021,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "TENGAH ILIR",
    district_code: 1508022,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "RIMBO BUJANG",
    district_code: 1508030,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "RIMBO ULU",
    district_code: 1508031,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "RIMBO ILIR",
    district_code: 1508032,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "TEBO ULU",
    district_code: 1508040,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "VII KOTO",
    district_code: 1508041,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "SERAI SERUMPUN",
    district_code: 1508042,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "VII KOTO ILIR",
    district_code: 1508043,
    regency_code: 1508,
    province_code: 15
  },
  {
    district_name: "PELEPAT",
    district_code: 1509010,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "PELEPAT ILIR",
    district_code: 1509011,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "BATHIN II BABEKO",
    district_code: 1509021,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "RIMBO TENGAH",
    district_code: 1509022,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "BUNGO DANI",
    district_code: 1509023,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "PASAR MUARA BUNGO",
    district_code: 1509024,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "BATHIN III",
    district_code: 1509025,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "RANTAU PANDAN",
    district_code: 1509030,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "MUKO-MUKO BATHIN VII",
    district_code: 1509031,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "BATHIN III ULU",
    district_code: 1509032,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "TANAH SEPENGGAL",
    district_code: 1509040,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "TANAH SEPENGGAL LINTAS",
    district_code: 1509041,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "TANAH TUMBUH",
    district_code: 1509050,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "LIMBUR LUBUK MENGKUANG",
    district_code: 1509051,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "BATHIN II PELAYANG",
    district_code: 1509052,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "JUJUHAN",
    district_code: 1509060,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "JUJUHAN ILIR",
    district_code: 1509061,
    regency_code: 1509,
    province_code: 15
  },
  {
    district_name: "KOTA BARU",
    district_code: 1571010,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "ALAM BARAJO",
    district_code: 1571011,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "JAMBI SELATAN",
    district_code: 1571020,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "PAAL MERAH",
    district_code: 1571021,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "JELUTUNG",
    district_code: 1571030,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "PASAR JAMBI",
    district_code: 1571040,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "TELANAIPURA",
    district_code: 1571050,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "DANAU SIPIN",
    district_code: 1571051,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "DANAU TELUK",
    district_code: 1571060,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "PELAYANGAN",
    district_code: 1571070,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "JAMBI TIMUR",
    district_code: 1571080,
    regency_code: 1571,
    province_code: 15
  },
  {
    district_name: "TANAH KAMPUNG",
    district_code: 1572010,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "KUMUN DEBAI",
    district_code: 1572020,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "SUNGAI PENUH",
    district_code: 1572030,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "PONDOK TINGGI",
    district_code: 1572031,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "SUNGAI BUNGKAL",
    district_code: 1572032,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "HAMPARAN RAWANG",
    district_code: 1572040,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "PESISIR BUKIT",
    district_code: 1572050,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "KOTO BARU",
    district_code: 1572051,
    regency_code: 1572,
    province_code: 15
  },
  {
    district_name: "LENGKITI",
    district_code: 1601052,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "SOSOH BUAY RAYAP",
    district_code: 1601070,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "PENGANDONAN",
    district_code: 1601080,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "SEMIDANG AJI",
    district_code: 1601081,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "ULU OGAN",
    district_code: 1601082,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "MUARA JAYA",
    district_code: 1601083,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "PENINJAUAN",
    district_code: 1601090,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "LUBUK BATANG",
    district_code: 1601091,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "SINAR PENINJAUAN",
    district_code: 1601092,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "KEDATON PENINJAUAN RAYA",
    district_code: 1601093,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "BATU RAJA TIMUR",
    district_code: 1601130,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "LUBUK RAJA",
    district_code: 1601131,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "BATU RAJA BARAT",
    district_code: 1601140,
    regency_code: 1601,
    province_code: 16
  },
  {
    district_name: "LEMPUING",
    district_code: 1602010,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "LEMPUING JAYA",
    district_code: 1602011,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "MESUJI",
    district_code: 1602020,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "SUNGAI MENANG",
    district_code: 1602021,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "MESUJI MAKMUR",
    district_code: 1602022,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "MESUJI RAYA",
    district_code: 1602023,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "TULUNG SELAPAN",
    district_code: 1602030,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "CENGAL",
    district_code: 1602031,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "PEDAMARAN",
    district_code: 1602040,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "PEDAMARAN TIMUR",
    district_code: 1602041,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "TANJUNG LUBUK",
    district_code: 1602050,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "TELUK GELAM",
    district_code: 1602051,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "KOTA KAYU AGUNG",
    district_code: 1602060,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "SIRAH PULAU PADANG",
    district_code: 1602120,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "JEJAWI",
    district_code: 1602121,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "PAMPANGAN",
    district_code: 1602130,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "PANGKALAN LAPAM",
    district_code: 1602131,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "AIR SUGIHAN",
    district_code: 1602140,
    regency_code: 1602,
    province_code: 16
  },
  {
    district_name: "SEMENDO DARAT LAUT",
    district_code: 1603010,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "SEMENDO DARAT ULU",
    district_code: 1603011,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "SEMENDO DARAT TENGAH",
    district_code: 1603012,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "TANJUNG AGUNG",
    district_code: 1603020,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "RAMBANG",
    district_code: 1603031,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "LUBAI",
    district_code: 1603032,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "LUBAI ULU",
    district_code: 1603033,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "LAWANG KIDUL",
    district_code: 1603040,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "MUARA ENIM",
    district_code: 1603050,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "UJAN MAS",
    district_code: 1603051,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "GUNUNG MEGANG",
    district_code: 1603060,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "BENAKAT",
    district_code: 1603061,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "BELIMBING",
    district_code: 1603062,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "RAMBANG DANGKU",
    district_code: 1603070,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "GELUMBANG",
    district_code: 1603090,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "LEMBAK",
    district_code: 1603091,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "SUNGAI ROTAN",
    district_code: 1603092,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "MUARA BELIDA",
    district_code: 1603093,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "KELEKAR",
    district_code: 1603094,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "BELIDA DARAT",
    district_code: 1603095,
    regency_code: 1603,
    province_code: 16
  },
  {
    district_name: "TANJUNG SAKTI PUMI",
    district_code: 1604011,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "TANJUNG SAKTI PUMU",
    district_code: 1604012,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "KOTA AGUNG",
    district_code: 1604040,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "MULAK ULU",
    district_code: 1604041,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "TANJUNG TEBAT",
    district_code: 1604042,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "MULAK SEBINGKAI",
    district_code: 1604043,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "PULAU PINANG",
    district_code: 1604050,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "PAGAR GUNUNG",
    district_code: 1604051,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "GUMAY ULU",
    district_code: 1604052,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "JARAI",
    district_code: 1604060,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "PAJAR BULAN",
    district_code: 1604061,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "MUARA PAYANG",
    district_code: 1604062,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "SUKAMERINDU",
    district_code: 1604063,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "KIKIM BARAT",
    district_code: 1604111,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "KIKIM TIMUR",
    district_code: 1604112,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "KIKIM SELATAN",
    district_code: 1604113,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "KIKIM TENGAH",
    district_code: 1604114,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "LAHAT",
    district_code: 1604120,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "GUMAY TALANG",
    district_code: 1604121,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "PSEKSU",
    district_code: 1604122,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "LAHAT SELATAN",
    district_code: 1604123,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "MERAPI BARAT",
    district_code: 1604131,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "MERAPI TIMUR",
    district_code: 1604132,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "MERAPI SELATAN",
    district_code: 1604133,
    regency_code: 1604,
    province_code: 16
  },
  {
    district_name: "SUKU TENGAH LAKITAN ULU",
    district_code: 1605030,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "SELANGIT",
    district_code: 1605031,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "SUMBER HARTA",
    district_code: 1605032,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "TUGUMULYO",
    district_code: 1605040,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "PURWODADI",
    district_code: 1605041,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "MUARA BELITI",
    district_code: 1605050,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "TIANG PUMPUNG KEPUNGUT",
    district_code: 1605051,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "JAYALOKA",
    district_code: 1605060,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "SUKA KARYA",
    district_code: 1605061,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "MUARA KELINGI",
    district_code: 1605070,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "BULANG TENGAH SUKU ULU",
    district_code: 1605071,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "TUAH NEGERI",
    district_code: 1605072,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "MUARA LAKITAN",
    district_code: 1605080,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "MEGANG SAKTI",
    district_code: 1605090,
    regency_code: 1605,
    province_code: 16
  },
  {
    district_name: "SANGA DESA",
    district_code: 1606010,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "BABAT TOMAN",
    district_code: 1606020,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "BATANGHARI LEKO",
    district_code: 1606021,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "PLAKAT TINGGI",
    district_code: 1606022,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "LAWANG WETAN",
    district_code: 1606023,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "SUNGAI KERUH",
    district_code: 1606030,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "SEKAYU",
    district_code: 1606040,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "LAIS",
    district_code: 1606041,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "SUNGAI LILIN",
    district_code: 1606090,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "KELUANG",
    district_code: 1606091,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "BABAT SUPAT",
    district_code: 1606092,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "BAYUNG LENCIR",
    district_code: 1606100,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "LALAN",
    district_code: 1606101,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "TUNGKAL JAYA",
    district_code: 1606102,
    regency_code: 1606,
    province_code: 16
  },
  {
    district_name: "RANTAU BAYUR",
    district_code: 1607010,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "BETUNG",
    district_code: 1607020,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "SUAK TAPEH",
    district_code: 1607021,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "PULAU RIMAU",
    district_code: 1607030,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "TUNGKAL ILIR",
    district_code: 1607031,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "BANYUASIN III",
    district_code: 1607040,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "SEMBAWA",
    district_code: 1607041,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "TALANG KELAPA",
    district_code: 1607050,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "TANJUNG LAGO",
    district_code: 1607051,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "BANYUASIN I",
    district_code: 1607060,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "AIR KUMBANG",
    district_code: 1607061,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "RAMBUTAN",
    district_code: 1607070,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "MUARA PADANG",
    district_code: 1607080,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "MUARA SUGIHAN",
    district_code: 1607081,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "MAKARTI JAYA",
    district_code: 1607090,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "AIR SALEH",
    district_code: 1607091,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "BANYUASIN II",
    district_code: 1607100,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "MUARA TELANG",
    district_code: 1607110,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "SUMBER MARGA TELANG",
    district_code: 1607111,
    regency_code: 1607,
    province_code: 16
  },
  {
    district_name: "MEKAKAU ILIR",
    district_code: 1608010,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "BANDING AGUNG",
    district_code: 1608020,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "WARKUK RANAU SELATAN",
    district_code: 1608021,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "BUAY PEMATANG RIBU RANAU TENGAH",
    district_code: 1608022,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "BUAY PEMACA",
    district_code: 1608030,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "SIMPANG",
    district_code: 1608040,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "BUANA PEMACA",
    district_code: 1608041,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "MUARADUA",
    district_code: 1608050,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "BUAY RAWAN",
    district_code: 1608051,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "BUAY SANDANG AJI",
    district_code: 1608060,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "TIGA DIHAJI",
    district_code: 1608061,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "BUAY RUNJUNG",
    district_code: 1608070,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "RUNJUNG AGUNG",
    district_code: 1608071,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "KISAM TINGGI",
    district_code: 1608080,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "MUARADUA KISAM",
    district_code: 1608090,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "KISAM ILIR",
    district_code: 1608091,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "PULAU BERINGIN",
    district_code: 1608100,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "SINDANG DANAU",
    district_code: 1608101,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "SUNGAI ARE",
    district_code: 1608102,
    regency_code: 1608,
    province_code: 16
  },
  {
    district_name: "MARTAPURA",
    district_code: 1609010,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BUNGA MAYANG",
    district_code: 1609011,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "JAYA PURA",
    district_code: 1609012,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BUAY PEMUKA PELIUNG",
    district_code: 1609020,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BUAY MADANG",
    district_code: 1609030,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BUAY MADANG TIMUR",
    district_code: 1609031,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BUAY PEMUKA BANGSA RAJA",
    district_code: 1609032,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "MADANG SUKU II",
    district_code: 1609040,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "MADANG SUKU III",
    district_code: 1609041,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "MADANG SUKU I",
    district_code: 1609050,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BELITANG MADANG RAYA",
    district_code: 1609051,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BELITANG",
    district_code: 1609060,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BELITANG JAYA",
    district_code: 1609061,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BELITANG III",
    district_code: 1609070,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BELITANG II",
    district_code: 1609080,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "BELITANG MULYA",
    district_code: 1609081,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "SEMENDAWAI SUKU III",
    district_code: 1609090,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "SEMENDAWAI TIMUR",
    district_code: 1609091,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "CEMPAKA",
    district_code: 1609100,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "SEMENDAWAI BARAT",
    district_code: 1609101,
    regency_code: 1609,
    province_code: 16
  },
  {
    district_name: "MUARA KUANG",
    district_code: 1610010,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "RAMBANG KUANG",
    district_code: 1610011,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "LUBUK KELIAT",
    district_code: 1610012,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "TANJUNG BATU",
    district_code: 1610020,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "PAYARAMAN",
    district_code: 1610021,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "RANTAU ALAI",
    district_code: 1610030,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "KANDIS",
    district_code: 1610031,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "TANJUNG RAJA",
    district_code: 1610040,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "RANTAU PANJANG",
    district_code: 1610041,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "SUNGAI PINANG",
    district_code: 1610042,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "PEMULUTAN",
    district_code: 1610050,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "PEMULUTAN SELATAN",
    district_code: 1610051,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "PEMULUTAN BARAT",
    district_code: 1610052,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "INDRALAYA",
    district_code: 1610060,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "INDRALAYA UTARA",
    district_code: 1610061,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "INDRALAYA SELATAN",
    district_code: 1610062,
    regency_code: 1610,
    province_code: 16
  },
  {
    district_name: "MUARA PINANG",
    district_code: 1611010,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "LINTANG KANAN",
    district_code: 1611020,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "PENDOPO",
    district_code: 1611030,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "PENDOPO BARAT",
    district_code: 1611031,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "PASEMAH AIR KERUH",
    district_code: 1611040,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "ULU MUSI",
    district_code: 1611050,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "SIKAP DALAM",
    district_code: 1611051,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "TALANG PADANG",
    district_code: 1611060,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "TEBING TINGGI",
    district_code: 1611070,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "SALING",
    district_code: 1611071,
    regency_code: 1611,
    province_code: 16
  },
  {
    district_name: "TALANG UBI",
    district_code: 1612010,
    regency_code: 1612,
    province_code: 16
  },
  {
    district_name: "TANAH ABANG",
    district_code: 1612020,
    regency_code: 1612,
    province_code: 16
  },
  {
    district_name: "ABAB",
    district_code: 1612030,
    regency_code: 1612,
    province_code: 16
  },
  {
    district_name: "PENUKAL",
    district_code: 1612040,
    regency_code: 1612,
    province_code: 16
  },
  {
    district_name: "PENUKAL UTARA",
    district_code: 1612050,
    regency_code: 1612,
    province_code: 16
  },
  {
    district_name: "ULU RAWAS",
    district_code: 1613010,
    regency_code: 1613,
    province_code: 16
  },
  {
    district_name: "KARANG JAYA",
    district_code: 1613020,
    regency_code: 1613,
    province_code: 16
  },
  {
    district_name: "RAWAS ULU",
    district_code: 1613030,
    regency_code: 1613,
    province_code: 16
  },
  {
    district_name: "RUPIT",
    district_code: 1613040,
    regency_code: 1613,
    province_code: 16
  },
  {
    district_name: "KARANG DAPO",
    district_code: 1613050,
    regency_code: 1613,
    province_code: 16
  },
  {
    district_name: "RAWAS ILIR",
    district_code: 1613060,
    regency_code: 1613,
    province_code: 16
  },
  {
    district_name: "NIBUNG",
    district_code: 1613070,
    regency_code: 1613,
    province_code: 16
  },
  {
    district_name: "ILIR BARAT II",
    district_code: 1671010,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "GANDUS",
    district_code: 1671011,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "SEBERANG ULU I",
    district_code: 1671020,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "KERTAPATI",
    district_code: 1671021,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "JAKABARING",
    district_code: 1671022,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "SEBERANG ULU II",
    district_code: 1671030,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "PLAJU",
    district_code: 1671031,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "ILIR BARAT I",
    district_code: 1671040,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "BUKIT KECIL",
    district_code: 1671041,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "ILIR TIMUR I",
    district_code: 1671050,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "KEMUNING",
    district_code: 1671051,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "ILIR TIMUR II",
    district_code: 1671060,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "KALIDONI",
    district_code: 1671061,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "ILIR TIMUR III",
    district_code: 1671062,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "SAKO",
    district_code: 1671070,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "SEMATANG BORANG",
    district_code: 1671071,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "SUKARAMI",
    district_code: 1671080,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "ALANG ALANG LEBAR",
    district_code: 1671081,
    regency_code: 1671,
    province_code: 16
  },
  {
    district_name: "RAMBANG KAPAK TENGAH",
    district_code: 1672010,
    regency_code: 1672,
    province_code: 16
  },
  {
    district_name: "PRABUMULIH TIMUR",
    district_code: 1672020,
    regency_code: 1672,
    province_code: 16
  },
  {
    district_name: "PRABUMULIH SELATAN",
    district_code: 1672021,
    regency_code: 1672,
    province_code: 16
  },
  {
    district_name: "PRABUMULIH BARAT",
    district_code: 1672030,
    regency_code: 1672,
    province_code: 16
  },
  {
    district_name: "PRABUMULIH UTARA",
    district_code: 1672031,
    regency_code: 1672,
    province_code: 16
  },
  {
    district_name: "CAMBAI",
    district_code: 1672040,
    regency_code: 1672,
    province_code: 16
  },
  {
    district_name: "DEMPO SELATAN",
    district_code: 1673010,
    regency_code: 1673,
    province_code: 16
  },
  {
    district_name: "DEMPO TENGAH",
    district_code: 1673011,
    regency_code: 1673,
    province_code: 16
  },
  {
    district_name: "DEMPO UTARA",
    district_code: 1673020,
    regency_code: 1673,
    province_code: 16
  },
  {
    district_name: "PAGAR ALAM SELATAN",
    district_code: 1673030,
    regency_code: 1673,
    province_code: 16
  },
  {
    district_name: "PAGAR ALAM UTARA",
    district_code: 1673040,
    regency_code: 1673,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU BARAT I",
    district_code: 1674011,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU BARAT II",
    district_code: 1674012,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU SELATAN I",
    district_code: 1674021,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU SELATAN II",
    district_code: 1674022,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU TIMUR I",
    district_code: 1674031,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU TIMUR II",
    district_code: 1674032,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU UTARA I",
    district_code: 1674041,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "LUBUK LINGGAU UTARA II",
    district_code: 1674042,
    regency_code: 1674,
    province_code: 16
  },
  {
    district_name: "MANNA",
    district_code: 1701040,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "KOTA MANNA",
    district_code: 1701041,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "KEDURANG",
    district_code: 1701042,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "BUNGA MAS",
    district_code: 1701043,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "PASAR MANNA",
    district_code: 1701044,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "KEDURANG ILIR",
    district_code: 1701045,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "SEGINIM",
    district_code: 1701050,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "AIR NIPIS",
    district_code: 1701051,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "PINO",
    district_code: 1701060,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "PINORAYA",
    district_code: 1701061,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "ULU MANNA",
    district_code: 1701062,
    regency_code: 1701,
    province_code: 17
  },
  {
    district_name: "KOTA PADANG",
    district_code: 1702020,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "SINDANG BELITI ILIR",
    district_code: 1702021,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "PADANG ULAK TANDING",
    district_code: 1702030,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "SINDANG KELINGI",
    district_code: 1702031,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "BINDU RIANG",
    district_code: 1702032,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "SINDANG BELITI ULU",
    district_code: 1702033,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "SINDANG DATARAN",
    district_code: 1702034,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "CURUP",
    district_code: 1702040,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "BERMANI ULU",
    district_code: 1702041,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "SELUPU REJANG",
    district_code: 1702042,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "CURUP SELATAN",
    district_code: 1702043,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "CURUP TENGAH",
    district_code: 1702044,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "BERMANI ULU RAYA",
    district_code: 1702045,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "CURUP UTARA",
    district_code: 1702046,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "CURUP TIMUR",
    district_code: 1702047,
    regency_code: 1702,
    province_code: 17
  },
  {
    district_name: "ENGGANO",
    district_code: 1703010,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "KERKAP",
    district_code: 1703050,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "AIR NAPAL",
    district_code: 1703051,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "AIR BESI",
    district_code: 1703052,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "HULU PALIK",
    district_code: 1703053,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "TANJUNG AGUNG PALIK",
    district_code: 1703054,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "ARGA MAKMUR",
    district_code: 1703060,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "ARMA JAYA",
    district_code: 1703061,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "LAIS",
    district_code: 1703070,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "BATIK NAU",
    district_code: 1703071,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "GIRI MULYA",
    district_code: 1703072,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "AIR PADANG",
    district_code: 1703073,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "PADANG JAYA",
    district_code: 1703080,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "KETAHUN",
    district_code: 1703090,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "NAPAL PUTIH",
    district_code: 1703091,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "ULOK KUPAI",
    district_code: 1703092,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "PINANG RAYA",
    district_code: 1703093,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "PUTRI HIJAU",
    district_code: 1703100,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "MARGA SAKTI SEBELAT",
    district_code: 1703101,
    regency_code: 1703,
    province_code: 17
  },
  {
    district_name: "NASAL",
    district_code: 1704010,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "MAJE",
    district_code: 1704020,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "KAUR SELATAN",
    district_code: 1704030,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "TETAP",
    district_code: 1704031,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "KAUR TENGAH",
    district_code: 1704040,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "LUAS",
    district_code: 1704041,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "MUARA SAHUNG",
    district_code: 1704042,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "KINAL",
    district_code: 1704050,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "SEMIDANG GUMAY",
    district_code: 1704051,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "TANJUNG KEMUNING",
    district_code: 1704060,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "KELAM TENGAH",
    district_code: 1704061,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "KAUR UTARA",
    district_code: 1704070,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "PADANG GUCI HILIR",
    district_code: 1704071,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "LUNGKANG KULE",
    district_code: 1704072,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "PADANG GUCI HULU",
    district_code: 1704073,
    regency_code: 1704,
    province_code: 17
  },
  {
    district_name: "SEMIDANG ALAS MARAS",
    district_code: 1705010,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "SEMIDANG ALAS",
    district_code: 1705020,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "TALO",
    district_code: 1705030,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "ILIR TALO",
    district_code: 1705031,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "TALO KECIL",
    district_code: 1705032,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "ULU TALO",
    district_code: 1705033,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "SELUMA",
    district_code: 1705040,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "SELUMA SELATAN",
    district_code: 1705041,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "SELUMA BARAT",
    district_code: 1705042,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "SELUMA TIMUR",
    district_code: 1705043,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "SELUMA UTARA",
    district_code: 1705044,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "SUKARAJA",
    district_code: 1705050,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "AIR PERIUKAN",
    district_code: 1705051,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "LUBUK SANDI",
    district_code: 1705052,
    regency_code: 1705,
    province_code: 17
  },
  {
    district_name: "IPUH",
    district_code: 1706010,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "AIR RAMI",
    district_code: 1706011,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "MALIN DEMAN",
    district_code: 1706012,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "PONDOK SUGUH",
    district_code: 1706020,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "SUNGAI RUMBAI",
    district_code: 1706021,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "TERAMANG JAYA",
    district_code: 1706022,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "TERAS TERUNJAM",
    district_code: 1706030,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "PENARIK",
    district_code: 1706031,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "SELAGAN RAYA",
    district_code: 1706032,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "KOTA MUKOMUKO",
    district_code: 1706040,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "AIR DIKIT",
    district_code: 1706041,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "XIV KOTO",
    district_code: 1706042,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "LUBUK PINANG",
    district_code: 1706050,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "AIR MANJUNTO",
    district_code: 1706051,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "V KOTO",
    district_code: 1706052,
    regency_code: 1706,
    province_code: 17
  },
  {
    district_name: "RIMBO PENGADANG",
    district_code: 1707010,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "TOPOS",
    district_code: 1707011,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "LEBONG SELATAN",
    district_code: 1707020,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "BINGIN KUNING",
    district_code: 1707021,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "LEBONG TENGAH",
    district_code: 1707030,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "LEBONG SAKTI",
    district_code: 1707031,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "LEBONG ATAS",
    district_code: 1707040,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "PELABAI",
    district_code: 1707042,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "LEBONG UTARA",
    district_code: 1707050,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "AMEN",
    district_code: 1707051,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "URAM JAYA",
    district_code: 1707052,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "PINANG BELAPIS",
    district_code: 1707053,
    regency_code: 1707,
    province_code: 17
  },
  {
    district_name: "MUARA KEMUMU",
    district_code: 1708010,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "BERMANI ILIR",
    district_code: 1708020,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "SEBERANG MUSI",
    district_code: 1708030,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "TEBAT KARAI",
    district_code: 1708040,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "KEPAHIANG",
    district_code: 1708050,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "KABA WETAN",
    district_code: 1708060,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "UJAN MAS",
    district_code: 1708070,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "MERIGI",
    district_code: 1708080,
    regency_code: 1708,
    province_code: 17
  },
  {
    district_name: "TALANG EMPAT",
    district_code: 1709010,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "KARANG TINGGI",
    district_code: 1709020,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "TABA PENANJUNG",
    district_code: 1709030,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "MERIGI KELINDANG",
    district_code: 1709031,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "PAGAR JATI",
    district_code: 1709040,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "MERIGI SAKTI",
    district_code: 1709041,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "PONDOK KELAPA",
    district_code: 1709050,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "PONDOK KUBANG",
    district_code: 1709051,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "PEMATANG TIGA",
    district_code: 1709060,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "BANG HAJI",
    district_code: 1709061,
    regency_code: 1709,
    province_code: 17
  },
  {
    district_name: "SELEBAR",
    district_code: 1771010,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "KAMPUNG MELAYU",
    district_code: 1771011,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "GADING CEMPAKA",
    district_code: 1771020,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "RATU AGUNG",
    district_code: 1771021,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "RATU SAMBAN",
    district_code: 1771022,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "SINGARAN PATI",
    district_code: 1771023,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "TELUK SEGARA",
    district_code: 1771030,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "SUNGAI SERUT",
    district_code: 1771031,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "MUARA BANGKA HULU",
    district_code: 1771040,
    regency_code: 1771,
    province_code: 17
  },
  {
    district_name: "BALIK BUKIT",
    district_code: 1801040,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "SUKAU",
    district_code: 1801041,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "LUMBOK SEMINUNG",
    district_code: 1801042,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "BELALAU",
    district_code: 1801050,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "SEKINCAU",
    district_code: 1801051,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "SUOH",
    district_code: 1801052,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "BATU BRAK",
    district_code: 1801053,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "PAGAR DEWA",
    district_code: 1801054,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "BATU KETULIS",
    district_code: 1801055,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "BANDAR NEGERI SUOH",
    district_code: 1801056,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "SUMBER JAYA",
    district_code: 1801060,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "WAY TENONG",
    district_code: 1801061,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "GEDUNG SURIAN",
    district_code: 1801062,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "KEBUN TEBU",
    district_code: 1801063,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "AIR HITAM",
    district_code: 1801064,
    regency_code: 1801,
    province_code: 18
  },
  {
    district_name: "WONOSOBO",
    district_code: 1802010,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "SEMAKA",
    district_code: 1802011,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "BANDAR NEGERI SEMUONG",
    district_code: 1802012,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "KOTA AGUNG",
    district_code: 1802020,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "PEMATANG SAWA",
    district_code: 1802021,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "KOTA AGUNG TIMUR",
    district_code: 1802022,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "KOTA AGUNG BARAT",
    district_code: 1802023,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "PULAU PANGGUNG",
    district_code: 1802030,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "ULUBELU",
    district_code: 1802031,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "AIR NANINGAN",
    district_code: 1802032,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "TALANG PADANG",
    district_code: 1802040,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "SUMBEREJO",
    district_code: 1802041,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "GISTING",
    district_code: 1802042,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "GUNUNG ALIP",
    district_code: 1802043,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "PUGUNG",
    district_code: 1802050,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "BULOK",
    district_code: 1802101,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "CUKUH BALAK",
    district_code: 1802110,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "KELUMBAYAN",
    district_code: 1802111,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "LIMAU",
    district_code: 1802112,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "KELUMBAYAN BARAT",
    district_code: 1802113,
    regency_code: 1802,
    province_code: 18
  },
  {
    district_name: "NATAR",
    district_code: 1803060,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "JATI AGUNG",
    district_code: 1803070,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "TANJUNG BINTANG",
    district_code: 1803080,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "TANJUNG SARI",
    district_code: 1803081,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "KATIBUNG",
    district_code: 1803090,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "MERBAU MATARAM",
    district_code: 1803091,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "WAY SULAN",
    district_code: 1803092,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "SIDOMULYO",
    district_code: 1803100,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "CANDIPURO",
    district_code: 1803101,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "WAY PANJI",
    district_code: 1803102,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "KALIANDA",
    district_code: 1803110,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "RAJABASA",
    district_code: 1803111,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "PALAS",
    district_code: 1803120,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "SRAGI",
    district_code: 1803121,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "PENENGAHAN",
    district_code: 1803130,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "KETAPANG",
    district_code: 1803131,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "BAKAUHENI",
    district_code: 1803132,
    regency_code: 1803,
    province_code: 18
  },
  {
    district_name: "METRO KIBANG",
    district_code: 1804010,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "BATANGHARI",
    district_code: 1804020,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "SEKAMPUNG",
    district_code: 1804030,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "MARGATIGA",
    district_code: 1804040,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "SEKAMPUNG UDIK",
    district_code: 1804050,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "JABUNG",
    district_code: 1804060,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "PASIR SAKTI",
    district_code: 1804061,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "WAWAY KARYA",
    district_code: 1804062,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "MARGA SEKAMPUNG",
    district_code: 1804063,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "LABUHAN MARINGGAI",
    district_code: 1804070,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "MATARAM BARU",
    district_code: 1804071,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "BANDAR SRIBAWONO",
    district_code: 1804072,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "MELINTING",
    district_code: 1804073,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "GUNUNG PELINDUNG",
    district_code: 1804074,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "WAY JEPARA",
    district_code: 1804080,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "BRAJA SLEBAH",
    district_code: 1804081,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "LABUHAN RATU",
    district_code: 1804082,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "SUKADANA",
    district_code: 1804090,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "BUMI AGUNG",
    district_code: 1804091,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "BATANGHARI NUBAN",
    district_code: 1804092,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "PEKALONGAN",
    district_code: 1804100,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "RAMAN UTARA",
    district_code: 1804110,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "PURBOLINGGO",
    district_code: 1804120,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "WAY BUNGUR",
    district_code: 1804121,
    regency_code: 1804,
    province_code: 18
  },
  {
    district_name: "PADANG RATU",
    district_code: 1805010,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "SELAGAI LINGGA",
    district_code: 1805011,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "PUBIAN",
    district_code: 1805012,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "ANAK TUHA",
    district_code: 1805013,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "ANAK RATU AJI",
    district_code: 1805014,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "KALIREJO",
    district_code: 1805020,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "SENDANG AGUNG",
    district_code: 1805021,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "BANGUNREJO",
    district_code: 1805030,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "GUNUNG SUGIH",
    district_code: 1805040,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "BEKRI",
    district_code: 1805041,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "BUMI RATU NUBAN",
    district_code: 1805042,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "TRIMURJO",
    district_code: 1805050,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "PUNGGUR",
    district_code: 1805060,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "KOTA GAJAH",
    district_code: 1805061,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "SEPUTIH RAMAN",
    district_code: 1805070,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "TERBANGGI BESAR",
    district_code: 1805080,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "SEPUTIH AGUNG",
    district_code: 1805081,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "WAY PENGUBUAN",
    district_code: 1805082,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "TERUSAN NUNYAI",
    district_code: 1805090,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "SEPUTIH MATARAM",
    district_code: 1805100,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "BANDAR MATARAM",
    district_code: 1805101,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "SEPUTIH BANYAK",
    district_code: 1805110,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "WAY SEPUTIH",
    district_code: 1805111,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "RUMBIA",
    district_code: 1805120,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "BUMI NABUNG",
    district_code: 1805121,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "PUTRA RUMBIA",
    district_code: 1805122,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "SEPUTIH SURABAYA",
    district_code: 1805130,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "BANDAR SURABAYA",
    district_code: 1805131,
    regency_code: 1805,
    province_code: 18
  },
  {
    district_name: "BUKIT KEMUNING",
    district_code: 1806010,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG TINGGI",
    district_code: 1806011,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "TANJUNG RAJA",
    district_code: 1806020,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG BARAT",
    district_code: 1806030,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG TENGAH",
    district_code: 1806031,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG  KUNANG",
    district_code: 1806032,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG PEKURUN",
    district_code: 1806033,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "KOTABUMI",
    district_code: 1806040,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "KOTABUMI UTARA",
    district_code: 1806041,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "KOTABUMI SELATAN",
    district_code: 1806042,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG SELATAN",
    district_code: 1806050,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG SEMULI",
    district_code: 1806051,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "BLAMBANGAN PAGAR",
    district_code: 1806052,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG TIMUR",
    district_code: 1806060,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "ABUNG SURAKARTA",
    district_code: 1806061,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "SUNGKAI SELATAN",
    district_code: 1806070,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "MUARA SUNGKAI",
    district_code: 1806071,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "BUNGA MAYANG",
    district_code: 1806072,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "SUNGKAI  BARAT",
    district_code: 1806073,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "SUNGKAI JAYA",
    district_code: 1806074,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "SUNGKAI UTARA",
    district_code: 1806080,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "HULUSUNGKAI",
    district_code: 1806081,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "SUNGKAI TENGAH",
    district_code: 1806082,
    regency_code: 1806,
    province_code: 18
  },
  {
    district_name: "BANJIT",
    district_code: 1807010,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "BARADATU",
    district_code: 1807020,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "GUNUNG LABUHAN",
    district_code: 1807021,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "KASUI",
    district_code: 1807030,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "REBANG TANGKAS",
    district_code: 1807031,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "BLAMBANGAN UMPU",
    district_code: 1807040,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "WAY TUBA",
    district_code: 1807041,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "NEGERI AGUNG",
    district_code: 1807042,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "BAHUGA",
    district_code: 1807050,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "BUAY  BAHUGA",
    district_code: 1807051,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "BUMI AGUNG",
    district_code: 1807052,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "PAKUAN RATU",
    district_code: 1807060,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "NEGARA BATIN",
    district_code: 1807061,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "NEGERI BESAR",
    district_code: 1807062,
    regency_code: 1807,
    province_code: 18
  },
  {
    district_name: "BANJAR AGUNG",
    district_code: 1808030,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "BANJAR MARGO",
    district_code: 1808031,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "BANJAR BARU",
    district_code: 1808032,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "GEDUNG AJI",
    district_code: 1808040,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "PENAWAR AJI",
    district_code: 1808041,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "MERAKSA AJI",
    district_code: 1808042,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "MENGGALA",
    district_code: 1808050,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "PENAWAR TAMA",
    district_code: 1808051,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "RAWAJITU SELATAN",
    district_code: 1808052,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "GEDUNG MENENG",
    district_code: 1808053,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "RAWAJITU TIMUR",
    district_code: 1808054,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "RAWA PITU",
    district_code: 1808055,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "GEDUNG AJI BARU",
    district_code: 1808056,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "DENTE TELADAS",
    district_code: 1808057,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "MENGGALA TIMUR",
    district_code: 1808058,
    regency_code: 1808,
    province_code: 18
  },
  {
    district_name: "PUNDUH PIDADA",
    district_code: 1809010,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "MARGA PUNDUH",
    district_code: 1809011,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "PADANG CERMIN",
    district_code: 1809020,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "TELUK PANDAN",
    district_code: 1809021,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "WAY RATAI",
    district_code: 1809022,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "KEDONDONG",
    district_code: 1809030,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "WAY KHILAU",
    district_code: 1809031,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "WAY LIMA",
    district_code: 1809040,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "GEDUNG TATAAN",
    district_code: 1809050,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "NEGERI KATON",
    district_code: 1809060,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "TEGINENENG",
    district_code: 1809070,
    regency_code: 1809,
    province_code: 18
  },
  {
    district_name: "PARDASUKA",
    district_code: 1810010,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "AMBARAWA",
    district_code: 1810020,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "PAGELARAN",
    district_code: 1810030,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "PAGELARAN UTARA",
    district_code: 1810031,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "PRINGSEWU",
    district_code: 1810040,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "GADING REJO",
    district_code: 1810050,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "SUKOHARJO",
    district_code: 1810060,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "BANYUMAS",
    district_code: 1810070,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "ADI LUWIH",
    district_code: 1810080,
    regency_code: 1810,
    province_code: 18
  },
  {
    district_name: "WAY SERDANG",
    district_code: 1811010,
    regency_code: 1811,
    province_code: 18
  },
  {
    district_name: "SIMPANG PEMATANG",
    district_code: 1811020,
    regency_code: 1811,
    province_code: 18
  },
  {
    district_name: "PANCA JAYA",
    district_code: 1811030,
    regency_code: 1811,
    province_code: 18
  },
  {
    district_name: "TANJUNG RAYA",
    district_code: 1811040,
    regency_code: 1811,
    province_code: 18
  },
  {
    district_name: "MESUJI",
    district_code: 1811050,
    regency_code: 1811,
    province_code: 18
  },
  {
    district_name: "MESUJI TIMUR",
    district_code: 1811060,
    regency_code: 1811,
    province_code: 18
  },
  {
    district_name: "RAWAJITU UTARA",
    district_code: 1811070,
    regency_code: 1811,
    province_code: 18
  },
  {
    district_name: "TULANG BAWANG UDIK",
    district_code: 1812010,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "TUMI JAJAR",
    district_code: 1812020,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "TULANG BAWANG TENGAH",
    district_code: 1812030,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "PAGAR DEWA",
    district_code: 1812040,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "LAMBU KIBANG",
    district_code: 1812050,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "GUNUNG TERANG",
    district_code: 1812060,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "BATU PUTIH",
    district_code: 1812061,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "GUNUNG AGUNG",
    district_code: 1812070,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "WAY KENANGA",
    district_code: 1812080,
    regency_code: 1812,
    province_code: 18
  },
  {
    district_name: "LEMONG",
    district_code: 1813010,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "PESISIR UTARA",
    district_code: 1813020,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "PULAU PISANG",
    district_code: 1813030,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "KARYA PENGGAWA",
    district_code: 1813040,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "WAY KRUI",
    district_code: 1813050,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "PESISIR TENGAH",
    district_code: 1813060,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "KRUI SELATAN",
    district_code: 1813070,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "PESISIR SELATAN",
    district_code: 1813080,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "NGAMBUR",
    district_code: 1813090,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "BENGKUNAT",
    district_code: 1813100,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "BENGKUNAT BELIMBING",
    district_code: 1813110,
    regency_code: 1813,
    province_code: 18
  },
  {
    district_name: "TELUK BETUNG BARAT",
    district_code: 1871010,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "TELUKBETUNG TIMUR",
    district_code: 1871011,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "TELUK BETUNG SELATAN",
    district_code: 1871020,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "BUMI WARAS",
    district_code: 1871021,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "PANJANG",
    district_code: 1871030,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "TANJUNG KARANG TIMUR",
    district_code: 1871040,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "KEDAMAIAN",
    district_code: 1871041,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "TELUK BETUNG UTARA",
    district_code: 1871050,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "TANJUNG KARANG PUSAT",
    district_code: 1871060,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "ENGGAL",
    district_code: 1871061,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "TANJUNG KARANG BARAT",
    district_code: 1871070,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "KEMILING",
    district_code: 1871071,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "LANGKAPURA",
    district_code: 1871072,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "KEDATON",
    district_code: 1871080,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "RAJABASA",
    district_code: 1871081,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "TANJUNG SENANG",
    district_code: 1871082,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "LABUHAN RATU",
    district_code: 1871083,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "SUKARAME",
    district_code: 1871090,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "SUKABUMI",
    district_code: 1871091,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "WAY HALIM",
    district_code: 1871092,
    regency_code: 1871,
    province_code: 18
  },
  {
    district_name: "METRO SELATAN",
    district_code: 1872011,
    regency_code: 1872,
    province_code: 18
  },
  {
    district_name: "METRO BARAT",
    district_code: 1872012,
    regency_code: 1872,
    province_code: 18
  },
  {
    district_name: "METRO TIMUR",
    district_code: 1872021,
    regency_code: 1872,
    province_code: 18
  },
  {
    district_name: "METRO PUSAT",
    district_code: 1872022,
    regency_code: 1872,
    province_code: 18
  },
  {
    district_name: "METRO UTARA",
    district_code: 1872023,
    regency_code: 1872,
    province_code: 18
  },
  {
    district_name: "MENDO BARAT",
    district_code: 1901070,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "MERAWANG",
    district_code: 1901080,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "PUDING BESAR",
    district_code: 1901081,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "SUNGAI LIAT",
    district_code: 1901090,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "PEMALI",
    district_code: 1901091,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "BAKAM",
    district_code: 1901092,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "BELINYU",
    district_code: 1901130,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "RIAU SILIP",
    district_code: 1901131,
    regency_code: 1901,
    province_code: 19
  },
  {
    district_name: "MEMBALONG",
    district_code: 1902010,
    regency_code: 1902,
    province_code: 19
  },
  {
    district_name: "TANJUNG PANDAN",
    district_code: 1902060,
    regency_code: 1902,
    province_code: 19
  },
  {
    district_name: "BADAU",
    district_code: 1902061,
    regency_code: 1902,
    province_code: 19
  },
  {
    district_name: "SIJUK",
    district_code: 1902062,
    regency_code: 1902,
    province_code: 19
  },
  {
    district_name: "SELAT NASIK",
    district_code: 1902063,
    regency_code: 1902,
    province_code: 19
  },
  {
    district_name: "KELAPA",
    district_code: 1903010,
    regency_code: 1903,
    province_code: 19
  },
  {
    district_name: "TEMPILANG",
    district_code: 1903020,
    regency_code: 1903,
    province_code: 19
  },
  {
    district_name: "MENTOK",
    district_code: 1903030,
    regency_code: 1903,
    province_code: 19
  },
  {
    district_name: "SIMPANG TERITIP",
    district_code: 1903040,
    regency_code: 1903,
    province_code: 19
  },
  {
    district_name: "JEBUS",
    district_code: 1903050,
    regency_code: 1903,
    province_code: 19
  },
  {
    district_name: "PARITTIGA",
    district_code: 1903051,
    regency_code: 1903,
    province_code: 19
  },
  {
    district_name: "KOBA",
    district_code: 1904010,
    regency_code: 1904,
    province_code: 19
  },
  {
    district_name: "LUBUK BESAR",
    district_code: 1904011,
    regency_code: 1904,
    province_code: 19
  },
  {
    district_name: "PANGKALAN BARU",
    district_code: 1904020,
    regency_code: 1904,
    province_code: 19
  },
  {
    district_name: "NAMANG",
    district_code: 1904021,
    regency_code: 1904,
    province_code: 19
  },
  {
    district_name: "SUNGAI SELAN",
    district_code: 1904030,
    regency_code: 1904,
    province_code: 19
  },
  {
    district_name: "SIMPANG KATIS",
    district_code: 1904040,
    regency_code: 1904,
    province_code: 19
  },
  {
    district_name: "PAYUNG",
    district_code: 1905010,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "PULAU BESAR",
    district_code: 1905011,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "SIMPANG RIMBA",
    district_code: 1905020,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "TOBOALI",
    district_code: 1905030,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "TUKAK SADAI",
    district_code: 1905031,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "AIR GEGAS",
    district_code: 1905040,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "LEPAR PONGOK",
    district_code: 1905050,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "KEPULAUAN PONGOK",
    district_code: 1905051,
    regency_code: 1905,
    province_code: 19
  },
  {
    district_name: "DENDANG",
    district_code: 1906010,
    regency_code: 1906,
    province_code: 19
  },
  {
    district_name: "SIMPANG PESAK",
    district_code: 1906011,
    regency_code: 1906,
    province_code: 19
  },
  {
    district_name: "GANTUNG",
    district_code: 1906020,
    regency_code: 1906,
    province_code: 19
  },
  {
    district_name: "SIMPANG RENGGIANG",
    district_code: 1906021,
    regency_code: 1906,
    province_code: 19
  },
  {
    district_name: "MANGGAR",
    district_code: 1906030,
    regency_code: 1906,
    province_code: 19
  },
  {
    district_name: "DAMAR",
    district_code: 1906031,
    regency_code: 1906,
    province_code: 19
  },
  {
    district_name: "KELAPA KAMPIT",
    district_code: 1906040,
    regency_code: 1906,
    province_code: 19
  },
  {
    district_name: "RANGKUI",
    district_code: 1971010,
    regency_code: 1971,
    province_code: 19
  },
  {
    district_name: "BUKIT INTAN",
    district_code: 1971020,
    regency_code: 1971,
    province_code: 19
  },
  {
    district_name: "GIRIMAYA",
    district_code: 1971021,
    regency_code: 1971,
    province_code: 19
  },
  {
    district_name: "PANGKAL BALAM",
    district_code: 1971030,
    regency_code: 1971,
    province_code: 19
  },
  {
    district_name: "GABEK",
    district_code: 1971031,
    regency_code: 1971,
    province_code: 19
  },
  {
    district_name: "TAMAN SARI",
    district_code: 1971040,
    regency_code: 1971,
    province_code: 19
  },
  {
    district_name: "GERUNGGANG",
    district_code: 1971041,
    regency_code: 1971,
    province_code: 19
  },
  {
    district_name: "MORO",
    district_code: 2101010,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "DURAI",
    district_code: 2101011,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "KUNDUR",
    district_code: 2101020,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "KUNDUR UTARA",
    district_code: 2101021,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "KUNDUR BARAT",
    district_code: 2101022,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "UNGAR",
    district_code: 2101023,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "BELAT",
    district_code: 2101024,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "KARIMUN",
    district_code: 2101030,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "BURU",
    district_code: 2101031,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "MERAL",
    district_code: 2101032,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "TEBING",
    district_code: 2101033,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "MERAL BARAT",
    district_code: 2101034,
    regency_code: 2101,
    province_code: 21
  },
  {
    district_name: "TELUK BINTAN",
    district_code: 2102040,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "BINTAN UTARA",
    district_code: 2102050,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "TELUK SEBONG",
    district_code: 2102051,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "SERI KUALA LOBAM",
    district_code: 2102052,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "BINTAN TIMUR",
    district_code: 2102060,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "GUNUNG KIJANG",
    district_code: 2102061,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "MANTANG",
    district_code: 2102062,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "BINTAN PESISIR",
    district_code: 2102063,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "TOAPAYA",
    district_code: 2102064,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "TAMBELAN",
    district_code: 2102070,
    regency_code: 2102,
    province_code: 21
  },
  {
    district_name: "MIDAI",
    district_code: 2103030,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "SUAK MIDAI",
    district_code: 2103031,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "BUNGURAN BARAT",
    district_code: 2103040,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "BUNGURAN UTARA",
    district_code: 2103041,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "PULAU LAUT",
    district_code: 2103042,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "PULAU TIGA",
    district_code: 2103043,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "BUNGURAN BATUBI",
    district_code: 2103044,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "PULAU TIGA BARAT",
    district_code: 2103045,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "BUNGURAN TIMUR",
    district_code: 2103050,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "BUNGURAN TIMUR LAUT",
    district_code: 2103051,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "BUNGURAN TENGAH",
    district_code: 2103052,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "BUNGURAN SELATAN",
    district_code: 2103053,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "SERASAN",
    district_code: 2103060,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "SUBI",
    district_code: 2103061,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "SERASAN TIMUR",
    district_code: 2103062,
    regency_code: 2103,
    province_code: 21
  },
  {
    district_name: "SINGKEP BARAT",
    district_code: 2104010,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "KEPULAUAN POSEK",
    district_code: 2104011,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "SINGKEP",
    district_code: 2104020,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "SINGKEP SELATAN",
    district_code: 2104021,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "SINGKEP PESISIR",
    district_code: 2104022,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "LINGGA",
    district_code: 2104030,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "SELAYAR",
    district_code: 2104031,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "LINGGA TIMUR",
    district_code: 2104032,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "LINGGA UTARA",
    district_code: 2104040,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "SENAYANG",
    district_code: 2104050,
    regency_code: 2104,
    province_code: 21
  },
  {
    district_name: "JEMAJA",
    district_code: 2105010,
    regency_code: 2105,
    province_code: 21
  },
  {
    district_name: "JEMAJA TIMUR",
    district_code: 2105020,
    regency_code: 2105,
    province_code: 21
  },
  {
    district_name: "SIANTAN SELATAN",
    district_code: 2105030,
    regency_code: 2105,
    province_code: 21
  },
  {
    district_name: "SIANTAN",
    district_code: 2105040,
    regency_code: 2105,
    province_code: 21
  },
  {
    district_name: "SIANTAN TIMUR",
    district_code: 2105050,
    regency_code: 2105,
    province_code: 21
  },
  {
    district_name: "SIANTAN TENGAH",
    district_code: 2105060,
    regency_code: 2105,
    province_code: 21
  },
  {
    district_name: "PALMATAK",
    district_code: 2105070,
    regency_code: 2105,
    province_code: 21
  },
  {
    district_name: "BELAKANG PADANG",
    district_code: 2171010,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "BULANG",
    district_code: 2171020,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "GALANG",
    district_code: 2171030,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "SEI BEDUK",
    district_code: 2171040,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "SAGULUNG",
    district_code: 2171041,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "NONGSA",
    district_code: 2171050,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "BATAM KOTA",
    district_code: 2171051,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "SEKUPANG",
    district_code: 2171060,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "BATU AJI",
    district_code: 2171061,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "LUBUK BAJA",
    district_code: 2171070,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "BATU AMPAR",
    district_code: 2171080,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "BENGKONG",
    district_code: 2171081,
    regency_code: 2171,
    province_code: 21
  },
  {
    district_name: "BUKIT BESTARI",
    district_code: 2172010,
    regency_code: 2172,
    province_code: 21
  },
  {
    district_name: "TANJUNGPINANG TIMUR",
    district_code: 2172020,
    regency_code: 2172,
    province_code: 21
  },
  {
    district_name: "TANJUNGPINANG KOTA",
    district_code: 2172030,
    regency_code: 2172,
    province_code: 21
  },
  {
    district_name: "TANJUNGPINANG BARAT",
    district_code: 2172040,
    regency_code: 2172,
    province_code: 21
  },
  {
    district_name: "KEPULAUAN SERIBU SELATAN",
    district_code: 3101010,
    regency_code: 3101,
    province_code: 31
  },
  {
    district_name: "KEPULAUAN SERIBU UTARA",
    district_code: 3101020,
    regency_code: 3101,
    province_code: 31
  },
  {
    district_name: "JAGAKARSA",
    district_code: 3171010,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "PASAR MINGGU",
    district_code: 3171020,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "CILANDAK",
    district_code: 3171030,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "PESANGGRAHAN",
    district_code: 3171040,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "KEBAYORAN LAMA",
    district_code: 3171050,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "KEBAYORAN BARU",
    district_code: 3171060,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "MAMPANG PRAPATAN",
    district_code: 3171070,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "PANCORAN",
    district_code: 3171080,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "TEBET",
    district_code: 3171090,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "SETIA BUDI",
    district_code: 3171100,
    regency_code: 3171,
    province_code: 31
  },
  {
    district_name: "PASAR REBO",
    district_code: 3172010,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "CIRACAS",
    district_code: 3172020,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "CIPAYUNG",
    district_code: 3172030,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "MAKASAR",
    district_code: 3172040,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "KRAMAT JATI",
    district_code: 3172050,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "JATINEGARA",
    district_code: 3172060,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "DUREN SAWIT",
    district_code: 3172070,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "CAKUNG",
    district_code: 3172080,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "PULO GADUNG",
    district_code: 3172090,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "MATRAMAN",
    district_code: 3172100,
    regency_code: 3172,
    province_code: 31
  },
  {
    district_name: "TANAH ABANG",
    district_code: 3173010,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "MENTENG",
    district_code: 3173020,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "SENEN",
    district_code: 3173030,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "JOHAR BARU",
    district_code: 3173040,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "CEMPAKA PUTIH",
    district_code: 3173050,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "KEMAYORAN",
    district_code: 3173060,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "SAWAH BESAR",
    district_code: 3173070,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "GAMBIR",
    district_code: 3173080,
    regency_code: 3173,
    province_code: 31
  },
  {
    district_name: "KEMBANGAN",
    district_code: 3174010,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "KEBON JERUK",
    district_code: 3174020,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "PALMERAH",
    district_code: 3174030,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "GROGOL PETAMBURAN",
    district_code: 3174040,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "TAMBORA",
    district_code: 3174050,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "TAMAN SARI",
    district_code: 3174060,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "CENGKARENG",
    district_code: 3174070,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "KALI DERES",
    district_code: 3174080,
    regency_code: 3174,
    province_code: 31
  },
  {
    district_name: "PENJARINGAN",
    district_code: 3175010,
    regency_code: 3175,
    province_code: 31
  },
  {
    district_name: "PADEMANGAN",
    district_code: 3175020,
    regency_code: 3175,
    province_code: 31
  },
  {
    district_name: "TANJUNG PRIOK",
    district_code: 3175030,
    regency_code: 3175,
    province_code: 31
  },
  {
    district_name: "KOJA",
    district_code: 3175040,
    regency_code: 3175,
    province_code: 31
  },
  {
    district_name: "KELAPA GADING",
    district_code: 3175050,
    regency_code: 3175,
    province_code: 31
  },
  {
    district_name: "CILINCING",
    district_code: 3175060,
    regency_code: 3175,
    province_code: 31
  },
  {
    district_name: "NANGGUNG",
    district_code: 3201010,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "LEUWILIANG",
    district_code: 3201020,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "LEUWISADENG",
    district_code: 3201021,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "PAMIJAHAN",
    district_code: 3201030,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIBUNGBULANG",
    district_code: 3201040,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIAMPEA",
    district_code: 3201050,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "TENJOLAYA",
    district_code: 3201051,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "DRAMAGA",
    district_code: 3201060,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIOMAS",
    district_code: 3201070,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "TAMANSARI",
    district_code: 3201071,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIJERUK",
    district_code: 3201080,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIGOMBONG",
    district_code: 3201081,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CARINGIN",
    district_code: 3201090,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIAWI",
    district_code: 3201100,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CISARUA",
    district_code: 3201110,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "MEGAMENDUNG",
    district_code: 3201120,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "SUKARAJA",
    district_code: 3201130,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "BABAKAN MADANG",
    district_code: 3201140,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "SUKAMAKMUR",
    district_code: 3201150,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CARIU",
    district_code: 3201160,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "TANJUNGSARI",
    district_code: 3201161,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "JONGGOL",
    district_code: 3201170,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CILEUNGSI",
    district_code: 3201180,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "KELAPA NUNGGAL",
    district_code: 3201181,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "GUNUNG PUTRI",
    district_code: 3201190,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CITEUREUP",
    district_code: 3201200,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIBINONG",
    district_code: 3201210,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "BOJONG GEDE",
    district_code: 3201220,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "TAJUR HALANG",
    district_code: 3201221,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "KEMANG",
    district_code: 3201230,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "RANCA BUNGUR",
    district_code: 3201231,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "PARUNG",
    district_code: 3201240,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CISEENG",
    district_code: 3201241,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "GUNUNG SINDUR",
    district_code: 3201250,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "RUMPIN",
    district_code: 3201260,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIGUDEG",
    district_code: 3201270,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "SUKAJAYA",
    district_code: 3201271,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "JASINGA",
    district_code: 3201280,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "TENJO",
    district_code: 3201290,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "PARUNG PANJANG",
    district_code: 3201300,
    regency_code: 3201,
    province_code: 32
  },
  {
    district_name: "CIEMAS",
    district_code: 3202010,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIRACAP",
    district_code: 3202020,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "WALURAN",
    district_code: 3202021,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "SURADE",
    district_code: 3202030,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIBITUNG",
    district_code: 3202031,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "JAMPANG KULON",
    district_code: 3202040,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIMANGGU",
    district_code: 3202041,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "KALI BUNDER",
    district_code: 3202050,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "TEGAL BULEUD",
    district_code: 3202060,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIDOLOG",
    district_code: 3202070,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "SAGARANTEN",
    district_code: 3202080,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIDADAP",
    district_code: 3202081,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CURUGKEMBAR",
    district_code: 3202082,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "PABUARAN",
    district_code: 3202090,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "LENGKONG",
    district_code: 3202100,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "PALABUHANRATU",
    district_code: 3202110,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "SIMPENAN",
    district_code: 3202111,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "WARUNG KIARA",
    district_code: 3202120,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "BANTARGADUNG",
    district_code: 3202121,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "JAMPANG TENGAH",
    district_code: 3202130,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "PURABAYA",
    district_code: 3202131,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIKEMBAR",
    district_code: 3202140,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "NYALINDUNG",
    district_code: 3202150,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "GEGER BITUNG",
    district_code: 3202160,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "SUKARAJA",
    district_code: 3202170,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "KEBONPEDES",
    district_code: 3202171,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIREUNGHAS",
    district_code: 3202172,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "SUKALARANG",
    district_code: 3202173,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "SUKABUMI",
    district_code: 3202180,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "KADUDAMPIT",
    district_code: 3202190,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CISAAT",
    district_code: 3202200,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "GUNUNGGURUH",
    district_code: 3202201,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIBADAK",
    district_code: 3202210,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CICANTAYAN",
    district_code: 3202211,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CARINGIN",
    district_code: 3202212,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "NAGRAK",
    district_code: 3202220,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIAMBAR",
    district_code: 3202221,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CICURUG",
    district_code: 3202230,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIDAHU",
    district_code: 3202240,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "PARAKAN SALAK",
    district_code: 3202250,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "PARUNG KUDA",
    district_code: 3202260,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "BOJONG GENTENG",
    district_code: 3202261,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "KALAPA NUNGGAL",
    district_code: 3202270,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIKIDANG",
    district_code: 3202280,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CISOLOK",
    district_code: 3202290,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "CIKAKAK",
    district_code: 3202291,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "KABANDUNGAN",
    district_code: 3202300,
    regency_code: 3202,
    province_code: 32
  },
  {
    district_name: "AGRABINTA",
    district_code: 3203010,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "LELES",
    district_code: 3203011,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "SINDANGBARANG",
    district_code: 3203020,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIDAUN",
    district_code: 3203030,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "NARINGGUL",
    district_code: 3203040,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIBINONG",
    district_code: 3203050,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIKADU",
    district_code: 3203051,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "TANGGEUNG",
    district_code: 3203060,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "PASIRKUDA",
    district_code: 3203061,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "KADUPANDAK",
    district_code: 3203070,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIJATI",
    district_code: 3203071,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "TAKOKAK",
    district_code: 3203080,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "SUKANAGARA",
    district_code: 3203090,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "PAGELARAN",
    district_code: 3203100,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CAMPAKA",
    district_code: 3203110,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CAMPAKA MULYA",
    district_code: 3203111,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIBEBER",
    district_code: 3203120,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "WARUNGKONDANG",
    district_code: 3203130,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "GEKBRONG",
    district_code: 3203131,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CILAKU",
    district_code: 3203140,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "SUKALUYU",
    district_code: 3203150,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "BOJONGPICUNG",
    district_code: 3203160,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "HAURWANGI",
    district_code: 3203161,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIRANJANG",
    district_code: 3203170,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "MANDE",
    district_code: 3203180,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "KARANGTENGAH",
    district_code: 3203190,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIANJUR",
    district_code: 3203200,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CUGENANG",
    district_code: 3203210,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "PACET",
    district_code: 3203220,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIPANAS",
    district_code: 3203221,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "SUKARESMI",
    district_code: 3203230,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIKALONGKULON",
    district_code: 3203240,
    regency_code: 3203,
    province_code: 32
  },
  {
    district_name: "CIWIDEY",
    district_code: 3204010,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "RANCABALI",
    district_code: 3204011,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "PASIRJAMBU",
    district_code: 3204020,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CIMAUNG",
    district_code: 3204030,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "PANGALENGAN",
    district_code: 3204040,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "KERTASARI",
    district_code: 3204050,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "PACET",
    district_code: 3204060,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "IBUN",
    district_code: 3204070,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "PASEH",
    district_code: 3204080,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CIKANCUNG",
    district_code: 3204090,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CICALENGKA",
    district_code: 3204100,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "NAGREG",
    district_code: 3204101,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "RANCAEKEK",
    district_code: 3204110,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "MAJALAYA",
    district_code: 3204120,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "SOLOKAN JERUK",
    district_code: 3204121,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CIPARAY",
    district_code: 3204130,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "BALEENDAH",
    district_code: 3204140,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "ARJASARI",
    district_code: 3204150,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "BANJARAN",
    district_code: 3204160,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CANGKUANG",
    district_code: 3204161,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "PAMEUNGPEUK",
    district_code: 3204170,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "KATAPANG",
    district_code: 3204180,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "SOREANG",
    district_code: 3204190,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "KUTAWARINGIN",
    district_code: 3204191,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "MARGAASIH",
    district_code: 3204250,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "MARGAHAYU",
    district_code: 3204260,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "DAYEUHKOLOT",
    district_code: 3204270,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "BOJONGSOANG",
    district_code: 3204280,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CILEUNYI",
    district_code: 3204290,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CILENGKRANG",
    district_code: 3204300,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CIMENYAN",
    district_code: 3204310,
    regency_code: 3204,
    province_code: 32
  },
  {
    district_name: "CISEWU",
    district_code: 3205010,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CARINGIN",
    district_code: 3205011,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "TALEGONG",
    district_code: 3205020,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "BUNGBULANG",
    district_code: 3205030,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "MEKARMUKTI",
    district_code: 3205031,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "PAMULIHAN",
    district_code: 3205040,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "PAKENJENG",
    district_code: 3205050,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIKELET",
    district_code: 3205060,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "PAMEUNGPEUK",
    district_code: 3205070,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIBALONG",
    district_code: 3205080,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CISOMPET",
    district_code: 3205090,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "PEUNDEUY",
    district_code: 3205100,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "SINGAJAYA",
    district_code: 3205110,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIHURIP",
    district_code: 3205111,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIKAJANG",
    district_code: 3205120,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "BANJARWANGI",
    district_code: 3205130,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CILAWU",
    district_code: 3205140,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "BAYONGBONG",
    district_code: 3205150,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIGEDUG",
    district_code: 3205151,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CISURUPAN",
    district_code: 3205160,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "SUKARESMI",
    district_code: 3205161,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "SAMARANG",
    district_code: 3205170,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "PASIRWANGI",
    district_code: 3205171,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "TAROGONG KIDUL",
    district_code: 3205181,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "TAROGONG KALER",
    district_code: 3205182,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "GARUT KOTA",
    district_code: 3205190,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "KARANGPAWITAN",
    district_code: 3205200,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "WANARAJA",
    district_code: 3205210,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "SUCINARAJA",
    district_code: 3205211,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "PANGATIKAN",
    district_code: 3205212,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "SUKAWENING",
    district_code: 3205220,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "KARANGTENGAH",
    district_code: 3205221,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "BANYURESMI",
    district_code: 3205230,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "LELES",
    district_code: 3205240,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "LEUWIGOONG",
    district_code: 3205250,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIBATU",
    district_code: 3205260,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "KERSAMANAH",
    district_code: 3205261,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIBIUK",
    district_code: 3205270,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "KADUNGORA",
    district_code: 3205280,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "BLUBUR LIMBANGAN",
    district_code: 3205290,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "SELAAWI",
    district_code: 3205300,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "MALANGBONG",
    district_code: 3205310,
    regency_code: 3205,
    province_code: 32
  },
  {
    district_name: "CIPATUJAH",
    district_code: 3206010,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "KARANGNUNGGAL",
    district_code: 3206020,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CIKALONG",
    district_code: 3206030,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "PANCATENGAH",
    district_code: 3206040,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CIKATOMAS",
    district_code: 3206050,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CIBALONG",
    district_code: 3206060,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "PARUNGPONTENG",
    district_code: 3206061,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "BANTARKALONG",
    district_code: 3206070,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "BOJONGASIH",
    district_code: 3206071,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CULAMEGA",
    district_code: 3206072,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "BOJONGGAMBIR",
    district_code: 3206080,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SODONGHILIR",
    district_code: 3206090,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "TARAJU",
    district_code: 3206100,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SALAWU",
    district_code: 3206110,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "PUSPAHIANG",
    district_code: 3206111,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "TANJUNGJAYA",
    district_code: 3206120,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SUKARAJA",
    district_code: 3206130,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SALOPA",
    district_code: 3206140,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "JATIWARAS",
    district_code: 3206141,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CINEAM",
    district_code: 3206150,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "KARANGJAYA",
    district_code: 3206151,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "MANONJAYA",
    district_code: 3206160,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "GUNUNGTANJUNG",
    district_code: 3206161,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SINGAPARNA",
    district_code: 3206190,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SUKARAME",
    district_code: 3206191,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "MANGUNREJA",
    district_code: 3206192,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CIGALONTANG",
    district_code: 3206200,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "LEUWISARI",
    district_code: 3206210,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SARIWANGI",
    district_code: 3206211,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "PADAKEMBANG",
    district_code: 3206212,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SUKARATU",
    district_code: 3206221,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CISAYONG",
    district_code: 3206230,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SUKAHENING",
    district_code: 3206231,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "RAJAPOLAH",
    district_code: 3206240,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "JAMANIS",
    district_code: 3206250,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "CIAWI",
    district_code: 3206260,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "KADIPATEN",
    district_code: 3206261,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "PAGERAGEUNG",
    district_code: 3206270,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "SUKARESIK",
    district_code: 3206271,
    regency_code: 3206,
    province_code: 32
  },
  {
    district_name: "BANJARSARI",
    district_code: 3207100,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "BANJARANYAR",
    district_code: 3207101,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "LAKBOK",
    district_code: 3207110,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "PURWADADI",
    district_code: 3207111,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "PAMARICAN",
    district_code: 3207120,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CIDOLOG",
    district_code: 3207130,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CIMARAGAS",
    district_code: 3207140,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CIJEUNGJING",
    district_code: 3207150,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CISAGA",
    district_code: 3207160,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "TAMBAKSARI",
    district_code: 3207170,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "RANCAH",
    district_code: 3207180,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "RAJADESA",
    district_code: 3207190,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "SUKADANA",
    district_code: 3207200,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CIAMIS",
    district_code: 3207210,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "BAREGBEG",
    district_code: 3207211,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CIKONENG",
    district_code: 3207220,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "SINDANGKASIH",
    district_code: 3207221,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CIHAURBEUTI",
    district_code: 3207230,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "SADANANYA",
    district_code: 3207240,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "CIPAKU",
    district_code: 3207250,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "JATINAGARA",
    district_code: 3207260,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "PANAWANGAN",
    district_code: 3207270,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "KAWALI",
    district_code: 3207280,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "LUMBUNG",
    district_code: 3207281,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "PANJALU",
    district_code: 3207290,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "SUKAMANTRI",
    district_code: 3207291,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "PANUMBANGAN",
    district_code: 3207300,
    regency_code: 3207,
    province_code: 32
  },
  {
    district_name: "DARMA",
    district_code: 3208010,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "KADUGEDE",
    district_code: 3208020,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "NUSAHERANG",
    district_code: 3208021,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CINIRU",
    district_code: 3208030,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "HANTARA",
    district_code: 3208031,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "SELAJAMBE",
    district_code: 3208040,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "SUBANG",
    district_code: 3208050,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CILEBAK",
    district_code: 3208051,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIWARU",
    district_code: 3208060,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "KARANGKANCANA",
    district_code: 3208061,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIBINGBIN",
    district_code: 3208070,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIBEUREUM",
    district_code: 3208071,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "LURAGUNG",
    district_code: 3208080,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIMAHI",
    district_code: 3208081,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIDAHU",
    district_code: 3208090,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "KALIMANGGIS",
    district_code: 3208091,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIAWIGEBANG",
    district_code: 3208100,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIPICUNG",
    district_code: 3208101,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "LEBAKWANGI",
    district_code: 3208110,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "MALEBER",
    district_code: 3208111,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "GARAWANGI",
    district_code: 3208120,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "SINDANGAGUNG",
    district_code: 3208121,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "KUNINGAN",
    district_code: 3208130,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIGUGUR",
    district_code: 3208140,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "KRAMATMULYA",
    district_code: 3208150,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "JALAKSANA",
    district_code: 3208160,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "JAPARA",
    district_code: 3208161,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CILIMUS",
    district_code: 3208170,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "CIGANDAMEKAR",
    district_code: 3208171,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "MANDIRANCAN",
    district_code: 3208180,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "PANCALANG",
    district_code: 3208181,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "PASAWAHAN",
    district_code: 3208190,
    regency_code: 3208,
    province_code: 32
  },
  {
    district_name: "WALED",
    district_code: 3209010,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PASALEMAN",
    district_code: 3209011,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "CILEDUG",
    district_code: 3209020,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PABUARAN",
    district_code: 3209021,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "LOSARI",
    district_code: 3209030,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PABEDILAN",
    district_code: 3209031,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "BABAKAN",
    district_code: 3209040,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "GEBANG",
    district_code: 3209041,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "KARANGSEMBUNG",
    district_code: 3209050,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "KARANGWARENG",
    district_code: 3209051,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "LEMAHABANG",
    district_code: 3209060,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "SUSUKANLEBAK",
    district_code: 3209061,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "SEDONG",
    district_code: 3209070,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "ASTANAJAPURA",
    district_code: 3209080,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PANGENAN",
    district_code: 3209081,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "MUNDU",
    district_code: 3209090,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "BEBER",
    district_code: 3209100,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "GREGED",
    district_code: 3209101,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "TALUN",
    district_code: 3209111,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "SUMBER",
    district_code: 3209120,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "DUKUPUNTANG",
    district_code: 3209121,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PALIMANAN",
    district_code: 3209130,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PLUMBON",
    district_code: 3209140,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "DEPOK",
    district_code: 3209141,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "WERU",
    district_code: 3209150,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PLERED",
    district_code: 3209151,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "TENGAH TANI",
    district_code: 3209161,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "KEDAWUNG",
    district_code: 3209162,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "GUNUNGJATI",
    district_code: 3209171,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "KAPETAKAN",
    district_code: 3209180,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "SURANENGGALA",
    district_code: 3209181,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "KLANGENAN",
    district_code: 3209190,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "JAMBLANG",
    district_code: 3209191,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "ARJAWINANGUN",
    district_code: 3209200,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "PANGURAGAN",
    district_code: 3209201,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "CIWARINGIN",
    district_code: 3209210,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "GEMPOL",
    district_code: 3209211,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "SUSUKAN",
    district_code: 3209220,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "GEGESIK",
    district_code: 3209230,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "KALIWEDI",
    district_code: 3209231,
    regency_code: 3209,
    province_code: 32
  },
  {
    district_name: "LEMAHSUGIH",
    district_code: 3210010,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "BANTARUJEG",
    district_code: 3210020,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "MALAUSMA",
    district_code: 3210021,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "CIKIJING",
    district_code: 3210030,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "CINGAMBUL",
    district_code: 3210031,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "TALAGA",
    district_code: 3210040,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "BANJARAN",
    district_code: 3210041,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "ARGAPURA",
    district_code: 3210050,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "MAJA",
    district_code: 3210060,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "MAJALENGKA",
    district_code: 3210070,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "CIGASONG",
    district_code: 3210080,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "SUKAHAJI",
    district_code: 3210090,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "SINDANG",
    district_code: 3210091,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "RAJAGALUH",
    district_code: 3210100,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "SINDANGWANGI",
    district_code: 3210110,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "LEUWIMUNDING",
    district_code: 3210120,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "PALASAH",
    district_code: 3210130,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "JATIWANGI",
    district_code: 3210140,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "DAWUAN",
    district_code: 3210150,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "KASOKANDEL",
    district_code: 3210151,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "PANYINGKIRAN",
    district_code: 3210160,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "KADIPATEN",
    district_code: 3210170,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "KERTAJATI",
    district_code: 3210180,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "JATITUJUH",
    district_code: 3210190,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "LIGUNG",
    district_code: 3210200,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "SUMBERJAYA",
    district_code: 3210210,
    regency_code: 3210,
    province_code: 32
  },
  {
    district_name: "JATINANGOR",
    district_code: 3211010,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "CIMANGGUNG",
    district_code: 3211020,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "TANJUNGSARI",
    district_code: 3211030,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "SUKASARI",
    district_code: 3211031,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "PAMULIHAN",
    district_code: 3211032,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "RANCAKALONG",
    district_code: 3211040,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "SUMEDANG SELATAN",
    district_code: 3211050,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "SUMEDANG UTARA",
    district_code: 3211060,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "GANEAS",
    district_code: 3211061,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "SITURAJA",
    district_code: 3211070,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "CISITU",
    district_code: 3211071,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "DARMARAJA",
    district_code: 3211080,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "CIBUGEL",
    district_code: 3211090,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "WADO",
    district_code: 3211100,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "JATINUNGGAL",
    district_code: 3211101,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "JATIGEDE",
    district_code: 3211111,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "TOMO",
    district_code: 3211120,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "UJUNG JAYA",
    district_code: 3211130,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "CONGGEANG",
    district_code: 3211140,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "PASEH",
    district_code: 3211150,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "CIMALAKA",
    district_code: 3211160,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "CISARUA",
    district_code: 3211161,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "TANJUNGKERTA",
    district_code: 3211170,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "TANJUNGMEDAR",
    district_code: 3211171,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "BUAHDUA",
    district_code: 3211180,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "SURIAN",
    district_code: 3211181,
    regency_code: 3211,
    province_code: 32
  },
  {
    district_name: "HAURGEULIS",
    district_code: 3212010,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "GANTAR",
    district_code: 3212011,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "KROYA",
    district_code: 3212020,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "GABUSWETAN",
    district_code: 3212030,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "CIKEDUNG",
    district_code: 3212040,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "TERISI",
    district_code: 3212041,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "LELEA",
    district_code: 3212050,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "BANGODUA",
    district_code: 3212060,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "TUKDANA",
    district_code: 3212061,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "WIDASARI",
    district_code: 3212070,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "KERTASEMAYA",
    district_code: 3212080,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "SUKAGUMIWANG",
    district_code: 3212081,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "KRANGKENG",
    district_code: 3212090,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "KARANGAMPEL",
    district_code: 3212100,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "KEDOKAN BUNDER",
    district_code: 3212101,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "JUNTINYUAT",
    district_code: 3212110,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "SLIYEG",
    district_code: 3212120,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "JATIBARANG",
    district_code: 3212130,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "BALONGAN",
    district_code: 3212140,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "INDRAMAYU",
    district_code: 3212150,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "SINDANG",
    district_code: 3212160,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "CANTIGI",
    district_code: 3212161,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "PASEKAN",
    district_code: 3212162,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "LOHBENER",
    district_code: 3212170,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "ARAHAN",
    district_code: 3212171,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "LOSARANG",
    district_code: 3212180,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "KANDANGHAUR",
    district_code: 3212190,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "BONGAS",
    district_code: 3212200,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "ANJATAN",
    district_code: 3212210,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "SUKRA",
    district_code: 3212220,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "PATROL",
    district_code: 3212221,
    regency_code: 3212,
    province_code: 32
  },
  {
    district_name: "SAGALAHERANG",
    district_code: 3213010,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "SERANGPANJANG",
    district_code: 3213011,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "JALANCAGAK",
    district_code: 3213020,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CIATER",
    district_code: 3213021,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CISALAK",
    district_code: 3213030,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "KASOMALANG",
    district_code: 3213031,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "TANJUNGSIANG",
    district_code: 3213040,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CIJAMBE",
    district_code: 3213050,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CIBOGO",
    district_code: 3213060,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "SUBANG",
    district_code: 3213070,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "KALIJATI",
    district_code: 3213080,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "DAWUAN",
    district_code: 3213081,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CIPEUNDEUY",
    district_code: 3213090,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PABUARAN",
    district_code: 3213100,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PATOKBEUSI",
    district_code: 3213110,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PURWADADI",
    district_code: 3213120,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CIKAUM",
    district_code: 3213130,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PAGADEN",
    district_code: 3213140,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PAGADEN BARAT",
    district_code: 3213141,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CIPUNAGARA",
    district_code: 3213150,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "COMPRENG",
    district_code: 3213160,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "BINONG",
    district_code: 3213170,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "TAMBAKDAHAN",
    district_code: 3213171,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "CIASEM",
    district_code: 3213180,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PAMANUKAN",
    district_code: 3213190,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "SUKASARI",
    district_code: 3213191,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PUSAKANAGARA",
    district_code: 3213200,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "PUSAKAJAYA",
    district_code: 3213201,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "LEGONKULON",
    district_code: 3213210,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "BLANAKAN",
    district_code: 3213220,
    regency_code: 3213,
    province_code: 32
  },
  {
    district_name: "JATILUHUR",
    district_code: 3214010,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "SUKASARI",
    district_code: 3214011,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "MANIIS",
    district_code: 3214020,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "TEGAL WARU",
    district_code: 3214030,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "PLERED",
    district_code: 3214040,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "SUKATANI",
    district_code: 3214050,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "DARANGDAN",
    district_code: 3214060,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "BOJONG",
    district_code: 3214070,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "WANAYASA",
    district_code: 3214080,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "KIARAPEDES",
    district_code: 3214081,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "PASAWAHAN",
    district_code: 3214090,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "PONDOK SALAM",
    district_code: 3214091,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "PURWAKARTA",
    district_code: 3214100,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "BABAKANCIKAO",
    district_code: 3214101,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "CAMPAKA",
    district_code: 3214110,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "CIBATU",
    district_code: 3214111,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "BUNGURSARI",
    district_code: 3214112,
    regency_code: 3214,
    province_code: 32
  },
  {
    district_name: "PANGKALAN",
    district_code: 3215010,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "TEGALWARU",
    district_code: 3215011,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "CIAMPEL",
    district_code: 3215020,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "TELUKJAMBE TIMUR",
    district_code: 3215031,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "TELUKJAMBE BARAT",
    district_code: 3215032,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "KLARI",
    district_code: 3215040,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "CIKAMPEK",
    district_code: 3215050,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "PURWASARI",
    district_code: 3215051,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "TIRTAMULYA",
    district_code: 3215060,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "JATISARI",
    district_code: 3215070,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "BANYUSARI",
    district_code: 3215071,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "KOTABARU",
    district_code: 3215072,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "CILAMAYA WETAN",
    district_code: 3215081,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "CILAMAYA KULON",
    district_code: 3215082,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "LEMAHABANG",
    district_code: 3215090,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "TALAGASARI",
    district_code: 3215100,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "MAJALAYA",
    district_code: 3215111,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "KARAWANG TIMUR",
    district_code: 3215112,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "KARAWANG BARAT",
    district_code: 3215113,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "RAWAMERTA",
    district_code: 3215120,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "TEMPURAN",
    district_code: 3215130,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "KUTAWALUYA",
    district_code: 3215140,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "RENGASDENGKLOK",
    district_code: 3215150,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "JAYAKERTA",
    district_code: 3215151,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "PEDES",
    district_code: 3215160,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "CILEBAR",
    district_code: 3215161,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "CIBUAYA",
    district_code: 3215170,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "TIRTAJAYA",
    district_code: 3215180,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "BATUJAYA",
    district_code: 3215190,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "PAKISJAYA",
    district_code: 3215200,
    regency_code: 3215,
    province_code: 32
  },
  {
    district_name: "SETU",
    district_code: 3216010,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "SERANG BARU",
    district_code: 3216021,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CIKARANG PUSAT",
    district_code: 3216022,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CIKARANG SELATAN",
    district_code: 3216023,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CIBARUSAH",
    district_code: 3216030,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "BOJONGMANGU",
    district_code: 3216031,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CIKARANG TIMUR",
    district_code: 3216041,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "KEDUNGWARINGIN",
    district_code: 3216050,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CIKARANG UTARA",
    district_code: 3216061,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "KARANGBAHAGIA",
    district_code: 3216062,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CIBITUNG",
    district_code: 3216070,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CIKARANG BARAT",
    district_code: 3216071,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "TAMBUN SELATAN",
    district_code: 3216081,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "TAMBUN UTARA",
    district_code: 3216082,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "BABELAN",
    district_code: 3216090,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "TARUMAJAYA",
    district_code: 3216100,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "TAMBELANG",
    district_code: 3216110,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "SUKAWANGI",
    district_code: 3216111,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "SUKATANI",
    district_code: 3216120,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "SUKAKARYA",
    district_code: 3216121,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "PEBAYURAN",
    district_code: 3216130,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "CABANGBUNGIN",
    district_code: 3216140,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "MUARA GEMBONG",
    district_code: 3216150,
    regency_code: 3216,
    province_code: 32
  },
  {
    district_name: "RONGGA",
    district_code: 3217010,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "GUNUNGHALU",
    district_code: 3217020,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "SINDANGKERTA",
    district_code: 3217030,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CILILIN",
    district_code: 3217040,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CIHAMPELAS",
    district_code: 3217050,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CIPONGKOR",
    district_code: 3217060,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "BATUJAJAR",
    district_code: 3217070,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "SAGULING",
    district_code: 3217071,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CIPATAT",
    district_code: 3217080,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "PADALARANG",
    district_code: 3217090,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "NGAMPRAH",
    district_code: 3217100,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "PARONGPONG",
    district_code: 3217110,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "LEMBANG",
    district_code: 3217120,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CISARUA",
    district_code: 3217130,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CIKALONG WETAN",
    district_code: 3217140,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CIPEUNDEUY",
    district_code: 3217150,
    regency_code: 3217,
    province_code: 32
  },
  {
    district_name: "CIMERAK",
    district_code: 3218010,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "CIJULANG",
    district_code: 3218020,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "CIGUGUR",
    district_code: 3218030,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "LANGKAPLANCAR",
    district_code: 3218040,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "PARIGI",
    district_code: 3218050,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "SIDAMULIH",
    district_code: 3218060,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "PANGANDARAN",
    district_code: 3218070,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "KALIPUCANG",
    district_code: 3218080,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "PADAHERANG",
    district_code: 3218090,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "MANGUNJAYA",
    district_code: 3218100,
    regency_code: 3218,
    province_code: 32
  },
  {
    district_name: "BOGOR SELATAN",
    district_code: 3271010,
    regency_code: 3271,
    province_code: 32
  },
  {
    district_name: "BOGOR TIMUR",
    district_code: 3271020,
    regency_code: 3271,
    province_code: 32
  },
  {
    district_name: "BOGOR UTARA",
    district_code: 3271030,
    regency_code: 3271,
    province_code: 32
  },
  {
    district_name: "BOGOR TENGAH",
    district_code: 3271040,
    regency_code: 3271,
    province_code: 32
  },
  {
    district_name: "BOGOR BARAT",
    district_code: 3271050,
    regency_code: 3271,
    province_code: 32
  },
  {
    district_name: "TANAH SEREAL",
    district_code: 3271060,
    regency_code: 3271,
    province_code: 32
  },
  {
    district_name: "BAROS",
    district_code: 3272010,
    regency_code: 3272,
    province_code: 32
  },
  {
    district_name: "LEMBURSITU",
    district_code: 3272011,
    regency_code: 3272,
    province_code: 32
  },
  {
    district_name: "CIBEUREUM",
    district_code: 3272012,
    regency_code: 3272,
    province_code: 32
  },
  {
    district_name: "CITAMIANG",
    district_code: 3272020,
    regency_code: 3272,
    province_code: 32
  },
  {
    district_name: "WARUDOYONG",
    district_code: 3272030,
    regency_code: 3272,
    province_code: 32
  },
  {
    district_name: "GUNUNG PUYUH",
    district_code: 3272040,
    regency_code: 3272,
    province_code: 32
  },
  {
    district_name: "CIKOLE",
    district_code: 3272050,
    regency_code: 3272,
    province_code: 32
  },
  {
    district_name: "BANDUNG KULON",
    district_code: 3273010,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "BABAKAN CIPARAY",
    district_code: 3273020,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "BOJONGLOA KALER",
    district_code: 3273030,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "BOJONGLOA KIDUL",
    district_code: 3273040,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "ASTANAANYAR",
    district_code: 3273050,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "REGOL",
    district_code: 3273060,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "LENGKONG",
    district_code: 3273070,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "BANDUNG KIDUL",
    district_code: 3273080,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "BUAHBATU",
    district_code: 3273090,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "RANCASARI",
    district_code: 3273100,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "GEDEBAGE",
    district_code: 3273101,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "CIBIRU",
    district_code: 3273110,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "PANYILEUKAN",
    district_code: 3273111,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "UJUNG BERUNG",
    district_code: 3273120,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "CINAMBO",
    district_code: 3273121,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "ARCAMANIK",
    district_code: 3273130,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "ANTAPANI",
    district_code: 3273141,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "MANDALAJATI",
    district_code: 3273142,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "KIARACONDONG",
    district_code: 3273150,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "BATUNUNGGAL",
    district_code: 3273160,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "SUMUR BANDUNG",
    district_code: 3273170,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "ANDIR",
    district_code: 3273180,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "CICENDO",
    district_code: 3273190,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "BANDUNG WETAN",
    district_code: 3273200,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "CIBEUNYING KIDUL",
    district_code: 3273210,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "CIBEUNYING KALER",
    district_code: 3273220,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "COBLONG",
    district_code: 3273230,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "SUKAJADI",
    district_code: 3273240,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "SUKASARI",
    district_code: 3273250,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "CIDADAP",
    district_code: 3273260,
    regency_code: 3273,
    province_code: 32
  },
  {
    district_name: "HARJAMUKTI",
    district_code: 3274010,
    regency_code: 3274,
    province_code: 32
  },
  {
    district_name: "LEMAHWUNGKUK",
    district_code: 3274020,
    regency_code: 3274,
    province_code: 32
  },
  {
    district_name: "PEKALIPAN",
    district_code: 3274030,
    regency_code: 3274,
    province_code: 32
  },
  {
    district_name: "KESAMBI",
    district_code: 3274040,
    regency_code: 3274,
    province_code: 32
  },
  {
    district_name: "KEJAKSAN",
    district_code: 3274050,
    regency_code: 3274,
    province_code: 32
  },
  {
    district_name: "PONDOKGEDE",
    district_code: 3275010,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "JATISAMPURNA",
    district_code: 3275011,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "PONDOKMELATI",
    district_code: 3275012,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "JATIASIH",
    district_code: 3275020,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "BANTARGEBANG",
    district_code: 3275030,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "MUSTIKAJAYA",
    district_code: 3275031,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "BEKASI TIMUR",
    district_code: 3275040,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "RAWALUMBU",
    district_code: 3275041,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "BEKASI SELATAN",
    district_code: 3275050,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "BEKASI BARAT",
    district_code: 3275060,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "MEDAN SATRIA",
    district_code: 3275061,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "BEKASI UTARA",
    district_code: 3275070,
    regency_code: 3275,
    province_code: 32
  },
  {
    district_name: "SAWANGAN",
    district_code: 3276010,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "BOJONGSARI",
    district_code: 3276011,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "PANCORAN MAS",
    district_code: 3276020,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "CIPAYUNG",
    district_code: 3276021,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "SUKMA JAYA",
    district_code: 3276030,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "CILODONG",
    district_code: 3276031,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "CIMANGGIS",
    district_code: 3276040,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "TAPOS",
    district_code: 3276041,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "BEJI",
    district_code: 3276050,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "LIMO",
    district_code: 3276060,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "CINERE",
    district_code: 3276061,
    regency_code: 3276,
    province_code: 32
  },
  {
    district_name: "CIMAHI SELATAN",
    district_code: 3277010,
    regency_code: 3277,
    province_code: 32
  },
  {
    district_name: "CIMAHI TENGAH",
    district_code: 3277020,
    regency_code: 3277,
    province_code: 32
  },
  {
    district_name: "CIMAHI UTARA",
    district_code: 3277030,
    regency_code: 3277,
    province_code: 32
  },
  {
    district_name: "KAWALU",
    district_code: 3278010,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "TAMANSARI",
    district_code: 3278020,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "CIBEUREUM",
    district_code: 3278030,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "PURBARATU",
    district_code: 3278031,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "TAWANG",
    district_code: 3278040,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "CIHIDEUNG",
    district_code: 3278050,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "MANGKUBUMI",
    district_code: 3278060,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "INDIHIANG",
    district_code: 3278070,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "BUNGURSARI",
    district_code: 3278071,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "CIPEDES",
    district_code: 3278080,
    regency_code: 3278,
    province_code: 32
  },
  {
    district_name: "BANJAR",
    district_code: 3279010,
    regency_code: 3279,
    province_code: 32
  },
  {
    district_name: "PURWAHARJA",
    district_code: 3279020,
    regency_code: 3279,
    province_code: 32
  },
  {
    district_name: "PATARUMAN",
    district_code: 3279030,
    regency_code: 3279,
    province_code: 32
  },
  {
    district_name: "LANGENSARI",
    district_code: 3279040,
    regency_code: 3279,
    province_code: 32
  },
  {
    district_name: "DAYEUHLUHUR",
    district_code: 3301010,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "WANAREJA",
    district_code: 3301020,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "MAJENANG",
    district_code: 3301030,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "CIMANGGU",
    district_code: 3301040,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "KARANGPUCUNG",
    district_code: 3301050,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "CIPARI",
    district_code: 3301060,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "SIDAREJA",
    district_code: 3301070,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "KEDUNGREJA",
    district_code: 3301080,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "PATIMUAN",
    district_code: 3301090,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "GANDRUNGMANGU",
    district_code: 3301100,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "BANTARSARI",
    district_code: 3301110,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "KAWUNGANTEN",
    district_code: 3301120,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "KAMPUNG LAUT",
    district_code: 3301121,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "JERUKLEGI",
    district_code: 3301130,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "KESUGIHAN",
    district_code: 3301140,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "ADIPALA",
    district_code: 3301150,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "MAOS",
    district_code: 3301160,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "SAMPANG",
    district_code: 3301170,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "KROYA",
    district_code: 3301180,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "BINANGUN",
    district_code: 3301190,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "NUSAWUNGU",
    district_code: 3301200,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "CILACAP SELATAN",
    district_code: 3301710,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "CILACAP TENGAH",
    district_code: 3301720,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "CILACAP UTARA",
    district_code: 3301730,
    regency_code: 3301,
    province_code: 33
  },
  {
    district_name: "LUMBIR",
    district_code: 3302010,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "WANGON",
    district_code: 3302020,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "JATILAWANG",
    district_code: 3302030,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "RAWALO",
    district_code: 3302040,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "KEBASEN",
    district_code: 3302050,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "KEMRANJEN",
    district_code: 3302060,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "SUMPIUH",
    district_code: 3302070,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "TAMBAK",
    district_code: 3302080,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "SOMAGEDE",
    district_code: 3302090,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "KALIBAGOR",
    district_code: 3302100,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "BANYUMAS",
    district_code: 3302110,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "PATIKRAJA",
    district_code: 3302120,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "PURWOJATI",
    district_code: 3302130,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "AJIBARANG",
    district_code: 3302140,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "GUMELAR",
    district_code: 3302150,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "PEKUNCEN",
    district_code: 3302160,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "CILONGOK",
    district_code: 3302170,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "KARANGLEWAS",
    district_code: 3302180,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "KEDUNG BANTENG",
    district_code: 3302190,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "BATURRADEN",
    district_code: 3302200,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "SUMBANG",
    district_code: 3302210,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "KEMBARAN",
    district_code: 3302220,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "SOKARAJA",
    district_code: 3302230,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "PURWOKERTO SELATAN",
    district_code: 3302710,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "PURWOKERTO BARAT",
    district_code: 3302720,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "PURWOKERTO TIMUR",
    district_code: 3302730,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "PURWOKERTO UTARA",
    district_code: 3302740,
    regency_code: 3302,
    province_code: 33
  },
  {
    district_name: "KEMANGKON",
    district_code: 3303010,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "BUKATEJA",
    district_code: 3303020,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KEJOBONG",
    district_code: 3303030,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "PENGADEGAN",
    district_code: 3303040,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KALIGONDANG",
    district_code: 3303050,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "PURBALINGGA",
    district_code: 3303060,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KALIMANAH",
    district_code: 3303070,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "PADAMARA",
    district_code: 3303080,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KUTASARI",
    district_code: 3303090,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "BOJONGSARI",
    district_code: 3303100,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "MREBET",
    district_code: 3303110,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "BOBOTSARI",
    district_code: 3303120,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KARANGREJA",
    district_code: 3303130,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KARANGJAMBU",
    district_code: 3303131,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KARANGANYAR",
    district_code: 3303140,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KERTANEGARA",
    district_code: 3303141,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "KARANGMONCOL",
    district_code: 3303150,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "REMBANG",
    district_code: 3303160,
    regency_code: 3303,
    province_code: 33
  },
  {
    district_name: "SUSUKAN",
    district_code: 3304010,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "PURWAREJA KLAMPOK",
    district_code: 3304020,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "MANDIRAJA",
    district_code: 3304030,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "PURWANEGARA",
    district_code: 3304040,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "BAWANG",
    district_code: 3304050,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "BANJARNEGARA",
    district_code: 3304060,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "PAGEDONGAN",
    district_code: 3304061,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "SIGALUH",
    district_code: 3304070,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "MADUKARA",
    district_code: 3304080,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "BANJARMANGU",
    district_code: 3304090,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "WANADADI",
    district_code: 3304100,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "RAKIT",
    district_code: 3304110,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "PUNGGELAN",
    district_code: 3304120,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "KARANGKOBAR",
    district_code: 3304130,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "PAGENTAN",
    district_code: 3304140,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "PEJAWARAN",
    district_code: 3304150,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "BATUR",
    district_code: 3304160,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "WANAYASA",
    district_code: 3304170,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "KALIBENING",
    district_code: 3304180,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "PANDANARUM",
    district_code: 3304181,
    regency_code: 3304,
    province_code: 33
  },
  {
    district_name: "AYAH",
    district_code: 3305010,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "BUAYAN",
    district_code: 3305020,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "PURING",
    district_code: 3305030,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "PETANAHAN",
    district_code: 3305040,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "KLIRONG",
    district_code: 3305050,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "BULUSPESANTREN",
    district_code: 3305060,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "AMBAL",
    district_code: 3305070,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "MIRIT",
    district_code: 3305080,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "BONOROWO",
    district_code: 3305081,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "PREMBUN",
    district_code: 3305090,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "PADURESO",
    district_code: 3305091,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "KUTOWINANGUN",
    district_code: 3305100,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "ALIAN",
    district_code: 3305110,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "PONCOWARNO",
    district_code: 3305111,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "KEBUMEN",
    district_code: 3305120,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "PEJAGOAN",
    district_code: 3305130,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "SRUWENG",
    district_code: 3305140,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "ADIMULYO",
    district_code: 3305150,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "KUWARASAN",
    district_code: 3305160,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "ROWOKELE",
    district_code: 3305170,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "SEMPOR",
    district_code: 3305180,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "GOMBONG",
    district_code: 3305190,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "KARANGANYAR",
    district_code: 3305200,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "KARANGGAYAM",
    district_code: 3305210,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "SADANG",
    district_code: 3305220,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "KARANGSAMBUNG",
    district_code: 3305221,
    regency_code: 3305,
    province_code: 33
  },
  {
    district_name: "GRABAG",
    district_code: 3306010,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "NGOMBOL",
    district_code: 3306020,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "PURWODADI",
    district_code: 3306030,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "BAGELEN",
    district_code: 3306040,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "KALIGESING",
    district_code: 3306050,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "PURWOREJO",
    district_code: 3306060,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "BANYU URIP",
    district_code: 3306070,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "BAYAN",
    district_code: 3306080,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "KUTOARJO",
    district_code: 3306090,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "BUTUH",
    district_code: 3306100,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "PITURUH",
    district_code: 3306110,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "KEMIRI",
    district_code: 3306120,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "BRUNO",
    district_code: 3306130,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "GEBANG",
    district_code: 3306140,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "LOANO",
    district_code: 3306150,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "BENER",
    district_code: 3306160,
    regency_code: 3306,
    province_code: 33
  },
  {
    district_name: "WADASLINTANG",
    district_code: 3307010,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "KEPIL",
    district_code: 3307020,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "SAPURAN",
    district_code: 3307030,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "KALIBAWANG",
    district_code: 3307031,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "KALIWIRO",
    district_code: 3307040,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "LEKSONO",
    district_code: 3307050,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "SUKOHARJO",
    district_code: 3307051,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "SELOMERTO",
    district_code: 3307060,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "KALIKAJAR",
    district_code: 3307070,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "KERTEK",
    district_code: 3307080,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "WONOSOBO",
    district_code: 3307090,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "WATUMALANG",
    district_code: 3307100,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "MOJOTENGAH",
    district_code: 3307110,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "GARUNG",
    district_code: 3307120,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "KEJAJAR",
    district_code: 3307130,
    regency_code: 3307,
    province_code: 33
  },
  {
    district_name: "SALAMAN",
    district_code: 3308010,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "BOROBUDUR",
    district_code: 3308020,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "NGLUWAR",
    district_code: 3308030,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "SALAM",
    district_code: 3308040,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "SRUMBUNG",
    district_code: 3308050,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "DUKUN",
    district_code: 3308060,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "MUNTILAN",
    district_code: 3308070,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "MUNGKID",
    district_code: 3308080,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "SAWANGAN",
    district_code: 3308090,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "CANDIMULYO",
    district_code: 3308100,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "MERTOYUDAN",
    district_code: 3308110,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "TEMPURAN",
    district_code: 3308120,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "KAJORAN",
    district_code: 3308130,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "KALIANGKRIK",
    district_code: 3308140,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "BANDONGAN",
    district_code: 3308150,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "WINDUSARI",
    district_code: 3308160,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "SECANG",
    district_code: 3308170,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "TEGALREJO",
    district_code: 3308180,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "PAKIS",
    district_code: 3308190,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "GRABAG",
    district_code: 3308200,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "NGABLAK",
    district_code: 3308210,
    regency_code: 3308,
    province_code: 33
  },
  {
    district_name: "SELO",
    district_code: 3309010,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "AMPEL",
    district_code: 3309020,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "CEPOGO",
    district_code: 3309030,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "MUSUK",
    district_code: 3309040,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "BOYOLALI",
    district_code: 3309050,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "MOJOSONGO",
    district_code: 3309060,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "TERAS",
    district_code: 3309070,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "SAWIT",
    district_code: 3309080,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "BANYUDONO",
    district_code: 3309090,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "SAMBI",
    district_code: 3309100,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "NGEMPLAK",
    district_code: 3309110,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "NOGOSARI",
    district_code: 3309120,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "SIMO",
    district_code: 3309130,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "KARANGGEDE",
    district_code: 3309140,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "KLEGO",
    district_code: 3309150,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "ANDONG",
    district_code: 3309160,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "KEMUSU",
    district_code: 3309170,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "WONOSEGORO",
    district_code: 3309180,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "JUWANGI",
    district_code: 3309190,
    regency_code: 3309,
    province_code: 33
  },
  {
    district_name: "PRAMBANAN",
    district_code: 3310010,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "GANTIWARNO",
    district_code: 3310020,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "WEDI",
    district_code: 3310030,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "BAYAT",
    district_code: 3310040,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "CAWAS",
    district_code: 3310050,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "TRUCUK",
    district_code: 3310060,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KALIKOTES",
    district_code: 3310070,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KEBONARUM",
    district_code: 3310080,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "JOGONALAN",
    district_code: 3310090,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "MANISRENGGO",
    district_code: 3310100,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KARANGNONGKO",
    district_code: 3310110,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "NGAWEN",
    district_code: 3310120,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "CEPER",
    district_code: 3310130,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "PEDAN",
    district_code: 3310140,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KARANGDOWO",
    district_code: 3310150,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "JUWIRING",
    district_code: 3310160,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "WONOSARI",
    district_code: 3310170,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "DELANGGU",
    district_code: 3310180,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "POLANHARJO",
    district_code: 3310190,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KARANGANOM",
    district_code: 3310200,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "TULUNG",
    district_code: 3310210,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "JATINOM",
    district_code: 3310220,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KEMALANG",
    district_code: 3310230,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KLATEN SELATAN",
    district_code: 3310710,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KLATEN TENGAH",
    district_code: 3310720,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "KLATEN UTARA",
    district_code: 3310730,
    regency_code: 3310,
    province_code: 33
  },
  {
    district_name: "WERU",
    district_code: 3311010,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "BULU",
    district_code: 3311020,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "TAWANGSARI",
    district_code: 3311030,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "SUKOHARJO",
    district_code: 3311040,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "NGUTER",
    district_code: 3311050,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "BENDOSARI",
    district_code: 3311060,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "POLOKARTO",
    district_code: 3311070,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "MOJOLABAN",
    district_code: 3311080,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "GROGOL",
    district_code: 3311090,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "BAKI",
    district_code: 3311100,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "GATAK",
    district_code: 3311110,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "KARTASURA",
    district_code: 3311120,
    regency_code: 3311,
    province_code: 33
  },
  {
    district_name: "PRACIMANTORO",
    district_code: 3312010,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "PARANGGUPITO",
    district_code: 3312020,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "GIRITONTRO",
    district_code: 3312030,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "GIRIWOYO",
    district_code: 3312040,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "BATUWARNO",
    district_code: 3312050,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "KARANGTENGAH",
    district_code: 3312060,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "TIRTOMOYO",
    district_code: 3312070,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "NGUNTORONADI",
    district_code: 3312080,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "BATURETNO",
    district_code: 3312090,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "EROMOKO",
    district_code: 3312100,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "WURYANTORO",
    district_code: 3312110,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "MANYARAN",
    district_code: 3312120,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "SELOGIRI",
    district_code: 3312130,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "WONOGIRI",
    district_code: 3312140,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "NGADIROJO",
    district_code: 3312150,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "SIDOHARJO",
    district_code: 3312160,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "JATIROTO",
    district_code: 3312170,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "KISMANTORO",
    district_code: 3312180,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "PURWANTORO",
    district_code: 3312190,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "BULUKERTO",
    district_code: 3312200,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "PUHPELEM",
    district_code: 3312201,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "SLOGOHIMO",
    district_code: 3312210,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "JATISRONO",
    district_code: 3312220,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "JATIPURNO",
    district_code: 3312230,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "GIRIMARTO",
    district_code: 3312240,
    regency_code: 3312,
    province_code: 33
  },
  {
    district_name: "JATIPURO",
    district_code: 3313010,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "JATIYOSO",
    district_code: 3313020,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "JUMAPOLO",
    district_code: 3313030,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "JUMANTONO",
    district_code: 3313040,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "MATESIH",
    district_code: 3313050,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "TAWANGMANGU",
    district_code: 3313060,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "NGARGOYOSO",
    district_code: 3313070,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "KARANGPANDAN",
    district_code: 3313080,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "KARANGANYAR",
    district_code: 3313090,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "TASIKMADU",
    district_code: 3313100,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "JATEN",
    district_code: 3313110,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "COLOMADU",
    district_code: 3313120,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "GONDANGREJO",
    district_code: 3313130,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "KEBAKKRAMAT",
    district_code: 3313140,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "MOJOGEDANG",
    district_code: 3313150,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "KERJO",
    district_code: 3313160,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "JENAWI",
    district_code: 3313170,
    regency_code: 3313,
    province_code: 33
  },
  {
    district_name: "KALIJAMBE",
    district_code: 3314010,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "PLUPUH",
    district_code: 3314020,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "MASARAN",
    district_code: 3314030,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "KEDAWUNG",
    district_code: 3314040,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "SAMBIREJO",
    district_code: 3314050,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "GONDANG",
    district_code: 3314060,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "SAMBUNG MACAN",
    district_code: 3314070,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "NGRAMPAL",
    district_code: 3314080,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "KARANGMALANG",
    district_code: 3314090,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "SRAGEN",
    district_code: 3314100,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "SIDOHARJO",
    district_code: 3314110,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "TANON",
    district_code: 3314120,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "GEMOLONG",
    district_code: 3314130,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "MIRI",
    district_code: 3314140,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "SUMBERLAWANG",
    district_code: 3314150,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "MONDOKAN",
    district_code: 3314160,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "SUKODONO",
    district_code: 3314170,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "GESI",
    district_code: 3314180,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "TANGEN",
    district_code: 3314190,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "JENAR",
    district_code: 3314200,
    regency_code: 3314,
    province_code: 33
  },
  {
    district_name: "KEDUNGJATI",
    district_code: 3315010,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "KARANGRAYUNG",
    district_code: 3315020,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "PENAWANGAN",
    district_code: 3315030,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "TOROH",
    district_code: 3315040,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "GEYER",
    district_code: 3315050,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "PULOKULON",
    district_code: 3315060,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "KRADENAN",
    district_code: 3315070,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "GABUS",
    district_code: 3315080,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "NGARINGAN",
    district_code: 3315090,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "WIROSARI",
    district_code: 3315100,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "TAWANGHARJO",
    district_code: 3315110,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "GROBOGAN",
    district_code: 3315120,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "PURWODADI",
    district_code: 3315130,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "BRATI",
    district_code: 3315140,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "KLAMBU",
    district_code: 3315150,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "GODONG",
    district_code: 3315160,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "GUBUG",
    district_code: 3315170,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "TEGOWANU",
    district_code: 3315180,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "TANGGUNGHARJO",
    district_code: 3315190,
    regency_code: 3315,
    province_code: 33
  },
  {
    district_name: "JATI",
    district_code: 3316010,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "RANDUBLATUNG",
    district_code: 3316020,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "KRADENAN",
    district_code: 3316030,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "KEDUNGTUBAN",
    district_code: 3316040,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "CEPU",
    district_code: 3316050,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "SAMBONG",
    district_code: 3316060,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "JIKEN",
    district_code: 3316070,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "BOGOREJO",
    district_code: 3316080,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "JEPON",
    district_code: 3316090,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "KOTA BLORA",
    district_code: 3316100,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "BANJAREJO",
    district_code: 3316110,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "TUNJUNGAN",
    district_code: 3316120,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "JAPAH",
    district_code: 3316130,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "NGAWEN",
    district_code: 3316140,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "KUNDURAN",
    district_code: 3316150,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "TODANAN",
    district_code: 3316160,
    regency_code: 3316,
    province_code: 33
  },
  {
    district_name: "SUMBER",
    district_code: 3317010,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "BULU",
    district_code: 3317020,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "GUNEM",
    district_code: 3317030,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "SALE",
    district_code: 3317040,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "SARANG",
    district_code: 3317050,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "SEDAN",
    district_code: 3317060,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "PAMOTAN",
    district_code: 3317070,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "SULANG",
    district_code: 3317080,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "KALIORI",
    district_code: 3317090,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "REMBANG",
    district_code: 3317100,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "PANCUR",
    district_code: 3317110,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "KRAGAN",
    district_code: 3317120,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "SLUKE",
    district_code: 3317130,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "LASEM",
    district_code: 3317140,
    regency_code: 3317,
    province_code: 33
  },
  {
    district_name: "SUKOLILO",
    district_code: 3318010,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "KAYEN",
    district_code: 3318020,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "TAMBAKROMO",
    district_code: 3318030,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "WINONG",
    district_code: 3318040,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "PUCAKWANGI",
    district_code: 3318050,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "JAKEN",
    district_code: 3318060,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "BATANGAN",
    district_code: 3318070,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "JUWANA",
    district_code: 3318080,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "JAKENAN",
    district_code: 3318090,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "PATI",
    district_code: 3318100,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "GABUS",
    district_code: 3318110,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "MARGOREJO",
    district_code: 3318120,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "GEMBONG",
    district_code: 3318130,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "TLOGOWUNGU",
    district_code: 3318140,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "WEDARIJAKSA",
    district_code: 3318150,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "TRANGKIL",
    district_code: 3318160,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "MARGOYOSO",
    district_code: 3318170,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "GUNUNG WUNGKAL",
    district_code: 3318180,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "CLUWAK",
    district_code: 3318190,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "TAYU",
    district_code: 3318200,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "DUKUHSETI",
    district_code: 3318210,
    regency_code: 3318,
    province_code: 33
  },
  {
    district_name: "KALIWUNGU",
    district_code: 3319010,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "KOTA KUDUS",
    district_code: 3319020,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "JATI",
    district_code: 3319030,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "UNDAAN",
    district_code: 3319040,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "MEJOBO",
    district_code: 3319050,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "JEKULO",
    district_code: 3319060,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "BAE",
    district_code: 3319070,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "GEBOG",
    district_code: 3319080,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "DAWE",
    district_code: 3319090,
    regency_code: 3319,
    province_code: 33
  },
  {
    district_name: "KEDUNG",
    district_code: 3320010,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "PECANGAAN",
    district_code: 3320020,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "KALINYAMATAN",
    district_code: 3320021,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "WELAHAN",
    district_code: 3320030,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "MAYONG",
    district_code: 3320040,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "NALUMSARI",
    district_code: 3320050,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "BATEALIT",
    district_code: 3320060,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "TAHUNAN",
    district_code: 3320070,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "JEPARA",
    district_code: 3320080,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "MLONGGO",
    district_code: 3320090,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "PAKIS AJI",
    district_code: 3320091,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "BANGSRI",
    district_code: 3320100,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "KEMBANG",
    district_code: 3320101,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "KELING",
    district_code: 3320110,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "DONOROJO",
    district_code: 3320111,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "KARIMUNJAWA",
    district_code: 3320120,
    regency_code: 3320,
    province_code: 33
  },
  {
    district_name: "MRANGGEN",
    district_code: 3321010,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "KARANGAWEN",
    district_code: 3321020,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "GUNTUR",
    district_code: 3321030,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "SAYUNG",
    district_code: 3321040,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "KARANG TENGAH",
    district_code: 3321050,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "BONANG",
    district_code: 3321060,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "DEMAK",
    district_code: 3321070,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "WONOSALAM",
    district_code: 3321080,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "DEMPET",
    district_code: 3321090,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "KEBONAGUNG",
    district_code: 3321091,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "GAJAH",
    district_code: 3321100,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "KARANGANYAR",
    district_code: 3321110,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "MIJEN",
    district_code: 3321120,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "WEDUNG",
    district_code: 3321130,
    regency_code: 3321,
    province_code: 33
  },
  {
    district_name: "GETASAN",
    district_code: 3322010,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "TENGARAN",
    district_code: 3322020,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "SUSUKAN",
    district_code: 3322030,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "KALIWUNGU",
    district_code: 3322031,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "SURUH",
    district_code: 3322040,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "PABELAN",
    district_code: 3322050,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "TUNTANG",
    district_code: 3322060,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "BANYUBIRU",
    district_code: 3322070,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "JAMBU",
    district_code: 3322080,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "SUMOWONO",
    district_code: 3322090,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "AMBARAWA",
    district_code: 3322100,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "BANDUNGAN",
    district_code: 3322101,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "BAWEN",
    district_code: 3322110,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "BRINGIN",
    district_code: 3322120,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "BANCAK",
    district_code: 3322121,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "PRINGAPUS",
    district_code: 3322130,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "BERGAS",
    district_code: 3322140,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "UNGARAN BARAT",
    district_code: 3322151,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "UNGARAN TIMUR",
    district_code: 3322152,
    regency_code: 3322,
    province_code: 33
  },
  {
    district_name: "PARAKAN",
    district_code: 3323010,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "KLEDUNG",
    district_code: 3323011,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "BANSARI",
    district_code: 3323012,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "BULU",
    district_code: 3323020,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "TEMANGGUNG",
    district_code: 3323030,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "TLOGOMULYO",
    district_code: 3323031,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "TEMBARAK",
    district_code: 3323040,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "SELOPAMPANG",
    district_code: 3323041,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "KRANGGAN",
    district_code: 3323050,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "PRINGSURAT",
    district_code: 3323060,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "KALORAN",
    district_code: 3323070,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "KANDANGAN",
    district_code: 3323080,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "KEDU",
    district_code: 3323090,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "NGADIREJO",
    district_code: 3323100,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "JUMO",
    district_code: 3323110,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "GEMAWANG",
    district_code: 3323111,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "CANDIROTO",
    district_code: 3323120,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "BEJEN",
    district_code: 3323121,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "TRETEP",
    district_code: 3323130,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "WONOBOYO",
    district_code: 3323131,
    regency_code: 3323,
    province_code: 33
  },
  {
    district_name: "PLANTUNGAN",
    district_code: 3324010,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "SUKOREJO",
    district_code: 3324020,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "PAGERRUYUNG",
    district_code: 3324030,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "PATEAN",
    district_code: 3324040,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "SINGOROJO",
    district_code: 3324050,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "LIMBANGAN",
    district_code: 3324060,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "BOJA",
    district_code: 3324070,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "KALIWUNGU",
    district_code: 3324080,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "KALIWUNGU SELATAN",
    district_code: 3324081,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "BRANGSONG",
    district_code: 3324090,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "PEGANDON",
    district_code: 3324100,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "NGAMPEL",
    district_code: 3324101,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "GEMUH",
    district_code: 3324110,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "RINGINARUM",
    district_code: 3324111,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "WELERI",
    district_code: 3324120,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "ROWOSARI",
    district_code: 3324130,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "KANGKUNG",
    district_code: 3324140,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "CEPIRING",
    district_code: 3324150,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "PATEBON",
    district_code: 3324160,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "KOTA KENDAL",
    district_code: 3324170,
    regency_code: 3324,
    province_code: 33
  },
  {
    district_name: "WONOTUNGGAL",
    district_code: 3325010,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "BANDAR",
    district_code: 3325020,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "BLADO",
    district_code: 3325030,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "REBAN",
    district_code: 3325040,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "BAWANG",
    district_code: 3325050,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "TERSONO",
    district_code: 3325060,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "GRINGSING",
    district_code: 3325070,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "LIMPUNG",
    district_code: 3325080,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "BANYUPUTIH",
    district_code: 3325081,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "SUBAH",
    district_code: 3325090,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "PECALUNGAN",
    district_code: 3325091,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "TULIS",
    district_code: 3325100,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "KANDEMAN",
    district_code: 3325101,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "BATANG",
    district_code: 3325110,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "WARUNG ASEM",
    district_code: 3325120,
    regency_code: 3325,
    province_code: 33
  },
  {
    district_name: "KANDANGSERANG",
    district_code: 3326010,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "PANINGGARAN",
    district_code: 3326020,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "LEBAKBARANG",
    district_code: 3326030,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "PETUNGKRIONO",
    district_code: 3326040,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "TALUN",
    district_code: 3326050,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "DORO",
    district_code: 3326060,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "KARANGANYAR",
    district_code: 3326070,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "KAJEN",
    district_code: 3326080,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "KESESI",
    district_code: 3326090,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "SRAGI",
    district_code: 3326100,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "SIWALAN",
    district_code: 3326101,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "BOJONG",
    district_code: 3326110,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "WONOPRINGGO",
    district_code: 3326120,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "KEDUNGWUNI",
    district_code: 3326130,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "KARANGDADAP",
    district_code: 3326131,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "BUARAN",
    district_code: 3326140,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "TIRTO",
    district_code: 3326150,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "WIRADESA",
    district_code: 3326160,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "WONOKERTO",
    district_code: 3326161,
    regency_code: 3326,
    province_code: 33
  },
  {
    district_name: "MOGA",
    district_code: 3327010,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "WARUNGPRING",
    district_code: 3327011,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "PULOSARI",
    district_code: 3327020,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "BELIK",
    district_code: 3327030,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "WATUKUMPUL",
    district_code: 3327040,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "BODEH",
    district_code: 3327050,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "BANTARBOLANG",
    district_code: 3327060,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "RANDUDONGKAL",
    district_code: 3327070,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "PEMALANG",
    district_code: 3327080,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "TAMAN",
    district_code: 3327090,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "PETARUKAN",
    district_code: 3327100,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "AMPELGADING",
    district_code: 3327110,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "COMAL",
    district_code: 3327120,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "ULUJAMI",
    district_code: 3327130,
    regency_code: 3327,
    province_code: 33
  },
  {
    district_name: "MARGASARI",
    district_code: 3328010,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "BUMIJAWA",
    district_code: 3328020,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "BOJONG",
    district_code: 3328030,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "BALAPULANG",
    district_code: 3328040,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "PAGERBARANG",
    district_code: 3328050,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "LEBAKSIU",
    district_code: 3328060,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "JATINEGARA",
    district_code: 3328070,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "KEDUNG BANTENG",
    district_code: 3328080,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "PANGKAH",
    district_code: 3328090,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "SLAWI",
    district_code: 3328100,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "DUKUHWARU",
    district_code: 3328110,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "ADIWERNA",
    district_code: 3328120,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "DUKUHTURI",
    district_code: 3328130,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "TALANG",
    district_code: 3328140,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "TARUB",
    district_code: 3328150,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "KRAMAT",
    district_code: 3328160,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "SURADADI",
    district_code: 3328170,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "WARUREJA",
    district_code: 3328180,
    regency_code: 3328,
    province_code: 33
  },
  {
    district_name: "SALEM",
    district_code: 3329010,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "BANTARKAWUNG",
    district_code: 3329020,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "BUMIAYU",
    district_code: 3329030,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "PAGUYANGAN",
    district_code: 3329040,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "SIRAMPOG",
    district_code: 3329050,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "TONJONG",
    district_code: 3329060,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "LARANGAN",
    district_code: 3329070,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "KETANGGUNGAN",
    district_code: 3329080,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "BANJARHARJO",
    district_code: 3329090,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "LOSARI",
    district_code: 3329100,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "TANJUNG",
    district_code: 3329110,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "KERSANA",
    district_code: 3329120,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "BULAKAMBA",
    district_code: 3329130,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "WANASARI",
    district_code: 3329140,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "SONGGOM",
    district_code: 3329150,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "JATIBARANG",
    district_code: 3329160,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "BREBES",
    district_code: 3329170,
    regency_code: 3329,
    province_code: 33
  },
  {
    district_name: "MAGELANG SELATAN",
    district_code: 3371010,
    regency_code: 3371,
    province_code: 33
  },
  {
    district_name: "MAGELANG TENGAH",
    district_code: 3371011,
    regency_code: 3371,
    province_code: 33
  },
  {
    district_name: "MAGELANG UTARA",
    district_code: 3371020,
    regency_code: 3371,
    province_code: 33
  },
  {
    district_name: "LAWEYAN",
    district_code: 3372010,
    regency_code: 3372,
    province_code: 33
  },
  {
    district_name: "SERENGAN",
    district_code: 3372020,
    regency_code: 3372,
    province_code: 33
  },
  {
    district_name: "PASAR KLIWON",
    district_code: 3372030,
    regency_code: 3372,
    province_code: 33
  },
  {
    district_name: "JEBRES",
    district_code: 3372040,
    regency_code: 3372,
    province_code: 33
  },
  {
    district_name: "BANJARSARI",
    district_code: 3372050,
    regency_code: 3372,
    province_code: 33
  },
  {
    district_name: "ARGOMULYO",
    district_code: 3373010,
    regency_code: 3373,
    province_code: 33
  },
  {
    district_name: "TINGKIR",
    district_code: 3373020,
    regency_code: 3373,
    province_code: 33
  },
  {
    district_name: "SIDOMUKTI",
    district_code: 3373030,
    regency_code: 3373,
    province_code: 33
  },
  {
    district_name: "SIDOREJO",
    district_code: 3373040,
    regency_code: 3373,
    province_code: 33
  },
  {
    district_name: "MIJEN",
    district_code: 3374010,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "GUNUNG PATI",
    district_code: 3374020,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "BANYUMANIK",
    district_code: 3374030,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "GAJAH MUNGKUR",
    district_code: 3374040,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "SEMARANG SELATAN",
    district_code: 3374050,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "CANDISARI",
    district_code: 3374060,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "TEMBALANG",
    district_code: 3374070,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "PEDURUNGAN",
    district_code: 3374080,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "GENUK",
    district_code: 3374090,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "GAYAMSARI",
    district_code: 3374100,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "SEMARANG TIMUR",
    district_code: 3374110,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "SEMARANG UTARA",
    district_code: 3374120,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "SEMARANG TENGAH",
    district_code: 3374130,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "SEMARANG BARAT",
    district_code: 3374140,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "TUGU",
    district_code: 3374150,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "NGALIYAN",
    district_code: 3374160,
    regency_code: 3374,
    province_code: 33
  },
  {
    district_name: "PEKALONGAN BARAT",
    district_code: 3375010,
    regency_code: 3375,
    province_code: 33
  },
  {
    district_name: "PEKALONGAN TIMUR",
    district_code: 3375020,
    regency_code: 3375,
    province_code: 33
  },
  {
    district_name: "PEKALONGAN SELATAN",
    district_code: 3375030,
    regency_code: 3375,
    province_code: 33
  },
  {
    district_name: "PEKALONGAN UTARA",
    district_code: 3375040,
    regency_code: 3375,
    province_code: 33
  },
  {
    district_name: "TEGAL SELATAN",
    district_code: 3376010,
    regency_code: 3376,
    province_code: 33
  },
  {
    district_name: "TEGAL TIMUR",
    district_code: 3376020,
    regency_code: 3376,
    province_code: 33
  },
  {
    district_name: "TEGAL BARAT",
    district_code: 3376030,
    regency_code: 3376,
    province_code: 33
  },
  {
    district_name: "MARGADANA",
    district_code: 3376040,
    regency_code: 3376,
    province_code: 33
  },
  {
    district_name: "TEMON",
    district_code: 3401010,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "WATES",
    district_code: 3401020,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "PANJATAN",
    district_code: 3401030,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "GALUR",
    district_code: 3401040,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "LENDAH",
    district_code: 3401050,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "SENTOLO",
    district_code: 3401060,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "PENGASIH",
    district_code: 3401070,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "KOKAP",
    district_code: 3401080,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "GIRIMULYO",
    district_code: 3401090,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "NANGGULAN",
    district_code: 3401100,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "KALIBAWANG",
    district_code: 3401110,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "SAMIGALUH",
    district_code: 3401120,
    regency_code: 3401,
    province_code: 34
  },
  {
    district_name: "SRANDAKAN",
    district_code: 3402010,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "SANDEN",
    district_code: 3402020,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "KRETEK",
    district_code: 3402030,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "PUNDONG",
    district_code: 3402040,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "BAMBANG LIPURO",
    district_code: 3402050,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "PANDAK",
    district_code: 3402060,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "BANTUL",
    district_code: 3402070,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "JETIS",
    district_code: 3402080,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "IMOGIRI",
    district_code: 3402090,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "DLINGO",
    district_code: 3402100,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "PLERET",
    district_code: 3402110,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "PIYUNGAN",
    district_code: 3402120,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "BANGUNTAPAN",
    district_code: 3402130,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "SEWON",
    district_code: 3402140,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "KASIHAN",
    district_code: 3402150,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "PAJANGAN",
    district_code: 3402160,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "SEDAYU",
    district_code: 3402170,
    regency_code: 3402,
    province_code: 34
  },
  {
    district_name: "PANGGANG",
    district_code: 3403010,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "PURWOSARI",
    district_code: 3403011,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "PALIYAN",
    district_code: 3403020,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "SAPTO SARI",
    district_code: 3403030,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "TEPUS",
    district_code: 3403040,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "TANJUNGSARI",
    district_code: 3403041,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "RONGKOP",
    district_code: 3403050,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "GIRISUBO",
    district_code: 3403051,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "SEMANU",
    district_code: 3403060,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "PONJONG",
    district_code: 3403070,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "KARANGMOJO",
    district_code: 3403080,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "WONOSARI",
    district_code: 3403090,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "PLAYEN",
    district_code: 3403100,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "PATUK",
    district_code: 3403110,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "GEDANG SARI",
    district_code: 3403120,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "NGLIPAR",
    district_code: 3403130,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "NGAWEN",
    district_code: 3403140,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "SEMIN",
    district_code: 3403150,
    regency_code: 3403,
    province_code: 34
  },
  {
    district_name: "MOYUDAN",
    district_code: 3404010,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "MINGGIR",
    district_code: 3404020,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "SEYEGAN",
    district_code: 3404030,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "GODEAN",
    district_code: 3404040,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "GAMPING",
    district_code: 3404050,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "MLATI",
    district_code: 3404060,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "DEPOK",
    district_code: 3404070,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "BERBAH",
    district_code: 3404080,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "PRAMBANAN",
    district_code: 3404090,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "KALASAN",
    district_code: 3404100,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "NGEMPLAK",
    district_code: 3404110,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "NGAGLIK",
    district_code: 3404120,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "SLEMAN",
    district_code: 3404130,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "TEMPEL",
    district_code: 3404140,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "TURI",
    district_code: 3404150,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "PAKEM",
    district_code: 3404160,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "CANGKRINGAN",
    district_code: 3404170,
    regency_code: 3404,
    province_code: 34
  },
  {
    district_name: "MANTRIJERON",
    district_code: 3471010,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "KRATON",
    district_code: 3471020,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "MERGANGSAN",
    district_code: 3471030,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "UMBULHARJO",
    district_code: 3471040,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "KOTAGEDE",
    district_code: 3471050,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "GONDOKUSUMAN",
    district_code: 3471060,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "DANUREJAN",
    district_code: 3471070,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "PAKUALAMAN",
    district_code: 3471080,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "GONDOMANAN",
    district_code: 3471090,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "NGAMPILAN",
    district_code: 3471100,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "WIROBRAJAN",
    district_code: 3471110,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "GEDONG TENGEN",
    district_code: 3471120,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "JETIS",
    district_code: 3471130,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "TEGALREJO",
    district_code: 3471140,
    regency_code: 3471,
    province_code: 34
  },
  {
    district_name: "DONOROJO",
    district_code: 3501010,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "PUNUNG",
    district_code: 3501020,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "PRINGKUKU",
    district_code: 3501030,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "PACITAN",
    district_code: 3501040,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "KEBONAGUNG",
    district_code: 3501050,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "ARJOSARI",
    district_code: 3501060,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "NAWANGAN",
    district_code: 3501070,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "BANDAR",
    district_code: 3501080,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "TEGALOMBO",
    district_code: 3501090,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "TULAKAN",
    district_code: 3501100,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "NGADIROJO",
    district_code: 3501110,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "SUDIMORO",
    district_code: 3501120,
    regency_code: 3501,
    province_code: 35
  },
  {
    district_name: "NGRAYUN",
    district_code: 3502010,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "SLAHUNG",
    district_code: 3502020,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "BUNGKAL",
    district_code: 3502030,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "SAMBIT",
    district_code: 3502040,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "SAWOO",
    district_code: 3502050,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "SOOKO",
    district_code: 3502060,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "PUDAK",
    district_code: 3502061,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "PULUNG",
    district_code: 3502070,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "MLARAK",
    district_code: 3502080,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "SIMAN",
    district_code: 3502090,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "JETIS",
    district_code: 3502100,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "BALONG",
    district_code: 3502110,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "KAUMAN",
    district_code: 3502120,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "JAMBON",
    district_code: 3502130,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "BADEGAN",
    district_code: 3502140,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "SAMPUNG",
    district_code: 3502150,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "SUKOREJO",
    district_code: 3502160,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "PONOROGO",
    district_code: 3502170,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "BABADAN",
    district_code: 3502180,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "JENANGAN",
    district_code: 3502190,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "NGEBEL",
    district_code: 3502200,
    regency_code: 3502,
    province_code: 35
  },
  {
    district_name: "PANGGUL",
    district_code: 3503010,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "MUNJUNGAN",
    district_code: 3503020,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "WATULIMO",
    district_code: 3503030,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "KAMPAK",
    district_code: 3503040,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "DONGKO",
    district_code: 3503050,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "PULE",
    district_code: 3503060,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "KARANGAN",
    district_code: 3503070,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "SURUH",
    district_code: 3503071,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "GANDUSARI",
    district_code: 3503080,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "DURENAN",
    district_code: 3503090,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "POGALAN",
    district_code: 3503100,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "TRENGGALEK",
    district_code: 3503110,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "TUGU",
    district_code: 3503120,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "BENDUNGAN",
    district_code: 3503130,
    regency_code: 3503,
    province_code: 35
  },
  {
    district_name: "BESUKI",
    district_code: 3504010,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "BANDUNG",
    district_code: 3504020,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "PAKEL",
    district_code: 3504030,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "CAMPUR DARAT",
    district_code: 3504040,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "TANGGUNG GUNUNG",
    district_code: 3504050,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "KALIDAWIR",
    district_code: 3504060,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "PUCANG LABAN",
    district_code: 3504070,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "REJOTANGAN",
    district_code: 3504080,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "NGUNUT",
    district_code: 3504090,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "SUMBERGEMPOL",
    district_code: 3504100,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "BOYOLANGU",
    district_code: 3504110,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "TULUNGAGUNG",
    district_code: 3504120,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "KEDUNGWARU",
    district_code: 3504130,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "NGANTRU",
    district_code: 3504140,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "KARANGREJO",
    district_code: 3504150,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "KAUMAN",
    district_code: 3504160,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "GONDANG",
    district_code: 3504170,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "PAGER WOJO",
    district_code: 3504180,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "SENDANG",
    district_code: 3504190,
    regency_code: 3504,
    province_code: 35
  },
  {
    district_name: "BAKUNG",
    district_code: 3505010,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "WONOTIRTO",
    district_code: 3505020,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "PANGGUNGREJO",
    district_code: 3505030,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "WATES",
    district_code: 3505040,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "BINANGUN",
    district_code: 3505050,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "SUTOJAYAN",
    district_code: 3505060,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "KADEMANGAN",
    district_code: 3505070,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "KANIGORO",
    district_code: 3505080,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "TALUN",
    district_code: 3505090,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "SELOPURO",
    district_code: 3505100,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "KESAMBEN",
    district_code: 3505110,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "SELOREJO",
    district_code: 3505120,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "DOKO",
    district_code: 3505130,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "WLINGI",
    district_code: 3505140,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "GANDUSARI",
    district_code: 3505150,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "GARUM",
    district_code: 3505160,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "NGLEGOK",
    district_code: 3505170,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "SANANKULON",
    district_code: 3505180,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "PONGGOK",
    district_code: 3505190,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "SRENGAT",
    district_code: 3505200,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "WONODADI",
    district_code: 3505210,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "UDANAWU",
    district_code: 3505220,
    regency_code: 3505,
    province_code: 35
  },
  {
    district_name: "MOJO",
    district_code: 3506010,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "SEMEN",
    district_code: 3506020,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "NGADILUWIH",
    district_code: 3506030,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "KRAS",
    district_code: 3506040,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "RINGINREJO",
    district_code: 3506050,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "KANDAT",
    district_code: 3506060,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "WATES",
    district_code: 3506070,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "NGANCAR",
    district_code: 3506080,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "PLOSOKLATEN",
    district_code: 3506090,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "GURAH",
    district_code: 3506100,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "PUNCU",
    district_code: 3506110,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "KEPUNG",
    district_code: 3506120,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "KANDANGAN",
    district_code: 3506130,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "PARE",
    district_code: 3506140,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "BADAS",
    district_code: 3506141,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "KUNJANG",
    district_code: 3506150,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "PLEMAHAN",
    district_code: 3506160,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "PURWOASRI",
    district_code: 3506170,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "PAPAR",
    district_code: 3506180,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "PAGU",
    district_code: 3506190,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "KAYEN KIDUL",
    district_code: 3506191,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "GAMPENGREJO",
    district_code: 3506200,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "NGASEM",
    district_code: 3506201,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "BANYAKAN",
    district_code: 3506210,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "GROGOL",
    district_code: 3506220,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "TAROKAN",
    district_code: 3506230,
    regency_code: 3506,
    province_code: 35
  },
  {
    district_name: "DONOMULYO",
    district_code: 3507010,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "KALIPARE",
    district_code: 3507020,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "PAGAK",
    district_code: 3507030,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "BANTUR",
    district_code: 3507040,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "GEDANGAN",
    district_code: 3507050,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "SUMBERMANJING",
    district_code: 3507060,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "DAMPIT",
    district_code: 3507070,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "TIRTO YUDO",
    district_code: 3507080,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "AMPELGADING",
    district_code: 3507090,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "PONCOKUSUMO",
    district_code: 3507100,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "WAJAK",
    district_code: 3507110,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "TUREN",
    district_code: 3507120,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "BULULAWANG",
    district_code: 3507130,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "GONDANGLEGI",
    district_code: 3507140,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "PAGELARAN",
    district_code: 3507150,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "KEPANJEN",
    district_code: 3507160,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "SUMBER PUCUNG",
    district_code: 3507170,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "KROMENGAN",
    district_code: 3507180,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "NGAJUM",
    district_code: 3507190,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "WONOSARI",
    district_code: 3507200,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "WAGIR",
    district_code: 3507210,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "PAKISAJI",
    district_code: 3507220,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "TAJINAN",
    district_code: 3507230,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "TUMPANG",
    district_code: 3507240,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "PAKIS",
    district_code: 3507250,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "JABUNG",
    district_code: 3507260,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "LAWANG",
    district_code: 3507270,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "SINGOSARI",
    district_code: 3507280,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "KARANGPLOSO",
    district_code: 3507290,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "DAU",
    district_code: 3507300,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "PUJON",
    district_code: 3507310,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "NGANTANG",
    district_code: 3507320,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "KASEMBON",
    district_code: 3507330,
    regency_code: 3507,
    province_code: 35
  },
  {
    district_name: "TEMPURSARI",
    district_code: 3508010,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "PRONOJIWO",
    district_code: 3508020,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "CANDIPURO",
    district_code: 3508030,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "PASIRIAN",
    district_code: 3508040,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "TEMPEH",
    district_code: 3508050,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "LUMAJANG",
    district_code: 3508060,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "SUMBERSUKO",
    district_code: 3508061,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "TEKUNG",
    district_code: 3508070,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "KUNIR",
    district_code: 3508080,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "YOSOWILANGUN",
    district_code: 3508090,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "ROWOKANGKUNG",
    district_code: 3508100,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "JATIROTO",
    district_code: 3508110,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "RANDUAGUNG",
    district_code: 3508120,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "SUKODONO",
    district_code: 3508130,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "PADANG",
    district_code: 3508140,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "PASRUJAMBE",
    district_code: 3508150,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "SENDURO",
    district_code: 3508160,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "GUCIALIT",
    district_code: 3508170,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "KEDUNGJAJANG",
    district_code: 3508180,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "KLAKAH",
    district_code: 3508190,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "RANUYOSO",
    district_code: 3508200,
    regency_code: 3508,
    province_code: 35
  },
  {
    district_name: "KENCONG",
    district_code: 3509010,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "GUMUK MAS",
    district_code: 3509020,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "PUGER",
    district_code: 3509030,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "WULUHAN",
    district_code: 3509040,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "AMBULU",
    district_code: 3509050,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "TEMPUREJO",
    district_code: 3509060,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "SILO",
    district_code: 3509070,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "MAYANG",
    district_code: 3509080,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "MUMBULSARI",
    district_code: 3509090,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "JENGGAWAH",
    district_code: 3509100,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "AJUNG",
    district_code: 3509110,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "RAMBIPUJI",
    district_code: 3509120,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "BALUNG",
    district_code: 3509130,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "UMBULSARI",
    district_code: 3509140,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "SEMBORO",
    district_code: 3509150,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "JOMBANG",
    district_code: 3509160,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "SUMBER BARU",
    district_code: 3509170,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "TANGGUL",
    district_code: 3509180,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "BANGSALSARI",
    district_code: 3509190,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "PANTI",
    district_code: 3509200,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "SUKORAMBI",
    district_code: 3509210,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "ARJASA",
    district_code: 3509220,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "PAKUSARI",
    district_code: 3509230,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "KALISAT",
    district_code: 3509240,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "LEDOKOMBO",
    district_code: 3509250,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "SUMBERJAMBE",
    district_code: 3509260,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "SUKOWONO",
    district_code: 3509270,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "JELBUK",
    district_code: 3509280,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "KALIWATES",
    district_code: 3509710,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "SUMBERSARI",
    district_code: 3509720,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "PATRANG",
    district_code: 3509730,
    regency_code: 3509,
    province_code: 35
  },
  {
    district_name: "PESANGGARAN",
    district_code: 3510010,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "SILIRAGUNG",
    district_code: 3510011,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "BANGOREJO",
    district_code: 3510020,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "PURWOHARJO",
    district_code: 3510030,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "TEGALDLIMO",
    district_code: 3510040,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "MUNCAR",
    district_code: 3510050,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "CLURING",
    district_code: 3510060,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "GAMBIRAN",
    district_code: 3510070,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "TEGALSARI",
    district_code: 3510071,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "GLENMORE",
    district_code: 3510080,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "KALIBARU",
    district_code: 3510090,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "GENTENG",
    district_code: 3510100,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "SRONO",
    district_code: 3510110,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "ROGOJAMPI",
    district_code: 3510120,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "BLIMBINGSARI",
    district_code: 3510121,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "KABAT",
    district_code: 3510130,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "SINGOJURUH",
    district_code: 3510140,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "SEMPU",
    district_code: 3510150,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "SONGGON",
    district_code: 3510160,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "GLAGAH",
    district_code: 3510170,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "LICIN",
    district_code: 3510171,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "BANYUWANGI",
    district_code: 3510180,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "GIRI",
    district_code: 3510190,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "KALIPURO",
    district_code: 3510200,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "WONGSOREJO",
    district_code: 3510210,
    regency_code: 3510,
    province_code: 35
  },
  {
    district_name: "MAESAN",
    district_code: 3511010,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "GRUJUGAN",
    district_code: 3511020,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "TAMANAN",
    district_code: 3511030,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "JAMBESARI DARUS SHOLAH",
    district_code: 3511031,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "PUJER",
    district_code: 3511040,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "TLOGOSARI",
    district_code: 3511050,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "SUKOSARI",
    district_code: 3511060,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "SUMBER WRINGIN",
    district_code: 3511061,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "TAPEN",
    district_code: 3511070,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "WONOSARI",
    district_code: 3511080,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "TENGGARANG",
    district_code: 3511090,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "BONDOWOSO",
    district_code: 3511100,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "CURAH DAMI",
    district_code: 3511110,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "BINAKAL",
    district_code: 3511111,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "PAKEM",
    district_code: 3511120,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "WRINGIN",
    district_code: 3511130,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "TEGALAMPEL",
    district_code: 3511140,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "TAMAN KROCOK",
    district_code: 3511141,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "KLABANG",
    district_code: 3511150,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "IJEN",
    district_code: 3511151,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "BOTOLINGGO",
    district_code: 3511152,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "PRAJEKAN",
    district_code: 3511160,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "CERMEE",
    district_code: 3511170,
    regency_code: 3511,
    province_code: 35
  },
  {
    district_name: "SUMBERMALANG",
    district_code: 3512010,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "JATIBANTENG",
    district_code: 3512020,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "BANYUGLUGUR",
    district_code: 3512030,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "BESUKI",
    district_code: 3512040,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "SUBOH",
    district_code: 3512050,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "MLANDINGAN",
    district_code: 3512060,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "BUNGATAN",
    district_code: 3512070,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "KENDIT",
    district_code: 3512080,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "PANARUKAN",
    district_code: 3512090,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "SITUBONDO",
    district_code: 3512100,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "MANGARAN",
    district_code: 3512110,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "PANJI",
    district_code: 3512120,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "KAPONGAN",
    district_code: 3512130,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "ARJASA",
    district_code: 3512140,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "JANGKAR",
    district_code: 3512150,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "ASEMBAGUS",
    district_code: 3512160,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "BANYUPUTIH",
    district_code: 3512170,
    regency_code: 3512,
    province_code: 35
  },
  {
    district_name: "SUKAPURA",
    district_code: 3513010,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "SUMBER",
    district_code: 3513020,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "KURIPAN",
    district_code: 3513030,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "BANTARAN",
    district_code: 3513040,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "LECES",
    district_code: 3513050,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "TEGALSIWALAN",
    district_code: 3513060,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "BANYUANYAR",
    district_code: 3513070,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "TIRIS",
    district_code: 3513080,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "KRUCIL",
    district_code: 3513090,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "GADING",
    district_code: 3513100,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "PAKUNIRAN",
    district_code: 3513110,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "KOTAANYAR",
    district_code: 3513120,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "PAITON",
    district_code: 3513130,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "BESUK",
    district_code: 3513140,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "KRAKSAAN",
    district_code: 3513150,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "KREJENGAN",
    district_code: 3513160,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "PAJARAKAN",
    district_code: 3513170,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "MARON",
    district_code: 3513180,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "GENDING",
    district_code: 3513190,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "DRINGU",
    district_code: 3513200,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "WONOMERTO",
    district_code: 3513210,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "LUMBANG",
    district_code: 3513220,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "TONGAS",
    district_code: 3513230,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "SUMBERASIH",
    district_code: 3513240,
    regency_code: 3513,
    province_code: 35
  },
  {
    district_name: "PURWODADI",
    district_code: 3514010,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "TUTUR",
    district_code: 3514020,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "PUSPO",
    district_code: 3514030,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "TOSARI",
    district_code: 3514040,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "LUMBANG",
    district_code: 3514050,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "PASREPAN",
    district_code: 3514060,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "KEJAYAN",
    district_code: 3514070,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "WONOREJO",
    district_code: 3514080,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "PURWOSARI",
    district_code: 3514090,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "PRIGEN",
    district_code: 3514100,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "SUKOREJO",
    district_code: 3514110,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "PANDAAN",
    district_code: 3514120,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "GEMPOL",
    district_code: 3514130,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "BEJI",
    district_code: 3514140,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "BANGIL",
    district_code: 3514150,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "REMBANG",
    district_code: 3514160,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "KRATON",
    district_code: 3514170,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "POHJENTREK",
    district_code: 3514180,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "GONDANG WETAN",
    district_code: 3514190,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "REJOSO",
    district_code: 3514200,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "WINONGAN",
    district_code: 3514210,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "GRATI",
    district_code: 3514220,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "LEKOK",
    district_code: 3514230,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "NGULING",
    district_code: 3514240,
    regency_code: 3514,
    province_code: 35
  },
  {
    district_name: "TARIK",
    district_code: 3515010,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "PRAMBON",
    district_code: 3515020,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "KREMBUNG",
    district_code: 3515030,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "PORONG",
    district_code: 3515040,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "JABON",
    district_code: 3515050,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "TANGGULANGIN",
    district_code: 3515060,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "CANDI",
    district_code: 3515070,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "TULANGAN",
    district_code: 3515080,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "WONOAYU",
    district_code: 3515090,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "SUKODONO",
    district_code: 3515100,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "SIDOARJO",
    district_code: 3515110,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "BUDURAN",
    district_code: 3515120,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "SEDATI",
    district_code: 3515130,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "WARU",
    district_code: 3515140,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "GEDANGAN",
    district_code: 3515150,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "TAMAN",
    district_code: 3515160,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "KRIAN",
    district_code: 3515170,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "BALONG BENDO",
    district_code: 3515180,
    regency_code: 3515,
    province_code: 35
  },
  {
    district_name: "JATIREJO",
    district_code: 3516010,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "GONDANG",
    district_code: 3516020,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "PACET",
    district_code: 3516030,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "TRAWAS",
    district_code: 3516040,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "NGORO",
    district_code: 3516050,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "PUNGGING",
    district_code: 3516060,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "KUTOREJO",
    district_code: 3516070,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "MOJOSARI",
    district_code: 3516080,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "BANGSAL",
    district_code: 3516090,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "MOJOANYAR",
    district_code: 3516091,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "DLANGGU",
    district_code: 3516100,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "PURI",
    district_code: 3516110,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "TROWULAN",
    district_code: 3516120,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "SOOKO",
    district_code: 3516130,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "GEDEK",
    district_code: 3516140,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "KEMLAGI",
    district_code: 3516150,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "JETIS",
    district_code: 3516160,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "DAWAR BLANDONG",
    district_code: 3516170,
    regency_code: 3516,
    province_code: 35
  },
  {
    district_name: "BANDAR KEDUNG MULYO",
    district_code: 3517010,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "PERAK",
    district_code: 3517020,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "GUDO",
    district_code: 3517030,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "DIWEK",
    district_code: 3517040,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "NGORO",
    district_code: 3517050,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "MOJOWARNO",
    district_code: 3517060,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "BARENG",
    district_code: 3517070,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "WONOSALAM",
    district_code: 3517080,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "MOJOAGUNG",
    district_code: 3517090,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "SUMOBITO",
    district_code: 3517100,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "JOGO ROTO",
    district_code: 3517110,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "PETERONGAN",
    district_code: 3517120,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "JOMBANG",
    district_code: 3517130,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "MEGALUH",
    district_code: 3517140,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "TEMBELANG",
    district_code: 3517150,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "KESAMBEN",
    district_code: 3517160,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "KUDU",
    district_code: 3517170,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "NGUSIKAN",
    district_code: 3517171,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "PLOSO",
    district_code: 3517180,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "KABUH",
    district_code: 3517190,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "PLANDAAN",
    district_code: 3517200,
    regency_code: 3517,
    province_code: 35
  },
  {
    district_name: "SAWAHAN",
    district_code: 3518010,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "NGETOS",
    district_code: 3518020,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "BERBEK",
    district_code: 3518030,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "LOCERET",
    district_code: 3518040,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "PACE",
    district_code: 3518050,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "TANJUNGANOM",
    district_code: 3518060,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "PRAMBON",
    district_code: 3518070,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "NGRONGGOT",
    district_code: 3518080,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "KERTOSONO",
    district_code: 3518090,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "PATIANROWO",
    district_code: 3518100,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "BARON",
    district_code: 3518110,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "GONDANG",
    district_code: 3518120,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "SUKOMORO",
    district_code: 3518130,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "NGANJUK",
    district_code: 3518140,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "BAGOR",
    district_code: 3518150,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "WILANGAN",
    district_code: 3518160,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "REJOSO",
    district_code: 3518170,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "NGLUYU",
    district_code: 3518180,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "LENGKONG",
    district_code: 3518190,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "JATIKALEN",
    district_code: 3518200,
    regency_code: 3518,
    province_code: 35
  },
  {
    district_name: "KEBONSARI",
    district_code: 3519010,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "GEGER",
    district_code: 3519020,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "DOLOPO",
    district_code: 3519030,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "DAGANGAN",
    district_code: 3519040,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "WUNGU",
    district_code: 3519050,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "KARE",
    district_code: 3519060,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "GEMARANG",
    district_code: 3519070,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "SARADAN",
    district_code: 3519080,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "PILANGKENCENG",
    district_code: 3519090,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "MEJAYAN",
    district_code: 3519100,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "WONOASRI",
    district_code: 3519110,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "BALEREJO",
    district_code: 3519120,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "MADIUN",
    district_code: 3519130,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "SAWAHAN",
    district_code: 3519140,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "JIWAN",
    district_code: 3519150,
    regency_code: 3519,
    province_code: 35
  },
  {
    district_name: "PONCOL",
    district_code: 3520010,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "PARANG",
    district_code: 3520020,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "LEMBEYAN",
    district_code: 3520030,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "TAKERAN",
    district_code: 3520040,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "NGUNTORONADI",
    district_code: 3520041,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "KAWEDANAN",
    district_code: 3520050,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "MAGETAN",
    district_code: 3520060,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "NGARIBOYO",
    district_code: 3520061,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "PLAOSAN",
    district_code: 3520070,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "SIDOREJO",
    district_code: 3520071,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "PANEKAN",
    district_code: 3520080,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "SUKOMORO",
    district_code: 3520090,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "BENDO",
    district_code: 3520100,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "MAOSPATI",
    district_code: 3520110,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "KARANGREJO",
    district_code: 3520120,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "KARAS",
    district_code: 3520121,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "BARAT",
    district_code: 3520130,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "KARTOHARJO",
    district_code: 3520131,
    regency_code: 3520,
    province_code: 35
  },
  {
    district_name: "SINE",
    district_code: 3521010,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "NGRAMBE",
    district_code: 3521020,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "JOGOROGO",
    district_code: 3521030,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "KENDAL",
    district_code: 3521040,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "GENENG",
    district_code: 3521050,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "GERIH",
    district_code: 3521051,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "KWADUNGAN",
    district_code: 3521060,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "PANGKUR",
    district_code: 3521070,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "KARANGJATI",
    district_code: 3521080,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "BRINGIN",
    district_code: 3521090,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "PADAS",
    district_code: 3521100,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "KASREMAN",
    district_code: 3521101,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "NGAWI",
    district_code: 3521110,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "PARON",
    district_code: 3521120,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "KEDUNGGALAR",
    district_code: 3521130,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "PITU",
    district_code: 3521140,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "WIDODAREN",
    district_code: 3521150,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "MANTINGAN",
    district_code: 3521160,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "KARANGANYAR",
    district_code: 3521170,
    regency_code: 3521,
    province_code: 35
  },
  {
    district_name: "MARGOMULYO",
    district_code: 3522010,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "NGRAHO",
    district_code: 3522020,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "TAMBAKREJO",
    district_code: 3522030,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "NGAMBON",
    district_code: 3522040,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "SEKAR",
    district_code: 3522041,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "BUBULAN",
    district_code: 3522050,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "GONDANG",
    district_code: 3522051,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "TEMAYANG",
    district_code: 3522060,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "SUGIHWARAS",
    district_code: 3522070,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KEDUNGADEM",
    district_code: 3522080,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KEPOH BARU",
    district_code: 3522090,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "BAURENO",
    district_code: 3522100,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KANOR",
    district_code: 3522110,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "SUMBEREJO",
    district_code: 3522120,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "BALEN",
    district_code: 3522130,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "SUKOSEWU",
    district_code: 3522140,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KAPAS",
    district_code: 3522150,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "BOJONEGORO",
    district_code: 3522160,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "TRUCUK",
    district_code: 3522170,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "DANDER",
    district_code: 3522180,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "NGASEM",
    district_code: 3522190,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "GAYAM",
    district_code: 3522191,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KALITIDU",
    district_code: 3522200,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "MALO",
    district_code: 3522210,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "PURWOSARI",
    district_code: 3522220,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "PADANGAN",
    district_code: 3522230,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KASIMAN",
    district_code: 3522240,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KEDEWAN",
    district_code: 3522241,
    regency_code: 3522,
    province_code: 35
  },
  {
    district_name: "KENDURUAN",
    district_code: 3523010,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "BANGILAN",
    district_code: 3523020,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "SENORI",
    district_code: 3523030,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "SINGGAHAN",
    district_code: 3523040,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "MONTONG",
    district_code: 3523050,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "PARENGAN",
    district_code: 3523060,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "SOKO",
    district_code: 3523070,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "RENGEL",
    district_code: 3523080,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "GRABAGAN",
    district_code: 3523081,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "PLUMPANG",
    district_code: 3523090,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "WIDANG",
    district_code: 3523100,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "PALANG",
    district_code: 3523110,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "SEMANDING",
    district_code: 3523120,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "TUBAN",
    district_code: 3523130,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "JENU",
    district_code: 3523140,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "MERAKURAK",
    district_code: 3523150,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "KEREK",
    district_code: 3523160,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "TAMBAKBOYO",
    district_code: 3523170,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "JATIROGO",
    district_code: 3523180,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "BANCAR",
    district_code: 3523190,
    regency_code: 3523,
    province_code: 35
  },
  {
    district_name: "SUKORAME",
    district_code: 3524010,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "BLULUK",
    district_code: 3524020,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "NGIMBANG",
    district_code: 3524030,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "SAMBENG",
    district_code: 3524040,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "MANTUP",
    district_code: 3524050,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "KEMBANGBAHU",
    district_code: 3524060,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "SUGIO",
    district_code: 3524070,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "KEDUNGPRING",
    district_code: 3524080,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "MODO",
    district_code: 3524090,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "BABAT",
    district_code: 3524100,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "PUCUK",
    district_code: 3524110,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "SUKODADI",
    district_code: 3524120,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "LAMONGAN",
    district_code: 3524130,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "TIKUNG",
    district_code: 3524140,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "SARIREJO",
    district_code: 3524141,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "DEKET",
    district_code: 3524150,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "GLAGAH",
    district_code: 3524160,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "KARANGBINANGUN",
    district_code: 3524170,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "TURI",
    district_code: 3524180,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "KALITENGAH",
    district_code: 3524190,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "KARANG GENENG",
    district_code: 3524200,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "SEKARAN",
    district_code: 3524210,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "MADURAN",
    district_code: 3524220,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "LAREN",
    district_code: 3524230,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "SOLOKURO",
    district_code: 3524240,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "PACIRAN",
    district_code: 3524250,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "BRONDONG",
    district_code: 3524260,
    regency_code: 3524,
    province_code: 35
  },
  {
    district_name: "WRINGINANOM",
    district_code: 3525010,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "DRIYOREJO",
    district_code: 3525020,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "KEDAMEAN",
    district_code: 3525030,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "MENGANTI",
    district_code: 3525040,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "CERME",
    district_code: 3525050,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "BENJENG",
    district_code: 3525060,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "BALONGPANGGANG",
    district_code: 3525070,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "DUDUKSAMPEYAN",
    district_code: 3525080,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "KEBOMAS",
    district_code: 3525090,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "GRESIK",
    district_code: 3525100,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "MANYAR",
    district_code: 3525110,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "BUNGAH",
    district_code: 3525120,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "SIDAYU",
    district_code: 3525130,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "DUKUN",
    district_code: 3525140,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "PANCENG",
    district_code: 3525150,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "UJUNGPANGKAH",
    district_code: 3525160,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "SANGKAPURA",
    district_code: 3525170,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "TAMBAK",
    district_code: 3525180,
    regency_code: 3525,
    province_code: 35
  },
  {
    district_name: "KAMAL",
    district_code: 3526010,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "LABANG",
    district_code: 3526020,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "KWANYAR",
    district_code: 3526030,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "MODUNG",
    district_code: 3526040,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "BLEGA",
    district_code: 3526050,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "KONANG",
    district_code: 3526060,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "GALIS",
    district_code: 3526070,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "TANAH MERAH",
    district_code: 3526080,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "TRAGAH",
    district_code: 3526090,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "SOCAH",
    district_code: 3526100,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "BANGKALAN",
    district_code: 3526110,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "BURNEH",
    district_code: 3526120,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "AROSBAYA",
    district_code: 3526130,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "GEGER",
    district_code: 3526140,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "KOKOP",
    district_code: 3526150,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "TANJUNGBUMI",
    district_code: 3526160,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "SEPULU",
    district_code: 3526170,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "KLAMPIS",
    district_code: 3526180,
    regency_code: 3526,
    province_code: 35
  },
  {
    district_name: "SRESEH",
    district_code: 3527010,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "TORJUN",
    district_code: 3527020,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "PANGARENGAN",
    district_code: 3527021,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "SAMPANG",
    district_code: 3527030,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "CAMPLONG",
    district_code: 3527040,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "OMBEN",
    district_code: 3527050,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "KEDUNGDUNG",
    district_code: 3527060,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "JRENGIK",
    district_code: 3527070,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "TAMBELANGAN",
    district_code: 3527080,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "BANYUATES",
    district_code: 3527090,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "ROBATAL",
    district_code: 3527100,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "KARANG PENANG",
    district_code: 3527101,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "KETAPANG",
    district_code: 3527110,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "SOKOBANAH",
    district_code: 3527120,
    regency_code: 3527,
    province_code: 35
  },
  {
    district_name: "TLANAKAN",
    district_code: 3528010,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PADEMAWU",
    district_code: 3528020,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "GALIS",
    district_code: 3528030,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "LARANGAN",
    district_code: 3528040,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PAMEKASAN",
    district_code: 3528050,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PROPPO",
    district_code: 3528060,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PALENGAAN",
    district_code: 3528070,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PEGANTENAN",
    district_code: 3528080,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "KADUR",
    district_code: 3528090,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PAKONG",
    district_code: 3528100,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "WARU",
    district_code: 3528110,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "BATU MARMAR",
    district_code: 3528120,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PASEAN",
    district_code: 3528130,
    regency_code: 3528,
    province_code: 35
  },
  {
    district_name: "PRAGAAN",
    district_code: 3529010,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "BLUTO",
    district_code: 3529020,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "SARONGGI",
    district_code: 3529030,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "GILIGENTENG",
    district_code: 3529040,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "TALANGO",
    district_code: 3529050,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "KALIANGET",
    district_code: 3529060,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "KOTA SUMENEP",
    district_code: 3529070,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "BATUAN",
    district_code: 3529071,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "LENTENG",
    district_code: 3529080,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "GANDING",
    district_code: 3529090,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "GULUK GULUK",
    district_code: 3529100,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "PASONGSONGAN",
    district_code: 3529110,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "AMBUNTEN",
    district_code: 3529120,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "RUBARU",
    district_code: 3529130,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "DASUK",
    district_code: 3529140,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "MANDING",
    district_code: 3529150,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "BATUPUTIH",
    district_code: 3529160,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "GAPURA",
    district_code: 3529170,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "BATANG BATANG",
    district_code: 3529180,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "DUNGKEK",
    district_code: 3529190,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "NONGGUNONG",
    district_code: 3529200,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "GAYAM",
    district_code: 3529210,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "RAAS",
    district_code: 3529220,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "SAPEKEN",
    district_code: 3529230,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "ARJASA",
    district_code: 3529240,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "KANGAYAN",
    district_code: 3529241,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "MASALEMBU",
    district_code: 3529250,
    regency_code: 3529,
    province_code: 35
  },
  {
    district_name: "MOJOROTO",
    district_code: 3571010,
    regency_code: 3571,
    province_code: 35
  },
  {
    district_name: "KOTA KEDIRI",
    district_code: 3571020,
    regency_code: 3571,
    province_code: 35
  },
  {
    district_name: "PESANTREN",
    district_code: 3571030,
    regency_code: 3571,
    province_code: 35
  },
  {
    district_name: "SUKOREJO",
    district_code: 3572010,
    regency_code: 3572,
    province_code: 35
  },
  {
    district_name: "KEPANJENKIDUL",
    district_code: 3572020,
    regency_code: 3572,
    province_code: 35
  },
  {
    district_name: "SANANWETAN",
    district_code: 3572030,
    regency_code: 3572,
    province_code: 35
  },
  {
    district_name: "KEDUNGKANDANG",
    district_code: 3573010,
    regency_code: 3573,
    province_code: 35
  },
  {
    district_name: "SUKUN",
    district_code: 3573020,
    regency_code: 3573,
    province_code: 35
  },
  {
    district_name: "KLOJEN",
    district_code: 3573030,
    regency_code: 3573,
    province_code: 35
  },
  {
    district_name: "BLIMBING",
    district_code: 3573040,
    regency_code: 3573,
    province_code: 35
  },
  {
    district_name: "LOWOKWARU",
    district_code: 3573050,
    regency_code: 3573,
    province_code: 35
  },
  {
    district_name: "KADEMANGAN",
    district_code: 3574010,
    regency_code: 3574,
    province_code: 35
  },
  {
    district_name: "KEDOPOK",
    district_code: 3574011,
    regency_code: 3574,
    province_code: 35
  },
  {
    district_name: "WONOASIH",
    district_code: 3574020,
    regency_code: 3574,
    province_code: 35
  },
  {
    district_name: "MAYANGAN",
    district_code: 3574030,
    regency_code: 3574,
    province_code: 35
  },
  {
    district_name: "KANIGARAN",
    district_code: 3574031,
    regency_code: 3574,
    province_code: 35
  },
  {
    district_name: "GADINGREJO",
    district_code: 3575010,
    regency_code: 3575,
    province_code: 35
  },
  {
    district_name: "PURWOREJO",
    district_code: 3575020,
    regency_code: 3575,
    province_code: 35
  },
  {
    district_name: "BUGULKIDUL",
    district_code: 3575030,
    regency_code: 3575,
    province_code: 35
  },
  {
    district_name: "PANGGUNGREJO",
    district_code: 3575031,
    regency_code: 3575,
    province_code: 35
  },
  {
    district_name: "PRAJURIT KULON",
    district_code: 3576010,
    regency_code: 3576,
    province_code: 35
  },
  {
    district_name: "MAGERSARI",
    district_code: 3576020,
    regency_code: 3576,
    province_code: 35
  },
  {
    district_name: "KRANGGAN",
    district_code: 3576021,
    regency_code: 3576,
    province_code: 35
  },
  {
    district_name: "MANGU HARJO",
    district_code: 3577010,
    regency_code: 3577,
    province_code: 35
  },
  {
    district_name: "TAMAN",
    district_code: 3577020,
    regency_code: 3577,
    province_code: 35
  },
  {
    district_name: "KARTOHARJO",
    district_code: 3577030,
    regency_code: 3577,
    province_code: 35
  },
  {
    district_name: "KARANG PILANG",
    district_code: 3578010,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "JAMBANGAN",
    district_code: 3578020,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "GAYUNGAN",
    district_code: 3578030,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "WONOCOLO",
    district_code: 3578040,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "TENGGILIS MEJOYO",
    district_code: 3578050,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "GUNUNG ANYAR",
    district_code: 3578060,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "RUNGKUT",
    district_code: 3578070,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "SUKOLILO",
    district_code: 3578080,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "MULYOREJO",
    district_code: 3578090,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "GUBENG",
    district_code: 3578100,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "WONOKROMO",
    district_code: 3578110,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "DUKUH PAKIS",
    district_code: 3578120,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "WIYUNG",
    district_code: 3578130,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "LAKARSANTRI",
    district_code: 3578140,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "SAMBIKEREP",
    district_code: 3578141,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "TANDES",
    district_code: 3578150,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "SUKO MANUNGGAL",
    district_code: 3578160,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "SAWAHAN",
    district_code: 3578170,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "TEGALSARI",
    district_code: 3578180,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "GENTENG",
    district_code: 3578190,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "TAMBAKSARI",
    district_code: 3578200,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "KENJERAN",
    district_code: 3578210,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "BULAK",
    district_code: 3578211,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "SIMOKERTO",
    district_code: 3578220,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "SEMAMPIR",
    district_code: 3578230,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "PABEAN CANTIAN",
    district_code: 3578240,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "BUBUTAN",
    district_code: 3578250,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "KREMBANGAN",
    district_code: 3578260,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "ASEMROWO",
    district_code: 3578270,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "BENOWO",
    district_code: 3578280,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "PAKAL",
    district_code: 3578281,
    regency_code: 3578,
    province_code: 35
  },
  {
    district_name: "BATU",
    district_code: 3579010,
    regency_code: 3579,
    province_code: 35
  },
  {
    district_name: "JUNREJO",
    district_code: 3579020,
    regency_code: 3579,
    province_code: 35
  },
  {
    district_name: "BUMIAJI",
    district_code: 3579030,
    regency_code: 3579,
    province_code: 35
  },
  {
    district_name: "SUMUR",
    district_code: 3601010,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIMANGGU",
    district_code: 3601020,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIBALIUNG",
    district_code: 3601030,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIBITUNG",
    district_code: 3601031,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIKEUSIK",
    district_code: 3601040,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIGEULIS",
    district_code: 3601050,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "PANIMBANG",
    district_code: 3601060,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "SOBANG",
    district_code: 3601061,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "MUNJUL",
    district_code: 3601070,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "ANGSANA",
    district_code: 3601071,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "SINDANGRESMI",
    district_code: 3601072,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "PICUNG",
    district_code: 3601080,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "BOJONG",
    district_code: 3601090,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "SAKETI",
    district_code: 3601100,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CISATA",
    district_code: 3601101,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "PAGELARAN",
    district_code: 3601110,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "PATIA",
    district_code: 3601111,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "SUKARESMI",
    district_code: 3601112,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "LABUAN",
    district_code: 3601120,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CARITA",
    district_code: 3601121,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "JIPUT",
    district_code: 3601130,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIKEDAL",
    district_code: 3601131,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "MENES",
    district_code: 3601140,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "PULOSARI",
    district_code: 3601141,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "MANDALAWANGI",
    district_code: 3601150,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIMANUK",
    district_code: 3601160,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CIPEUCANG",
    district_code: 3601161,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "BANJAR",
    district_code: 3601170,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "KADUHEJO",
    district_code: 3601171,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "MEKARJAYA",
    district_code: 3601172,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "PANDEGLANG",
    district_code: 3601180,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "MAJASARI",
    district_code: 3601181,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "CADASARI",
    district_code: 3601190,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "KARANGTANJUNG",
    district_code: 3601191,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "KORONCONG",
    district_code: 3601192,
    regency_code: 3601,
    province_code: 36
  },
  {
    district_name: "MALINGPING",
    district_code: 3602010,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "WANASALAM",
    district_code: 3602011,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "PANGGARANGAN",
    district_code: 3602020,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIHARA",
    district_code: 3602021,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "BAYAH",
    district_code: 3602030,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CILOGRANG",
    district_code: 3602031,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIBEBER",
    district_code: 3602040,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIJAKU",
    district_code: 3602050,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIGEMBLONG",
    district_code: 3602051,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "BANJARSARI",
    district_code: 3602060,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CILELES",
    district_code: 3602070,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "GUNUNG KENCANA",
    district_code: 3602080,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "BOJONGMANIK",
    district_code: 3602090,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIRINTEN",
    district_code: 3602091,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "LEUWIDAMAR",
    district_code: 3602100,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "MUNCANG",
    district_code: 3602110,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "SOBANG",
    district_code: 3602111,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIPANAS",
    district_code: 3602120,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "LEBAKGEDONG",
    district_code: 3602121,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "SAJIRA",
    district_code: 3602130,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIMARGA",
    district_code: 3602140,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIKULUR",
    district_code: 3602150,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "WARUNGGUNUNG",
    district_code: 3602160,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CIBADAK",
    district_code: 3602170,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "RANGKASBITUNG",
    district_code: 3602180,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "KALANGANYAR",
    district_code: 3602181,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "MAJA",
    district_code: 3602190,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CURUGBITUNG",
    district_code: 3602191,
    regency_code: 3602,
    province_code: 36
  },
  {
    district_name: "CISOKA",
    district_code: 3603010,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "SOLEAR",
    district_code: 3603011,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "TIGARAKSA",
    district_code: 3603020,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "JAMBE",
    district_code: 3603021,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "CIKUPA",
    district_code: 3603030,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "PANONGAN",
    district_code: 3603040,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "CURUG",
    district_code: 3603050,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "KELAPA DUA",
    district_code: 3603051,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "LEGOK",
    district_code: 3603060,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "PAGEDANGAN",
    district_code: 3603070,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "CISAUK",
    district_code: 3603081,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "PASARKEMIS",
    district_code: 3603120,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "SINDANG JAYA",
    district_code: 3603121,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "BALARAJA",
    district_code: 3603130,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "JAYANTI",
    district_code: 3603131,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "SUKAMULYA",
    district_code: 3603132,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "KRESEK",
    district_code: 3603140,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "GUNUNG KALER",
    district_code: 3603141,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "KRONJO",
    district_code: 3603150,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "MEKAR BARU",
    district_code: 3603151,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "MAUK",
    district_code: 3603160,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "KEMIRI",
    district_code: 3603161,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "SUKADIRI",
    district_code: 3603162,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "RAJEG",
    district_code: 3603170,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "SEPATAN",
    district_code: 3603180,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "SEPATAN TIMUR",
    district_code: 3603181,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "PAKUHAJI",
    district_code: 3603190,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "TELUKNAGA",
    district_code: 3603200,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "KOSAMBI",
    district_code: 3603210,
    regency_code: 3603,
    province_code: 36
  },
  {
    district_name: "CINANGKA",
    district_code: 3604010,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "PADARINCANG",
    district_code: 3604020,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "CIOMAS",
    district_code: 3604030,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "PABUARAN",
    district_code: 3604040,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "GUNUNG SARI",
    district_code: 3604041,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "BAROS",
    district_code: 3604050,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "PETIR",
    district_code: 3604060,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "TUNJUNG TEJA",
    district_code: 3604061,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "CIKEUSAL",
    district_code: 3604080,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "PAMARAYAN",
    district_code: 3604090,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "BANDUNG",
    district_code: 3604091,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "JAWILAN",
    district_code: 3604100,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "KOPO",
    district_code: 3604110,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "CIKANDE",
    district_code: 3604120,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "KIBIN",
    district_code: 3604121,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "KRAGILAN",
    district_code: 3604130,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "WARINGINKURUNG",
    district_code: 3604180,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "MANCAK",
    district_code: 3604190,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "ANYAR",
    district_code: 3604200,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "BOJONEGARA",
    district_code: 3604210,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "PULO AMPEL",
    district_code: 3604211,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "KRAMATWATU",
    district_code: 3604220,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "CIRUAS",
    district_code: 3604240,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "PONTANG",
    district_code: 3604250,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "LEBAK WANGI",
    district_code: 3604251,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "CARENANG",
    district_code: 3604260,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "BINUANG",
    district_code: 3604261,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "TIRTAYASA",
    district_code: 3604270,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "TANARA",
    district_code: 3604271,
    regency_code: 3604,
    province_code: 36
  },
  {
    district_name: "CILEDUG",
    district_code: 3671010,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "LARANGAN",
    district_code: 3671011,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "KARANG TENGAH",
    district_code: 3671012,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "CIPONDOH",
    district_code: 3671020,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "PINANG",
    district_code: 3671021,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "TANGERANG",
    district_code: 3671030,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "KARAWACI",
    district_code: 3671031,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "JATI UWUNG",
    district_code: 3671040,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "CIBODAS",
    district_code: 3671041,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "PERIUK",
    district_code: 3671042,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "BATUCEPER",
    district_code: 3671050,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "NEGLASARI",
    district_code: 3671051,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "BENDA",
    district_code: 3671060,
    regency_code: 3671,
    province_code: 36
  },
  {
    district_name: "CIWANDAN",
    district_code: 3672010,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "CITANGKIL",
    district_code: 3672011,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "PULOMERAK",
    district_code: 3672020,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "PURWAKARTA",
    district_code: 3672021,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "GROGOL",
    district_code: 3672022,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "CILEGON",
    district_code: 3672030,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "JOMBANG",
    district_code: 3672031,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "CIBEBER",
    district_code: 3672040,
    regency_code: 3672,
    province_code: 36
  },
  {
    district_name: "CURUG",
    district_code: 3673010,
    regency_code: 3673,
    province_code: 36
  },
  {
    district_name: "WALANTAKA",
    district_code: 3673020,
    regency_code: 3673,
    province_code: 36
  },
  {
    district_name: "CIPOCOK JAYA",
    district_code: 3673030,
    regency_code: 3673,
    province_code: 36
  },
  {
    district_name: "SERANG",
    district_code: 3673040,
    regency_code: 3673,
    province_code: 36
  },
  {
    district_name: "TAKTAKAN",
    district_code: 3673050,
    regency_code: 3673,
    province_code: 36
  },
  {
    district_name: "KASEMEN",
    district_code: 3673060,
    regency_code: 3673,
    province_code: 36
  },
  {
    district_name: "SETU",
    district_code: 3674010,
    regency_code: 3674,
    province_code: 36
  },
  {
    district_name: "SERPONG",
    district_code: 3674020,
    regency_code: 3674,
    province_code: 36
  },
  {
    district_name: "PAMULANG",
    district_code: 3674030,
    regency_code: 3674,
    province_code: 36
  },
  {
    district_name: "CIPUTAT",
    district_code: 3674040,
    regency_code: 3674,
    province_code: 36
  },
  {
    district_name: "CIPUTAT TIMUR",
    district_code: 3674050,
    regency_code: 3674,
    province_code: 36
  },
  {
    district_name: "PONDOK AREN",
    district_code: 3674060,
    regency_code: 3674,
    province_code: 36
  },
  {
    district_name: "SERPONG UTARA",
    district_code: 3674070,
    regency_code: 3674,
    province_code: 36
  },
  {
    district_name: "MELAYA",
    district_code: 5101010,
    regency_code: 5101,
    province_code: 51
  },
  {
    district_name: "NEGARA",
    district_code: 5101020,
    regency_code: 5101,
    province_code: 51
  },
  {
    district_name: "JEMBRANA",
    district_code: 5101021,
    regency_code: 5101,
    province_code: 51
  },
  {
    district_name: "MENDOYO",
    district_code: 5101030,
    regency_code: 5101,
    province_code: 51
  },
  {
    district_name: "PEKUTATAN",
    district_code: 5101040,
    regency_code: 5101,
    province_code: 51
  },
  {
    district_name: "SELEMADEG",
    district_code: 5102010,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "SELEMADEG TIMUR",
    district_code: 5102011,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "SELEMADEG BARAT",
    district_code: 5102012,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "KERAMBITAN",
    district_code: 5102020,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "TABANAN",
    district_code: 5102030,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "KEDIRI",
    district_code: 5102040,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "MARGA",
    district_code: 5102050,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "BATURITI",
    district_code: 5102060,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "PENEBEL",
    district_code: 5102070,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "PUPUAN",
    district_code: 5102080,
    regency_code: 5102,
    province_code: 51
  },
  {
    district_name: "KUTA SELATAN",
    district_code: 5103010,
    regency_code: 5103,
    province_code: 51
  },
  {
    district_name: "KUTA",
    district_code: 5103020,
    regency_code: 5103,
    province_code: 51
  },
  {
    district_name: "KUTA UTARA",
    district_code: 5103030,
    regency_code: 5103,
    province_code: 51
  },
  {
    district_name: "MENGWI",
    district_code: 5103040,
    regency_code: 5103,
    province_code: 51
  },
  {
    district_name: "ABIANSEMAL",
    district_code: 5103050,
    regency_code: 5103,
    province_code: 51
  },
  {
    district_name: "PETANG",
    district_code: 5103060,
    regency_code: 5103,
    province_code: 51
  },
  {
    district_name: "SUKAWATI",
    district_code: 5104010,
    regency_code: 5104,
    province_code: 51
  },
  {
    district_name: "BLAHBATUH",
    district_code: 5104020,
    regency_code: 5104,
    province_code: 51
  },
  {
    district_name: "GIANYAR",
    district_code: 5104030,
    regency_code: 5104,
    province_code: 51
  },
  {
    district_name: "TAMPAKSIRING",
    district_code: 5104040,
    regency_code: 5104,
    province_code: 51
  },
  {
    district_name: "UBUD",
    district_code: 5104050,
    regency_code: 5104,
    province_code: 51
  },
  {
    district_name: "TEGALLALANG",
    district_code: 5104060,
    regency_code: 5104,
    province_code: 51
  },
  {
    district_name: "PAYANGAN",
    district_code: 5104070,
    regency_code: 5104,
    province_code: 51
  },
  {
    district_name: "NUSAPENIDA",
    district_code: 5105010,
    regency_code: 5105,
    province_code: 51
  },
  {
    district_name: "BANJARANGKAN",
    district_code: 5105020,
    regency_code: 5105,
    province_code: 51
  },
  {
    district_name: "KLUNGKUNG",
    district_code: 5105030,
    regency_code: 5105,
    province_code: 51
  },
  {
    district_name: "DAWAN",
    district_code: 5105040,
    regency_code: 5105,
    province_code: 51
  },
  {
    district_name: "SUSUT",
    district_code: 5106010,
    regency_code: 5106,
    province_code: 51
  },
  {
    district_name: "BANGLI",
    district_code: 5106020,
    regency_code: 5106,
    province_code: 51
  },
  {
    district_name: "TEMBUKU",
    district_code: 5106030,
    regency_code: 5106,
    province_code: 51
  },
  {
    district_name: "KINTAMANI",
    district_code: 5106040,
    regency_code: 5106,
    province_code: 51
  },
  {
    district_name: "RENDANG",
    district_code: 5107010,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "SIDEMEN",
    district_code: 5107020,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "MANGGIS",
    district_code: 5107030,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "KARANGASEM",
    district_code: 5107040,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "ABANG",
    district_code: 5107050,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "BEBANDEM",
    district_code: 5107060,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "SELAT",
    district_code: 5107070,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "KUBU",
    district_code: 5107080,
    regency_code: 5107,
    province_code: 51
  },
  {
    district_name: "GEROKGAK",
    district_code: 5108010,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "SERIRIT",
    district_code: 5108020,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "BUSUNGBIU",
    district_code: 5108030,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "BANJAR",
    district_code: 5108040,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "SUKASADA",
    district_code: 5108050,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "BULELENG",
    district_code: 5108060,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "SAWAN",
    district_code: 5108070,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "KUBUTAMBAHAN",
    district_code: 5108080,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "TEJAKULA",
    district_code: 5108090,
    regency_code: 5108,
    province_code: 51
  },
  {
    district_name: "DENPASAR SELATAN",
    district_code: 5171010,
    regency_code: 5171,
    province_code: 51
  },
  {
    district_name: "DENPASAR TIMUR",
    district_code: 5171020,
    regency_code: 5171,
    province_code: 51
  },
  {
    district_name: "DENPASAR BARAT",
    district_code: 5171030,
    regency_code: 5171,
    province_code: 51
  },
  {
    district_name: "DENPASAR UTARA",
    district_code: 5171031,
    regency_code: 5171,
    province_code: 51
  },
  {
    district_name: "SEKOTONG",
    district_code: 5201010,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "LEMBAR",
    district_code: 5201011,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "GERUNG",
    district_code: 5201020,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "LABU API",
    district_code: 5201030,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "KEDIRI",
    district_code: 5201040,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "KURIPAN",
    district_code: 5201041,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "NARMADA",
    district_code: 5201050,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "LINGSAR",
    district_code: 5201051,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "GUNUNG SARI",
    district_code: 5201060,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "BATU LAYAR",
    district_code: 5201061,
    regency_code: 5201,
    province_code: 52
  },
  {
    district_name: "PRAYA BARAT",
    district_code: 5202010,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "PRAYA BARAT DAYA",
    district_code: 5202011,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "PUJUT",
    district_code: 5202020,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "PRAYA TIMUR",
    district_code: 5202030,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "JANAPRIA",
    district_code: 5202040,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "KOPANG",
    district_code: 5202050,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "PRAYA",
    district_code: 5202060,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "PRAYA TENGAH",
    district_code: 5202061,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "JONGGAT",
    district_code: 5202070,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "PRINGGARATA",
    district_code: 5202080,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "BATUKLIANG",
    district_code: 5202090,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "BATUKLIANG UTARA",
    district_code: 5202091,
    regency_code: 5202,
    province_code: 52
  },
  {
    district_name: "KERUAK",
    district_code: 5203010,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "JEROWARU",
    district_code: 5203011,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SAKRA",
    district_code: 5203020,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SAKRA BARAT",
    district_code: 5203021,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SAKRA TIMUR",
    district_code: 5203022,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "TERARA",
    district_code: 5203030,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "MONTONG GADING",
    district_code: 5203031,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SIKUR",
    district_code: 5203040,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "MASBAGIK",
    district_code: 5203050,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "PRINGGASELA",
    district_code: 5203051,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SUKAMULIA",
    district_code: 5203060,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SURALAGA",
    district_code: 5203061,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SELONG",
    district_code: 5203070,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "LABUHAN HAJI",
    district_code: 5203071,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "PRINGGABAYA",
    district_code: 5203080,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SUELA",
    district_code: 5203081,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "AIKMEL",
    district_code: 5203090,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "WANASABA",
    district_code: 5203091,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SEMBALUN",
    district_code: 5203092,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "SAMBELIA",
    district_code: 5203100,
    regency_code: 5203,
    province_code: 52
  },
  {
    district_name: "LUNYUK",
    district_code: 5204020,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "ORONG TELU",
    district_code: 5204021,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "ALAS",
    district_code: 5204050,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "ALAS BARAT",
    district_code: 5204051,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "BUER",
    district_code: 5204052,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "UTAN",
    district_code: 5204061,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "RHEE",
    district_code: 5204062,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "BATULANTEH",
    district_code: 5204070,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "SUMBAWA",
    district_code: 5204080,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "LABUHAN BADAS",
    district_code: 5204081,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "UNTER IWES",
    district_code: 5204082,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "MOYOHILIR",
    district_code: 5204090,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "MOYO UTARA",
    district_code: 5204091,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "MOYOHULU",
    district_code: 5204100,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "ROPANG",
    district_code: 5204110,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "LENANGGUAR",
    district_code: 5204111,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "LANTUNG",
    district_code: 5204112,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "LAPE",
    district_code: 5204121,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "LOPOK",
    district_code: 5204122,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "PLAMPANG",
    district_code: 5204130,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "LABANGKA",
    district_code: 5204131,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "MARONGE",
    district_code: 5204132,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "EMPANG",
    district_code: 5204140,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "TARANO",
    district_code: 5204141,
    regency_code: 5204,
    province_code: 52
  },
  {
    district_name: "HU'U",
    district_code: 5205010,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "PAJO",
    district_code: 5205011,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "DOMPU",
    district_code: 5205020,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "WOJA",
    district_code: 5205030,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "KILO",
    district_code: 5205040,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "KEMPO",
    district_code: 5205050,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "MANGGALEWA",
    district_code: 5205051,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "PEKAT",
    district_code: 5205060,
    regency_code: 5205,
    province_code: 52
  },
  {
    district_name: "MONTA",
    district_code: 5206010,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "PARADO",
    district_code: 5206011,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "BOLO",
    district_code: 5206020,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "MADA PANGGA",
    district_code: 5206021,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "WOHA",
    district_code: 5206030,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "BELO",
    district_code: 5206040,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "PALIBELO",
    district_code: 5206041,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "WAWO",
    district_code: 5206050,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "LANGGUDU",
    district_code: 5206051,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "LAMBITU",
    district_code: 5206052,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "SAPE",
    district_code: 5206060,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "LAMBU",
    district_code: 5206061,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "WERA",
    district_code: 5206070,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "AMBALAWI",
    district_code: 5206071,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "DONGGO",
    district_code: 5206080,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "SOROMANDI",
    district_code: 5206081,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "SANGGAR",
    district_code: 5206090,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "TAMBORA",
    district_code: 5206091,
    regency_code: 5206,
    province_code: 52
  },
  {
    district_name: "SEKONGKANG",
    district_code: 5207010,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "JEREWEH",
    district_code: 5207020,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "MALUK",
    district_code: 5207021,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "TALIWANG",
    district_code: 5207030,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "BRANG ENE",
    district_code: 5207031,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "BRANG REA",
    district_code: 5207040,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "SETELUK",
    district_code: 5207050,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "POTO TANO",
    district_code: 5207051,
    regency_code: 5207,
    province_code: 52
  },
  {
    district_name: "PEMENANG",
    district_code: 5208010,
    regency_code: 5208,
    province_code: 52
  },
  {
    district_name: "TANJUNG",
    district_code: 5208020,
    regency_code: 5208,
    province_code: 52
  },
  {
    district_name: "GANGGA",
    district_code: 5208030,
    regency_code: 5208,
    province_code: 52
  },
  {
    district_name: "KAYANGAN",
    district_code: 5208040,
    regency_code: 5208,
    province_code: 52
  },
  {
    district_name: "BAYAN",
    district_code: 5208050,
    regency_code: 5208,
    province_code: 52
  },
  {
    district_name: "AMPENAN",
    district_code: 5271010,
    regency_code: 5271,
    province_code: 52
  },
  {
    district_name: "SEKARBELA",
    district_code: 5271011,
    regency_code: 5271,
    province_code: 52
  },
  {
    district_name: "MATARAM",
    district_code: 5271020,
    regency_code: 5271,
    province_code: 52
  },
  {
    district_name: "SELAPARANG",
    district_code: 5271021,
    regency_code: 5271,
    province_code: 52
  },
  {
    district_name: "CAKRANEGARA",
    district_code: 5271030,
    regency_code: 5271,
    province_code: 52
  },
  {
    district_name: "SANDUBAYA",
    district_code: 5271031,
    regency_code: 5271,
    province_code: 52
  },
  {
    district_name: "RASANAE BARAT",
    district_code: 5272010,
    regency_code: 5272,
    province_code: 52
  },
  {
    district_name: "MPUNDA",
    district_code: 5272011,
    regency_code: 5272,
    province_code: 52
  },
  {
    district_name: "RASANAE TIMUR",
    district_code: 5272020,
    regency_code: 5272,
    province_code: 52
  },
  {
    district_name: "RABA",
    district_code: 5272021,
    regency_code: 5272,
    province_code: 52
  },
  {
    district_name: "ASAKOTA",
    district_code: 5272030,
    regency_code: 5272,
    province_code: 52
  },
  {
    district_name: "LAMBOYA",
    district_code: 5301021,
    regency_code: 5301,
    province_code: 53
  },
  {
    district_name: "WANOKAKA",
    district_code: 5301022,
    regency_code: 5301,
    province_code: 53
  },
  {
    district_name: "LABOYA BARAT",
    district_code: 5301023,
    regency_code: 5301,
    province_code: 53
  },
  {
    district_name: "LOLI",
    district_code: 5301050,
    regency_code: 5301,
    province_code: 53
  },
  {
    district_name: "KOTA WAIKABUBAK",
    district_code: 5301060,
    regency_code: 5301,
    province_code: 53
  },
  {
    district_name: "TANA RIGHU",
    district_code: 5301072,
    regency_code: 5301,
    province_code: 53
  },
  {
    district_name: "LEWA",
    district_code: 5302010,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "NGGAHA ORIANGU",
    district_code: 5302011,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "LEWA TIDAHU",
    district_code: 5302012,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "KATALA HAMU LINGU",
    district_code: 5302013,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "TABUNDUNG",
    district_code: 5302020,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "PINUPAHAR",
    district_code: 5302021,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "PABERIWAI",
    district_code: 5302030,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "KARERA",
    district_code: 5302031,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "MATAWAI LA PAWU",
    district_code: 5302032,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "KAHAUNGU ETI",
    district_code: 5302033,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "MAHU",
    district_code: 5302034,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "NGADU NGALA",
    district_code: 5302035,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "PAHUNGA LODU",
    district_code: 5302040,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "WULA WAIJELU",
    district_code: 5302041,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "RINDI",
    district_code: 5302051,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "UMALULU",
    district_code: 5302052,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "PANDAWAI",
    district_code: 5302060,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "KAMBATA MAPAMBUHANG",
    district_code: 5302061,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "KOTA WAINGAPU",
    district_code: 5302070,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "KAMBERA",
    district_code: 5302071,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "HAHARU",
    district_code: 5302080,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "KANATANG",
    district_code: 5302081,
    regency_code: 5302,
    province_code: 53
  },
  {
    district_name: "SEMAU",
    district_code: 5303100,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "SEMAU SELATAN",
    district_code: 5303101,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "KUPANG BARAT",
    district_code: 5303110,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "NEKAMESE",
    district_code: 5303111,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "KUPANG TENGAH",
    district_code: 5303120,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "TAEBENU",
    district_code: 5303121,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMARASI",
    district_code: 5303130,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMARASI BARAT",
    district_code: 5303131,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMARASI SELATAN",
    district_code: 5303132,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMARASI TIMUR",
    district_code: 5303133,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "KUPANG TIMUR",
    district_code: 5303140,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMABI OEFETO TIMUR",
    district_code: 5303141,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMABI OEFETO",
    district_code: 5303142,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "SULAMU",
    district_code: 5303150,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "FATULEU",
    district_code: 5303160,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "FATULEU TENGAH",
    district_code: 5303161,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "FATULEU BARAT",
    district_code: 5303162,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "TAKARI",
    district_code: 5303170,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMFOANG SELATAN",
    district_code: 5303180,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMFOANG BARAT DAYA",
    district_code: 5303181,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMFOANG TENGAH",
    district_code: 5303182,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMFOANG UTARA",
    district_code: 5303190,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMFOANG BARAT LAUT",
    district_code: 5303191,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "AMFOANG TIMUR",
    district_code: 5303192,
    regency_code: 5303,
    province_code: 53
  },
  {
    district_name: "MOLLO UTARA",
    district_code: 5304010,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "FATUMNASI",
    district_code: 5304011,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "TOBU",
    district_code: 5304012,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "NUNBENA",
    district_code: 5304013,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "MOLLO SELATAN",
    district_code: 5304020,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "POLEN",
    district_code: 5304021,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "MOLLO BARAT",
    district_code: 5304022,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "MOLLO TENGAH",
    district_code: 5304023,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KOTA SOE",
    district_code: 5304030,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "AMANUBAN BARAT",
    district_code: 5304040,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "BATU PUTIH",
    district_code: 5304041,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KUATNANA",
    district_code: 5304042,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "AMANUBAN SELATAN",
    district_code: 5304050,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "NOEBEBA",
    district_code: 5304051,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KUAN FATU",
    district_code: 5304060,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KUALIN",
    district_code: 5304061,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "AMANUBAN TENGAH",
    district_code: 5304070,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KOLBANO",
    district_code: 5304071,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "OENINO",
    district_code: 5304072,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "AMANUBAN TIMUR",
    district_code: 5304080,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "FAUTMOLO",
    district_code: 5304081,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "FATUKOPA",
    district_code: 5304082,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KIE",
    district_code: 5304090,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KOT'OLIN",
    district_code: 5304091,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "AMANATUN SELATAN",
    district_code: 5304100,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "BOKING",
    district_code: 5304101,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "NUNKOLO",
    district_code: 5304102,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "NOEBANA",
    district_code: 5304103,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "SANTIAN",
    district_code: 5304104,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "AMANATUN UTARA",
    district_code: 5304110,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "TOIANAS",
    district_code: 5304111,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "KOKBAUN",
    district_code: 5304112,
    regency_code: 5304,
    province_code: 53
  },
  {
    district_name: "MIOMAFFO BARAT",
    district_code: 5305010,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "MIOMAFFO TENGAH",
    district_code: 5305011,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "MUSI",
    district_code: 5305012,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "MUTIS",
    district_code: 5305013,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "MIOMAFFO TIMUR",
    district_code: 5305020,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "NOEMUTI",
    district_code: 5305021,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIKOMI SELATAN",
    district_code: 5305022,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIKOMI TENGAH",
    district_code: 5305023,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIKOMI NILULAT",
    district_code: 5305024,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIKOMI UTARA",
    district_code: 5305025,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "NAIBENU",
    district_code: 5305026,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "NOEMUTI TIMUR",
    district_code: 5305027,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "KOTA KEFAMENANU",
    district_code: 5305030,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "INSANA",
    district_code: 5305040,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "INSANA UTARA",
    district_code: 5305041,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "INSANA BARAT",
    district_code: 5305042,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "INSANA TENGAH",
    district_code: 5305043,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "INSANA FAFINESU",
    district_code: 5305044,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIBOKI SELATAN",
    district_code: 5305050,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIBOKI TANPAH",
    district_code: 5305051,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIBOKI MOENLEU",
    district_code: 5305052,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIBOKI UTARA",
    district_code: 5305060,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIBOKI ANLEU",
    district_code: 5305061,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "BIBOKI FEOTLEU",
    district_code: 5305062,
    regency_code: 5305,
    province_code: 53
  },
  {
    district_name: "RAI MANUK",
    district_code: 5306032,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "TASIFETO BARAT",
    district_code: 5306050,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "KAKULUK MESAK",
    district_code: 5306051,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "NANAET DUBESI",
    district_code: 5306052,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "ATAMBUA",
    district_code: 5306060,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "ATAMBUA BARAT",
    district_code: 5306061,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "ATAMBUA SELATAN",
    district_code: 5306062,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "TASIFETO TIMUR",
    district_code: 5306070,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "RAIHAT",
    district_code: 5306071,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "LASIOLAT",
    district_code: 5306072,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "LAMAKNEN",
    district_code: 5306080,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "LAMAKNEN SELATAN",
    district_code: 5306081,
    regency_code: 5306,
    province_code: 53
  },
  {
    district_name: "PANTAR",
    district_code: 5307010,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "PANTAR BARAT",
    district_code: 5307011,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "PANTAR TIMUR",
    district_code: 5307012,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "PANTAR BARAT LAUT",
    district_code: 5307013,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "PANTAR TENGAH",
    district_code: 5307014,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "ALOR BARAT DAYA",
    district_code: 5307020,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "MATARU",
    district_code: 5307021,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "ALOR SELATAN",
    district_code: 5307030,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "ALOR TIMUR",
    district_code: 5307040,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "ALOR TIMUR LAUT",
    district_code: 5307041,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "PUREMAN",
    district_code: 5307042,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "TELUK MUTIARA",
    district_code: 5307050,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "KABOLA",
    district_code: 5307051,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "ALOR BARAT LAUT",
    district_code: 5307060,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "ALOR TENGAH UTARA",
    district_code: 5307061,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "PULAU PURA",
    district_code: 5307062,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "LEMBUR",
    district_code: 5307063,
    regency_code: 5307,
    province_code: 53
  },
  {
    district_name: "NAGAWUTUNG",
    district_code: 5308010,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "WULANDONI",
    district_code: 5308011,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "ATADEI",
    district_code: 5308020,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "ILE APE",
    district_code: 5308030,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "ILE APE TIMUR",
    district_code: 5308031,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "LEBATUKAN",
    district_code: 5308040,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "NUBATUKAN",
    district_code: 5308050,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "OMESURI",
    district_code: 5308060,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "BUYASARI",
    district_code: 5308070,
    regency_code: 5308,
    province_code: 53
  },
  {
    district_name: "WULANGGITANG",
    district_code: 5309010,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "TITEHENA",
    district_code: 5309011,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "ILEBURA",
    district_code: 5309012,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "TANJUNG BUNGA",
    district_code: 5309020,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "LEWO LEMA",
    district_code: 5309021,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "LARANTUKA",
    district_code: 5309030,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "ILE MANDIRI",
    district_code: 5309031,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "DEMON PAGONG",
    district_code: 5309032,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "SOLOR BARAT",
    district_code: 5309040,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "SOLOR SELATAN",
    district_code: 5309041,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "SOLOR TIMUR",
    district_code: 5309050,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "ADONARA BARAT",
    district_code: 5309060,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "WOTAN ULU MADO",
    district_code: 5309061,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "ADONARA TENGAH",
    district_code: 5309062,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "ADONARA TIMUR",
    district_code: 5309070,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "ILE BOLENG",
    district_code: 5309071,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "WITIHAMA",
    district_code: 5309072,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "KELUBAGOLIT",
    district_code: 5309073,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "ADONARA",
    district_code: 5309074,
    regency_code: 5309,
    province_code: 53
  },
  {
    district_name: "PAGA",
    district_code: 5310010,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "MEGO",
    district_code: 5310011,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "TANA WAWO",
    district_code: 5310012,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "LELA",
    district_code: 5310020,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "BOLA",
    district_code: 5310030,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "DORENG",
    district_code: 5310031,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "MAPITARA",
    district_code: 5310032,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "TALIBURA",
    district_code: 5310040,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "WAIGETE",
    district_code: 5310041,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "WAIBLAMA",
    district_code: 5310042,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "KEWAPANTE",
    district_code: 5310050,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "HEWOKLOANG",
    district_code: 5310051,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "KANGAE",
    district_code: 5310052,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "PALUE",
    district_code: 5310061,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "KOTING",
    district_code: 5310062,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "NELLE",
    district_code: 5310063,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "NITA",
    district_code: 5310070,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "MAGEPANDA",
    district_code: 5310071,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "ALOK",
    district_code: 5310080,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "ALOK BARAT",
    district_code: 5310081,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "ALOK TIMUR",
    district_code: 5310082,
    regency_code: 5310,
    province_code: 53
  },
  {
    district_name: "NANGAPANDA",
    district_code: 5311010,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "PULAU ENDE",
    district_code: 5311011,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "MAUKARO",
    district_code: 5311012,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "ENDE",
    district_code: 5311020,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "ENDE SELATAN",
    district_code: 5311030,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "ENDE TIMUR",
    district_code: 5311031,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "ENDE TENGAH",
    district_code: 5311032,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "ENDE UTARA",
    district_code: 5311033,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "NDONA",
    district_code: 5311040,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "NDONA TIMUR",
    district_code: 5311041,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "WOLOWARU",
    district_code: 5311050,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "WOLOJITA",
    district_code: 5311051,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "LIO TIMUR",
    district_code: 5311052,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "KELIMUTU",
    district_code: 5311053,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "NDORI",
    district_code: 5311054,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "MAUROLE",
    district_code: 5311060,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "KOTABARU",
    district_code: 5311061,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "DETUKELI",
    district_code: 5311062,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "LEPEMBUSU KELISOKE",
    district_code: 5311063,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "DETUSOKO",
    district_code: 5311070,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "WEWARIA",
    district_code: 5311071,
    regency_code: 5311,
    province_code: 53
  },
  {
    district_name: "AIMERE",
    district_code: 5312010,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "JEREBUU",
    district_code: 5312011,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "INERIE",
    district_code: 5312012,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "BAJAWA",
    district_code: 5312020,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "GOLEWA",
    district_code: 5312030,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "GOLEWA SELATAN",
    district_code: 5312031,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "GOLEWA BARAT",
    district_code: 5312032,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "BAJAWA UTARA",
    district_code: 5312070,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "SOA",
    district_code: 5312071,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "RIUNG",
    district_code: 5312080,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "RIUNG BARAT",
    district_code: 5312081,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "WOLOMEZE",
    district_code: 5312082,
    regency_code: 5312,
    province_code: 53
  },
  {
    district_name: "SATAR MESE",
    district_code: 5313040,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "SATAR MESE BARAT",
    district_code: 5313041,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "SATAR MESE UTARA",
    district_code: 5313042,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "LANGKE REMBONG",
    district_code: 5313110,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "RUTENG",
    district_code: 5313120,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "WAE RII",
    district_code: 5313121,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "LELAK",
    district_code: 5313122,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "RAHONG UTARA",
    district_code: 5313123,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "CIBAL",
    district_code: 5313130,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "CIBAL BARAT",
    district_code: 5313131,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "REOK",
    district_code: 5313140,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "REOK BARAT",
    district_code: 5313141,
    regency_code: 5313,
    province_code: 53
  },
  {
    district_name: "ROTE BARAT DAYA",
    district_code: 5314010,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "ROTE BARAT LAUT",
    district_code: 5314020,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "LOBALAIN",
    district_code: 5314030,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "ROTE TENGAH",
    district_code: 5314040,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "ROTE SELATAN",
    district_code: 5314041,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "PANTAI BARU",
    district_code: 5314050,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "ROTE TIMUR",
    district_code: 5314060,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "LANDU LEKO",
    district_code: 5314061,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "ROTE BARAT",
    district_code: 5314070,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "NDAO NUSE",
    district_code: 5314071,
    regency_code: 5314,
    province_code: 53
  },
  {
    district_name: "KOMODO",
    district_code: 5315010,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "BOLENG",
    district_code: 5315011,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "SANO NGGOANG",
    district_code: 5315020,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "MBELILING",
    district_code: 5315021,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "LEMBOR",
    district_code: 5315030,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "WELAK",
    district_code: 5315031,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "LEMBOR SELATAN",
    district_code: 5315032,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "KUWUS",
    district_code: 5315040,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "NDOSO",
    district_code: 5315041,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "MACANG PACAR",
    district_code: 5315050,
    regency_code: 5315,
    province_code: 53
  },
  {
    district_name: "KATIKUTANA",
    district_code: 5316010,
    regency_code: 5316,
    province_code: 53
  },
  {
    district_name: "KATIKUTANA SELATAN",
    district_code: 5316011,
    regency_code: 5316,
    province_code: 53
  },
  {
    district_name: "UMBU RATU NGGAY BARAT",
    district_code: 5316020,
    regency_code: 5316,
    province_code: 53
  },
  {
    district_name: "UMBU RATU NGGAY",
    district_code: 5316030,
    regency_code: 5316,
    province_code: 53
  },
  {
    district_name: "MAMBORO",
    district_code: 5316040,
    regency_code: 5316,
    province_code: 53
  },
  {
    district_name: "KODI BANGEDO",
    district_code: 5317010,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "KODI BALAGHAR",
    district_code: 5317011,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "KODI",
    district_code: 5317020,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "KODI UTARA",
    district_code: 5317030,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "WEWEWA SELATAN",
    district_code: 5317040,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "WEWEWA BARAT",
    district_code: 5317050,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "WEWEWA TIMUR",
    district_code: 5317060,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "WEWEWA TENGAH",
    district_code: 5317061,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "WEWEWA UTARA",
    district_code: 5317070,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "LOURA",
    district_code: 5317080,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "KOTA TAMBOLAKA",
    district_code: 5317081,
    regency_code: 5317,
    province_code: 53
  },
  {
    district_name: "MAUPONGGO",
    district_code: 5318010,
    regency_code: 5318,
    province_code: 53
  },
  {
    district_name: "KEO TENGAH",
    district_code: 5318020,
    regency_code: 5318,
    province_code: 53
  },
  {
    district_name: "NANGARORO",
    district_code: 5318030,
    regency_code: 5318,
    province_code: 53
  },
  {
    district_name: "BOAWAE",
    district_code: 5318040,
    regency_code: 5318,
    province_code: 53
  },
  {
    district_name: "AESESA SELATAN",
    district_code: 5318050,
    regency_code: 5318,
    province_code: 53
  },
  {
    district_name: "AESESA",
    district_code: 5318060,
    regency_code: 5318,
    province_code: 53
  },
  {
    district_name: "WOLOWAE",
    district_code: 5318070,
    regency_code: 5318,
    province_code: 53
  },
  {
    district_name: "BORONG",
    district_code: 5319010,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "RANA MESE",
    district_code: 5319011,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "KOTA KOMBA",
    district_code: 5319020,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "ELAR",
    district_code: 5319030,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "ELAR SELATAN",
    district_code: 5319031,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "SAMBI RAMPAS",
    district_code: 5319040,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "POCO RANAKA",
    district_code: 5319050,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "POCO RANAKA TIMUR",
    district_code: 5319051,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "LAMBA LEDA",
    district_code: 5319060,
    regency_code: 5319,
    province_code: 53
  },
  {
    district_name: "RAIJUA",
    district_code: 5320010,
    regency_code: 5320,
    province_code: 53
  },
  {
    district_name: "HAWU MEHARA",
    district_code: 5320020,
    regency_code: 5320,
    province_code: 53
  },
  {
    district_name: "SABU LIAE",
    district_code: 5320030,
    regency_code: 5320,
    province_code: 53
  },
  {
    district_name: "SABU BARAT",
    district_code: 5320040,
    regency_code: 5320,
    province_code: 53
  },
  {
    district_name: "SABU TENGAH",
    district_code: 5320050,
    regency_code: 5320,
    province_code: 53
  },
  {
    district_name: "SABU TIMUR",
    district_code: 5320060,
    regency_code: 5320,
    province_code: 53
  },
  {
    district_name: "WEWIKU",
    district_code: 5321010,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "MALAKA BARAT",
    district_code: 5321020,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "WELIMAN",
    district_code: 5321030,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "RINHAT",
    district_code: 5321040,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "IO KUFEU",
    district_code: 5321050,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "SASITA MEAN",
    district_code: 5321060,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "MALAKA TENGAH",
    district_code: 5321070,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "BOTIN LEOBELE",
    district_code: 5321080,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "LAEN MANEN",
    district_code: 5321090,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "MALAKA TIMUR",
    district_code: 5321100,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "KOBALIMA",
    district_code: 5321110,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "KOBALIMA TIMUR",
    district_code: 5321120,
    regency_code: 5321,
    province_code: 53
  },
  {
    district_name: "ALAK",
    district_code: 5371010,
    regency_code: 5371,
    province_code: 53
  },
  {
    district_name: "MAULAFA",
    district_code: 5371020,
    regency_code: 5371,
    province_code: 53
  },
  {
    district_name: "OEBOBO",
    district_code: 5371030,
    regency_code: 5371,
    province_code: 53
  },
  {
    district_name: "KOTA RAJA",
    district_code: 5371031,
    regency_code: 5371,
    province_code: 53
  },
  {
    district_name: "KELAPA LIMA",
    district_code: 5371040,
    regency_code: 5371,
    province_code: 53
  },
  {
    district_name: "KOTA LAMA",
    district_code: 5371041,
    regency_code: 5371,
    province_code: 53
  },
  {
    district_name: "SELAKAU",
    district_code: 6101010,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SELAKAU TIMUR",
    district_code: 6101011,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "PEMANGKAT",
    district_code: 6101020,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SEMPARUK",
    district_code: 6101021,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SALATIGA",
    district_code: 6101022,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "TEBAS",
    district_code: 6101030,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "TEKARANG",
    district_code: 6101031,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SAMBAS",
    district_code: 6101040,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SUBAH",
    district_code: 6101041,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SEBAWI",
    district_code: 6101042,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SAJAD",
    district_code: 6101043,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "JAWAI",
    district_code: 6101050,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "JAWAI SELATAN",
    district_code: 6101051,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "TELUK KERAMAT",
    district_code: 6101060,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "GALING",
    district_code: 6101061,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "TANGARAN",
    district_code: 6101062,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SEJANGKUNG",
    district_code: 6101070,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SAJINGAN BESAR",
    district_code: 6101080,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "PALOH",
    district_code: 6101090,
    regency_code: 6101,
    province_code: 61
  },
  {
    district_name: "SUNGAI RAYA",
    district_code: 6102010,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "CAPKALA",
    district_code: 6102011,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SUNGAI RAYA KEPULAUAN",
    district_code: 6102012,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SAMALANTAN",
    district_code: 6102030,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "MONTERADO",
    district_code: 6102031,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "LEMBAH BAWANG",
    district_code: 6102032,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "BENGKAYANG",
    district_code: 6102040,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "TERIAK",
    district_code: 6102041,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SUNGAI BETUNG",
    district_code: 6102042,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "LEDO",
    district_code: 6102050,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SUTI SEMARANG",
    district_code: 6102051,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "LUMAR",
    district_code: 6102052,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SANGGAU LEDO",
    district_code: 6102060,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "TUJUHBELAS",
    district_code: 6102061,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SELUAS",
    district_code: 6102070,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "JAGOI BABANG",
    district_code: 6102080,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SIDING",
    district_code: 6102081,
    regency_code: 6102,
    province_code: 61
  },
  {
    district_name: "SEBANGKI",
    district_code: 6103020,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "NGABANG",
    district_code: 6103030,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "JELIMPO",
    district_code: 6103031,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "SENGAH TEMILA",
    district_code: 6103040,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "MANDOR",
    district_code: 6103050,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "MENJALIN",
    district_code: 6103060,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "MEMPAWAH HULU",
    district_code: 6103070,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "SOMPAK",
    district_code: 6103071,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "MENYUKE",
    district_code: 6103080,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "BANYUKE HULU",
    district_code: 6103081,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "MERANTI",
    district_code: 6103090,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "KUALA BEHE",
    district_code: 6103100,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "AIR BESAR",
    district_code: 6103110,
    regency_code: 6103,
    province_code: 61
  },
  {
    district_name: "SIANTAN",
    district_code: 6104080,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "SEGEDONG",
    district_code: 6104081,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "SUNGAI PINYUH",
    district_code: 6104090,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "ANJONGAN",
    district_code: 6104091,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "MEMPAWAH HILIR",
    district_code: 6104100,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "MEMPAWAH TIMUR",
    district_code: 6104101,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "SUNGAI KUNYIT",
    district_code: 6104110,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "TOHO",
    district_code: 6104120,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "SADANIANG",
    district_code: 6104121,
    regency_code: 6104,
    province_code: 61
  },
  {
    district_name: "TOBA",
    district_code: 6105010,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "MELIAU",
    district_code: 6105020,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "KAPUAS",
    district_code: 6105060,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "MUKOK",
    district_code: 6105070,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "JANGKANG",
    district_code: 6105120,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "BONTI",
    district_code: 6105130,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "PARINDU",
    district_code: 6105140,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "TAYAN HILIR",
    district_code: 6105150,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "BALAI",
    district_code: 6105160,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "TAYAN HULU",
    district_code: 6105170,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "KEMBAYAN",
    district_code: 6105180,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "BEDUWAI",
    district_code: 6105190,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "NOYAN",
    district_code: 6105200,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "SEKAYAM",
    district_code: 6105210,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "ENTIKONG",
    district_code: 6105220,
    regency_code: 6105,
    province_code: 61
  },
  {
    district_name: "KENDAWANGAN",
    district_code: 6106010,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "MANIS MATA",
    district_code: 6106020,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "MARAU",
    district_code: 6106030,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "SINGKUP",
    district_code: 6106031,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "AIR UPAS",
    district_code: 6106032,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "JELAI HULU",
    district_code: 6106040,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "TUMBANG TITI",
    district_code: 6106050,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "PEMAHAN",
    district_code: 6106051,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "SUNGAI MELAYU RAYAK",
    district_code: 6106052,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "MATAN HILIR SELATAN",
    district_code: 6106060,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "BENUA KAYONG",
    district_code: 6106061,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "MATAN HILIR UTARA",
    district_code: 6106070,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "DELTA PAWAN",
    district_code: 6106071,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "MUARA PAWAN",
    district_code: 6106072,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "NANGA TAYAP",
    district_code: 6106090,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "SANDAI",
    district_code: 6106100,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "HULU SUNGAI",
    district_code: 6106101,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "SUNGAI LAUR",
    district_code: 6106110,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "SIMPANG HULU",
    district_code: 6106120,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "SIMPANG DUA",
    district_code: 6106121,
    regency_code: 6106,
    province_code: 61
  },
  {
    district_name: "SERAWAI",
    district_code: 6107060,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "AMBALAU",
    district_code: 6107070,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "KAYAN HULU",
    district_code: 6107080,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "SEPAUK",
    district_code: 6107110,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "TEMPUNAK",
    district_code: 6107120,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "SUNGAI TEBELIAN",
    district_code: 6107130,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "SINTANG",
    district_code: 6107140,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "DEDAI",
    district_code: 6107150,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "KAYAN HILIR",
    district_code: 6107160,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "KELAM PERMAI",
    district_code: 6107170,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "BINJAI HULU",
    district_code: 6107180,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "KETUNGAU HILIR",
    district_code: 6107190,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "KETUNGAU TENGAH",
    district_code: 6107200,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "KETUNGAU HULU",
    district_code: 6107210,
    regency_code: 6107,
    province_code: 61
  },
  {
    district_name: "SILAT HILIR",
    district_code: 6108010,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "SILAT HULU",
    district_code: 6108020,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "HULU GURUNG",
    district_code: 6108030,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "BUNUT HULU",
    district_code: 6108040,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "MENTEBAH",
    district_code: 6108050,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "BIKA",
    district_code: 6108060,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "KALIS",
    district_code: 6108070,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "PUTUSSIBAU SELATAN",
    district_code: 6108080,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "EMBALOH HILIR",
    district_code: 6108090,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "BUNUT HILIR",
    district_code: 6108100,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "BOYAN TANJUNG",
    district_code: 6108110,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "PENGKADAN",
    district_code: 6108120,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "JONGKONG",
    district_code: 6108130,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "SELIMBAU",
    district_code: 6108140,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "SUHAID",
    district_code: 6108150,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "SEBERUANG",
    district_code: 6108160,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "SEMITAU",
    district_code: 6108170,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "EMPANANG",
    district_code: 6108180,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "PURING KENCANA",
    district_code: 6108190,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "BADAU",
    district_code: 6108200,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "BATANG LUPAR",
    district_code: 6108210,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "EMBALOH HULU",
    district_code: 6108220,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "PUTUSSIBAU UTARA",
    district_code: 6108230,
    regency_code: 6108,
    province_code: 61
  },
  {
    district_name: "NANGA MAHAP",
    district_code: 6109010,
    regency_code: 6109,
    province_code: 61
  },
  {
    district_name: "NANGA TAMAN",
    district_code: 6109020,
    regency_code: 6109,
    province_code: 61
  },
  {
    district_name: "SEKADAU HULU",
    district_code: 6109030,
    regency_code: 6109,
    province_code: 61
  },
  {
    district_name: "SEKADAU HILIR",
    district_code: 6109040,
    regency_code: 6109,
    province_code: 61
  },
  {
    district_name: "BELITANG HILIR",
    district_code: 6109050,
    regency_code: 6109,
    province_code: 61
  },
  {
    district_name: "BELITANG",
    district_code: 6109060,
    regency_code: 6109,
    province_code: 61
  },
  {
    district_name: "BELITANG HULU",
    district_code: 6109070,
    regency_code: 6109,
    province_code: 61
  },
  {
    district_name: "SOKAN",
    district_code: 6110010,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "TANAH PINOH",
    district_code: 6110020,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "TANAH PINOH BARAT",
    district_code: 6110021,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "SAYAN",
    district_code: 6110030,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "BELIMBING",
    district_code: 6110040,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "BELIMBING HULU",
    district_code: 6110041,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "NANGA PINOH",
    district_code: 6110050,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "PINOH SELATAN",
    district_code: 6110051,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "PINOH UTARA",
    district_code: 6110052,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "ELLA HILIR",
    district_code: 6110060,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "MENUKUNG",
    district_code: 6110070,
    regency_code: 6110,
    province_code: 61
  },
  {
    district_name: "PULAU MAYA",
    district_code: 6111010,
    regency_code: 6111,
    province_code: 61
  },
  {
    district_name: "KEPULAUAN KARIMATA",
    district_code: 6111011,
    regency_code: 6111,
    province_code: 61
  },
  {
    district_name: "SUKADANA",
    district_code: 6111020,
    regency_code: 6111,
    province_code: 61
  },
  {
    district_name: "SIMPANG HILIR",
    district_code: 6111030,
    regency_code: 6111,
    province_code: 61
  },
  {
    district_name: "TELUK BATANG",
    district_code: 6111040,
    regency_code: 6111,
    province_code: 61
  },
  {
    district_name: "SEPONTI",
    district_code: 6111050,
    regency_code: 6111,
    province_code: 61
  },
  {
    district_name: "BATU AMPAR",
    district_code: 6112010,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "TERENTANG",
    district_code: 6112020,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "KUBU",
    district_code: 6112030,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "TELOK PA'KEDAI",
    district_code: 6112040,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "SUNGAI KAKAP",
    district_code: 6112050,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "RASAU JAYA",
    district_code: 6112060,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "SUNGAI RAYA",
    district_code: 6112070,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "SUNGAI AMBAWANG",
    district_code: 6112080,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "KUALA MANDOR-B",
    district_code: 6112090,
    regency_code: 6112,
    province_code: 61
  },
  {
    district_name: "PONTIANAK SELATAN",
    district_code: 6171010,
    regency_code: 6171,
    province_code: 61
  },
  {
    district_name: "PONTIANAK TENGGARA",
    district_code: 6171011,
    regency_code: 6171,
    province_code: 61
  },
  {
    district_name: "PONTIANAK TIMUR",
    district_code: 6171020,
    regency_code: 6171,
    province_code: 61
  },
  {
    district_name: "PONTIANAK BARAT",
    district_code: 6171030,
    regency_code: 6171,
    province_code: 61
  },
  {
    district_name: "PONTIANAK KOTA",
    district_code: 6171031,
    regency_code: 6171,
    province_code: 61
  },
  {
    district_name: "PONTIANAK UTARA",
    district_code: 6171040,
    regency_code: 6171,
    province_code: 61
  },
  {
    district_name: "SINGKAWANG SELATAN",
    district_code: 6172010,
    regency_code: 6172,
    province_code: 61
  },
  {
    district_name: "SINGKAWANG TIMUR",
    district_code: 6172020,
    regency_code: 6172,
    province_code: 61
  },
  {
    district_name: "SINGKAWANG UTARA",
    district_code: 6172030,
    regency_code: 6172,
    province_code: 61
  },
  {
    district_name: "SINGKAWANG BARAT",
    district_code: 6172040,
    regency_code: 6172,
    province_code: 61
  },
  {
    district_name: "SINGKAWANG TENGAH",
    district_code: 6172050,
    regency_code: 6172,
    province_code: 61
  },
  {
    district_name: "KOTAWARINGIN LAMA",
    district_code: 6201040,
    regency_code: 6201,
    province_code: 62
  },
  {
    district_name: "ARUT SELATAN",
    district_code: 6201050,
    regency_code: 6201,
    province_code: 62
  },
  {
    district_name: "KUMAI",
    district_code: 6201060,
    regency_code: 6201,
    province_code: 62
  },
  {
    district_name: "PANGKALAN BANTENG",
    district_code: 6201061,
    regency_code: 6201,
    province_code: 62
  },
  {
    district_name: "PANGKALAN LADA",
    district_code: 6201062,
    regency_code: 6201,
    province_code: 62
  },
  {
    district_name: "ARUT UTARA",
    district_code: 6201070,
    regency_code: 6201,
    province_code: 62
  },
  {
    district_name: "MENTAYA HILIR SELATAN",
    district_code: 6202020,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "TELUK SAMPIT",
    district_code: 6202021,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "PULAU HANAUT",
    district_code: 6202050,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "MENTAWA BARU/KETAPANG",
    district_code: 6202060,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "SERANAU",
    district_code: 6202061,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "MENTAYA HILIR UTARA",
    district_code: 6202070,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "KOTA BESI",
    district_code: 6202110,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "TELAWANG",
    district_code: 6202111,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "BAAMANG",
    district_code: 6202120,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "CEMPAGA",
    district_code: 6202190,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "CEMPAGA HULU",
    district_code: 6202191,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "PARENGGEAN",
    district_code: 6202200,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "TUALAN HULU",
    district_code: 6202201,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "MENTAYA HULU",
    district_code: 6202210,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "BUKIT SANTUAI",
    district_code: 6202211,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "ANTANG KALANG",
    district_code: 6202230,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "TELAGA ANTANG",
    district_code: 6202231,
    regency_code: 6202,
    province_code: 62
  },
  {
    district_name: "KAPUAS KUALA",
    district_code: 6203020,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "TAMBAN CATUR",
    district_code: 6203021,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "KAPUAS TIMUR",
    district_code: 6203030,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "SELAT",
    district_code: 6203040,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "BATAGUH",
    district_code: 6203041,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "BASARANG",
    district_code: 6203070,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "KAPUAS HILIR",
    district_code: 6203080,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "PULAU PETAK",
    district_code: 6203090,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "KAPUAS MURUNG",
    district_code: 6203100,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "DADAHUP",
    district_code: 6203101,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "KAPUAS BARAT",
    district_code: 6203110,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "MANTANGAI",
    district_code: 6203150,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "TIMPAH",
    district_code: 6203160,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "KAPUAS TENGAH",
    district_code: 6203170,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "PASAK TALAWANG",
    district_code: 6203171,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "KAPUAS HULU",
    district_code: 6203180,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "MANDAU TALAWANG",
    district_code: 6203181,
    regency_code: 6203,
    province_code: 62
  },
  {
    district_name: "JENAMAS",
    district_code: 6204010,
    regency_code: 6204,
    province_code: 62
  },
  {
    district_name: "DUSUN HILIR",
    district_code: 6204020,
    regency_code: 6204,
    province_code: 62
  },
  {
    district_name: "KARAU KUALA",
    district_code: 6204030,
    regency_code: 6204,
    province_code: 62
  },
  {
    district_name: "DUSUN SELATAN",
    district_code: 6204040,
    regency_code: 6204,
    province_code: 62
  },
  {
    district_name: "DUSUN UTARA",
    district_code: 6204050,
    regency_code: 6204,
    province_code: 62
  },
  {
    district_name: "GUNUNG BINTANG AWAI",
    district_code: 6204060,
    regency_code: 6204,
    province_code: 62
  },
  {
    district_name: "MONTALLAT",
    district_code: 6205010,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "GUNUNG TIMANG",
    district_code: 6205020,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "GUNUNG PUREI",
    district_code: 6205030,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "TEWEH TIMUR",
    district_code: 6205040,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "TEWEH TENGAH",
    district_code: 6205050,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "TEWEH  BARU",
    district_code: 6205051,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "TEWEH SELATAN",
    district_code: 6205052,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "LAHEI",
    district_code: 6205060,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "LAHEI BARAT",
    district_code: 6205061,
    regency_code: 6205,
    province_code: 62
  },
  {
    district_name: "JELAI",
    district_code: 6206010,
    regency_code: 6206,
    province_code: 62
  },
  {
    district_name: "PANTAI LUNCI",
    district_code: 6206011,
    regency_code: 6206,
    province_code: 62
  },
  {
    district_name: "SUKAMARA",
    district_code: 6206020,
    regency_code: 6206,
    province_code: 62
  },
  {
    district_name: "BALAI RIAM",
    district_code: 6206030,
    regency_code: 6206,
    province_code: 62
  },
  {
    district_name: "PERMATA KECUBUNG",
    district_code: 6206031,
    regency_code: 6206,
    province_code: 62
  },
  {
    district_name: "BULIK",
    district_code: 6207010,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "SEMATU JAYA",
    district_code: 6207011,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "MENTHOBI RAYA",
    district_code: 6207012,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "BULIK TIMUR",
    district_code: 6207013,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "LAMANDAU",
    district_code: 6207020,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "BELANTIKAN RAYA",
    district_code: 6207021,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "DELANG",
    district_code: 6207030,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "BATANGKAWA",
    district_code: 6207031,
    regency_code: 6207,
    province_code: 62
  },
  {
    district_name: "SERUYAN HILIR",
    district_code: 6208010,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "SERUYAN HILIR TIMUR",
    district_code: 6208011,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "DANAU SEMBULUH",
    district_code: 6208020,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "SERUYAN RAYA",
    district_code: 6208021,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "HANAU",
    district_code: 6208030,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "DANAU SELULUK",
    district_code: 6208031,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "SERUYAN TENGAH",
    district_code: 6208040,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "BATU AMPAR",
    district_code: 6208041,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "SERUYAN HULU",
    district_code: 6208050,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "SULING TAMBUN",
    district_code: 6208051,
    regency_code: 6208,
    province_code: 62
  },
  {
    district_name: "KATINGAN KUALA",
    district_code: 6209010,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "MENDAWAI",
    district_code: 6209020,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "KAMIPANG",
    district_code: 6209030,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "TASIK PAYAWAN",
    district_code: 6209040,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "KATINGAN HILIR",
    district_code: 6209050,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "TEWANG SANGALANG GARING",
    district_code: 6209060,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "PULAU MALAN",
    district_code: 6209070,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "KATINGAN TENGAH",
    district_code: 6209080,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "SANAMAN MANTIKEI",
    district_code: 6209090,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "PETAK MALAI",
    district_code: 6209091,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "MARIKIT",
    district_code: 6209100,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "KATINGAN HULU",
    district_code: 6209110,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "BUKIT RAYA",
    district_code: 6209111,
    regency_code: 6209,
    province_code: 62
  },
  {
    district_name: "KAHAYAN KUALA",
    district_code: 6210010,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "SEBANGAU KUALA",
    district_code: 6210011,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "PANDIH BATU",
    district_code: 6210020,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "MALIKU",
    district_code: 6210030,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "KAHAYAN HILIR",
    district_code: 6210040,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "JABIREN RAYA",
    district_code: 6210041,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "KAHAYAN TENGAH",
    district_code: 6210050,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "BANAMA TINGANG",
    district_code: 6210060,
    regency_code: 6210,
    province_code: 62
  },
  {
    district_name: "MANUHING",
    district_code: 6211010,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "MANUHING RAYA",
    district_code: 6211011,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "RUNGAN",
    district_code: 6211020,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "RUNGAN HULU",
    district_code: 6211021,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "RUNGAN BARAT",
    district_code: 6211022,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "SEPANG",
    district_code: 6211030,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "MIHING RAYA",
    district_code: 6211031,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "KURUN",
    district_code: 6211040,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "TEWAH",
    district_code: 6211050,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "KAHAYAN HULU UTARA",
    district_code: 6211060,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "DAMANG BATU",
    district_code: 6211061,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "MIRI MANASA",
    district_code: 6211062,
    regency_code: 6211,
    province_code: 62
  },
  {
    district_name: "BENUA LIMA",
    district_code: 6212010,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "DUSUN TIMUR",
    district_code: 6212020,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "PAJU EPAT",
    district_code: 6212021,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "AWANG",
    district_code: 6212030,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "PATANGKEP TUTUI",
    district_code: 6212040,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "DUSUN TENGAH",
    district_code: 6212050,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "RAREN BATUAH",
    district_code: 6212051,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "PAKU",
    district_code: 6212052,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "KARUSEN JANANG",
    district_code: 6212053,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "PEMATANG KARAU",
    district_code: 6212060,
    regency_code: 6212,
    province_code: 62
  },
  {
    district_name: "PERMATA INTAN",
    district_code: 6213010,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "SUNGAI BABUAT",
    district_code: 6213011,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "MURUNG",
    district_code: 6213020,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "LAUNG TUHUP",
    district_code: 6213030,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "BARITO TUHUP RAYA",
    district_code: 6213031,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "TANAH SIANG",
    district_code: 6213040,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "TANAH SIANG SELATAN",
    district_code: 6213041,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "SUMBER BARITO",
    district_code: 6213050,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "SERIBU RIAM",
    district_code: 6213051,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "UUT MURUNG",
    district_code: 6213052,
    regency_code: 6213,
    province_code: 62
  },
  {
    district_name: "PAHANDUT",
    district_code: 6271010,
    regency_code: 6271,
    province_code: 62
  },
  {
    district_name: "SABANGAU",
    district_code: 6271011,
    regency_code: 6271,
    province_code: 62
  },
  {
    district_name: "JEKAN RAYA",
    district_code: 6271012,
    regency_code: 6271,
    province_code: 62
  },
  {
    district_name: "BUKIT BATU",
    district_code: 6271020,
    regency_code: 6271,
    province_code: 62
  },
  {
    district_name: "RAKUMPIT",
    district_code: 6271021,
    regency_code: 6271,
    province_code: 62
  },
  {
    district_name: "PANYIPATAN",
    district_code: 6301010,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "TAKISUNG",
    district_code: 6301020,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "KURAU",
    district_code: 6301030,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "BUMI MAKMUR",
    district_code: 6301031,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "BATI - BATI",
    district_code: 6301040,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "TAMBANG ULANG",
    district_code: 6301050,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "PELAIHARI",
    district_code: 6301060,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "BAJUIN",
    district_code: 6301061,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "BATU AMPAR",
    district_code: 6301070,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "JORONG",
    district_code: 6301080,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "KINTAP",
    district_code: 6301090,
    regency_code: 6301,
    province_code: 63
  },
  {
    district_name: "PULAU SEMBILAN",
    district_code: 6302010,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU LAUT BARAT",
    district_code: 6302020,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU LAUT TANJUNG SELAYAR",
    district_code: 6302021,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU LAUT SELATAN",
    district_code: 6302030,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU LAUT KEPULAUAN",
    district_code: 6302031,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU LAUT TIMUR",
    district_code: 6302040,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU SEBUKU",
    district_code: 6302050,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU LAUT UTARA",
    district_code: 6302060,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PULAU LAUT TENGAH",
    district_code: 6302061,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "KELUMPANG SELATAN",
    district_code: 6302120,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "KELUMPANG HILIR",
    district_code: 6302121,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "KELUMPANG HULU",
    district_code: 6302130,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "HAMPANG",
    district_code: 6302140,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "SUNGAI DURIAN",
    district_code: 6302150,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "KELUMPANG TENGAH",
    district_code: 6302160,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "KELUMPANG BARAT",
    district_code: 6302161,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "KELUMPANG UTARA",
    district_code: 6302170,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PAMUKAN SELATAN",
    district_code: 6302180,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "SAMPANAHAN",
    district_code: 6302190,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PAMUKAN UTARA",
    district_code: 6302200,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "PAMUKAN BARAT",
    district_code: 6302201,
    regency_code: 6302,
    province_code: 63
  },
  {
    district_name: "ALUH - ALUH",
    district_code: 6303010,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "BERUNTUNG BARU",
    district_code: 6303011,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "GAMBUT",
    district_code: 6303020,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "KERTAK HANYAR",
    district_code: 6303030,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "TATAH MAKMUR",
    district_code: 6303031,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "SUNGAI TABUK",
    district_code: 6303040,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "MARTAPURA",
    district_code: 6303050,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "MARTAPURA TIMUR",
    district_code: 6303051,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "MARTAPURA BARAT",
    district_code: 6303052,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "ASTAMBUL",
    district_code: 6303060,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "KARANG INTAN",
    district_code: 6303070,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "ARANIO",
    district_code: 6303080,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "SUNGAI PINANG",
    district_code: 6303090,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "PARAMASAN",
    district_code: 6303091,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "PENGARON",
    district_code: 6303100,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "SAMBUNG MAKMUR",
    district_code: 6303101,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "MATARAMAN",
    district_code: 6303110,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "SIMPANG EMPAT",
    district_code: 6303120,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "TELAGA BAUNTUNG",
    district_code: 6303121,
    regency_code: 6303,
    province_code: 63
  },
  {
    district_name: "TABUNGANEN",
    district_code: 6304010,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "TAMBAN",
    district_code: 6304020,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "MEKAR SARI",
    district_code: 6304030,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "ANJIR PASAR",
    district_code: 6304040,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "ANJIR MUARA",
    district_code: 6304050,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "ALALAK",
    district_code: 6304060,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "MANDASTANA",
    district_code: 6304070,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "JEJANGKIT",
    district_code: 6304071,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "BELAWANG",
    district_code: 6304080,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "WANARAYA",
    district_code: 6304090,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "BARAMBAI",
    district_code: 6304100,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "RANTAU BADAUH",
    district_code: 6304110,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "CERBON",
    district_code: 6304120,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "BAKUMPAI",
    district_code: 6304130,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "MARABAHAN",
    district_code: 6304140,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "TABUKAN",
    district_code: 6304150,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "KURIPAN",
    district_code: 6304160,
    regency_code: 6304,
    province_code: 63
  },
  {
    district_name: "BINUANG",
    district_code: 6305010,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "HATUNGUN",
    district_code: 6305011,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "TAPIN SELATAN",
    district_code: 6305020,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "SALAM BABARIS",
    district_code: 6305021,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "TAPIN TENGAH",
    district_code: 6305030,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "BUNGUR",
    district_code: 6305040,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "PIANI",
    district_code: 6305050,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "LOKPAIKAT",
    district_code: 6305060,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "TAPIN UTARA",
    district_code: 6305070,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "BAKARANGAN",
    district_code: 6305080,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "CANDI LARAS SELATAN",
    district_code: 6305090,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "CANDI LARAS UTARA",
    district_code: 6305100,
    regency_code: 6305,
    province_code: 63
  },
  {
    district_name: "PADANG BATUNG",
    district_code: 6306010,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "LOKSADO",
    district_code: 6306020,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "TELAGA LANGSAT",
    district_code: 6306030,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "ANGKINANG",
    district_code: 6306040,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "KANDANGAN",
    district_code: 6306050,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "SUNGAI RAYA",
    district_code: 6306060,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "SIMPUR",
    district_code: 6306070,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "KALUMPANG",
    district_code: 6306080,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "DAHA SELATAN",
    district_code: 6306090,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "DAHA BARAT",
    district_code: 6306091,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "DAHA UTARA",
    district_code: 6306100,
    regency_code: 6306,
    province_code: 63
  },
  {
    district_name: "HARUYAN",
    district_code: 6307010,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "BATU BENAWA",
    district_code: 6307020,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "HANTAKAN",
    district_code: 6307030,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "BATANG ALAI SELATAN",
    district_code: 6307040,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "BATANG ALAI TIMUR",
    district_code: 6307041,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "BARABAI",
    district_code: 6307050,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "LABUAN AMAS SELATAN",
    district_code: 6307060,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "LABUAN AMAS UTARA",
    district_code: 6307070,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "PANDAWAN",
    district_code: 6307080,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "BATANG ALAI UTARA",
    district_code: 6307090,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "LIMPASU",
    district_code: 6307091,
    regency_code: 6307,
    province_code: 63
  },
  {
    district_name: "DANAU PANGGANG",
    district_code: 6308010,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "PAMINGGIR",
    district_code: 6308011,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "BABIRIK",
    district_code: 6308020,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "SUNGAI PANDAN",
    district_code: 6308030,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "SUNGAI TABUKAN",
    district_code: 6308031,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "AMUNTAI SELATAN",
    district_code: 6308040,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "AMUNTAI TENGAH",
    district_code: 6308050,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "BANJANG",
    district_code: 6308060,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "AMUNTAI UTARA",
    district_code: 6308070,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "HAUR GADING",
    district_code: 6308071,
    regency_code: 6308,
    province_code: 63
  },
  {
    district_name: "BANUA LAWAS",
    district_code: 6309010,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "PUGAAN",
    district_code: 6309020,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "KELUA",
    district_code: 6309030,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "MUARA HARUS",
    district_code: 6309040,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "TANTA",
    district_code: 6309050,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "TANJUNG",
    district_code: 6309060,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "MURUNG PUDAK",
    district_code: 6309070,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "HARUAI",
    district_code: 6309080,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "BINTANG ARA",
    district_code: 6309081,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "UPAU",
    district_code: 6309090,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "MUARA UYA",
    district_code: 6309100,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "JARO",
    district_code: 6309110,
    regency_code: 6309,
    province_code: 63
  },
  {
    district_name: "KUSAN HILIR",
    district_code: 6310010,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "SUNGAI LOBAN",
    district_code: 6310020,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "SATUI",
    district_code: 6310030,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "ANGSANA",
    district_code: 6310031,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "KUSAN HULU",
    district_code: 6310040,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "KURANJI",
    district_code: 6310041,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "BATU LICIN",
    district_code: 6310050,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "KARANG BINTANG",
    district_code: 6310051,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "SIMPANG EMPAT",
    district_code: 6310052,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "MANTEWE",
    district_code: 6310053,
    regency_code: 6310,
    province_code: 63
  },
  {
    district_name: "LAMPIHONG",
    district_code: 6311010,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "BATU MANDI",
    district_code: 6311020,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "AWAYAN",
    district_code: 6311030,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "TEBING TINGGI",
    district_code: 6311031,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "PARINGIN",
    district_code: 6311040,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "PARINGIN SELATAN",
    district_code: 6311041,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "JUAI",
    district_code: 6311050,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "HALONG",
    district_code: 6311060,
    regency_code: 6311,
    province_code: 63
  },
  {
    district_name: "BANJARMASIN SELATAN",
    district_code: 6371010,
    regency_code: 6371,
    province_code: 63
  },
  {
    district_name: "BANJARMASIN TIMUR",
    district_code: 6371020,
    regency_code: 6371,
    province_code: 63
  },
  {
    district_name: "BANJARMASIN BARAT",
    district_code: 6371030,
    regency_code: 6371,
    province_code: 63
  },
  {
    district_name: "BANJARMASIN TENGAH",
    district_code: 6371031,
    regency_code: 6371,
    province_code: 63
  },
  {
    district_name: "BANJARMASIN UTARA",
    district_code: 6371040,
    regency_code: 6371,
    province_code: 63
  },
  {
    district_name: "LANDASAN ULIN",
    district_code: 6372010,
    regency_code: 6372,
    province_code: 63
  },
  {
    district_name: "LIANG ANGGANG",
    district_code: 6372011,
    regency_code: 6372,
    province_code: 63
  },
  {
    district_name: "CEMPAKA",
    district_code: 6372020,
    regency_code: 6372,
    province_code: 63
  },
  {
    district_name: "BANJAR BARU UTARA",
    district_code: 6372031,
    regency_code: 6372,
    province_code: 63
  },
  {
    district_name: "BANJAR BARU SELATAN",
    district_code: 6372032,
    regency_code: 6372,
    province_code: 63
  },
  {
    district_name: "BATU SOPANG",
    district_code: 6401010,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "MUARA SAMU",
    district_code: 6401011,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "BATU ENGAU",
    district_code: 6401021,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "TANJUNG HARAPAN",
    district_code: 6401022,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "PASIR BELENGKONG",
    district_code: 6401030,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "TANAH GROGOT",
    district_code: 6401040,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "KUARO",
    district_code: 6401050,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "LONG IKIS",
    district_code: 6401060,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "MUARA KOMAM",
    district_code: 6401070,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "LONG KALI",
    district_code: 6401080,
    regency_code: 6401,
    province_code: 64
  },
  {
    district_name: "BONGAN",
    district_code: 6402010,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "JEMPANG",
    district_code: 6402020,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "PENYINGGAHAN",
    district_code: 6402030,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "MUARA PAHU",
    district_code: 6402040,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "SILUQ NGURAI",
    district_code: 6402041,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "MUARA LAWA",
    district_code: 6402050,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "BENTIAN BESAR",
    district_code: 6402051,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "DAMAI",
    district_code: 6402060,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "NYUATAN",
    district_code: 6402061,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "BARONG TONGKOK",
    district_code: 6402070,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "LINGGANG BIGUNG",
    district_code: 6402071,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "MELAK",
    district_code: 6402080,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "SEKOLAQ DARAT",
    district_code: 6402081,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "MANOR BULATN",
    district_code: 6402082,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "LONG IRAM",
    district_code: 6402090,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "TERING",
    district_code: 6402091,
    regency_code: 6402,
    province_code: 64
  },
  {
    district_name: "SEMBOJA",
    district_code: 6403010,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "MUARA JAWA",
    district_code: 6403020,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "SANGA-SANGA",
    district_code: 6403030,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "LOA JANAN",
    district_code: 6403040,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "LOA KULU",
    district_code: 6403050,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "MUARA MUNTAI",
    district_code: 6403060,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "MUARA WIS",
    district_code: 6403070,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "KOTABANGUN",
    district_code: 6403080,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "TENGGARONG",
    district_code: 6403090,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "SEBULU",
    district_code: 6403100,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "TENGGARONG SEBERANG",
    district_code: 6403110,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "ANGGANA",
    district_code: 6403120,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "MUARA BADAK",
    district_code: 6403130,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "MARANG KAYU",
    district_code: 6403140,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "MUARA KAMAN",
    district_code: 6403150,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "KENOHAN",
    district_code: 6403160,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "KEMBANG JANGGUT",
    district_code: 6403170,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "TABANG",
    district_code: 6403180,
    regency_code: 6403,
    province_code: 64
  },
  {
    district_name: "MUARA ANCALONG",
    district_code: 6404010,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "BUSANG",
    district_code: 6404011,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "LONG MESANGAT",
    district_code: 6404012,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "MUARA WAHAU",
    district_code: 6404020,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "TELEN",
    district_code: 6404021,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "KONGBENG",
    district_code: 6404022,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "MUARA BENGKAL",
    district_code: 6404030,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "BATU AMPAR",
    district_code: 6404031,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "SANGATTA UTARA",
    district_code: 6404040,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "BENGALON",
    district_code: 6404041,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "TELUK PANDAN",
    district_code: 6404042,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "SANGATTA SELATAN",
    district_code: 6404043,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "RANTAU PULUNG",
    district_code: 6404044,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "SANGKULIRANG",
    district_code: 6404050,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "KALIORANG",
    district_code: 6404051,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "SANDARAN",
    district_code: 6404052,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "KAUBUN",
    district_code: 6404053,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "KARANGAN",
    district_code: 6404054,
    regency_code: 6404,
    province_code: 64
  },
  {
    district_name: "KELAY",
    district_code: 6405010,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "TALISAYAN",
    district_code: 6405020,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "TABALAR",
    district_code: 6405021,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "BIDUK BIDUK",
    district_code: 6405030,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "PULAU DERAWAN",
    district_code: 6405040,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "MARATUA",
    district_code: 6405041,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "SAMBALIUNG",
    district_code: 6405050,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "TANJUNG REDEB",
    district_code: 6405060,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "GUNUNG TABUR",
    district_code: 6405070,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "SEGAH",
    district_code: 6405080,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "TELUK BAYUR",
    district_code: 6405090,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "BATU PUTIH",
    district_code: 6405100,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "BIATAN",
    district_code: 6405110,
    regency_code: 6405,
    province_code: 64
  },
  {
    district_name: "BABULU",
    district_code: 6409010,
    regency_code: 6409,
    province_code: 64
  },
  {
    district_name: "WARU",
    district_code: 6409020,
    regency_code: 6409,
    province_code: 64
  },
  {
    district_name: "PENAJAM",
    district_code: 6409030,
    regency_code: 6409,
    province_code: 64
  },
  {
    district_name: "SEPAKU",
    district_code: 6409040,
    regency_code: 6409,
    province_code: 64
  },
  {
    district_name: "LAHAM",
    district_code: 6411010,
    regency_code: 6411,
    province_code: 64
  },
  {
    district_name: "LONG HUBUNG",
    district_code: 6411020,
    regency_code: 6411,
    province_code: 64
  },
  {
    district_name: "LONG BAGUN",
    district_code: 6411030,
    regency_code: 6411,
    province_code: 64
  },
  {
    district_name: "LONG PAHANGAI",
    district_code: 6411040,
    regency_code: 6411,
    province_code: 64
  },
  {
    district_name: "LONG APARI",
    district_code: 6411050,
    regency_code: 6411,
    province_code: 64
  },
  {
    district_name: "BALIKPAPAN SELATAN",
    district_code: 6471010,
    regency_code: 6471,
    province_code: 64
  },
  {
    district_name: "BALIKPAPAN KOTA",
    district_code: 6471011,
    regency_code: 6471,
    province_code: 64
  },
  {
    district_name: "BALIKPAPAN TIMUR",
    district_code: 6471020,
    regency_code: 6471,
    province_code: 64
  },
  {
    district_name: "BALIKPAPAN UTARA",
    district_code: 6471030,
    regency_code: 6471,
    province_code: 64
  },
  {
    district_name: "BALIKPAPAN TENGAH",
    district_code: 6471040,
    regency_code: 6471,
    province_code: 64
  },
  {
    district_name: "BALIKPAPAN BARAT",
    district_code: 6471050,
    regency_code: 6471,
    province_code: 64
  },
  {
    district_name: "PALARAN",
    district_code: 6472010,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SAMARINDA ILIR",
    district_code: 6472020,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SAMARINDA KOTA",
    district_code: 6472021,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SAMBUTAN",
    district_code: 6472022,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SAMARINDA SEBERANG",
    district_code: 6472030,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "LOA JANAN ILIR",
    district_code: 6472031,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SUNGAI KUNJANG",
    district_code: 6472040,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SAMARINDA ULU",
    district_code: 6472050,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SAMARINDA UTARA",
    district_code: 6472060,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "SUNGAI PINANG",
    district_code: 6472061,
    regency_code: 6472,
    province_code: 64
  },
  {
    district_name: "BONTANG SELATAN",
    district_code: 6474010,
    regency_code: 6474,
    province_code: 64
  },
  {
    district_name: "BONTANG UTARA",
    district_code: 6474020,
    regency_code: 6474,
    province_code: 64
  },
  {
    district_name: "BONTANG BARAT",
    district_code: 6474030,
    regency_code: 6474,
    province_code: 64
  },
  {
    district_name: "SUNGAI BOH",
    district_code: 6501010,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "KAYAN SELATAN",
    district_code: 6501020,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "KAYAN HULU",
    district_code: 6501030,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "KAYAN HILIR",
    district_code: 6501040,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "PUJUNGAN",
    district_code: 6501050,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "BAHAU HULU",
    district_code: 6501060,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "SUNGAI TUBU",
    district_code: 6501070,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MALINAU SELATAN HULU",
    district_code: 6501080,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MALINAU SELATAN",
    district_code: 6501090,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MALINAU SELATAN HILIR",
    district_code: 6501100,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MENTARANG",
    district_code: 6501110,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MENTARANG HULU",
    district_code: 6501120,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MALINAU UTARA",
    district_code: 6501130,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MALINAU BARAT",
    district_code: 6501140,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "MALINAU KOTA",
    district_code: 6501150,
    regency_code: 6501,
    province_code: 65
  },
  {
    district_name: "PESO",
    district_code: 6502010,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "PESO HILIR",
    district_code: 6502020,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "TANJUNG PALAS BARAT",
    district_code: 6502030,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "TANJUNG PALAS",
    district_code: 6502040,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "TANJUNG SELOR",
    district_code: 6502050,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "TANJUNG PALAS TIMUR",
    district_code: 6502060,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "TANJUNG PALAS TENGAH",
    district_code: 6502070,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "TANJUNG PALAS UTARA",
    district_code: 6502080,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "SEKATAK",
    district_code: 6502090,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "BUNYU",
    district_code: 6502100,
    regency_code: 6502,
    province_code: 65
  },
  {
    district_name: "MURUK RIAN",
    district_code: 6503010,
    regency_code: 6503,
    province_code: 65
  },
  {
    district_name: "SESAYAP",
    district_code: 6503020,
    regency_code: 6503,
    province_code: 65
  },
  {
    district_name: "BETAYAU",
    district_code: 6503030,
    regency_code: 6503,
    province_code: 65
  },
  {
    district_name: "SESAYAP HILIR",
    district_code: 6503040,
    regency_code: 6503,
    province_code: 65
  },
  {
    district_name: "TANA LIA",
    district_code: 6503050,
    regency_code: 6503,
    province_code: 65
  },
  {
    district_name: "KRAYAN SELATAN",
    district_code: 6504010,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "KRAYAN TENGAH",
    district_code: 6504011,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "KRAYAN",
    district_code: 6504020,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "KRAYAN TIMUR",
    district_code: 6504021,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "KRAYAN BARAT",
    district_code: 6504022,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "LUMBIS OGONG",
    district_code: 6504030,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "LUMBIS",
    district_code: 6504040,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEMBAKUNG ATULAI",
    district_code: 6504050,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEMBAKUNG",
    district_code: 6504060,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEBUKU",
    district_code: 6504070,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "TULIN ONSOI",
    district_code: 6504080,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEI MENGGARIS",
    district_code: 6504090,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "NUNUKAN",
    district_code: 6504100,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "NUNUKAN SELATAN",
    district_code: 6504110,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEBATIK BARAT",
    district_code: 6504120,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEBATIK",
    district_code: 6504130,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEBATIK TIMUR",
    district_code: 6504140,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEBATIK TENGAH",
    district_code: 6504150,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "SEBATIK UTARA",
    district_code: 6504160,
    regency_code: 6504,
    province_code: 65
  },
  {
    district_name: "TARAKAN TIMUR",
    district_code: 6571010,
    regency_code: 6571,
    province_code: 65
  },
  {
    district_name: "TARAKAN TENGAH",
    district_code: 6571020,
    regency_code: 6571,
    province_code: 65
  },
  {
    district_name: "TARAKAN BARAT",
    district_code: 6571030,
    regency_code: 6571,
    province_code: 65
  },
  {
    district_name: "TARAKAN UTARA",
    district_code: 6571040,
    regency_code: 6571,
    province_code: 65
  },
  {
    district_name: "DUMOGA BARAT",
    district_code: 7101021,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "DUMOGA UTARA",
    district_code: 7101022,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "DUMOGA TIMUR",
    district_code: 7101023,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "DUMOGA TENGAH",
    district_code: 7101024,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "DUMOGA TENGGARA",
    district_code: 7101025,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "DUMOGA",
    district_code: 7101026,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "LOLAYAN",
    district_code: 7101060,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "PASSI BARAT",
    district_code: 7101081,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "PASSI TIMUR",
    district_code: 7101082,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "BILALANG",
    district_code: 7101083,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "POIGAR",
    district_code: 7101090,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "BOLAANG",
    district_code: 7101100,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "BOLAANG TIMUR",
    district_code: 7101101,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "LOLAK",
    district_code: 7101110,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "SANGTOMBOLANG",
    district_code: 7101120,
    regency_code: 7101,
    province_code: 71
  },
  {
    district_name: "LANGOWAN TIMUR",
    district_code: 7102091,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "LANGOWAN BARAT",
    district_code: 7102092,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "LANGOWAN SELATAN",
    district_code: 7102093,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "LANGOWAN UTARA",
    district_code: 7102094,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TOMPASO",
    district_code: 7102110,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TOMPASO BARAT",
    district_code: 7102111,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "KAWANGKOAN",
    district_code: 7102120,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "KAWANGKOAN BARAT",
    district_code: 7102121,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "KAWANGKOAN UTARA",
    district_code: 7102122,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "SONDER",
    district_code: 7102130,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TOMBARIRI",
    district_code: 7102160,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TOMBARIRI TIMUR",
    district_code: 7102161,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "PINELENG",
    district_code: 7102170,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TOMBULU",
    district_code: 7102171,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "MANDOLANG",
    district_code: 7102172,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TONDANO BARAT",
    district_code: 7102190,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TONDANO SELATAN",
    district_code: 7102191,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "REMBOKEN",
    district_code: 7102200,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "KAKAS",
    district_code: 7102210,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "KAKAS BARAT",
    district_code: 7102211,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "LEMBEAN TIMUR",
    district_code: 7102220,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "ERIS",
    district_code: 7102230,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "KOMBI",
    district_code: 7102240,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TONDANO TIMUR",
    district_code: 7102250,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "TONDANO UTARA",
    district_code: 7102251,
    regency_code: 7102,
    province_code: 71
  },
  {
    district_name: "MANGANITU SELATAN",
    district_code: 7103040,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TATOARENG",
    district_code: 7103041,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TAMAKO",
    district_code: 7103050,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TABUKAN SELATAN",
    district_code: 7103060,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TABUKAN SELATAN TENGAH",
    district_code: 7103061,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TABUKAN SELATAN TENGGARA",
    district_code: 7103062,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TABUKAN TENGAH",
    district_code: 7103070,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "MANGANITU",
    district_code: 7103080,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TAHUNA",
    district_code: 7103090,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TAHUNA TIMUR",
    district_code: 7103091,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TAHUNA BARAT",
    district_code: 7103092,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "TABUKAN UTARA",
    district_code: 7103100,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "NUSA TABUKAN",
    district_code: 7103101,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "KEPULAUAN MARORE",
    district_code: 7103102,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "KENDAHE",
    district_code: 7103110,
    regency_code: 7103,
    province_code: 71
  },
  {
    district_name: "KABARUAN",
    district_code: 7104010,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "DAMAU",
    district_code: 7104011,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "LIRUNG",
    district_code: 7104020,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "SALIBABU",
    district_code: 7104021,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "KALONGAN",
    district_code: 7104022,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "MORONGE",
    district_code: 7104023,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "MELONGUANE",
    district_code: 7104030,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "MELONGUANE TIMUR",
    district_code: 7104031,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "BEO",
    district_code: 7104040,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "BEO UTARA",
    district_code: 7104041,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "BEO SELATAN",
    district_code: 7104042,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "RAINIS",
    district_code: 7104050,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "TAMPA NA'MMA",
    district_code: 7104051,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "PULUTAN",
    district_code: 7104052,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "ESSANG",
    district_code: 7104060,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "ESSANG SELATAN",
    district_code: 7104061,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "GEMEH",
    district_code: 7104070,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "NANUSA",
    district_code: 7104080,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "MIANGAS",
    district_code: 7104081,
    regency_code: 7104,
    province_code: 71
  },
  {
    district_name: "MODOINDING",
    district_code: 7105010,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "TOMPASO BARU",
    district_code: 7105020,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "MAESAAN",
    district_code: 7105021,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "RANOYAPO",
    district_code: 7105070,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "MOTOLING",
    district_code: 7105080,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "KUMELEMBUAI",
    district_code: 7105081,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "MOTOLING BARAT",
    district_code: 7105082,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "MOTOLING TIMUR",
    district_code: 7105083,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "SINONSAYANG",
    district_code: 7105090,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "TENGA",
    district_code: 7105100,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "AMURANG",
    district_code: 7105111,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "AMURANG BARAT",
    district_code: 7105112,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "AMURANG TIMUR",
    district_code: 7105113,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "TARERAN",
    district_code: 7105120,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "SULTA",
    district_code: 7105121,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "TUMPAAN",
    district_code: 7105130,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "TATAPAAN",
    district_code: 7105131,
    regency_code: 7105,
    province_code: 71
  },
  {
    district_name: "KEMA",
    district_code: 7106010,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "KAUDITAN",
    district_code: 7106020,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "AIRMADIDI",
    district_code: 7106030,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "KALAWAT",
    district_code: 7106040,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "DIMEMBE",
    district_code: 7106050,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "TALAWAAN",
    district_code: 7106051,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "WORI",
    district_code: 7106060,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "LIKUPANG BARAT",
    district_code: 7106070,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "LIKUPANG TIMUR",
    district_code: 7106080,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "LIKUPANG SELATAN",
    district_code: 7106081,
    regency_code: 7106,
    province_code: 71
  },
  {
    district_name: "SANGKUB",
    district_code: 7107010,
    regency_code: 7107,
    province_code: 71
  },
  {
    district_name: "BINTAUNA",
    district_code: 7107020,
    regency_code: 7107,
    province_code: 71
  },
  {
    district_name: "BOLANG ITANG TIMUR",
    district_code: 7107030,
    regency_code: 7107,
    province_code: 71
  },
  {
    district_name: "BOLANG ITANG BARAT",
    district_code: 7107040,
    regency_code: 7107,
    province_code: 71
  },
  {
    district_name: "KAIDIPANG",
    district_code: 7107050,
    regency_code: 7107,
    province_code: 71
  },
  {
    district_name: "PINOGALUMAN",
    district_code: 7107060,
    regency_code: 7107,
    province_code: 71
  },
  {
    district_name: "BIARO",
    district_code: 7108010,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "TAGULANDANG SELATAN",
    district_code: 7108020,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "TAGULANDANG",
    district_code: 7108030,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "TAGULANDANG UTARA",
    district_code: 7108040,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "SIAU BARAT SELATAN",
    district_code: 7108050,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "SIAU TIMUR SELATAN",
    district_code: 7108060,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "SIAU BARAT",
    district_code: 7108070,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "SIAU TENGAH",
    district_code: 7108080,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "SIAU TIMUR",
    district_code: 7108090,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "SIAU BARAT UTARA",
    district_code: 7108100,
    regency_code: 7108,
    province_code: 71
  },
  {
    district_name: "RATATOTOK",
    district_code: 7109010,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "PUSOMAEN",
    district_code: 7109020,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "BELANG",
    district_code: 7109030,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "RATAHAN",
    district_code: 7109040,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "PASAN",
    district_code: 7109041,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "RATAHAN TIMUR",
    district_code: 7109042,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "TOMBATU",
    district_code: 7109050,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "TOMBATU TIMUR",
    district_code: 7109051,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "TOMBATU UTARA",
    district_code: 7109052,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "TOULUAAN",
    district_code: 7109060,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "TOULUAAN SELATAN",
    district_code: 7109061,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "SILIAN RAYA",
    district_code: 7109062,
    regency_code: 7109,
    province_code: 71
  },
  {
    district_name: "POSIGADAN",
    district_code: 7110010,
    regency_code: 7110,
    province_code: 71
  },
  {
    district_name: "TOMINI",
    district_code: 7110011,
    regency_code: 7110,
    province_code: 71
  },
  {
    district_name: "BOLANG UKI",
    district_code: 7110020,
    regency_code: 7110,
    province_code: 71
  },
  {
    district_name: "HELUMO",
    district_code: 7110021,
    regency_code: 7110,
    province_code: 71
  },
  {
    district_name: "PINOLOSIAN",
    district_code: 7110030,
    regency_code: 7110,
    province_code: 71
  },
  {
    district_name: "PINOLOSIAN TENGAH",
    district_code: 7110040,
    regency_code: 7110,
    province_code: 71
  },
  {
    district_name: "PINOLOSIAN TIMUR",
    district_code: 7110050,
    regency_code: 7110,
    province_code: 71
  },
  {
    district_name: "NUANGAN",
    district_code: 7111010,
    regency_code: 7111,
    province_code: 71
  },
  {
    district_name: "MOTONGKAD",
    district_code: 7111011,
    regency_code: 7111,
    province_code: 71
  },
  {
    district_name: "TUTUYAN",
    district_code: 7111020,
    regency_code: 7111,
    province_code: 71
  },
  {
    district_name: "KOTABUNAN",
    district_code: 7111030,
    regency_code: 7111,
    province_code: 71
  },
  {
    district_name: "MODAYAG",
    district_code: 7111040,
    regency_code: 7111,
    province_code: 71
  },
  {
    district_name: "MOOAT",
    district_code: 7111041,
    regency_code: 7111,
    province_code: 71
  },
  {
    district_name: "MODAYAG BARAT",
    district_code: 7111050,
    regency_code: 7111,
    province_code: 71
  },
  {
    district_name: "MALALAYANG",
    district_code: 7171010,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "SARIO",
    district_code: 7171020,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "WANEA",
    district_code: 7171021,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "WENANG",
    district_code: 7171030,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "TIKALA",
    district_code: 7171031,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "PAAL DUA",
    district_code: 7171032,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "MAPANGET",
    district_code: 7171040,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "SINGKIL",
    district_code: 7171051,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "TUMINTING",
    district_code: 7171052,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "BUNAKEN",
    district_code: 7171053,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "BUNAKEN KEPULAUAN",
    district_code: 7171054,
    regency_code: 7171,
    province_code: 71
  },
  {
    district_name: "MADIDIR",
    district_code: 7172010,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "MATUARI",
    district_code: 7172011,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "GIRIAN",
    district_code: 7172012,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "LEMBEH SELATAN",
    district_code: 7172021,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "LEMBEH UTARA",
    district_code: 7172022,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "AERTEMBAGA",
    district_code: 7172030,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "MAESA",
    district_code: 7172031,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "RANOWULU",
    district_code: 7172040,
    regency_code: 7172,
    province_code: 71
  },
  {
    district_name: "TOMOHON SELATAN",
    district_code: 7173010,
    regency_code: 7173,
    province_code: 71
  },
  {
    district_name: "TOMOHON TENGAH",
    district_code: 7173020,
    regency_code: 7173,
    province_code: 71
  },
  {
    district_name: "TOMOHON TIMUR",
    district_code: 7173021,
    regency_code: 7173,
    province_code: 71
  },
  {
    district_name: "TOMOHON BARAT",
    district_code: 7173022,
    regency_code: 7173,
    province_code: 71
  },
  {
    district_name: "TOMOHON UTARA",
    district_code: 7173030,
    regency_code: 7173,
    province_code: 71
  },
  {
    district_name: "KOTAMOBAGU SELATAN",
    district_code: 7174010,
    regency_code: 7174,
    province_code: 71
  },
  {
    district_name: "KOTAMOBAGU TIMUR",
    district_code: 7174020,
    regency_code: 7174,
    province_code: 71
  },
  {
    district_name: "KOTAMOBAGU BARAT",
    district_code: 7174030,
    regency_code: 7174,
    province_code: 71
  },
  {
    district_name: "KOTAMOBAGU UTARA",
    district_code: 7174040,
    regency_code: 7174,
    province_code: 71
  },
  {
    district_name: "TOTIKUM",
    district_code: 7201030,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "TOTIKUM SELATAN",
    district_code: 7201031,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "TINANGKUNG",
    district_code: 7201040,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "TINANGKUNG SELATAN",
    district_code: 7201041,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "TINANGKUNG UTARA",
    district_code: 7201042,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "LIANG",
    district_code: 7201050,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "PELING TENGAH",
    district_code: 7201051,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "BULAGI",
    district_code: 7201060,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "BULAGI SELATAN",
    district_code: 7201061,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "BULAGI UTARA",
    district_code: 7201062,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "BUKO",
    district_code: 7201070,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "BUKO SELATAN",
    district_code: 7201071,
    regency_code: 7201,
    province_code: 72
  },
  {
    district_name: "TOILI",
    district_code: 7202010,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "TOILI BARAT",
    district_code: 7202011,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "MOILONG",
    district_code: 7202012,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "BATUI",
    district_code: 7202020,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "BATUI SELATAN",
    district_code: 7202021,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "BUNTA",
    district_code: 7202030,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "NUHON",
    district_code: 7202031,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "SIMPANG RAYA",
    district_code: 7202032,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "KINTOM",
    district_code: 7202040,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "LUWUK",
    district_code: 7202050,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "LUWUK TIMUR",
    district_code: 7202051,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "LUWUK UTARA",
    district_code: 7202052,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "LUWUK SELATAN",
    district_code: 7202053,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "NAMBO",
    district_code: 7202054,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "PAGIMANA",
    district_code: 7202060,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "BUALEMO",
    district_code: 7202061,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "LOBU",
    district_code: 7202062,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "LAMALA",
    district_code: 7202070,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "MASAMA",
    district_code: 7202071,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "MANTOH",
    district_code: 7202072,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "BALANTAK",
    district_code: 7202080,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "BALANTAK SELATAN",
    district_code: 7202081,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "BALANTAK UTARA",
    district_code: 7202082,
    regency_code: 7202,
    province_code: 72
  },
  {
    district_name: "MENUI KEPULAUAN",
    district_code: 7203010,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "BUNGKU SELATAN",
    district_code: 7203020,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "BAHODOPI",
    district_code: 7203021,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "BUNGKU PESISIR",
    district_code: 7203022,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "BUNGKU TENGAH",
    district_code: 7203030,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "BUNGKU TIMUR",
    district_code: 7203031,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "BUNGKU BARAT",
    district_code: 7203040,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "BUMI RAYA",
    district_code: 7203041,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "WITA PONDA",
    district_code: 7203042,
    regency_code: 7203,
    province_code: 72
  },
  {
    district_name: "PAMONA SELATAN",
    district_code: 7204010,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "PAMONA BARAT",
    district_code: 7204011,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "PAMONA TENGGARA",
    district_code: 7204012,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "LORE SELATAN",
    district_code: 7204020,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "LORE BARAT",
    district_code: 7204021,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "PAMONA PUSALEMBA",
    district_code: 7204030,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "PAMONA TIMUR",
    district_code: 7204031,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "PAMONA UTARA",
    district_code: 7204032,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "LORE UTARA",
    district_code: 7204040,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "LORE TENGAH",
    district_code: 7204041,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "LORE TIMUR",
    district_code: 7204042,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "LORE PEORE",
    district_code: 7204043,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "POSO PESISIR",
    district_code: 7204050,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "POSO PESISIR SELATAN",
    district_code: 7204051,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "POSO PESISIR UTARA",
    district_code: 7204052,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "LAGE",
    district_code: 7204060,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "POSO KOTA",
    district_code: 7204070,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "POSO KOTA UTARA",
    district_code: 7204071,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "POSO KOTA SELATAN",
    district_code: 7204072,
    regency_code: 7204,
    province_code: 72
  },
  {
    district_name: "RIO PAKAVA",
    district_code: 7205041,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "PINEMBANI",
    district_code: 7205051,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "BANAWA",
    district_code: 7205080,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "BANAWA SELATAN",
    district_code: 7205081,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "BANAWA TENGAH",
    district_code: 7205082,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "LABUAN",
    district_code: 7205090,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "TANANTOVEA",
    district_code: 7205091,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "SINDUE",
    district_code: 7205100,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "SINDUE TOMBUSABORA",
    district_code: 7205101,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "SINDUE TOBATA",
    district_code: 7205102,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "SIRENJA",
    district_code: 7205120,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "BALAESANG",
    district_code: 7205130,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "BALAESANG TANJUNG",
    district_code: 7205131,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "DAMPELAS",
    district_code: 7205140,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "SOJOL",
    district_code: 7205160,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "SOJOL UTARA",
    district_code: 7205161,
    regency_code: 7205,
    province_code: 72
  },
  {
    district_name: "DAMPAL SELATAN",
    district_code: 7206010,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "DAMPAL UTARA",
    district_code: 7206020,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "DONDO",
    district_code: 7206030,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "OGODEIDE",
    district_code: 7206031,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "BASIDONDO",
    district_code: 7206032,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "BAOLAN",
    district_code: 7206040,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "LAMPASIO",
    district_code: 7206041,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "GALANG",
    district_code: 7206050,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "TOLITOLI UTARA",
    district_code: 7206060,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "DAKO PAMEAN",
    district_code: 7206061,
    regency_code: 7206,
    province_code: 72
  },
  {
    district_name: "LAKEA",
    district_code: 7207010,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "BIAU",
    district_code: 7207011,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "KARAMAT",
    district_code: 7207012,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "MOMUNU",
    district_code: 7207020,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "TILOAN",
    district_code: 7207021,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "BOKAT",
    district_code: 7207030,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "BUKAL",
    district_code: 7207031,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "BUNOBOGU",
    district_code: 7207040,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "GADUNG",
    district_code: 7207041,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "PALELEH",
    district_code: 7207050,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "PALELEH BARAT",
    district_code: 7207051,
    regency_code: 7207,
    province_code: 72
  },
  {
    district_name: "SAUSU",
    district_code: 7208010,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "TORUE",
    district_code: 7208011,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "BALINGGI",
    district_code: 7208012,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "PARIGI",
    district_code: 7208020,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "PARIGI SELATAN",
    district_code: 7208021,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "PARIGI BARAT",
    district_code: 7208022,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "PARIGI UTARA",
    district_code: 7208023,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "PARIGI TENGAH",
    district_code: 7208024,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "AMPIBABO",
    district_code: 7208030,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "KASIMBAR",
    district_code: 7208031,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "TORIBULU",
    district_code: 7208032,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "SINIU",
    district_code: 7208033,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "TINOMBO",
    district_code: 7208040,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "TINOMBO SELATAN",
    district_code: 7208041,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "SIDOAN",
    district_code: 7208042,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "TOMINI",
    district_code: 7208050,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "MEPANGA",
    district_code: 7208051,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "PALASA",
    district_code: 7208052,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "MOUTONG",
    district_code: 7208060,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "BOLANO LAMBUNU",
    district_code: 7208061,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "TAOPA",
    district_code: 7208062,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "BOLANO",
    district_code: 7208063,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "ONGKA MALINO",
    district_code: 7208064,
    regency_code: 7208,
    province_code: 72
  },
  {
    district_name: "TOJO BARAT",
    district_code: 7209010,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "TOJO",
    district_code: 7209020,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "ULUBONGKA",
    district_code: 7209030,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "AMPANA TETE",
    district_code: 7209040,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "AMPANA KOTA",
    district_code: 7209050,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "RATOLINDO",
    district_code: 7209051,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "UNA - UNA",
    district_code: 7209060,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "BATUDAKA",
    district_code: 7209061,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "TOGEAN",
    district_code: 7209070,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "WALEA KEPULAUAN",
    district_code: 7209080,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "WALEA BESAR",
    district_code: 7209081,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "TALATAKO",
    district_code: 7209082,
    regency_code: 7209,
    province_code: 72
  },
  {
    district_name: "PIPIKORO",
    district_code: 7210010,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "KULAWI SELATAN",
    district_code: 7210020,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "KULAWI",
    district_code: 7210030,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "LINDU",
    district_code: 7210040,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "NOKILALAKI",
    district_code: 7210050,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "PALOLO",
    district_code: 7210060,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "GUMBASA",
    district_code: 7210070,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "DOLO SELATAN",
    district_code: 7210080,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "DOLO BARAT",
    district_code: 7210090,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "TANAMBULAVA",
    district_code: 7210100,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "DOLO",
    district_code: 7210110,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "SIGI BIROMARU",
    district_code: 7210120,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "MARAWOLA",
    district_code: 7210130,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "MARAWOLA BARAT",
    district_code: 7210140,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "KINOVARO",
    district_code: 7210150,
    regency_code: 7210,
    province_code: 72
  },
  {
    district_name: "BANGKURUNG",
    district_code: 7211010,
    regency_code: 7211,
    province_code: 72
  },
  {
    district_name: "LABOBO",
    district_code: 7211020,
    regency_code: 7211,
    province_code: 72
  },
  {
    district_name: "BANGGAI UTARA",
    district_code: 7211030,
    regency_code: 7211,
    province_code: 72
  },
  {
    district_name: "BANGGAI",
    district_code: 7211040,
    regency_code: 7211,
    province_code: 72
  },
  {
    district_name: "BANGGAI TENGAH",
    district_code: 7211050,
    regency_code: 7211,
    province_code: 72
  },
  {
    district_name: "BANGGAI SELATAN",
    district_code: 7211060,
    regency_code: 7211,
    province_code: 72
  },
  {
    district_name: "BOKAN KEPULAUAN",
    district_code: 7211070,
    regency_code: 7211,
    province_code: 72
  },
  {
    district_name: "MORI ATAS",
    district_code: 7212010,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "LEMBO",
    district_code: 7212020,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "LEMBO RAYA",
    district_code: 7212030,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "PETASIA TIMUR",
    district_code: 7212040,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "PETASIA",
    district_code: 7212050,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "PETASIA BARAT",
    district_code: 7212060,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "MORI UTARA",
    district_code: 7212070,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "SOYO JAYA",
    district_code: 7212080,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "BUNGKU UTARA",
    district_code: 7212090,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "MAMOSALATO",
    district_code: 7212100,
    regency_code: 7212,
    province_code: 72
  },
  {
    district_name: "PALU BARAT",
    district_code: 7271010,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "TATANGA",
    district_code: 7271011,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "ULUJADI",
    district_code: 7271012,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "PALU SELATAN",
    district_code: 7271020,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "PALU TIMUR",
    district_code: 7271030,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "MANTIKULORE",
    district_code: 7271031,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "PALU UTARA",
    district_code: 7271040,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "TAWAELI",
    district_code: 7271041,
    regency_code: 7271,
    province_code: 72
  },
  {
    district_name: "PASIMARANNU",
    district_code: 7301010,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "PASILAMBENA",
    district_code: 7301011,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "PASIMASSUNGGU",
    district_code: 7301020,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "TAKABONERATE",
    district_code: 7301021,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "PASIMASSUNGGU TIMUR",
    district_code: 7301022,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "BONTOSIKUYU",
    district_code: 7301030,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "BONTOHARU",
    district_code: 7301040,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "BENTENG",
    district_code: 7301041,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "BONTOMANAI",
    district_code: 7301042,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "BONTOMATENE",
    district_code: 7301050,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "BUKI",
    district_code: 7301051,
    regency_code: 7301,
    province_code: 73
  },
  {
    district_name: "GANTARANG",
    district_code: 7302010,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "UJUNG BULU",
    district_code: 7302020,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "UJUNG LOE",
    district_code: 7302021,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "BONTO BAHARI",
    district_code: 7302030,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "BONTOTIRO",
    district_code: 7302040,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "HERO LANGE-LANGE",
    district_code: 7302050,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "KAJANG",
    district_code: 7302060,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "BULUKUMPA",
    district_code: 7302070,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "RILAU ALE",
    district_code: 7302080,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "KINDANG",
    district_code: 7302090,
    regency_code: 7302,
    province_code: 73
  },
  {
    district_name: "BISSAPPU",
    district_code: 7303010,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "ULUERE",
    district_code: 7303011,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "SINOA",
    district_code: 7303012,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "BANTAENG",
    district_code: 7303020,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "EREMERASA",
    district_code: 7303021,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "TOMPOBULU",
    district_code: 7303030,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "PA'JUKUKANG",
    district_code: 7303031,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "GANTARANGKEKE",
    district_code: 7303032,
    regency_code: 7303,
    province_code: 73
  },
  {
    district_name: "BANGKALA",
    district_code: 7304010,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "BANGKALA BARAT",
    district_code: 7304011,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "TAMALATEA",
    district_code: 7304020,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "BONTORAMBA",
    district_code: 7304021,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "BINAMU",
    district_code: 7304030,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "TURATEA",
    district_code: 7304031,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "BATANG",
    district_code: 7304040,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "ARUNGKEKE",
    district_code: 7304041,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "TAROWANG",
    district_code: 7304042,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "KELARA",
    district_code: 7304050,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "RUMBIA",
    district_code: 7304051,
    regency_code: 7304,
    province_code: 73
  },
  {
    district_name: "MANGARA BOMBANG",
    district_code: 7305010,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "MAPPAKASUNGGU",
    district_code: 7305020,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "SANROBONE",
    district_code: 7305021,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "POLOMBANGKENG SELATAN",
    district_code: 7305030,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "PATTALLASSANG",
    district_code: 7305031,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "POLOMBANGKENG UTARA",
    district_code: 7305040,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "GALESONG SELATAN",
    district_code: 7305050,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "GALESONG",
    district_code: 7305051,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "GALESONG UTARA",
    district_code: 7305060,
    regency_code: 7305,
    province_code: 73
  },
  {
    district_name: "BONTONOMPO",
    district_code: 7306010,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BONTONOMPO SELATAN",
    district_code: 7306011,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BAJENG",
    district_code: 7306020,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BAJENG BARAT",
    district_code: 7306021,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "PALLANGGA",
    district_code: 7306030,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BAROMBONG",
    district_code: 7306031,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "SOMBA OPU",
    district_code: 7306040,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BONTOMARANNU",
    district_code: 7306050,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "PATTALLASSANG",
    district_code: 7306051,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "PARANGLOE",
    district_code: 7306060,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "MANUJU",
    district_code: 7306061,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "TINGGIMONCONG",
    district_code: 7306070,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "TOMBOLO PAO",
    district_code: 7306071,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "PARIGI",
    district_code: 7306072,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BUNGAYA",
    district_code: 7306080,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BONTOLEMPANGAN",
    district_code: 7306081,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "TOMPOBULU",
    district_code: 7306090,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "BIRINGBULU",
    district_code: 7306091,
    regency_code: 7306,
    province_code: 73
  },
  {
    district_name: "SINJAI BARAT",
    district_code: 7307010,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "SINJAI BORONG",
    district_code: 7307020,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "SINJAI SELATAN",
    district_code: 7307030,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "TELLU LIMPOE",
    district_code: 7307040,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "SINJAI TIMUR",
    district_code: 7307050,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "SINJAI TENGAH",
    district_code: 7307060,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "SINJAI UTARA",
    district_code: 7307070,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "BULUPODDO",
    district_code: 7307080,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "PULAU SEMBILAN",
    district_code: 7307090,
    regency_code: 7307,
    province_code: 73
  },
  {
    district_name: "MANDAI",
    district_code: 7308010,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "MONCONGLOE",
    district_code: 7308011,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "MAROS BARU",
    district_code: 7308020,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "MARUSU",
    district_code: 7308021,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "TURIKALE",
    district_code: 7308022,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "LAU",
    district_code: 7308023,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "BONTOA",
    district_code: 7308030,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "BANTIMURUNG",
    district_code: 7308040,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "SIMBANG",
    district_code: 7308041,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "TANRALILI",
    district_code: 7308050,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "TOMPU BULU",
    district_code: 7308051,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "CAMBA",
    district_code: 7308060,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "CENRANA",
    district_code: 7308061,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "MALLAWA",
    district_code: 7308070,
    regency_code: 7308,
    province_code: 73
  },
  {
    district_name: "LIUKANG TANGAYA",
    district_code: 7309010,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "LIUKANG KALMAS",
    district_code: 7309020,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "LIUKANG TUPABBIRING",
    district_code: 7309030,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "LIUKANG TUPABBIRING UTARA",
    district_code: 7309031,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "PANGKAJENE",
    district_code: 7309040,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "MINASATENE",
    district_code: 7309041,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "BALOCCI",
    district_code: 7309050,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "TONDONG TALLASA",
    district_code: 7309051,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "BUNGORO",
    district_code: 7309060,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "LABAKKANG",
    district_code: 7309070,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "MA'RANG",
    district_code: 7309080,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "SEGERI",
    district_code: 7309091,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "MANDALLE",
    district_code: 7309092,
    regency_code: 7309,
    province_code: 73
  },
  {
    district_name: "TANETE RIAJA",
    district_code: 7310010,
    regency_code: 7310,
    province_code: 73
  },
  {
    district_name: "PUJANANTING",
    district_code: 7310011,
    regency_code: 7310,
    province_code: 73
  },
  {
    district_name: "TANETE RILAU",
    district_code: 7310020,
    regency_code: 7310,
    province_code: 73
  },
  {
    district_name: "BARRU",
    district_code: 7310030,
    regency_code: 7310,
    province_code: 73
  },
  {
    district_name: "SOPPENG RIAJA",
    district_code: 7310040,
    regency_code: 7310,
    province_code: 73
  },
  {
    district_name: "BALUSU",
    district_code: 7310041,
    regency_code: 7310,
    province_code: 73
  },
  {
    district_name: "MALLUSETASI",
    district_code: 7310050,
    regency_code: 7310,
    province_code: 73
  },
  {
    district_name: "BONTOCANI",
    district_code: 7311010,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "KAHU",
    district_code: 7311020,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "KAJUARA",
    district_code: 7311030,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "SALOMEKKO",
    district_code: 7311040,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "TONRA",
    district_code: 7311050,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "PATIMPENG",
    district_code: 7311060,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "LIBURENG",
    district_code: 7311070,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "MARE",
    district_code: 7311080,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "SIBULUE",
    district_code: 7311090,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "CINA",
    district_code: 7311100,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "BAREBBO",
    district_code: 7311110,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "PONRE",
    district_code: 7311120,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "LAPPARIAJA",
    district_code: 7311130,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "LAMURU",
    district_code: 7311140,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "TELLU LIMPOE",
    district_code: 7311141,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "BENGO",
    district_code: 7311150,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "ULAWENG",
    district_code: 7311160,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "PALAKKA",
    district_code: 7311170,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "AWANGPONE",
    district_code: 7311180,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "TELLU SIATTINGE",
    district_code: 7311190,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "AMALI",
    district_code: 7311200,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "AJANGALE",
    district_code: 7311210,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "DUA BOCCOE",
    district_code: 7311220,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "CENRANA",
    district_code: 7311230,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "TANETE RIATTANG BARAT",
    district_code: 7311710,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "TANETE RIATTANG",
    district_code: 7311720,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "TANETE RIATTANG TIMUR",
    district_code: 7311730,
    regency_code: 7311,
    province_code: 73
  },
  {
    district_name: "MARIO RIWAWO",
    district_code: 7312010,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "LALABATA",
    district_code: 7312020,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "LILI RIAJA",
    district_code: 7312030,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "GANRA",
    district_code: 7312031,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "CITTA",
    district_code: 7312032,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "LILI RILAU",
    district_code: 7312040,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "DONRI DONRI",
    district_code: 7312050,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "MARIO RIAWA",
    district_code: 7312060,
    regency_code: 7312,
    province_code: 73
  },
  {
    district_name: "SABBANG PARU",
    district_code: 7313010,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "TEMPE",
    district_code: 7313020,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "PAMMANA",
    district_code: 7313030,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "BOLA",
    district_code: 7313040,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "TAKKALALLA",
    district_code: 7313050,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "SAJOANGING",
    district_code: 7313060,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "PENRANG",
    district_code: 7313061,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "MAJAULENG",
    district_code: 7313070,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "TANA SITOLO",
    district_code: 7313080,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "BELAWA",
    district_code: 7313090,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "MANIANG PAJO",
    district_code: 7313100,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "GILIRENG",
    district_code: 7313101,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "KEERA",
    district_code: 7313110,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "PITUMPANUA",
    district_code: 7313120,
    regency_code: 7313,
    province_code: 73
  },
  {
    district_name: "PANCA LAUTANG",
    district_code: 7314010,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "TELLULIMPO E",
    district_code: 7314020,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "WATANG PULU",
    district_code: 7314030,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "BARANTI",
    district_code: 7314040,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "PANCA RIJANG",
    district_code: 7314050,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "KULO",
    district_code: 7314051,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "MARITENGNGAE",
    district_code: 7314060,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "WATANG SIDENRENG",
    district_code: 7314061,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "PITU RIAWA",
    district_code: 7314070,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "DUAPITUE",
    district_code: 7314080,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "PITU RIASE",
    district_code: 7314081,
    regency_code: 7314,
    province_code: 73
  },
  {
    district_name: "SUPPA",
    district_code: 7315010,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "MATTIROSOMPE",
    district_code: 7315020,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "LANRISANG",
    district_code: 7315021,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "MATTIRO BULU",
    district_code: 7315030,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "WATANG SAWITTO",
    district_code: 7315040,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "PALETEANG",
    district_code: 7315041,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "TIROANG",
    district_code: 7315042,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "PATAMPANUA",
    district_code: 7315050,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "CEMPA",
    district_code: 7315060,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "DUAMPANUA",
    district_code: 7315070,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "BATULAPPA",
    district_code: 7315071,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "LEMBANG",
    district_code: 7315080,
    regency_code: 7315,
    province_code: 73
  },
  {
    district_name: "MAIWA",
    district_code: 7316010,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "BUNGIN",
    district_code: 7316011,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "ENREKANG",
    district_code: 7316020,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "CENDANA",
    district_code: 7316021,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "BARAKA",
    district_code: 7316030,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "BUNTU BATU",
    district_code: 7316031,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "ANGGERAJA",
    district_code: 7316040,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "MALUA",
    district_code: 7316041,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "ALLA",
    district_code: 7316050,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "CURIO",
    district_code: 7316051,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "MASALLE",
    district_code: 7316052,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "BAROKO",
    district_code: 7316053,
    regency_code: 7316,
    province_code: 73
  },
  {
    district_name: "LAROMPONG",
    district_code: 7317010,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "LAROMPONG SELATAN",
    district_code: 7317011,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "SULI",
    district_code: 7317020,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "SULI BARAT",
    district_code: 7317021,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BELOPA",
    district_code: 7317030,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "KAMANRE",
    district_code: 7317031,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BELOPA UTARA",
    district_code: 7317032,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BAJO",
    district_code: 7317040,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BAJO BARAT",
    district_code: 7317041,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BASSESANGTEMPE",
    district_code: 7317050,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "LATIMOJONG",
    district_code: 7317051,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BASSESANGTEMPE UTARA",
    district_code: 7317052,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BUPON",
    district_code: 7317060,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "PONRANG",
    district_code: 7317061,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "PONRANG SELATAN",
    district_code: 7317062,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BUA",
    district_code: 7317070,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "WALENRANG",
    district_code: 7317080,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "WALENRANG TIMUR",
    district_code: 7317081,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "LAMASI",
    district_code: 7317090,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "WALENRANG UTARA",
    district_code: 7317091,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "WALENRANG BARAT",
    district_code: 7317092,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "LAMASI TIMUR",
    district_code: 7317093,
    regency_code: 7317,
    province_code: 73
  },
  {
    district_name: "BONGGAKARADENG",
    district_code: 7318010,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "SIMBUANG",
    district_code: 7318011,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "RANO",
    district_code: 7318012,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "MAPPAK",
    district_code: 7318013,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "MENGKENDEK",
    district_code: 7318020,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "GANDANG BATU SILANAN",
    district_code: 7318021,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "SANGALLA",
    district_code: 7318030,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "SANGALA SELATAN",
    district_code: 7318031,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "SANGALLA UTARA",
    district_code: 7318032,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "MAKALE",
    district_code: 7318040,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "MAKALE SELATAN",
    district_code: 7318041,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "MAKALE UTARA",
    district_code: 7318042,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "SALUPUTTI",
    district_code: 7318050,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "BITTUANG",
    district_code: 7318051,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "REMBON",
    district_code: 7318052,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "MASANDA",
    district_code: 7318053,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "MALIMBONG BALEPE",
    district_code: 7318054,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "RANTETAYO",
    district_code: 7318061,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "KURRA",
    district_code: 7318067,
    regency_code: 7318,
    province_code: 73
  },
  {
    district_name: "SABBANG",
    district_code: 7322010,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "BAEBUNTA",
    district_code: 7322020,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "MALANGKE",
    district_code: 7322030,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "MALANGKE BARAT",
    district_code: 7322031,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "SUKAMAJU",
    district_code: 7322040,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "BONE-BONE",
    district_code: 7322050,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "TANA LILI",
    district_code: 7322051,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "MASAMBA",
    district_code: 7322120,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "MAPPEDECENG",
    district_code: 7322121,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "RAMPI",
    district_code: 7322122,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "LIMBONG",
    district_code: 7322130,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "SEKO",
    district_code: 7322131,
    regency_code: 7322,
    province_code: 73
  },
  {
    district_name: "BURAU",
    district_code: 7325010,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "WOTU",
    district_code: 7325020,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "TOMONI",
    district_code: 7325030,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "TOMONI TIMUR",
    district_code: 7325031,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "ANGKONA",
    district_code: 7325040,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "MALILI",
    district_code: 7325050,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "TOWUTI",
    district_code: 7325060,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "NUHA",
    district_code: 7325070,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "WASUPONDA",
    district_code: 7325071,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "MANGKUTANA",
    district_code: 7325080,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "KALAENA",
    district_code: 7325081,
    regency_code: 7325,
    province_code: 73
  },
  {
    district_name: "SOPAI",
    district_code: 7326010,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "KESU",
    district_code: 7326020,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "SANGGALANGI",
    district_code: 7326030,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "BUNTAO",
    district_code: 7326040,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "RANTEBUA",
    district_code: 7326050,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "NANGGALA",
    district_code: 7326060,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "TONDON",
    district_code: 7326070,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "TALLUNGLIPU",
    district_code: 7326080,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "RANTEPAO",
    district_code: 7326090,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "TIKALA",
    district_code: 7326100,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "SESEAN",
    district_code: 7326110,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "BALUSU",
    district_code: 7326120,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "SA'DAN",
    district_code: 7326130,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "BENGKELEKILA",
    district_code: 7326140,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "SESEAN SULOARA",
    district_code: 7326150,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "KAPALA PITU",
    district_code: 7326160,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "DENDE PIONGAN NAPO",
    district_code: 7326170,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "AWAN RANTE KARUA",
    district_code: 7326180,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "RINDINGALO",
    district_code: 7326190,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "BUNTU PEPASAN",
    district_code: 7326200,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "BARUPPU",
    district_code: 7326210,
    regency_code: 7326,
    province_code: 73
  },
  {
    district_name: "MARISO",
    district_code: 7371010,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "MAMAJANG",
    district_code: 7371020,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "TAMALATE",
    district_code: 7371030,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "RAPPOCINI",
    district_code: 7371031,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "MAKASSAR",
    district_code: 7371040,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "UJUNG PANDANG",
    district_code: 7371050,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "WAJO",
    district_code: 7371060,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "BONTOALA",
    district_code: 7371070,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "UJUNG TANAH",
    district_code: 7371080,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "KEPULAUAN SANGKARRANG",
    district_code: 7371081,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "TALLO",
    district_code: 7371090,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "PANAKKUKANG",
    district_code: 7371100,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "MANGGALA",
    district_code: 7371101,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "BIRING KANAYA",
    district_code: 7371110,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "TAMALANREA",
    district_code: 7371111,
    regency_code: 7371,
    province_code: 73
  },
  {
    district_name: "BACUKIKI",
    district_code: 7372010,
    regency_code: 7372,
    province_code: 73
  },
  {
    district_name: "BACUKIKI BARAT",
    district_code: 7372011,
    regency_code: 7372,
    province_code: 73
  },
  {
    district_name: "UJUNG",
    district_code: 7372020,
    regency_code: 7372,
    province_code: 73
  },
  {
    district_name: "SOREANG",
    district_code: 7372030,
    regency_code: 7372,
    province_code: 73
  },
  {
    district_name: "WARA SELATAN",
    district_code: 7373010,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "SENDANA",
    district_code: 7373011,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "WARA",
    district_code: 7373020,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "WARA TIMUR",
    district_code: 7373021,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "MUNGKAJANG",
    district_code: 7373022,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "WARA UTARA",
    district_code: 7373030,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "BARA",
    district_code: 7373031,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "TELLUWANUA",
    district_code: 7373040,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "WARA BARAT",
    district_code: 7373041,
    regency_code: 7373,
    province_code: 73
  },
  {
    district_name: "LASALIMU",
    district_code: 7401050,
    regency_code: 7401,
    province_code: 74
  },
  {
    district_name: "LASALIMU SELATAN",
    district_code: 7401051,
    regency_code: 7401,
    province_code: 74
  },
  {
    district_name: "SIONTAPINA",
    district_code: 7401052,
    regency_code: 7401,
    province_code: 74
  },
  {
    district_name: "PASAR WAJO",
    district_code: 7401060,
    regency_code: 7401,
    province_code: 74
  },
  {
    district_name: "WOLOWA",
    district_code: 7401061,
    regency_code: 7401,
    province_code: 74
  },
  {
    district_name: "WABULA",
    district_code: 7401062,
    regency_code: 7401,
    province_code: 74
  },
  {
    district_name: "KAPONTORI",
    district_code: 7401110,
    regency_code: 7401,
    province_code: 74
  },
  {
    district_name: "TONGKUNO",
    district_code: 7402010,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "TONGKUNO SELATAN",
    district_code: 7402011,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "PARIGI",
    district_code: 7402020,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "BONE",
    district_code: 7402021,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "MAROBO",
    district_code: 7402022,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "KABAWO",
    district_code: 7402030,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "KABANGKA",
    district_code: 7402031,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "KONTUKOWUNA",
    district_code: 7402032,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "KONTUNAGA",
    district_code: 7402061,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "WATOPUTE",
    district_code: 7402062,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "KATOBU",
    district_code: 7402070,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "LOHIA",
    district_code: 7402071,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "DURUKA",
    district_code: 7402072,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "BATALAIWORU",
    district_code: 7402073,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "NAPABALANO",
    district_code: 7402080,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "LASALEPA",
    district_code: 7402081,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "TOWEA",
    district_code: 7402083,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "WAKORUMBA SELATAN",
    district_code: 7402090,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "PASIR PUTIH",
    district_code: 7402091,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "PASI KOLAGA",
    district_code: 7402092,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "MALIGANO",
    district_code: 7402111,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "BATUKARA",
    district_code: 7402112,
    regency_code: 7402,
    province_code: 74
  },
  {
    district_name: "SOROPIA",
    district_code: 7403090,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "LALONGGASUMEETO",
    district_code: 7403091,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "SAMPARA",
    district_code: 7403100,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "BONDOALA",
    district_code: 7403101,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "BESULUTU",
    district_code: 7403102,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "KAPOIALA",
    district_code: 7403103,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "ANGGALOMOARE",
    district_code: 7403104,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "MOROSI",
    district_code: 7403105,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "LAMBUYA",
    district_code: 7403130,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "UEPAI",
    district_code: 7403131,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "PURIALA",
    district_code: 7403132,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "ONEMBUTE",
    district_code: 7403133,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "PONDIDAHA",
    district_code: 7403140,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "WONGGEDUKU",
    district_code: 7403141,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "AMONGGEDO",
    district_code: 7403142,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "WONGGEDUKU BARAT",
    district_code: 7403143,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "WAWOTOBI",
    district_code: 7403150,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "MELUHU",
    district_code: 7403151,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "KONAWE",
    district_code: 7403152,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "ANGGOTOA",
    district_code: 7403153,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "UNAAHA",
    district_code: 7403170,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "ANGGABERI",
    district_code: 7403171,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "ABUKI",
    district_code: 7403180,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "LATOMA",
    district_code: 7403181,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "TONGAUNA",
    district_code: 7403182,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "ASINUA",
    district_code: 7403183,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "PADANGGUNI",
    district_code: 7403184,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "TONGAUNA UTARA",
    district_code: 7403185,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "ROUTA",
    district_code: 7403193,
    regency_code: 7403,
    province_code: 74
  },
  {
    district_name: "WATUBANGGA",
    district_code: 7404010,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "TANGGETADA",
    district_code: 7404011,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "TOARI",
    district_code: 7404012,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "POLINGGONA",
    district_code: 7404013,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "POMALAA",
    district_code: 7404020,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "WUNDULAKO",
    district_code: 7404030,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "BAULA",
    district_code: 7404031,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "KOLAKA",
    district_code: 7404060,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "LATAMBAGA",
    district_code: 7404061,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "WOLO",
    district_code: 7404070,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "SAMATURU",
    district_code: 7404071,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "IWOIMENDAA",
    district_code: 7404072,
    regency_code: 7404,
    province_code: 74
  },
  {
    district_name: "TINANGGEA",
    district_code: 7405010,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "LALEMBUU",
    district_code: 7405011,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "ANDOOLO",
    district_code: 7405020,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "BUKE",
    district_code: 7405021,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "ANDOOLO BARAT",
    district_code: 7405022,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "PALANGGA",
    district_code: 7405030,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "PALANGGA SELATAN",
    district_code: 7405031,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "BAITO",
    district_code: 7405032,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "LAINEA",
    district_code: 7405040,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "LAEYA",
    district_code: 7405041,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "KOLONO",
    district_code: 7405050,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "KOLONO TIMUR",
    district_code: 7405051,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "LAONTI",
    district_code: 7405060,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "MORAMO",
    district_code: 7405070,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "MORAMO UTARA",
    district_code: 7405071,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "KONDA",
    district_code: 7405080,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "WOLASI",
    district_code: 7405081,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "RANOMEETO",
    district_code: 7405090,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "RANOMEETO BARAT",
    district_code: 7405091,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "LANDONO",
    district_code: 7405100,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "MOWILA",
    district_code: 7405101,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "SABULAKOA",
    district_code: 7405102,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "ANGATA",
    district_code: 7405110,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "BENUA",
    district_code: 7405111,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "BASALA",
    district_code: 7405112,
    regency_code: 7405,
    province_code: 74
  },
  {
    district_name: "KABAENA",
    district_code: 7406010,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "KABAENA UTARA",
    district_code: 7406011,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "KABAENA SELATAN",
    district_code: 7406012,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "KABAENA BARAT",
    district_code: 7406013,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "KABAENA TIMUR",
    district_code: 7406020,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "KABAENA TENGAH",
    district_code: 7406021,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "RUMBIA",
    district_code: 7406030,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "MATA OLEO",
    district_code: 7406031,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "KEP. MASALOKA RAYA",
    district_code: 7406032,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "RUMBIA TENGAH",
    district_code: 7406033,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "RAROWATU",
    district_code: 7406040,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "RAROWATU UTARA",
    district_code: 7406041,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "MATA USU",
    district_code: 7406042,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "LANTARI JAYA",
    district_code: 7406043,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "POLEANG TIMUR",
    district_code: 7406050,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "POLEANG UTARA",
    district_code: 7406051,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "POLEANG SELATAN",
    district_code: 7406052,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "POLEANG TENGGARA",
    district_code: 7406053,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "POLEANG",
    district_code: 7406060,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "POLEANG BARAT",
    district_code: 7406061,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "TONTONUNU",
    district_code: 7406062,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "POLEANG TENGAH",
    district_code: 7406063,
    regency_code: 7406,
    province_code: 74
  },
  {
    district_name: "BINONGKO",
    district_code: 7407010,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "TOGO BINONGKO",
    district_code: 7407011,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "TOMIA",
    district_code: 7407020,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "TOMIA TIMUR",
    district_code: 7407021,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "KALEDUPA",
    district_code: 7407030,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "KALEDUPA SELATAN",
    district_code: 7407031,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "WANGI-WANGI",
    district_code: 7407040,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "WANGI-WANGI SELATAN",
    district_code: 7407050,
    regency_code: 7407,
    province_code: 74
  },
  {
    district_name: "RANTEANGIN",
    district_code: 7408010,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "LAMBAI",
    district_code: 7408011,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "WAWO",
    district_code: 7408012,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "LASUSUA",
    district_code: 7408020,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "KATOI",
    district_code: 7408021,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "KODEOHA",
    district_code: 7408030,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "TIWU",
    district_code: 7408031,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "NGAPA",
    district_code: 7408040,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "WATUNOHU",
    district_code: 7408041,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "PAKUE",
    district_code: 7408050,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "PAKUE UTARA",
    district_code: 7408051,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "PAKUE TENGAH",
    district_code: 7408052,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "BATU PUTIH",
    district_code: 7408060,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "POREHU",
    district_code: 7408061,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "TOLALA",
    district_code: 7408062,
    regency_code: 7408,
    province_code: 74
  },
  {
    district_name: "BONEGUNU",
    district_code: 7409100,
    regency_code: 7409,
    province_code: 74
  },
  {
    district_name: "KAMBOWA",
    district_code: 7409101,
    regency_code: 7409,
    province_code: 74
  },
  {
    district_name: "WAKORUMBA",
    district_code: 7409110,
    regency_code: 7409,
    province_code: 74
  },
  {
    district_name: "KULISUSU",
    district_code: 7409120,
    regency_code: 7409,
    province_code: 74
  },
  {
    district_name: "KULISUSU BARAT",
    district_code: 7409121,
    regency_code: 7409,
    province_code: 74
  },
  {
    district_name: "KULISUSU UTARA",
    district_code: 7409122,
    regency_code: 7409,
    province_code: 74
  },
  {
    district_name: "SAWA",
    district_code: 7410010,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "MOTUI",
    district_code: 7410011,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "LEMBO",
    district_code: 7410020,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "LASOLO",
    district_code: 7410030,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "WAWOLESEA",
    district_code: 7410031,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "LASOLO KEPULAUAN",
    district_code: 7410032,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "MOLAWE",
    district_code: 7410040,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "ASERA",
    district_code: 7410050,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "ANDOWIA",
    district_code: 7410051,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "OHEO",
    district_code: 7410052,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "LANGGIKIMA",
    district_code: 7410060,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "WIWIRANO",
    district_code: 7410070,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "LANDAWE",
    district_code: 7410071,
    regency_code: 7410,
    province_code: 74
  },
  {
    district_name: "AERE",
    district_code: 7411010,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "LAMBANDIA",
    district_code: 7411020,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "POLI-POLIA",
    district_code: 7411030,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "DANGIA",
    district_code: 7411040,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "LADONGI",
    district_code: 7411050,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "LOEA",
    district_code: 7411060,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "TIRAWUTA",
    district_code: 7411070,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "LALOLAE",
    district_code: 7411080,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "MOWEWE",
    district_code: 7411090,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "TINONDO",
    district_code: 7411100,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "ULUIWOI",
    district_code: 7411110,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "UEESI",
    district_code: 7411120,
    regency_code: 7411,
    province_code: 74
  },
  {
    district_name: "WAWONII TENGGARA",
    district_code: 7412010,
    regency_code: 7412,
    province_code: 74
  },
  {
    district_name: "WAWONII TIMUR",
    district_code: 7412020,
    regency_code: 7412,
    province_code: 74
  },
  {
    district_name: "WAWONII TIMUR LAUT",
    district_code: 7412030,
    regency_code: 7412,
    province_code: 74
  },
  {
    district_name: "WAWONII UTARA",
    district_code: 7412040,
    regency_code: 7412,
    province_code: 74
  },
  {
    district_name: "WAWONII SELATAN",
    district_code: 7412050,
    regency_code: 7412,
    province_code: 74
  },
  {
    district_name: "WAWONII TENGAH",
    district_code: 7412060,
    regency_code: 7412,
    province_code: 74
  },
  {
    district_name: "WAWONII BARAT",
    district_code: 7412070,
    regency_code: 7412,
    province_code: 74
  },
  {
    district_name: "TIWORO KEPULAUAN",
    district_code: 7413010,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "MAGINTI",
    district_code: 7413020,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "TIWORO TENGAH",
    district_code: 7413030,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "TIWORO SELATAN",
    district_code: 7413040,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "TIWORO UTARA",
    district_code: 7413050,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "LAWA",
    district_code: 7413060,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "SAWERIGADI",
    district_code: 7413070,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "BARANGKA",
    district_code: 7413080,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "WA DAGA",
    district_code: 7413090,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "KUSAMBI",
    district_code: 7413100,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "NAPANO KUSAMBI",
    district_code: 7413110,
    regency_code: 7413,
    province_code: 74
  },
  {
    district_name: "TALAGA RAYA",
    district_code: 7414010,
    regency_code: 7414,
    province_code: 74
  },
  {
    district_name: "MAWASANGKA",
    district_code: 7414020,
    regency_code: 7414,
    province_code: 74
  },
  {
    district_name: "MAWASANGKA TENGAH",
    district_code: 7414030,
    regency_code: 7414,
    province_code: 74
  },
  {
    district_name: "MAWASANGKA TIMUR",
    district_code: 7414040,
    regency_code: 7414,
    province_code: 74
  },
  {
    district_name: "LAKUDO",
    district_code: 7414050,
    regency_code: 7414,
    province_code: 74
  },
  {
    district_name: "GU",
    district_code: 7414060,
    regency_code: 7414,
    province_code: 74
  },
  {
    district_name: "SANGIA WAMBULU",
    district_code: 7414070,
    regency_code: 7414,
    province_code: 74
  },
  {
    district_name: "BATU ATAS",
    district_code: 7415010,
    regency_code: 7415,
    province_code: 74
  },
  {
    district_name: "LAPANDEWA",
    district_code: 7415020,
    regency_code: 7415,
    province_code: 74
  },
  {
    district_name: "SAMPOLAWA",
    district_code: 7415030,
    regency_code: 7415,
    province_code: 74
  },
  {
    district_name: "BATAUGA",
    district_code: 7415040,
    regency_code: 7415,
    province_code: 74
  },
  {
    district_name: "SIOMPU BARAT",
    district_code: 7415050,
    regency_code: 7415,
    province_code: 74
  },
  {
    district_name: "SIOMPU",
    district_code: 7415060,
    regency_code: 7415,
    province_code: 74
  },
  {
    district_name: "KADATUA",
    district_code: 7415070,
    regency_code: 7415,
    province_code: 74
  },
  {
    district_name: "MANDONGA",
    district_code: 7471010,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "BARUGA",
    district_code: 7471011,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "PUUWATU",
    district_code: 7471012,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "KADIA",
    district_code: 7471013,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "WUA-WUA",
    district_code: 7471014,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "POASIA",
    district_code: 7471020,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "ABELI",
    district_code: 7471021,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "KAMBU",
    district_code: 7471022,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "NAMBO",
    district_code: 7471023,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "KENDARI",
    district_code: 7471030,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "KENDARI BARAT",
    district_code: 7471031,
    regency_code: 7471,
    province_code: 74
  },
  {
    district_name: "BETOAMBARI",
    district_code: 7472010,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "MURHUM",
    district_code: 7472011,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "BATUPOARO",
    district_code: 7472012,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "WOLIO",
    district_code: 7472020,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "KOKALUKUNA",
    district_code: 7472021,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "SORAWOLIO",
    district_code: 7472030,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "BUNGI",
    district_code: 7472040,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "LEA-LEA",
    district_code: 7472041,
    regency_code: 7472,
    province_code: 74
  },
  {
    district_name: "MANANGGU",
    district_code: 7501031,
    regency_code: 7501,
    province_code: 75
  },
  {
    district_name: "TILAMUTA",
    district_code: 7501040,
    regency_code: 7501,
    province_code: 75
  },
  {
    district_name: "DULUPI",
    district_code: 7501041,
    regency_code: 7501,
    province_code: 75
  },
  {
    district_name: "BOTUMOITO",
    district_code: 7501042,
    regency_code: 7501,
    province_code: 75
  },
  {
    district_name: "PAGUYAMAN",
    district_code: 7501050,
    regency_code: 7501,
    province_code: 75
  },
  {
    district_name: "WONOSARI",
    district_code: 7501051,
    regency_code: 7501,
    province_code: 75
  },
  {
    district_name: "PAGUYAMAN PANTAI",
    district_code: 7501052,
    regency_code: 7501,
    province_code: 75
  },
  {
    district_name: "BATUDAA PANTAI",
    district_code: 7502010,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "BILUHU",
    district_code: 7502011,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "BATUDAA",
    district_code: 7502020,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "BONGOMEME",
    district_code: 7502021,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "TABONGO",
    district_code: 7502022,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "DUNGALIYO",
    district_code: 7502023,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "TIBAWA",
    district_code: 7502030,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "PULUBALA",
    district_code: 7502031,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "BOLIYOHUTO",
    district_code: 7502040,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "MOOTILANGO",
    district_code: 7502041,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "TOLANGOHULA",
    district_code: 7502042,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "ASPARAGA",
    district_code: 7502043,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "BILATO",
    district_code: 7502044,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "LIMBOTO",
    district_code: 7502070,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "LIMBOTO BARAT",
    district_code: 7502071,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "TELAGA",
    district_code: 7502080,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "TELAGA BIRU",
    district_code: 7502081,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "TILANGO",
    district_code: 7502082,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "TELAGA JAYA",
    district_code: 7502083,
    regency_code: 7502,
    province_code: 75
  },
  {
    district_name: "POPAYATO",
    district_code: 7503010,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "POPAYATO BARAT",
    district_code: 7503011,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "POPAYATO TIMUR",
    district_code: 7503012,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "LEMITO",
    district_code: 7503020,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "WANGGARASI",
    district_code: 7503021,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "MARISA",
    district_code: 7503030,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "PATILANGGIO",
    district_code: 7503031,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "BUNTULIA",
    district_code: 7503032,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "DUHIADAA",
    district_code: 7503033,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "RANDANGAN",
    district_code: 7503040,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "TALUDITI",
    district_code: 7503041,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "PAGUAT",
    district_code: 7503050,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "DENGILO",
    district_code: 7503051,
    regency_code: 7503,
    province_code: 75
  },
  {
    district_name: "TAPA",
    district_code: 7504010,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BULANGO UTARA",
    district_code: 7504011,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BULANGO SELATAN",
    district_code: 7504012,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BULANGO TIMUR",
    district_code: 7504013,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BULANGO ULU",
    district_code: 7504014,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "KABILA",
    district_code: 7504020,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BOTU PINGGE",
    district_code: 7504021,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "TILONGKABILA",
    district_code: 7504022,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "SUWAWA",
    district_code: 7504030,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "SUWAWA SELATAN",
    district_code: 7504031,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "SUWAWA TIMUR",
    district_code: 7504032,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "SUWAWA TENGAH",
    district_code: 7504033,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "PINOGU",
    district_code: 7504034,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BONEPANTAI",
    district_code: 7504040,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "KABILA BONE",
    district_code: 7504041,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BONE RAYA",
    district_code: 7504042,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BONE",
    district_code: 7504043,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "BULAWA",
    district_code: 7504044,
    regency_code: 7504,
    province_code: 75
  },
  {
    district_name: "ATINGGOLA",
    district_code: 7505010,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "GENTUMA RAYA",
    district_code: 7505011,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "KWANDANG",
    district_code: 7505020,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "TOMILITO",
    district_code: 7505021,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "PONELO KEPULAUAN",
    district_code: 7505022,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "ANGGREK",
    district_code: 7505030,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "MONANO",
    district_code: 7505031,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "SUMALATA",
    district_code: 7505040,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "SUMALATA TIMUR",
    district_code: 7505041,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "TOLINGGULA",
    district_code: 7505050,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "BIAU",
    district_code: 7505051,
    regency_code: 7505,
    province_code: 75
  },
  {
    district_name: "KOTA BARAT",
    district_code: 7571010,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "DUNGINGI",
    district_code: 7571011,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "KOTA SELATAN",
    district_code: 7571020,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "KOTA TIMUR",
    district_code: 7571021,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "HULONTHALANGI",
    district_code: 7571022,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "DUMBO RAYA",
    district_code: 7571023,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "KOTA UTARA",
    district_code: 7571030,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "KOTA TENGAH",
    district_code: 7571031,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "SIPATANA",
    district_code: 7571032,
    regency_code: 7571,
    province_code: 75
  },
  {
    district_name: "BANGGAE",
    district_code: 7601010,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "BANGGAE TIMUR",
    district_code: 7601011,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "PAMBOANG",
    district_code: 7601020,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "SENDANA",
    district_code: 7601030,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "TAMMERODO",
    district_code: 7601031,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "TUBO SENDANA",
    district_code: 7601033,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "MALUNDA",
    district_code: 7601040,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "ULUMANDA",
    district_code: 7601041,
    regency_code: 7601,
    province_code: 76
  },
  {
    district_name: "TINAMBUNG",
    district_code: 7602010,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "BALANIPA",
    district_code: 7602011,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "LIMBORO",
    district_code: 7602012,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "TUBBI TARAMANU",
    district_code: 7602020,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "ALU",
    district_code: 7602021,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "CAMPALAGIAN",
    district_code: 7602030,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "LUYO",
    district_code: 7602031,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "WONOMULYO",
    district_code: 7602040,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "MAPILLI",
    district_code: 7602041,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "TAPANGO",
    district_code: 7602042,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "MATAKALI",
    district_code: 7602043,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "B U L O",
    district_code: 7602044,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "POLEWALI",
    district_code: 7602050,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "BINUANG",
    district_code: 7602051,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "ANREAPI",
    district_code: 7602052,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "MATANGNGA",
    district_code: 7602061,
    regency_code: 7602,
    province_code: 76
  },
  {
    district_name: "SUMARORONG",
    district_code: 7603010,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "MESSAWA",
    district_code: 7603020,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "PANA",
    district_code: 7603030,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "NOSU",
    district_code: 7603031,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "TABANG",
    district_code: 7603040,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "MAMASA",
    district_code: 7603050,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "TANDUK KALUA",
    district_code: 7603060,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "BALLA",
    district_code: 7603061,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "SESENAPADANG",
    district_code: 7603070,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "TAWALIAN",
    district_code: 7603071,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "MAMBI",
    district_code: 7603080,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "BAMBANG",
    district_code: 7603081,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "RANTEBULAHAN TIMUR",
    district_code: 7603082,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "MEHALAAN",
    district_code: 7603083,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "ARALLE",
    district_code: 7603090,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "BUNTU MALANGKA",
    district_code: 7603091,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "TABULAHAN",
    district_code: 7603100,
    regency_code: 7603,
    province_code: 76
  },
  {
    district_name: "TAPALANG",
    district_code: 7604010,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "TAPALANG BARAT",
    district_code: 7604011,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "MAMUJU",
    district_code: 7604020,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "SIMBORO",
    district_code: 7604022,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "BALABALAKANG",
    district_code: 7604023,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "KALUKKU",
    district_code: 7604030,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "PAPALANG",
    district_code: 7604031,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "SAMPAGA",
    district_code: 7604032,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "TOMMO",
    district_code: 7604033,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "KALUMPANG",
    district_code: 7604040,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "BONEHAU",
    district_code: 7604041,
    regency_code: 7604,
    province_code: 76
  },
  {
    district_name: "SARUDU",
    district_code: 7605010,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "DAPURANG",
    district_code: 7605011,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "DURIPOKU",
    district_code: 7605012,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "BARAS",
    district_code: 7605020,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "BULU TABA",
    district_code: 7605021,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "LARIANG",
    district_code: 7605022,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "PASANGKAYU",
    district_code: 7605030,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "TIKKE RAYA",
    district_code: 7605031,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "PEDONGGA",
    district_code: 7605032,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "BAMBALAMOTU",
    district_code: 7605040,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "BAMBAIRA",
    district_code: 7605041,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "SARJO",
    district_code: 7605042,
    regency_code: 7605,
    province_code: 76
  },
  {
    district_name: "PANGALE",
    district_code: 7606010,
    regency_code: 7606,
    province_code: 76
  },
  {
    district_name: "BUDONG-BUDONG",
    district_code: 7606020,
    regency_code: 7606,
    province_code: 76
  },
  {
    district_name: "TOBADAK",
    district_code: 7606030,
    regency_code: 7606,
    province_code: 76
  },
  {
    district_name: "TOPOYO",
    district_code: 7606040,
    regency_code: 7606,
    province_code: 76
  },
  {
    district_name: "KAROSSA",
    district_code: 7606050,
    regency_code: 7606,
    province_code: 76
  },
  {
    district_name: "TANIMBAR SELATAN",
    district_code: 8101040,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "WER TAMRIAN",
    district_code: 8101041,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "WER MAKTIAN",
    district_code: 8101042,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "SELARU",
    district_code: 8101043,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "TANIMBAR UTARA",
    district_code: 8101050,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "YARU",
    district_code: 8101051,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "WUAR LABOBAR",
    district_code: 8101052,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "NIRUNMAS",
    district_code: 8101053,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "KORMOMOLIN",
    district_code: 8101054,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "MOLU MARU",
    district_code: 8101055,
    regency_code: 8101,
    province_code: 81
  },
  {
    district_name: "KEI KECIL",
    district_code: 8102010,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI KECIL BARAT",
    district_code: 8102012,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI KECIL TIMUR",
    district_code: 8102013,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "HOAT SORBAY",
    district_code: 8102014,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "MANYEUW",
    district_code: 8102015,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI KECIL TIMUR SELATAN",
    district_code: 8102016,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI BESAR",
    district_code: 8102020,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI BESAR UTARA TIMUR",
    district_code: 8102021,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI BESAR SELATAN",
    district_code: 8102022,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI BESAR UTARA BARAT",
    district_code: 8102023,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "KEI BESAR SELATAN BARAT",
    district_code: 8102024,
    regency_code: 8102,
    province_code: 81
  },
  {
    district_name: "BANDA",
    district_code: 8103010,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "TEHORU",
    district_code: 8103040,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "TELUTIH",
    district_code: 8103041,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "AMAHAI",
    district_code: 8103050,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "KOTA MASOHI",
    district_code: 8103051,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "TELUK ELPAPUTIH",
    district_code: 8103052,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "TEON NILA SERUA",
    district_code: 8103060,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "SAPARUA",
    district_code: 8103080,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "NUSALAUT",
    district_code: 8103081,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "SAPARUA TIMUR",
    district_code: 8103082,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "P. HARUKU",
    district_code: 8103090,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "SALAHUTU",
    district_code: 8103100,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "LEIHITU",
    district_code: 8103110,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "LEIHITU BARAT",
    district_code: 8103111,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "SERAM UTARA",
    district_code: 8103140,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "SERAM UTARA BARAT",
    district_code: 8103141,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "SERAM UTARA TIMUR KOBI",
    district_code: 8103142,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "SERAM UTARA TIMUR SETI",
    district_code: 8103143,
    regency_code: 8103,
    province_code: 81
  },
  {
    district_name: "NAMLEA",
    district_code: 8104020,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "WAEAPO",
    district_code: 8104021,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "WAPLAU",
    district_code: 8104022,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "BATA BUAL",
    district_code: 8104023,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "TELUK KAIELY",
    district_code: 8104024,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "WAELATA",
    district_code: 8104025,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "LOLONG GUBA",
    district_code: 8104026,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "LILIALY",
    district_code: 8104027,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "AIR BUAYA",
    district_code: 8104030,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "FENA LEISELA",
    district_code: 8104031,
    regency_code: 8104,
    province_code: 81
  },
  {
    district_name: "ARU SELATAN",
    district_code: 8105010,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "ARU SELATAN TIMUR",
    district_code: 8105011,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "ARU SELATAN UTARA",
    district_code: 8105012,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "ARU TENGAH",
    district_code: 8105020,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "ARU TENGAH TIMUR",
    district_code: 8105021,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "ARU TENGAH SELATAN",
    district_code: 8105022,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "PULAU-PULAU ARU",
    district_code: 8105030,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "ARU UTARA",
    district_code: 8105031,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "ARU UTARA TIMUR BATULEY",
    district_code: 8105032,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "SIR-SIR",
    district_code: 8105033,
    regency_code: 8105,
    province_code: 81
  },
  {
    district_name: "HUAMUAL BELAKANG",
    district_code: 8106010,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "KEPULAUAN MANIPA",
    district_code: 8106011,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "SERAM BARAT",
    district_code: 8106020,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "HUAMUAL",
    district_code: 8106021,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "KAIRATU",
    district_code: 8106030,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "KAIRATU BARAT",
    district_code: 8106031,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "INAMOSOL",
    district_code: 8106032,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "AMALATU",
    district_code: 8106033,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "ELPAPUTIH",
    district_code: 8106034,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "TANIWEL",
    district_code: 8106040,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "TANIWEL TIMUR",
    district_code: 8106041,
    regency_code: 8106,
    province_code: 81
  },
  {
    district_name: "PULAU GOROM",
    district_code: 8107010,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "WAKATE",
    district_code: 8107011,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "TEOR",
    district_code: 8107012,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "GOROM TIMUR",
    district_code: 8107013,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "PULAU PANJANG",
    district_code: 8107014,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "SERAM TIMUR",
    district_code: 8107020,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "TUTUK TOLU",
    district_code: 8107021,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "KILMURY",
    district_code: 8107022,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "LIAN VITU",
    district_code: 8107023,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "KIAN DARAT",
    district_code: 8107024,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "WERINAMA",
    district_code: 8107030,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "SIWALALAT",
    district_code: 8107031,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "BULA",
    district_code: 8107040,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "BULA BARAT",
    district_code: 8107041,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "TELUK WARU",
    district_code: 8107042,
    regency_code: 8107,
    province_code: 81
  },
  {
    district_name: "WETAR",
    district_code: 8108010,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "WETAR BARAT",
    district_code: 8108011,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "WETAR UTARA",
    district_code: 8108012,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "WETAR TIMUR",
    district_code: 8108013,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "PP. TERSELATAN",
    district_code: 8108020,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "KISAR UTARA",
    district_code: 8108021,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "KEPULAUAN ROMANG",
    district_code: 8108022,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "LETTI",
    district_code: 8108030,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "MOA",
    district_code: 8108041,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "LAKOR",
    district_code: 8108042,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "DAMER",
    district_code: 8108050,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "MDONA HIERA",
    district_code: 8108060,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "PP. BABAR",
    district_code: 8108070,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "PULAU WETANG",
    district_code: 8108071,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "BABAR TIMUR",
    district_code: 8108080,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "PULAU MASELA",
    district_code: 8108081,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "DAWELOR DAWERA",
    district_code: 8108082,
    regency_code: 8108,
    province_code: 81
  },
  {
    district_name: "KEPALA MADAN",
    district_code: 8109010,
    regency_code: 8109,
    province_code: 81
  },
  {
    district_name: "LEKSULA",
    district_code: 8109020,
    regency_code: 8109,
    province_code: 81
  },
  {
    district_name: "FENA FAFAN",
    district_code: 8109021,
    regency_code: 8109,
    province_code: 81
  },
  {
    district_name: "NAMROLE",
    district_code: 8109030,
    regency_code: 8109,
    province_code: 81
  },
  {
    district_name: "WAISAMA",
    district_code: 8109040,
    regency_code: 8109,
    province_code: 81
  },
  {
    district_name: "AMBALAU",
    district_code: 8109050,
    regency_code: 8109,
    province_code: 81
  },
  {
    district_name: "NUSANIWE",
    district_code: 8171010,
    regency_code: 8171,
    province_code: 81
  },
  {
    district_name: "SIRIMAU",
    district_code: 8171020,
    regency_code: 8171,
    province_code: 81
  },
  {
    district_name: "LEITIMUR SELATAN",
    district_code: 8171021,
    regency_code: 8171,
    province_code: 81
  },
  {
    district_name: "TELUK AMBON BAGUALA",
    district_code: 8171030,
    regency_code: 8171,
    province_code: 81
  },
  {
    district_name: "TELUK AMBON",
    district_code: 8171031,
    regency_code: 8171,
    province_code: 81
  },
  {
    district_name: "PP. KUR",
    district_code: 8172010,
    regency_code: 8172,
    province_code: 81
  },
  {
    district_name: "KUR SELATAN",
    district_code: 8172011,
    regency_code: 8172,
    province_code: 81
  },
  {
    district_name: "TAYANDO TAM",
    district_code: 8172020,
    regency_code: 8172,
    province_code: 81
  },
  {
    district_name: "PULAU DULLAH UTARA",
    district_code: 8172030,
    regency_code: 8172,
    province_code: 81
  },
  {
    district_name: "PULAU DULLAH SELATAN",
    district_code: 8172040,
    regency_code: 8172,
    province_code: 81
  },
  {
    district_name: "JAILOLO",
    district_code: 8201090,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "JAILOLO SELATAN",
    district_code: 8201091,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "SAHU",
    district_code: 8201100,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "SAHU TIMUR",
    district_code: 8201101,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "IBU",
    district_code: 8201130,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "IBU SELATAN",
    district_code: 8201131,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "TABARU",
    district_code: 8201132,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "LOLODA",
    district_code: 8201140,
    regency_code: 8201,
    province_code: 82
  },
  {
    district_name: "WEDA",
    district_code: 8202030,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "WEDA SELATAN",
    district_code: 8202031,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "WEDA UTARA",
    district_code: 8202032,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "WEDA TENGAH",
    district_code: 8202033,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "WEDA TIMUR",
    district_code: 8202034,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "PULAU GEBE",
    district_code: 8202041,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "PATANI",
    district_code: 8202042,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "PATANI UTARA",
    district_code: 8202043,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "PATANI BARAT",
    district_code: 8202044,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "PATANI TIMUR",
    district_code: 8202045,
    regency_code: 8202,
    province_code: 82
  },
  {
    district_name: "SULA BESI BARAT",
    district_code: 8203010,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "SULABESI SELATAN",
    district_code: 8203011,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "SANANA",
    district_code: 8203020,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "SULA BESI TENGAH",
    district_code: 8203021,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "SULABESI TIMUR",
    district_code: 8203022,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "SANANA UTARA",
    district_code: 8203023,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "MANGOLI TIMUR",
    district_code: 8203030,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "MANGOLI TENGAH",
    district_code: 8203031,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "MANGOLI UTARA TIMUR",
    district_code: 8203032,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "MANGOLI BARAT",
    district_code: 8203040,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "MANGOLI UTARA",
    district_code: 8203041,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "MANGOLI SELATAN",
    district_code: 8203042,
    regency_code: 8203,
    province_code: 82
  },
  {
    district_name: "OBI SELATAN",
    district_code: 8204010,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "OBI",
    district_code: 8204020,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "OBI BARAT",
    district_code: 8204021,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "OBI TIMUR",
    district_code: 8204022,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "OBI UTARA",
    district_code: 8204023,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BACAN",
    district_code: 8204030,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "MANDIOLI SELATAN",
    district_code: 8204031,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "MANDIOLI UTARA",
    district_code: 8204032,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BACAN SELATAN",
    district_code: 8204033,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BATANG LOMANG",
    district_code: 8204034,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BACAN TIMUR",
    district_code: 8204040,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BACAN TIMUR SELATAN",
    district_code: 8204041,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BACAN TIMUR TENGAH",
    district_code: 8204042,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BACAN BARAT",
    district_code: 8204050,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "KASIRUTA BARAT",
    district_code: 8204051,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "KASIRUTA TIMUR",
    district_code: 8204052,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "BACAN BARAT UTARA",
    district_code: 8204053,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "KAYOA",
    district_code: 8204060,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "KAYOA BARAT",
    district_code: 8204061,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "KAYOA SELATAN",
    district_code: 8204062,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "KAYOA UTARA",
    district_code: 8204063,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "PULAU MAKIAN",
    district_code: 8204070,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "MAKIAN BARAT",
    district_code: 8204071,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "GANE BARAT",
    district_code: 8204080,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "GANE BARAT SELATAN",
    district_code: 8204081,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "GANE BARAT UTARA",
    district_code: 8204082,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "KEPULAUAN JORONGA",
    district_code: 8204083,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "GANE TIMUR",
    district_code: 8204090,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "GANE TIMUR TENGAH",
    district_code: 8204091,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "GANE TIMUR SELATAN",
    district_code: 8204092,
    regency_code: 8204,
    province_code: 82
  },
  {
    district_name: "MALIFUT",
    district_code: 8205010,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "KAO TELUK",
    district_code: 8205011,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "KAO",
    district_code: 8205020,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "KAO BARAT",
    district_code: 8205021,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "KAO UTARA",
    district_code: 8205022,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "TOBELO SELATAN",
    district_code: 8205030,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "TOBELO BARAT",
    district_code: 8205031,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "TOBELO TIMUR",
    district_code: 8205032,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "TOBELO",
    district_code: 8205040,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "TOBELO TENGAH",
    district_code: 8205041,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "TOBELO UTARA",
    district_code: 8205042,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "GALELA",
    district_code: 8205050,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "GALELA SELATAN",
    district_code: 8205051,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "GALELA BARAT",
    district_code: 8205052,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "GALELA UTARA",
    district_code: 8205053,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "LOLODA UTARA",
    district_code: 8205060,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "LOLODA KEPULAUAN",
    district_code: 8205061,
    regency_code: 8205,
    province_code: 82
  },
  {
    district_name: "MABA SELATAN",
    district_code: 8206010,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "KOTA MABA",
    district_code: 8206011,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "WASILE SELATAN",
    district_code: 8206020,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "WASILE",
    district_code: 8206030,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "WASILE TIMUR",
    district_code: 8206031,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "WASILE TENGAH",
    district_code: 8206032,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "WASILE UTARA",
    district_code: 8206033,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "MABA",
    district_code: 8206040,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "MABA TENGAH",
    district_code: 8206041,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "MABA UTARA",
    district_code: 8206042,
    regency_code: 8206,
    province_code: 82
  },
  {
    district_name: "MOROTAI SELATAN",
    district_code: 8207010,
    regency_code: 8207,
    province_code: 82
  },
  {
    district_name: "MOROTAI TIMUR",
    district_code: 8207020,
    regency_code: 8207,
    province_code: 82
  },
  {
    district_name: "MOROTAI SELATAN BARAT",
    district_code: 8207030,
    regency_code: 8207,
    province_code: 82
  },
  {
    district_name: "MOROTAI JAYA",
    district_code: 8207040,
    regency_code: 8207,
    province_code: 82
  },
  {
    district_name: "MOROTAI UTARA",
    district_code: 8207050,
    regency_code: 8207,
    province_code: 82
  },
  {
    district_name: "TALIABU BARAT",
    district_code: 8208010,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "TALIABU SELATAN",
    district_code: 8208020,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "TABONA",
    district_code: 8208030,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "TALIABU TIMUR SELATAN",
    district_code: 8208040,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "TALIABU TIMUR",
    district_code: 8208050,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "TALIABU UTARA",
    district_code: 8208060,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "LEDE",
    district_code: 8208070,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "TALIABU BARAT LAUT",
    district_code: 8208080,
    regency_code: 8208,
    province_code: 82
  },
  {
    district_name: "PULAU TERNATE",
    district_code: 8271010,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "MOTI",
    district_code: 8271011,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "PULAU BATANG DUA",
    district_code: 8271012,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "PULAU HIRI",
    district_code: 8271013,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "TERNATE BARAT",
    district_code: 8271014,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "TERNATE SELATAN",
    district_code: 8271020,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "TERNATE TENGAH",
    district_code: 8271021,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "TERNATE UTARA",
    district_code: 8271030,
    regency_code: 8271,
    province_code: 82
  },
  {
    district_name: "TIDORE SELATAN",
    district_code: 8272010,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "TIDORE UTARA",
    district_code: 8272020,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "TIDORE",
    district_code: 8272030,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "TIDORE TIMUR",
    district_code: 8272031,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "OBA",
    district_code: 8272040,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "OBA SELATAN",
    district_code: 8272041,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "OBA UTARA",
    district_code: 8272050,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "OBA TENGAH",
    district_code: 8272051,
    regency_code: 8272,
    province_code: 82
  },
  {
    district_name: "FAKFAK TIMUR",
    district_code: 9101050,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "KARAS",
    district_code: 9101051,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "FAKFAK TIMUR TENGAH",
    district_code: 9101052,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "FAKFAK",
    district_code: 9101060,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "FAKFAK TENGAH",
    district_code: 9101061,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "PARIWARI",
    district_code: 9101062,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "FAKFAK BARAT",
    district_code: 9101070,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "WARTUTIN",
    district_code: 9101071,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "KOKAS",
    district_code: 9101080,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "TELUK PATIPI",
    district_code: 9101081,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "KRAMONGMONGGA",
    district_code: 9101082,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "BOMBERAY",
    district_code: 9101083,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "ARGUNI",
    district_code: 9101084,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "MBAHAMDANDARA",
    district_code: 9101085,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "FURWAGI",
    district_code: 9101086,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "KAYAUNI",
    district_code: 9101087,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "TOMAGE",
    district_code: 9101088,
    regency_code: 9101,
    province_code: 91
  },
  {
    district_name: "BURUWAY",
    district_code: 9102010,
    regency_code: 9102,
    province_code: 91
  },
  {
    district_name: "TELUK ARGUNI",
    district_code: 9102020,
    regency_code: 9102,
    province_code: 91
  },
  {
    district_name: "TELUK ARGUNI BAWAH",
    district_code: 9102021,
    regency_code: 9102,
    province_code: 91
  },
  {
    district_name: "KAIMANA",
    district_code: 9102030,
    regency_code: 9102,
    province_code: 91
  },
  {
    district_name: "KAMBRAU",
    district_code: 9102031,
    regency_code: 9102,
    province_code: 91
  },
  {
    district_name: "TELUK ETNA",
    district_code: 9102040,
    regency_code: 9102,
    province_code: 91
  },
  {
    district_name: "YAMOR",
    district_code: 9102041,
    regency_code: 9102,
    province_code: 91
  },
  {
    district_name: "NAIKERE",
    district_code: 9103010,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "WONDIBOY",
    district_code: 9103020,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "RASIEY",
    district_code: 9103021,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "KURI WAMESA",
    district_code: 9103022,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "WASIOR",
    district_code: 9103030,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "DUAIRI",
    district_code: 9103040,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "ROON",
    district_code: 9103041,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "WINDESI",
    district_code: 9103050,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "NIKIWAR",
    district_code: 9103051,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "WAMESA",
    district_code: 9103060,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "ROSWAR",
    district_code: 9103061,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "RUMBERPON",
    district_code: 9103070,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "SOUG JAYA",
    district_code: 9103071,
    regency_code: 9103,
    province_code: 91
  },
  {
    district_name: "FAFURWAR",
    district_code: 9104010,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "BABO",
    district_code: 9104020,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "SUMURI",
    district_code: 9104021,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "AROBA",
    district_code: 9104022,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "KAITARO",
    district_code: 9104023,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "KURI",
    district_code: 9104030,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "WAMESA",
    district_code: 9104040,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "BINTUNI",
    district_code: 9104050,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MANIMERI",
    district_code: 9104051,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "TUHIBA",
    district_code: 9104052,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "DATARAN BEIMES",
    district_code: 9104053,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "TEMBUNI",
    district_code: 9104060,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "ARANDAY",
    district_code: 9104070,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "KAMUNDAN",
    district_code: 9104071,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "TOMU",
    district_code: 9104072,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "WERIAGAR",
    district_code: 9104073,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MOSKONA SELATAN",
    district_code: 9104080,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MEYADO",
    district_code: 9104081,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MOSKONA BARAT",
    district_code: 9104082,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MERDEY",
    district_code: 9104090,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "BISCOOP",
    district_code: 9104091,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MASYETA",
    district_code: 9104092,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MOSKONA UTARA",
    district_code: 9104100,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "MOSKONA TIMUR",
    district_code: 9104101,
    regency_code: 9104,
    province_code: 91
  },
  {
    district_name: "WARMARE",
    district_code: 9105110,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "PRAFI",
    district_code: 9105120,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "MANOKWARI BARAT",
    district_code: 9105141,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "MANOKWARI TIMUR",
    district_code: 9105142,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "MANOKWARI UTARA",
    district_code: 9105143,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "MANOKWARI SELATAN",
    district_code: 9105144,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "TANAH RUBU",
    district_code: 9105146,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "MASNI",
    district_code: 9105170,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "SIDEY",
    district_code: 9105171,
    regency_code: 9105,
    province_code: 91
  },
  {
    district_name: "INANWATAN",
    district_code: 9106010,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "METEMANI",
    district_code: 9106011,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "KOKODA",
    district_code: 9106020,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "KAIS",
    district_code: 9106021,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "KOKODA UTARA",
    district_code: 9106022,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "KAIS DARAT",
    district_code: 9106023,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "MOSWAREN",
    district_code: 9106060,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "TEMINABUAN",
    district_code: 9106070,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "SEREMUK",
    district_code: 9106071,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "WAYER",
    district_code: 9106072,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "KONDA",
    district_code: 9106073,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "SAIFI",
    district_code: 9106074,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "SAWIAT",
    district_code: 9106080,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "FOKOUR",
    district_code: 9106081,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "SALKMA",
    district_code: 9106082,
    regency_code: 9106,
    province_code: 91
  },
  {
    district_name: "KLASO",
    district_code: 9107061,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SAENGKEDUK",
    district_code: 9107062,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "MAKBON",
    district_code: 9107100,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "KLAYILI",
    district_code: 9107101,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "BERAUR",
    district_code: 9107110,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "KLAMONO",
    district_code: 9107111,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "KLABOT",
    district_code: 9107112,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "KLAWAK",
    district_code: 9107113,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "BAGUN",
    district_code: 9107114,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "KLASAFET",
    district_code: 9107115,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "MALABOTOM",
    district_code: 9107116,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "BOTAIN",
    district_code: 9107118,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "KONHIR",
    district_code: 9107119,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SALAWATI",
    district_code: 9107120,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "MAYAMUK",
    district_code: 9107121,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SALAWATI TIMUR",
    district_code: 9107122,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "HOBARD",
    district_code: 9107123,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "BUK",
    district_code: 9107124,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SEGET",
    district_code: 9107130,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SEGUN",
    district_code: 9107131,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SALAWATI SELATAN",
    district_code: 9107132,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SALAWATI TENGAH",
    district_code: 9107133,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "AIMAS",
    district_code: 9107170,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "MARIAT",
    district_code: 9107171,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SORONG",
    district_code: 9107172,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SAYOSA",
    district_code: 9107180,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "MAUDUS",
    district_code: 9107181,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "WEMAK",
    district_code: 9107182,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SAYOSA TIMUR",
    district_code: 9107183,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "SUNOOK",
    district_code: 9107184,
    regency_code: 9107,
    province_code: 91
  },
  {
    district_name: "MISOOL SELATAN",
    district_code: 9108011,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "MISOOL BARAT",
    district_code: 9108012,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "MISOOL",
    district_code: 9108020,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "KOFIAU",
    district_code: 9108021,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "MISOOL TIMUR",
    district_code: 9108022,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "KEPULAUAN SEMBILAN",
    district_code: 9108023,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "SALAWATI UTARA",
    district_code: 9108031,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "SALAWATI TENGAH",
    district_code: 9108033,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "SALAWATI BARAT",
    district_code: 9108034,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "BATANTA SELATAN",
    district_code: 9108035,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "BATANTA UTARA",
    district_code: 9108036,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "WAIGEO SELATAN",
    district_code: 9108040,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "TELUK MAYALIBIT",
    district_code: 9108041,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "MEOS MANSAR",
    district_code: 9108042,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "KOTA WAISAI",
    district_code: 9108043,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "TIPLOL MAYALIBIT",
    district_code: 9108044,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "WAIGEO BARAT",
    district_code: 9108050,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "WAIGEO BARAT KEPULAUAN",
    district_code: 9108051,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "WAIGEO UTARA",
    district_code: 9108060,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "WARWARBOMI",
    district_code: 9108061,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "SUPNIN",
    district_code: 9108062,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "KEPULAUAN AYAU",
    district_code: 9108070,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "AYAU",
    district_code: 9108071,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "WAIGEO TIMUR",
    district_code: 9108080,
    regency_code: 9108,
    province_code: 91
  },
  {
    district_name: "FEF",
    district_code: 9109010,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "SYUJAK",
    district_code: 9109011,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "ASES",
    district_code: 9109012,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "TINGGOUW",
    district_code: 9109013,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "MIYAH",
    district_code: 9109020,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "MIYAH SELATAN",
    district_code: 9109021,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "IRERES",
    district_code: 9109022,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "WILHEM ROUMBOUTS",
    district_code: 9109023,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "ABUN",
    district_code: 9109030,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "KWOOR",
    district_code: 9109040,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "TOBOUW",
    district_code: 9109041,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "KWESEFO",
    district_code: 9109042,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "SAUSAPOR",
    district_code: 9109050,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "BIKAR",
    district_code: 9109051,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "YEMBUN",
    district_code: 9109060,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "BAMUSBAMA",
    district_code: 9109061,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "KEBAR",
    district_code: 9109070,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "KEBAR TIMUR",
    district_code: 9109071,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "KEBAR SELATAN",
    district_code: 9109072,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "MANEKAR",
    district_code: 9109073,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "SENOPI",
    district_code: 9109080,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "MAWABUAN",
    district_code: 9109081,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "AMBERBAKEN",
    district_code: 9109090,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "MPUR",
    district_code: 9109091,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "AMBERBAKEN BARAT",
    district_code: 9109092,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "MUBARNI / ARFU",
    district_code: 9109100,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "MORAID",
    district_code: 9109110,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "SELEMKAI",
    district_code: 9109111,
    regency_code: 9109,
    province_code: 91
  },
  {
    district_name: "AITINYO BARAT/ATHABU",
    district_code: 9110010,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU SELATAN JAYA",
    district_code: 9110011,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AITINYO",
    district_code: 9110020,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AITINYO TENGAH",
    district_code: 9110021,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AIFAT SELATAN",
    district_code: 9110030,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AIFAT TIMUR SELATAN",
    district_code: 9110031,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AIFAT",
    district_code: 9110040,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AITINYO UTARA",
    district_code: 9110050,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AITINYO RAYA",
    district_code: 9110051,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU TIMUR",
    district_code: 9110060,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU TIMUR SELATAN",
    district_code: 9110061,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU",
    district_code: 9110070,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU SELATAN",
    district_code: 9110071,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU JAYA",
    district_code: 9110072,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU TENGAH",
    district_code: 9110073,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU BARAT",
    district_code: 9110074,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU UTARA",
    district_code: 9110080,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AYAMARU UTARA TIMUR",
    district_code: 9110081,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "MARE",
    district_code: 9110090,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "MARE SELATAN",
    district_code: 9110091,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AIFAT UTARA",
    district_code: 9110100,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AIFAT TIMUR",
    district_code: 9110110,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AIFAT TIMUR TENGAH",
    district_code: 9110111,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "AIFAT TIMUR JAUH",
    district_code: 9110112,
    regency_code: 9110,
    province_code: 91
  },
  {
    district_name: "TAHOTA",
    district_code: 9111010,
    regency_code: 9111,
    province_code: 91
  },
  {
    district_name: "DATARAN ISIM",
    district_code: 9111020,
    regency_code: 9111,
    province_code: 91
  },
  {
    district_name: "NENEI",
    district_code: 9111030,
    regency_code: 9111,
    province_code: 91
  },
  {
    district_name: "MOMI WAREN",
    district_code: 9111040,
    regency_code: 9111,
    province_code: 91
  },
  {
    district_name: "RANSIKI",
    district_code: 9111050,
    regency_code: 9111,
    province_code: 91
  },
  {
    district_name: "ORANSBARI",
    district_code: 9111060,
    regency_code: 9111,
    province_code: 91
  },
  {
    district_name: "DIDOHU",
    district_code: 9112010,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "SURUREY",
    district_code: 9112020,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "ANGGI GIDA",
    district_code: 9112030,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "MEMBEY",
    district_code: 9112040,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "ANGGI",
    district_code: 9112050,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "TAIGE",
    district_code: 9112060,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "HINGK",
    district_code: 9112070,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "MENYAMBOUW",
    district_code: 9112080,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "CATUBOUW",
    district_code: 9112090,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "TESTEGA",
    district_code: 9112100,
    regency_code: 9112,
    province_code: 91
  },
  {
    district_name: "SORONG BARAT",
    district_code: 9171010,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "SORONG KEPULAUAN",
    district_code: 9171011,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "MALADUM MES",
    district_code: 9171012,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "SORONG TIMUR",
    district_code: 9171020,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "SORONG UTARA",
    district_code: 9171021,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "SORONG",
    district_code: 9171022,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "SORONG MANOI",
    district_code: 9171023,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "KLAURUNG",
    district_code: 9171024,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "MALAIMSIMSA",
    district_code: 9171025,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "SORONG KOTA",
    district_code: 9171026,
    regency_code: 9171,
    province_code: 91
  },
  {
    district_name: "KIMAAM",
    district_code: 9401010,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "WAAN",
    district_code: 9401011,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "TABONJI",
    district_code: 9401012,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "ILWAYAB",
    district_code: 9401013,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "OKABA",
    district_code: 9401020,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "TUBANG",
    district_code: 9401021,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "NGGUTI",
    district_code: 9401022,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "KAPTEL",
    district_code: 9401023,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "KURIK",
    district_code: 9401030,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "MALIND",
    district_code: 9401031,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "ANIMHA",
    district_code: 9401032,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "MERAUKE",
    district_code: 9401040,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "SEMANGGA",
    district_code: 9401041,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "TANAH MIRING",
    district_code: 9401042,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "JAGEBOB",
    district_code: 9401043,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "SOTA",
    district_code: 9401044,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "NAUKENJERAI",
    district_code: 9401045,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "MUTING",
    district_code: 9401050,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "ELIGOBEL",
    district_code: 9401051,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "ULILIN",
    district_code: 9401052,
    regency_code: 9401,
    province_code: 94
  },
  {
    district_name: "WAMENA",
    district_code: 9402110,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "ASOLOKOBAL",
    district_code: 9402111,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WALELAGAMA",
    district_code: 9402112,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "TRIKORA",
    district_code: 9402113,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "NAPUA",
    district_code: 9402114,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WALAIK",
    district_code: 9402115,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WOUMA",
    district_code: 9402116,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WALESI",
    district_code: 9402117,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "ASOTIPO",
    district_code: 9402118,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "MAIMA",
    district_code: 9402119,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "HUBIKOSI",
    district_code: 9402120,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "PELEBAGA",
    district_code: 9402121,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "IBELE",
    district_code: 9402122,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "TAILAREK",
    district_code: 9402123,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "HUBIKIAK",
    district_code: 9402124,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "ASOLOGAIMA",
    district_code: 9402180,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "MUSATFAK",
    district_code: 9402181,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "SILO KARNO DOGA",
    district_code: 9402182,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "PYRAMID",
    district_code: 9402183,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "MULIAMA",
    district_code: 9402184,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WAME",
    district_code: 9402185,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "KURULU",
    district_code: 9402190,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "USILIMO",
    district_code: 9402191,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WITA WAYA",
    district_code: 9402192,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "LIBAREK",
    district_code: 9402193,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WADANGKU",
    district_code: 9402194,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "PISUGI",
    district_code: 9402195,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "BOLAKME",
    district_code: 9402220,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "WOLLO",
    district_code: 9402221,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "YALENGGA",
    district_code: 9402222,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "TAGIME",
    district_code: 9402223,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "MOLAGALOME",
    district_code: 9402224,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "TAGINERI",
    district_code: 9402225,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "BUGI",
    district_code: 9402226,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "BPIRI",
    district_code: 9402227,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "KORAGI",
    district_code: 9402228,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "ITLAY HASIGE",
    district_code: 9402611,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "SIEPKOSI",
    district_code: 9402612,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "POPUGOBA",
    district_code: 9402614,
    regency_code: 9402,
    province_code: 94
  },
  {
    district_name: "KAUREH",
    district_code: 9403080,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "AIRU",
    district_code: 9403081,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "YAPSI",
    district_code: 9403082,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "KEMTUK",
    district_code: 9403140,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "KEMTUK GRESI",
    district_code: 9403150,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "GRESI SELATAN",
    district_code: 9403151,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "NIMBORAN",
    district_code: 9403160,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "NIMBORAN TIMUR / NAMBLONG",
    district_code: 9403161,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "NIMBOKRANG",
    district_code: 9403170,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "UNURUM GUAY",
    district_code: 9403180,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "DEMTA",
    district_code: 9403200,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "YOKARI",
    district_code: 9403201,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "DEPAPRE",
    district_code: 9403210,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "RAVENIRARA",
    district_code: 9403211,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "SENTANI BARAT",
    district_code: 9403220,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "WAIBU",
    district_code: 9403221,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "SENTANI",
    district_code: 9403230,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "EBUNGFAU",
    district_code: 9403231,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "SENTANI TIMUR",
    district_code: 9403240,
    regency_code: 9403,
    province_code: 94
  },
  {
    district_name: "UWAPA",
    district_code: 9404050,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "MENOU",
    district_code: 9404051,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "DIPA",
    district_code: 9404052,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "YAUR",
    district_code: 9404060,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "TELUK UMAR",
    district_code: 9404061,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "WANGGAR",
    district_code: 9404070,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "NABIRE BARAT",
    district_code: 9404071,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "NABIRE",
    district_code: 9404080,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "TELUK KIMI",
    district_code: 9404081,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "NAPAN",
    district_code: 9404090,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "MAKIMI",
    district_code: 9404091,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "WAPOGA",
    district_code: 9404092,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "KEPULAUAN MOORA",
    district_code: 9404093,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "SIRIWO",
    district_code: 9404100,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "YARO",
    district_code: 9404110,
    regency_code: 9404,
    province_code: 94
  },
  {
    district_name: "YAPEN TIMUR",
    district_code: 9408040,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "PANTURA YAPEN",
    district_code: 9408041,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "TELUK AMPIMOI",
    district_code: 9408042,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "RAIMBAWI",
    district_code: 9408043,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "PULAU KURUDU",
    district_code: 9408044,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "ANGKAISERA",
    district_code: 9408050,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "KEP. AMBAI",
    district_code: 9408051,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "YAWAKUKAT",
    district_code: 9408052,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "YAPEN SELATAN",
    district_code: 9408060,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "KOSIWO",
    district_code: 9408061,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "ANATAUREI",
    district_code: 9408062,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "YAPEN BARAT",
    district_code: 9408070,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "WONAWA",
    district_code: 9408071,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "PULAU YERUI",
    district_code: 9408072,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "POOM",
    district_code: 9408080,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "WINDESI",
    district_code: 9408081,
    regency_code: 9408,
    province_code: 94
  },
  {
    district_name: "NUMFOR BARAT",
    district_code: 9409010,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "ORKERI",
    district_code: 9409011,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "NUMFOR TIMUR",
    district_code: 9409020,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "BRUYADORI",
    district_code: 9409021,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "POIRU",
    district_code: 9409022,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "PADAIDO",
    district_code: 9409030,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "AIMANDO PADAIDO",
    district_code: 9409031,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "BIAK TIMUR",
    district_code: 9409040,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "ORIDEK",
    district_code: 9409041,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "BIAK KOTA",
    district_code: 9409050,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "SAMOFA",
    district_code: 9409060,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "YENDIDORI",
    district_code: 9409070,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "BIAK UTARA",
    district_code: 9409080,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "ANDEY",
    district_code: 9409081,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "WARSA",
    district_code: 9409090,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "YAWOSI",
    district_code: 9409091,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "BONDIFUAR",
    district_code: 9409092,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "BIAK BARAT",
    district_code: 9409100,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "SWANDIWE",
    district_code: 9409101,
    regency_code: 9409,
    province_code: 94
  },
  {
    district_name: "PANIAI TIMUR",
    district_code: 9410030,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "YATAMO",
    district_code: 9410031,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "KEBO",
    district_code: 9410032,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "PUGO DAGI",
    district_code: 9410033,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "WEGE MUKA",
    district_code: 9410034,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "WEGEE BINO",
    district_code: 9410035,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "YAGAI",
    district_code: 9410036,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "BIBIDA",
    district_code: 9410040,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "DUMADAMA",
    district_code: 9410041,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "ARADIDE",
    district_code: 9410070,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "EKADIDE",
    district_code: 9410071,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "AWEIDA",
    district_code: 9410072,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "FAJAR TIMUR",
    district_code: 9410073,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "TOPIYAI",
    district_code: 9410074,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "PANIAI BARAT",
    district_code: 9410080,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "SIRIWO",
    district_code: 9410081,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "MUYE",
    district_code: 9410082,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "NAKAMA",
    district_code: 9410083,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "TELUK DEYA",
    district_code: 9410084,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "BOGOBAIDA",
    district_code: 9410090,
    regency_code: 9410,
    province_code: 94
  },
  {
    district_name: "FAWI",
    district_code: 9411040,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "DAGAI",
    district_code: 9411041,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "KIYAGE",
    district_code: 9411042,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "MULIA",
    district_code: 9411050,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "YAMBI",
    district_code: 9411053,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "ILAMBURAWI",
    district_code: 9411054,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "MUARA",
    district_code: 9411055,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "PAGALEME",
    district_code: 9411056,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "GURAGE",
    district_code: 9411057,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "IRIMULI",
    district_code: 9411058,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "ILU",
    district_code: 9411060,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "TORERE",
    district_code: 9411061,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "YAMONERI",
    district_code: 9411063,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "WAEGI",
    district_code: 9411064,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "NUME",
    district_code: 9411065,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "NIOGA",
    district_code: 9411066,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "GUBUME",
    district_code: 9411067,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "TAGANOMBAK",
    district_code: 9411068,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "TINGGINAMBUT",
    district_code: 9411070,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "KALOME",
    district_code: 9411071,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "WANWI",
    district_code: 9411072,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "MEWOLUK",
    district_code: 9411080,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "LUMO",
    district_code: 9411081,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "MOLANIKIME",
    district_code: 9411082,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "YAMO",
    district_code: 9411090,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "DOKOME",
    district_code: 9411091,
    regency_code: 9411,
    province_code: 94
  },
  {
    district_name: "MIMIKA BARAT",
    district_code: 9412010,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "MIMIKA BARAT JAUH",
    district_code: 9412011,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "MIMIKA BARAT TENGAH",
    district_code: 9412012,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "AMAR",
    district_code: 9412013,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "MIMIKA TIMUR",
    district_code: 9412020,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "MIMIKA TENGAH",
    district_code: 9412021,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "MIMIKA TIMUR JAUH",
    district_code: 9412022,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "MIMIKA BARU",
    district_code: 9412030,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "KUALA KENCANA",
    district_code: 9412031,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "TEMBAGAPURA",
    district_code: 9412032,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "WANIA",
    district_code: 9412033,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "IWAKA",
    district_code: 9412034,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "KWAMKI NARAMA",
    district_code: 9412035,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "AGIMUGA",
    district_code: 9412040,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "JILA",
    district_code: 9412041,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "JITA",
    district_code: 9412042,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "ALAMA",
    district_code: 9412043,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "HOYA",
    district_code: 9412044,
    regency_code: 9412,
    province_code: 94
  },
  {
    district_name: "JAIR",
    district_code: 9413010,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "SUBUR",
    district_code: 9413011,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "KIA",
    district_code: 9413013,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "MINDIPTANA",
    district_code: 9413020,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "INIYANDIT",
    district_code: 9413021,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "KOMBUT",
    district_code: 9413022,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "SESNUK",
    district_code: 9413023,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "MANDOBO",
    district_code: 9413030,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "FOFI",
    district_code: 9413031,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "ARIMOP",
    district_code: 9413032,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "KOUH",
    district_code: 9413040,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "BOMAKIA",
    district_code: 9413041,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "FIRIWAGE",
    district_code: 9413042,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "MANGGELUM",
    district_code: 9413043,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "YANIRUMA",
    district_code: 9413044,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "KAWAGIT",
    district_code: 9413045,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "KOMBAY",
    district_code: 9413046,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "WAROPKO",
    district_code: 9413050,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "AMBATKWI",
    district_code: 9413051,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "NINATI",
    district_code: 9413052,
    regency_code: 9413,
    province_code: 94
  },
  {
    district_name: "NAMBIOMAN BAPAI",
    district_code: 9414010,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "MINYAMUR",
    district_code: 9414011,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "EDERA",
    district_code: 9414020,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "VENAHA",
    district_code: 9414021,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "SYAHCAME",
    district_code: 9414022,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "BAMGI",
    district_code: 9414023,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "YAKOMI",
    district_code: 9414024,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "OBAA",
    district_code: 9414030,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "PASSUE",
    district_code: 9414031,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "HAJU",
    district_code: 9414040,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "ASSUE",
    district_code: 9414050,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "CITAKMITAK",
    district_code: 9414060,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "KAIBAR",
    district_code: 9414061,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "PASSUE BAWAH",
    district_code: 9414062,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "TI-ZAIN",
    district_code: 9414063,
    regency_code: 9414,
    province_code: 94
  },
  {
    district_name: "PANTAI KASUARI",
    district_code: 9415010,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "KOPAY",
    district_code: 9415011,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "DER KOUMUR",
    district_code: 9415012,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "SAFAN",
    district_code: 9415013,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "AWYU",
    district_code: 9415014,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "FAYIT",
    district_code: 9415020,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "ASWI",
    district_code: 9415021,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "ATSY",
    district_code: 9415030,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "SIRETS",
    district_code: 9415031,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "AYIP",
    district_code: 9415032,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "BECTBAMU",
    district_code: 9415033,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "SUATOR",
    district_code: 9415040,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "KOLF BRAZA",
    district_code: 9415041,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "JOUTU",
    district_code: 9415042,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "KOROWAY BULUANOP",
    district_code: 9415043,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "AKAT",
    district_code: 9415050,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "JETSY",
    district_code: 9415051,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "AGATS",
    district_code: 9415060,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "SAWA ERMA",
    district_code: 9415070,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "SURU-SURU",
    district_code: 9415071,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "UNIR SIRAU",
    district_code: 9415072,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "JOERAT",
    district_code: 9415073,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "PULAU TIGA",
    district_code: 9415074,
    regency_code: 9415,
    province_code: 94
  },
  {
    district_name: "KURIMA",
    district_code: 9416010,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "MUSAIK",
    district_code: 9416011,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "DEKAI",
    district_code: 9416013,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "OBIO",
    district_code: 9416014,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "PASEMA",
    district_code: 9416015,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "AMUMA",
    district_code: 9416016,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SURU-SURU",
    district_code: 9416017,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "WUSAMA",
    district_code: 9416018,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SILIMO",
    district_code: 9416019,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "NINIA",
    district_code: 9416020,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "HOLUWON",
    district_code: 9416021,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "LOLAT",
    district_code: 9416022,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "LANGDA",
    district_code: 9416023,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "BOMELA",
    district_code: 9416024,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SUNTAMON",
    district_code: 9416025,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SOBAHAM",
    district_code: 9416026,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "KORUPUN",
    district_code: 9416027,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SELA",
    district_code: 9416028,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "KWELAMDUA",
    district_code: 9416029,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "ANGGRUK",
    district_code: 9416030,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "PANGGEMA",
    district_code: 9416031,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "WALMA",
    district_code: 9416032,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "KOSAREK",
    district_code: 9416033,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "UBAHAK",
    district_code: 9416034,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "NALCA",
    district_code: 9416035,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "PULDAMA",
    district_code: 9416036,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "NIPSAN",
    district_code: 9416037,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SAMENAGE",
    district_code: 9416041,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "TANGMA",
    district_code: 9416042,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SOBA",
    district_code: 9416043,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "MUGI",
    district_code: 9416044,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "YOGOSEM",
    district_code: 9416045,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "KAYO",
    district_code: 9416046,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SUMO",
    district_code: 9416047,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "HOGIO",
    district_code: 9416048,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "UKHA",
    district_code: 9416049,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "WERIMA",
    district_code: 9416051,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SOLOIKMA",
    district_code: 9416052,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "SERADALA",
    district_code: 9416053,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "KABIANGGAMA",
    district_code: 9416054,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "KWIKMA",
    district_code: 9416055,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "HILIPUK",
    district_code: 9416056,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "YAHULIAMBUT",
    district_code: 9416057,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "HEREAPINI",
    district_code: 9416058,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "UBALIHI",
    district_code: 9416059,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "TALAMBO",
    district_code: 9416061,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "PRONGGOLI",
    district_code: 9416062,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "ENDOMEN",
    district_code: 9416063,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "KONA",
    district_code: 9416065,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "DURAM",
    district_code: 9416066,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "DIRWEMNA",
    district_code: 9416067,
    regency_code: 9416,
    province_code: 94
  },
  {
    district_name: "IWUR",
    district_code: 9417010,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "KAWOR",
    district_code: 9417011,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "TARUP",
    district_code: 9417012,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "AWINBON",
    district_code: 9417013,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKSIBIL",
    district_code: 9417020,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "PEPERA",
    district_code: 9417021,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "ALEMSOM",
    district_code: 9417022,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "SERAMBAKON",
    district_code: 9417023,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "KOLOMDOL",
    district_code: 9417024,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKSOP",
    district_code: 9417025,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OK BAPE",
    district_code: 9417026,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OK AON",
    district_code: 9417027,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "BORME",
    district_code: 9417030,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "BIME",
    district_code: 9417031,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "EPUMEK",
    district_code: 9417032,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "WEIME",
    district_code: 9417033,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "PAMEK",
    district_code: 9417034,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "NONGME",
    district_code: 9417035,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "BATANI",
    district_code: 9417036,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKBI",
    district_code: 9417040,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "ABOY",
    district_code: 9417041,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKBAB",
    district_code: 9417042,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "TEIRAPLU",
    district_code: 9417043,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "YEFTA",
    district_code: 9417044,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "KIWIROK",
    district_code: 9417050,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "KIWIROK TIMUR",
    district_code: 9417051,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKSEBANG",
    district_code: 9417052,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKHIKA",
    district_code: 9417053,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKLIP",
    district_code: 9417054,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKSAMOL",
    district_code: 9417055,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "OKBEMTA",
    district_code: 9417056,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "BATOM",
    district_code: 9417060,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "MURKIM",
    district_code: 9417061,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "MOFINOP",
    district_code: 9417062,
    regency_code: 9417,
    province_code: 94
  },
  {
    district_name: "KANGGIME",
    district_code: 9418010,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "WONIKI",
    district_code: 9418011,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "NABUNAGE",
    district_code: 9418012,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "GILUBANDU",
    district_code: 9418013,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "WAKUO",
    district_code: 9418014,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "AWEKU",
    district_code: 9418015,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "BOGONUK",
    district_code: 9418016,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "KARUBAGA",
    district_code: 9418020,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "GOYAGE",
    district_code: 9418021,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "WUNIN",
    district_code: 9418022,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "KONDAGA",
    district_code: 9418023,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "NELAWI",
    district_code: 9418024,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "KUARI",
    district_code: 9418025,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "LIANOGOMA",
    district_code: 9418026,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "BIUK",
    district_code: 9418027,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "BOKONDINI",
    district_code: 9418030,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "BOKONERI",
    district_code: 9418031,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "BEWANI",
    district_code: 9418032,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "KEMBU",
    district_code: 9418040,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "WINA",
    district_code: 9418041,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "UMAGI",
    district_code: 9418042,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "PANAGA",
    district_code: 9418043,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "POGANERI",
    district_code: 9418044,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "KAMBONERI",
    district_code: 9418045,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "AIR GARAM",
    district_code: 9418046,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "DOW",
    district_code: 9418047,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "WARI / TAIYEVE",
    district_code: 9418048,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "EGIAM",
    district_code: 9418049,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "NUNGGAWI",
    district_code: 9418051,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "KUBU",
    district_code: 9418060,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "ANAWI",
    district_code: 9418061,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "WUGI",
    district_code: 9418062,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "GEYA",
    district_code: 9418070,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "WENAM",
    district_code: 9418071,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "NUMBA",
    district_code: 9418080,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "KAI",
    district_code: 9418081,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "DUNDU",
    district_code: 9418090,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "GUNDAGI",
    district_code: 9418100,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "TIMORI",
    district_code: 9418110,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "YUNERI",
    district_code: 9418121,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "TAGIME",
    district_code: 9418125,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "DANIME",
    district_code: 9418126,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "YUKO",
    district_code: 9418127,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "TELENGGEME",
    district_code: 9418541,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "GIKA",
    district_code: 9418542,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "TAGINERI",
    district_code: 9418543,
    regency_code: 9418,
    province_code: 94
  },
  {
    district_name: "PANTAI TIMUR BAGIAN BARAT",
    district_code: 9419021,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "PANTAI TIMUR",
    district_code: 9419022,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "SUNGAI BIRI",
    district_code: 9419024,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "VEEN",
    district_code: 9419025,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "BONGGO",
    district_code: 9419031,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "BONGGO TIMUR",
    district_code: 9419032,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "BONGGO BARAT",
    district_code: 9419033,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "TOR ATAS",
    district_code: 9419040,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "ISMARI",
    district_code: 9419041,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "SARMI",
    district_code: 9419050,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "SARMI TIMUR",
    district_code: 9419051,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "SARMI SELATAN",
    district_code: 9419052,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "SOBEY",
    district_code: 9419053,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "MUARA TOR",
    district_code: 9419054,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "VERKAM",
    district_code: 9419055,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "PANTAI BARAT",
    district_code: 9419060,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "APAWER HULU",
    district_code: 9419061,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "APAWER HILIR",
    district_code: 9419062,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "APAWER TENGAH",
    district_code: 9419063,
    regency_code: 9419,
    province_code: 94
  },
  {
    district_name: "WEB",
    district_code: 9420010,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "TOWE",
    district_code: 9420011,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "YAFFI",
    district_code: 9420012,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "SENGGI",
    district_code: 9420020,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "KAISENAR",
    district_code: 9420021,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "WARIS",
    district_code: 9420030,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "ARSO",
    district_code: 9420040,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "ARSO TIMUR",
    district_code: 9420041,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "ARSO BARAT",
    district_code: 9420042,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "MANNEM",
    district_code: 9420043,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "SKANTO",
    district_code: 9420050,
    regency_code: 9420,
    province_code: 94
  },
  {
    district_name: "WAROPEN BAWAH",
    district_code: 9426010,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "INGGERUS",
    district_code: 9426011,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "UREI FAISEI",
    district_code: 9426012,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "OUDATE",
    district_code: 9426013,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "WAPOGA",
    district_code: 9426014,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "MASIREI",
    district_code: 9426020,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "RISEI SAYATI",
    district_code: 9426021,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "DEMBA",
    district_code: 9426022,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "SOYOI MAMBAI",
    district_code: 9426023,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "WONTI",
    district_code: 9426024,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "WALANI",
    district_code: 9426030,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "KIRIHI",
    district_code: 9426040,
    regency_code: 9426,
    province_code: 94
  },
  {
    district_name: "SUPIORI SELATAN",
    district_code: 9427010,
    regency_code: 9427,
    province_code: 94
  },
  {
    district_name: "KEPULAUAN ARURI",
    district_code: 9427011,
    regency_code: 9427,
    province_code: 94
  },
  {
    district_name: "SUPIORI UTARA",
    district_code: 9427020,
    regency_code: 9427,
    province_code: 94
  },
  {
    district_name: "SUPIORI BARAT",
    district_code: 9427021,
    regency_code: 9427,
    province_code: 94
  },
  {
    district_name: "SUPIORI TIMUR",
    district_code: 9427030,
    regency_code: 9427,
    province_code: 94
  },
  {
    district_name: "WAROPEN ATAS",
    district_code: 9428030,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "BENUKI",
    district_code: 9428031,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "SAWAI",
    district_code: 9428032,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "MAMBERAMO ILIR",
    district_code: 9428040,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "MAMBERAMO TENGAH",
    district_code: 9428050,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "IWASO",
    district_code: 9428051,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "MAMBERAMO TENGAH TIMUR",
    district_code: 9428060,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "ROFAER",
    district_code: 9428070,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "MAMBERAMO ULU",
    district_code: 9428080,
    regency_code: 9428,
    province_code: 94
  },
  {
    district_name: "WOSAK",
    district_code: 9429010,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MOBA",
    district_code: 9429011,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "PIJA",
    district_code: 9429012,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "KORA",
    district_code: 9429013,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "KENYAM",
    district_code: 9429020,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MBUWA TENGAH",
    district_code: 9429021,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "KREPKURI",
    district_code: 9429022,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "EMBETPEM",
    district_code: 9429023,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "GESELMA",
    district_code: 9429030,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "KILMID",
    district_code: 9429031,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "YENGGELO",
    district_code: 9429032,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "ALAMA",
    district_code: 9429033,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MEBOROK",
    district_code: 9429034,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MAPENDUMA",
    district_code: 9429040,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "KROPTAK",
    district_code: 9429041,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "PARO",
    district_code: 9429042,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "KEGAYEM",
    district_code: 9429043,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MUGI",
    district_code: 9429050,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "YAL",
    district_code: 9429051,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MAM",
    district_code: 9429052,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "YIGI",
    district_code: 9429060,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "DAL",
    district_code: 9429061,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "NIRKURI",
    district_code: 9429062,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "INIKGAL",
    district_code: 9429063,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MBUWA",
    district_code: 9429070,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "INIYE",
    district_code: 9429071,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "WUTPAGA",
    district_code: 9429072,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "NENGGEANGIN",
    district_code: 9429073,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MBULMU YALMA",
    district_code: 9429074,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "GEAREK",
    district_code: 9429080,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "PASIR PUTIH",
    district_code: 9429081,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "WUSI",
    district_code: 9429082,
    regency_code: 9429,
    province_code: 94
  },
  {
    district_name: "MAKKI",
    district_code: 9430010,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "GUPURA",
    district_code: 9430011,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "KOLAWA",
    district_code: 9430012,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "GELOK BEAM",
    district_code: 9430013,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "AWINA",
    district_code: 9430014,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "PIRIME",
    district_code: 9430020,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "BUGUK GONA",
    district_code: 9430021,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "MILIMBO",
    district_code: 9430022,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "GOLLO",
    district_code: 9430023,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "WIRINGGABUT",
    district_code: 9430024,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "TIOM",
    district_code: 9430030,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "NOGI",
    district_code: 9430031,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "MOKONI",
    district_code: 9430032,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "NINAME",
    district_code: 9430033,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "YIGINUA",
    district_code: 9430034,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "TIOM OLLO",
    district_code: 9430035,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "YUGUNWI",
    district_code: 9430036,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "LANNYNA",
    district_code: 9430037,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "BALINGGA",
    district_code: 9430040,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "BALINGGA BARAT",
    district_code: 9430041,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "BRUWA",
    district_code: 9430042,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "AYUMNATI",
    district_code: 9430043,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "KUYAWAGE",
    district_code: 9430050,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "WANO BARAT",
    district_code: 9430051,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "MALAGAINERI",
    district_code: 9430060,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "MELAGAI",
    district_code: 9430061,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "TIOMNERI",
    district_code: 9430070,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "WEREKA",
    district_code: 9430071,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "DIMBA",
    district_code: 9430080,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "KELULOME",
    district_code: 9430081,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "NIKOGWE",
    district_code: 9430082,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "GAMELIA",
    district_code: 9430090,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "KARU",
    district_code: 9430091,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "YILUK",
    district_code: 9430092,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "GUNA",
    district_code: 9430093,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "POGA",
    district_code: 9430100,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "MUARA",
    district_code: 9430101,
    regency_code: 9430,
    province_code: 94
  },
  {
    district_name: "KOBAKMA",
    district_code: 9431010,
    regency_code: 9431,
    province_code: 94
  },
  {
    district_name: "ILUGWA",
    district_code: 9431020,
    regency_code: 9431,
    province_code: 94
  },
  {
    district_name: "KELILA",
    district_code: 9431030,
    regency_code: 9431,
    province_code: 94
  },
  {
    district_name: "ERAGAYAM",
    district_code: 9431040,
    regency_code: 9431,
    province_code: 94
  },
  {
    district_name: "MEGAMBILIS",
    district_code: 9431050,
    regency_code: 9431,
    province_code: 94
  },
  {
    district_name: "WELAREK",
    district_code: 9432010,
    regency_code: 9432,
    province_code: 94
  },
  {
    district_name: "APALAPSILI",
    district_code: 9432020,
    regency_code: 9432,
    province_code: 94
  },
  {
    district_name: "ABENAHO",
    district_code: 9432030,
    regency_code: 9432,
    province_code: 94
  },
  {
    district_name: "ELELIM",
    district_code: 9432040,
    regency_code: 9432,
    province_code: 94
  },
  {
    district_name: "BENAWA",
    district_code: 9432050,
    regency_code: 9432,
    province_code: 94
  },
  {
    district_name: "AGADUGUME",
    district_code: 9433010,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "LAMBEWI",
    district_code: 9433011,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "ONERI",
    district_code: 9433012,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "GOME",
    district_code: 9433020,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "AMUNGKALPIA",
    district_code: 9433021,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "GOME UTARA",
    district_code: 9433022,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "ERELMAKAWIA",
    district_code: 9433023,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "ILAGA",
    district_code: 9433030,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "ILAGA UTARA",
    district_code: 9433031,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "MABUGI",
    district_code: 9433032,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "OMUKIA",
    district_code: 9433033,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "SINAK",
    district_code: 9433040,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "SINAK BARAT",
    district_code: 9433041,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "MAGE√ÅBUME",
    district_code: 9433042,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "YUGUMUAK",
    district_code: 9433043,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "POGOMA",
    district_code: 9433050,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "KEMBRU",
    district_code: 9433051,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "BINA",
    district_code: 9433052,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "WANGBE",
    district_code: 9433060,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "OGAMANIM",
    district_code: 9433061,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "BEOGA",
    district_code: 9433070,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "BEOGA BARAT",
    district_code: 9433071,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "BEOGA TIMUR",
    district_code: 9433072,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "DOUFO",
    district_code: 9433080,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "DERVOS",
    district_code: 9433081,
    regency_code: 9433,
    province_code: 94
  },
  {
    district_name: "SUKIKAI SELATAN",
    district_code: 9434010,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "PIYAIYE",
    district_code: 9434020,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "MAPIA BARAT",
    district_code: 9434030,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "MAPIA TENGAH",
    district_code: 9434040,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "MAPIA",
    district_code: 9434050,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "DOGIYAI",
    district_code: 9434060,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "KAMU SELATAN",
    district_code: 9434070,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "KAMU",
    district_code: 9434080,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "KAMU TIMUR",
    district_code: 9434090,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "KAMU UTARA",
    district_code: 9434100,
    regency_code: 9434,
    province_code: 94
  },
  {
    district_name: "HOMEYO",
    district_code: 9435010,
    regency_code: 9435,
    province_code: 94
  },
  {
    district_name: "SUGAPA",
    district_code: 9435020,
    regency_code: 9435,
    province_code: 94
  },
  {
    district_name: "HITADIPA",
    district_code: 9435030,
    regency_code: 9435,
    province_code: 94
  },
  {
    district_name: "AGISIGA",
    district_code: 9435040,
    regency_code: 9435,
    province_code: 94
  },
  {
    district_name: "BIANDOGA",
    district_code: 9435050,
    regency_code: 9435,
    province_code: 94
  },
  {
    district_name: "WANDAI",
    district_code: 9435060,
    regency_code: 9435,
    province_code: 94
  },
  {
    district_name: "KAPIRAYA",
    district_code: 9436010,
    regency_code: 9436,
    province_code: 94
  },
  {
    district_name: "TIGI BARAT",
    district_code: 9436020,
    regency_code: 9436,
    province_code: 94
  },
  {
    district_name: "TIGI",
    district_code: 9436030,
    regency_code: 9436,
    province_code: 94
  },
  {
    district_name: "TIGI TIMUR",
    district_code: 9436040,
    regency_code: 9436,
    province_code: 94
  },
  {
    district_name: "BOWOBADO",
    district_code: 9436050,
    regency_code: 9436,
    province_code: 94
  },
  {
    district_name: "MUARA TAMI",
    district_code: 9471010,
    regency_code: 9471,
    province_code: 94
  },
  {
    district_name: "ABEPURA",
    district_code: 9471020,
    regency_code: 9471,
    province_code: 94
  },
  {
    district_name: "HERAM",
    district_code: 9471021,
    regency_code: 9471,
    province_code: 94
  },
  {
    district_name: "JAYAPURA SELATAN",
    district_code: 9471030,
    regency_code: 9471,
    province_code: 94
  },
  {
    district_name: "JAYAPURA UTARA",
    district_code: 9471040,
    regency_code: 9471,
    province_code: 94
  }
];
