import pandas as pd
import sys

def printOutput(typeOfOutput, name, code):
    print("vm.{i} = [".format(i=typeOfOutput))

    if typeOfOutput == "province":
        for (i, j) in zip(name, code):
            print('{{ province_name: "{name}", province_code: {code} }},'
                    .format(name=i, code=j))
    elif typeOfOutput == "regency":
        for (i, j) in zip(name, code):
            print('{{regency_name: "{name}", regency_code: {code}, province_code: {pc} }},'
                    .format(name=i, code=j, pc=str(j)[:2]))
    elif typeOfOutput == "district":
        for (i, j) in zip(name, code):
            print('{{district_name: "{name}", district_code: {code}, regency_code: {rc}, province_code: {pc} }},'
                    .format(name=i, code=j, rc=str(j)[:4], pc=str(j)[:2]))
    elif typeOfOutput == "village":
        for (i, j) in zip(name, code):
            print('{{village_name: "{name}", village_code: {code}, district_code: {dc}, regency_code: {rc}, province_code: {pc} }},'
                    .format(name=i, code=j, dc=str(j)[:7], rc=str(j)[:4], pc=str(j)[:2]))

    print("]")

df = pd.read_csv('./indonesia-address.csv')

province_info = df[['PROVINCE_NAME', 'PROVINCE_CODE']].drop_duplicates()
provinceName = province_info['PROVINCE_NAME'].tolist()
provinceCode = province_info['PROVINCE_CODE'].tolist()
printOutput("province", provinceName, provinceCode)


regency_info = df[['REGENCY_NAME', 'REGENCY_CODE']].drop_duplicates()
regencyName = regency_info['REGENCY_NAME'].tolist()
regencyCode = regency_info['REGENCY_CODE'].tolist()
printOutput("regency", regencyName, regencyCode)

district_info = df[['DISTRICT_NAME', 'DISTRICT_CODE']].drop_duplicates()
districtName = district_info['DISTRICT_NAME'].tolist()
districtCode = district_info['DISTRICT_CODE'].tolist()
printOutput("district", districtName, districtCode)

village_info = df[['VILLAGE_NAME', 'VILLAGE_CODE']].drop_duplicates()
villageName = village_info['VILLAGE_NAME'].tolist()
villageCode = village_info['VILLAGE_CODE'].tolist()
printOutput("village", villageName, villageCode)

# if __name__ == '__main__':
#     from timeit import Timer
#     t = Timer(lambda: printOutput("village", villageName, villageCode))
#     print(t.timeit(number=100))